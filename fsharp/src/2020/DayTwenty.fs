module DayTwenty
 
open FSharp.Core.Operators.Checked

open Common
open Common.DataStructures

module Grid =

    let findCorners (grids: Grid<_> list) =
        let edges =
            grids
            |> List.map (fun grid -> grid.Id, Grid.getAllEdges grid)
            |> Array.ofList

        edges
        |> Array.Parallel.choose (fun (edgeId, thisGridsEdges) ->
            let foundEdges =
                edges
                |> Array.filter (fun (otherEdgeId, otherEdges) ->
                    otherEdgeId <> edgeId
                    && Set.intersect thisGridsEdges otherEdges
                       |> Set.count > 0)

            if foundEdges.Length = 2 then
                Some edgeId
            else
                None)

    let findStartingCorner (grids: Grid<_> list) =
        let edges =
            grids
            |> List.map (fun grid -> grid, Grid.getAllEdges grid)
            |> Array.ofList

        edges
        |> Array.pick (fun (grid, thisGridsEdges) ->
            let foundEdges =
                edges
                |> Array.filter (fun (otherGrid, otherEdges) ->
                    otherGrid.Id <> grid.Id
                    && Set.intersect thisGridsEdges otherEdges
                       |> Set.count > 0)

            if foundEdges.Length = 2 then
                Some grid
            else
                None)

    let parseId (s: string) =
        s.Split(' ')
        |> Array.item 1
        |> fun s -> s.TrimEnd(':')

    let parseGrid (line: string list) : Grid<char> =
        let id = line |> List.head |> parseId

        let gridData =
            line
            |> List.tail
            |> List.filter Common.Strings.notBlank
            |> List.map Seq.toArray
            |> List.toArray

        Grid.Create id gridData

    let removeEdge (edge: Edge) (grid: Grid<char>) =
        match edge with
        | Top ->
            grid.Data
            |> Array.tail
            |> Grid.Create<char> grid.Id
        | Left ->
            grid.Data
            |> Array.map Array.tail
            |> Grid.Create<char> grid.Id
        | Right ->
            let len = grid.Data |> Array.head |> Array.length

            grid.Data
            |> Array.map (fun arr -> arr.[..len - 2])
            |> Grid.Create<char> grid.Id

        | Bottom ->
            let items = grid.Data.Length
            Grid.Create<char> grid.Id grid.Data.[..items - 2]

    let tryRotateAndMatch (edge: Edge) (firstGrid: Grid<_>) (secondGrid: Grid<_>) =
        let fge = firstGrid.GetEdge edge
        let inverse = edge.Inverse

        if fge = secondGrid.GetEdge(inverse) then
            secondGrid |> Some
        else if fge = secondGrid.Rotate().GetEdge(inverse) then
            secondGrid.Rotate() |> Some
        else if fge = secondGrid.Rotate().Rotate().GetEdge(inverse) then
            secondGrid.Rotate().Rotate() |> Some
        else if
            fge
            = secondGrid.Rotate().Rotate().Rotate().GetEdge(inverse)
        then
            secondGrid.Rotate().Rotate().Rotate() |> Some
        else
            None

    let tryAlignAndMatch (firstGrid: Grid<_>) (secondGrid: Grid<_>) =
        Edge.All
        |> Seq.tryPick (fun edge ->
            tryRotateAndMatch edge firstGrid secondGrid
            |> Option.map (fun grid -> grid, edge)
            |> Option.bindNone (fun () ->
                tryRotateAndMatch edge firstGrid (secondGrid.Flip())
                |> Option.map (fun grid -> grid, edge)))

module PartOne =

    let parseInput (input: Common.Input) =
        let rec makeGrids remaining acc =

            match remaining with
            | [] -> acc
            | lines ->
                match lines |> List.tryFindIndex Common.Strings.blank with
                | Some nextIdx ->
                    let gridLines = lines.[0..nextIdx - 1]
                    let newRemaining = lines.[nextIdx + 1..]
                    let grid = gridLines |> Grid.parseGrid

                    makeGrids newRemaining (grid :: acc)
                | None ->
                    let grid = lines |> Grid.parseGrid

                    grid :: acc

        makeGrids (input |> List.ofArray) List.empty

    let partOne (input: Common.Input) =
        input
        |> parseInput
        |> Grid.findCorners
        |> Array.map int64
        |> Array.reduce (*)
        |> string

module PartTwo =

    let addGridToRows (incomingRows: Grid<char> list) (grid: Grid<char>) =
        match incomingRows with
        | [] -> [ grid ]
        | rows ->
            let tryAlignAndMatch = Grid.tryAlignAndMatch grid
            // We're going to fold over our rows, reducing them to a new,
            // possibly updated rows, plus a flag signaling if an update
            // occured.
            let newRows, added =
                rows
                |> List.fold
                    (fun (newRows, added) rowGrid ->
                        match tryAlignAndMatch rowGrid with
                        // We found a match; merge, signal the change happened.
                        | Some (rotated, edge) ->
                            let baseTrimmed = Grid.removeEdge edge grid
                            let rotatedTrimmed = Grid.removeEdge (edge.Inverse) rotated

                            let newGrid =
                                baseTrimmed.Merge baseTrimmed.Id edge rotatedTrimmed

                            newGrid :: newRows, true
                        // No update, just add the grid into the acc and don't
                        // fiddle with the update var
                        | None -> rowGrid :: newRows, added)
                    (List.empty, false)

            if added then newRows else grid :: rows

    // We need some form of directional control. The way to do this is:
    // 1. Find a corner.
    // 2. Figure out which sides are the "live" sides
    // 3. Follow each direction. One can be used to pre-populate an array of arrays, the other can be assembled.
    // Really, this can be two folds. One to pre-build "down", the other to build "across".


    let buildInitial (grids: Grid<char> list) =
        let corner = Grid.findStartingCorner grids

        let gridsWithoutCorner =
            grids |> List.filter (fun g -> g.Id <> corner.Id)

        // let direction =
        //     gridsWithoutCorner
        //     |> List.pick (fun grid -> Grid.tryFindMatchingEdge corner grid)
        //     |> fst
        //     |> Edge.inverse

        gridsWithoutCorner
        |> List.fold
            (fun (prevGrid: Grid<char>, acc: Grid<char> list) (nextGrid: Grid<char>) ->
                if Grid.hasMatchingEdge prevGrid nextGrid then
                    (prevGrid, acc)
                else
                    (prevGrid, acc))
            (corner, List.empty)



    let assembleRows (grids: Grid<char> list) =
        let rec work (rows: Grid<char> list) (unprocessed: Grid<char> list) =
            match unprocessed with
            | [] -> rows
            | grid :: remaining ->
                let (builtRow, unused) =
                    remaining
                    |> List.fold
                        (fun (workingRow, (unused: Grid<char> list)) nextGrid ->
                            printfn $"Checking {workingRow.Id} against {nextGrid.Id}"

                            match Grid.tryAlignAndMatch workingRow nextGrid with
                            // We found a match; merge, signal the change happened.
                            | Some (rotated, edge) ->
                                printfn $"\tIt's a match on the {edge} edge"
                                let baseTrimmed = Grid.removeEdge edge workingRow
                                let rotatedTrimmed = Grid.removeEdge (edge.Inverse) rotated

                                let newGrid =
                                    baseTrimmed.Merge baseTrimmed.Id edge rotatedTrimmed

                                newGrid, unused
                            | None -> workingRow, nextGrid :: unused)
                        (grid, List.empty)

                work (builtRow :: rows) unused

        work List.empty grids

    let assembleImage (grids: Grid<char> list) =
        // let rec assembleRows (rows: Grid<char> list) (unprocessed: Grid<char> list) =
        //     match unprocessed with
        //     | [] -> rows
        //     | grid :: remaining ->
        //         let newRows = addGridToRows rows grid
        //         assembleRows newRows remaining

        // let reduceRows (rows: Grid<char> list) =
        //     let rec work (assembledRows: Grid<char> list) (incoming: Grid<char> list) =
        //         match incoming with
        //             | grid :: remaining ->

        grids |> assembleRows

    let partTwo (input: Common.Input) =
        input
        |> PartOne.parseInput
        |> assembleImage
        |> List.iter (fun s -> printfn $"{s.Id}")

        "Done"

type Solution() =
    let name = "twenty"

    interface Common.ISolution with
        member __.Name() = name |> Common.titleCase
        member __.LoadData(year) : Common.Input option = Common.loadData name year

        member __.PartOne(input: Common.Input) : string = input |> PartOne.partOne

        member __.PartTwo(input: Common.Input) : string = input |> PartTwo.partTwo
