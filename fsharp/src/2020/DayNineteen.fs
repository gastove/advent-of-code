module DayNineteen

open FSharp.Core.Operators.Checked

module PartOne =

    open System.Text

    type Specification =
        | Sequence of int list
        | Alternation of Specification * Specification // Always Sequence
        | Character of char

        static member FromString(s: string) =
            if s.Contains("\"") then
                s.Trim('"') |> char |> Character
            else if s.Contains("|") then
                let parts =
                    s.Split("|")
                    |> Array.map (fun s -> s.Trim() |> Specification.FromString)

                let firstHalf = parts.[0]
                let secondHalf = parts.[1]

                (firstHalf, secondHalf) |> Alternation
            else
                s.Split(" ")
                |> Array.map (fun s -> s.Trim() |> int)
                |> List.ofArray
                |> Sequence


    type RuleBook = Map<int, Specification>

    let parseLine (line: string) =
        let parts =
            line.Split(":") |> Array.map (fun s -> s.Trim())

        let ruleNo = parts.[0] |> int
        let spec = parts.[1] |> Specification.FromString

        ruleNo, spec

    let buildRuleBook (input: string array) =
        input
        |> Array.map parseLine
        |> Array.fold (fun rb (ruleNo, spec) -> Map.add ruleNo spec rb) Map.empty

    let buildRegex (rb: RuleBook) =
        let rec build num =
            match Map.find num rb with
            | Character (c) -> [ c ]
            | Alternation (Sequence (s1), Sequence (s2)) ->
                let optOne = s1 |> List.collect build
                let optTwo = s2 |> List.collect build

                [ '(' ] @ optOne @ [ '|' ] @ optTwo @ [ ')' ]
            | Sequence (s) -> s |> List.collect build
            | Alternation (_, _) -> failwith "Not Implemented"

        build 0
        |> Array.ofList
        |> System.String
        |> fun s -> "^" + s + "$"

    let parseInput (input: Common.Input) =
        let ruleSpec =
            input
            |> Array.takeWhile (System.String.IsNullOrWhiteSpace >> not)

        let lines =
            input |> Array.skip (ruleSpec.Length + 1)

        ruleSpec |> buildRuleBook, lines

    let partOne (input: Common.Input) =
        let ruleBook, lines = parseInput input

        let regex =
            buildRegex ruleBook |> RegularExpressions.Regex

        lines
        |> Array.filter regex.IsMatch
        |> Array.length

module PartTwo =
    let matchesRules (candidate: string) (ruleBook: PartOne.RuleBook) =

        let rec work (currentPos: int) (rule: PartOne.Specification) =
            if currentPos = candidate.Length then
                true, candidate.Length
            else if currentPos > candidate.Length then
                false, currentPos
            else
                match rule with
                | PartOne.Character (c) -> candidate.[currentPos] = c, 1
                | PartOne.Alternation (seq1, seq2) ->
                    let firstMatched, firstConsumed = (work currentPos seq1)

                    if firstMatched then
                        firstMatched, firstConsumed
                    else
                        let secondMatched, secondConsumed = (work currentPos seq2)

                        if secondMatched then
                            secondMatched, secondConsumed
                        else
                            false, 0

                | PartOne.Sequence (rules) ->

                    let rec consumeSequence (rules: int list) (consumed: int) =
                        match rules with
                            | ruleId :: remainingRules ->
                                let rule =
                                    match Map.tryFind ruleId ruleBook with
                                        | Some (r) -> r
                                        | None -> failwithf "Failed to find rule %i" ruleId

                                match work (currentPos + consumed) rule with
                                | true, alsoConsumed -> consumeSequence remainingRules (consumed + alsoConsumed)
                                | false, _ -> false, 0

                            | [] -> true, consumed

                    consumeSequence rules 0

        let matched, consumed = work 0 (Map.find 0 ruleBook)
        matched && consumed = candidate.Length

    let partTwo (input: Common.Input) =
        let baseRuleBook, lines = PartOne.parseInput input

        // 8: 42 | 42 8
        // 11: 42 31 | 42 11 31

        let ruleBook =
            baseRuleBook
            |> Map.add 8 (PartOne.Alternation(PartOne.Sequence([ 42 ]), PartOne.Sequence([ 42; 8 ])))
            |> Map.add 11 (PartOne.Alternation(PartOne.Sequence([ 42; 31 ]), (PartOne.Sequence([ 42; 11; 31 ]))))

        lines
        |> Array.filter (fun l -> matchesRules l ruleBook)
        |> Array.length


type Solution() =
    let name = "nineteen"

    interface Common.ISolution with
        member __.Name() = name |> Common.titleCase
        member __.LoadData(year) : Common.Input option = Common.loadData name year
        member __.PartOne(input: Common.Input) : string = input |> PartOne.partOne |> string
        member __.PartTwo(input: Common.Input) : string = input |> PartTwo.partTwo |> string
