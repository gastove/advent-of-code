module DayNine

open FSharp.Core.Operators.Checked

type Rope =
    { Head: Common.Geometry.Point
      Tail: Common.Geometry.Point }

    static member Create head tail = { Head = head; Tail = tail }

    static member Default =
        Rope.Create(Common.Geometry.Point.Create 0 0) (Common.Geometry.Point.Create 0 0)

    override this.ToString() =
        sprintf $"Rope<Head={this.Head};Tail={this.Tail}>"

type Motion =
    | Up of int
    | Down of int
    | Left of int
    | Right of int

    static member FromString(s: string) =
        let parse = System.Int32.Parse

        match s.Split(' ') with
        | [| "R"; motion |] -> motion |> parse |> Right
        | [| "U"; motion |] -> motion |> parse |> Up
        | [| "D"; motion |] -> motion |> parse |> Down
        | [| "L"; motion |] -> motion |> parse |> Left
        | _ -> failwith $"Failed to parse as Motion {s}"

    override this.ToString() =
        match this with
        | Up motion -> $"Up<{motion}>"
        | Down motion -> $"Down<{motion}>"
        | Left motion -> $"Left<{motion}>"
        | Right motion -> $"Right<{motion}>"

module PartOne =
    open Common.Geometry

    let computeMotion (delta: int) =
        if delta <= -1 then -1
        else if delta >= 1 then 1
        else 0

    let moveSecondTowardsFirst (first: Point) (second: Point) =
        if second.AllNeighbors |> Seq.contains first then
            second
        else
            let xDelta = first.X - second.X |> computeMotion
            let yDelta = first.Y - second.Y |> computeMotion

            { second with
                X = second.X + xDelta
                Y = second.Y + yDelta }

    let makeUpdater =
        function
        | Right amount -> (fun (p: Point) -> { p with X = p.X + 1 }), amount
        | Left amount -> (fun (p: Point) -> { p with X = p.X - 1 }), amount
        | Up amount -> (fun (p: Point) -> { p with Y = p.Y + 1 }), amount
        | Down amount -> (fun (p: Point) -> { p with Y = p.Y - 1 }), amount

    let rec doUpdate (firstPoint: Point) (secondPoint: Point) (updater: Point -> Point) (times: int) (visited: Set<Point>) =
        if times = 0 then
            firstPoint, secondPoint, visited
        else
            let newHead = firstPoint |> updater
            let newTail = moveSecondTowardsFirst newHead secondPoint
            let newVisited = visited |> Set.add newTail
            doUpdate newHead newTail updater (times - 1) newVisited

    let update rope motion =
        let (updater, times) = motion |> makeUpdater
            
        let (newHead, newTail, visited) =
            doUpdate rope.Head rope.Tail updater times Set.empty

        Rope.Create newHead newTail, visited

module PartTwo =
    open Common.Geometry

    type Rope = Point list
    
    let defaltRope = List.init 10 (fun _ -> Point.Create 0 0)

    let update rope motion =
        let (updater, amount) = PartOne.makeUpdater motion
        
        let rec work (points: Point list) (times: int) (newPoints: Point list) (visited: Set<Point>) =            
            if times = 0 then
                points, visited
            else 
                match (points, newPoints) with
                | firstPoint :: remaining,  [] ->                    
                    let newFirst = firstPoint |> updater

                    work remaining times (newFirst :: newPoints) visited
                | secondPoint :: [], firstPoint :: remainingNewPoints ->
                    let newSecond = secondPoint |> PartOne.moveSecondTowardsFirst firstPoint
                    let newVisited = visited |> Set.add newSecond
                    let finalNewPoints = newSecond :: firstPoint :: remainingNewPoints

                    work (finalNewPoints |> List.rev) (times - 1) List.empty newVisited

                | secondPoint :: remaining, firstPoint :: remainingNewPoints ->
                    let newSecond = secondPoint |> PartOne.moveSecondTowardsFirst firstPoint
                    let newNewPoints = newSecond :: firstPoint :: remainingNewPoints

                    work remaining times newNewPoints visited

                | wrong -> failwith $"How the fuck did we get: {wrong}"
            
        work rope amount List.empty Set.empty

let partOne (input: Common.Input) =
    let motions = input |> Array.map Motion.FromString

    let rope = Rope.Default
    let visitedPoints: Set<Common.Geometry.Point> = Set.empty |> Set.add rope.Tail

    let (_, finalVisited) =
        motions
        |> Array.fold
            (fun (rope, points) motion ->
                let (newRope, newPoints) = PartOne.update rope motion

                newRope, Set.union points newPoints)
            (rope, visitedPoints)

    finalVisited.Count |> string

let partTwo (input: Common.Input) =
    let motions = input |> Array.map Motion.FromString

    let rope = PartTwo.defaltRope
    
    let visitedPoints: Set<Common.Geometry.Point> = Set.empty |> Set.add (Common.Geometry.Point.Create 0 0)

    let (_, finalVisited) =
        motions
        |> Array.fold
            (fun (rope, points) motion ->
                let (newRope, newPoints) = PartTwo.update rope motion

                newRope, Set.union points newPoints)
            (rope, visitedPoints)

    finalVisited.Count |> string
    

type Solution() =
    let name = "nine"

    interface Common.ISolution with
        member __.Name() = name |> Common.titleCase
        member __.LoadData(year) : Common.Input option = Common.loadData name year
        member __.PartOne(input: Common.Input) : string = input |> partOne
        member __.PartTwo(input: Common.Input) : string = input |> partTwo
