module DayTen

open FSharp.Core.Operators.Checked

let ExampleInput =
    [| "addx 15"
       "addx -11"
       "addx 6"
       "addx -3"
       "addx 5"
       "addx -1"
       "addx -8"
       "addx 13"
       "addx 4"
       "noop"
       "addx -1"
       "addx 5"
       "addx -1"
       "addx 5"
       "addx -1"
       "addx 5"
       "addx -1"
       "addx 5"
       "addx -1"
       "addx -35"
       "addx 1"
       "addx 24"
       "addx -19"
       "addx 1"
       "addx 16"
       "addx -11"
       "noop"
       "noop"
       "addx 21"
       "addx -15"
       "noop"
       "noop"
       "addx -3"
       "addx 9"
       "addx 1"
       "addx -3"
       "addx 8"
       "addx 1"
       "addx 5"
       "noop"
       "noop"
       "noop"
       "noop"
       "noop"
       "addx -36"
       "noop"
       "addx 1"
       "addx 7"
       "noop"
       "noop"
       "noop"
       "addx 2"
       "addx 6"
       "noop"
       "noop"
       "noop"
       "noop"
       "noop"
       "addx 1"
       "noop"
       "noop"
       "addx 7"
       "addx 1"
       "noop"
       "addx -13"
       "addx 13"
       "addx 7"
       "noop"
       "addx 1"
       "addx -33"
       "noop"
       "noop"
       "noop"
       "addx 2"
       "noop"
       "noop"
       "noop"
       "addx 8"
       "noop"
       "addx -1"
       "addx 2"
       "addx 1"
       "noop"
       "addx 17"
       "addx -9"
       "addx 1"
       "addx 1"
       "addx -3"
       "addx 11"
       "noop"
       "noop"
       "addx 1"
       "noop"
       "addx 1"
       "noop"
       "noop"
       "addx -13"
       "addx -19"
       "addx 1"
       "addx 3"
       "addx 26"
       "addx -30"
       "addx 12"
       "addx -1"
       "addx 3"
       "addx 1"
       "noop"
       "noop"
       "noop"
       "addx -9"
       "addx 18"
       "addx 1"
       "addx 2"
       "noop"
       "noop"
       "addx 9"
       "noop"
       "noop"
       "noop"
       "addx -1"
       "addx 2"
       "addx -37"
       "addx 1"
       "addx 3"
       "noop"
       "addx 15"
       "addx -21"
       "addx 22"
       "addx -6"
       "addx 1"
       "noop"
       "addx 2"
       "addx 1"
       "noop"
       "addx -10"
       "noop"
       "noop"
       "addx 20"
       "addx 1"
       "addx 2"
       "addx 2"
       "addx -6"
       "addx -11"
       "noop"
       "noop"
       "noop" |]


type Instruction =
    | Noop
    | Addx of int * bool

    static member FromString(s: string) =
        if s = "noop" then
            Noop
        else
            s.Split(" ")
            |> Array.item 1
            |> System.Int32.Parse
            |> fun i -> Addx(i, false)

let SampleIntervals = [| 20; 60; 100; 140; 180; 220 |]

let runProgram (instructions: Instruction list) =
    let rec work (instr: Instruction list) (tick: int) (register: int) (signals: int list) =
        match instr with
        | Noop :: remainingInstructions ->
            if SampleIntervals |> Array.contains tick then
                work remainingInstructions (tick + 1) register ((register * tick) :: signals)
            else
                work remainingInstructions (tick + 1) register signals
        | Addx (toAdd, ready) :: remainingInstructions ->
            let shouldRecordTick = SampleIntervals |> Array.contains tick

            match (ready, shouldRecordTick) with
            | true, false -> work remainingInstructions (tick + 1) (register + toAdd) signals
            | false, true ->
                let requeue = (toAdd, true) |> Addx
                work (requeue :: remainingInstructions) (tick + 1) register ((register * tick) :: signals)
            | true, true -> work remainingInstructions (tick + 1) (register + toAdd) ((register * tick) :: signals)
            | false, false ->
                let requeue = (toAdd, true) |> Addx
                work (requeue :: remainingInstructions) (tick + 1) register signals
        | [] -> signals

    work instructions 1 1 List.empty

let getPixelToWrite (tick: int) (register: int) =
    let normalizedTick = tick % 40

    if (register - 1) <= normalizedTick
       && normalizedTick <= (register + 1) then
        '#'
    else
        '.'

let runDrawingProgram (instructions: Instruction list) =
    let rec work (instr: Instruction list) (tick: int) (register: int) (pixels: char list) =
        let pixel = getPixelToWrite tick register
        let newTick = tick + 1
        let withUpdatedPixel = pixel :: pixels

        match instr with
        | Noop :: remainingInstructions -> work remainingInstructions newTick register withUpdatedPixel
        | Addx (toAdd, ready) :: remainingInstructions ->
            if ready then
                work remainingInstructions (tick + 1) (register + toAdd) withUpdatedPixel
            else
                let requeue = (toAdd, true) |> Addx
                work (requeue :: remainingInstructions) (tick + 1) register withUpdatedPixel

        | [] -> pixels |> List.rev |> List.toArray

    work instructions 0 1 List.empty

let partOne (input: Common.Input) =
    input
    |> Array.map Instruction.FromString
    |> Array.toList
    |> runProgram
    |> List.sum
    |> string

let partTwo (input: Common.Input) =
    input
    |> Array.map Instruction.FromString
    |> Array.toList
    |> runDrawingProgram
    |> Array.chunkBySize 40
    |> Array.map System.String
    |> String.concat "\n"
    |> sprintf "\n%s"

type Solution() =
    let name = "ten"

    interface Common.ISolution with
        member __.Name() = name |> Common.titleCase
        member __.LoadData(year) : Common.Input option = Common.loadData name year
        member __.PartOne(input: Common.Input) : string = input |> partOne
        member __.PartTwo(input: Common.Input) : string = input |> partTwo
