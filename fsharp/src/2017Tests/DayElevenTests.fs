module DayElevenTests

open Expecto

open DayEleven

[<Tests>]
let hexPointTests =
    testList
        "Testing the HexPoint API"
        [ testCase "Test basic in the Hex Coordinate system"
          <| fun _ ->
              let directions = [ NorthEast; NorthEast; NorthEast ]
              let finalPoint = directions |> followDirections
              let expectedDistance = 3
              let got = HexPoint.Origin.Distance finalPoint

              Expect.equal got expectedDistance "We should be able to solve easy distances"

          testCase "Test looping back to start"
          <| fun _ ->
              let directions =
                  [ NorthEast
                    NorthEast
                    SouthWest
                    SouthWest ]

              let finalPoint = directions |> followDirections
              let expectedDistance = 0
              let got = HexPoint.Origin.Distance finalPoint

              Expect.equal got expectedDistance "We should be back at origin"

          testCase "Test being a short distance away"
          <| fun _ ->
              let directions = [ NorthEast; NorthEast; South; South ]
              let finalPoint = directions |> followDirections
              let expectedDistance = 2
              let got = HexPoint.Origin.Distance finalPoint

              Expect.equal got expectedDistance "We should be two steps away"

          testCase "Test being slightly further away"
          <| fun _ ->
              let directions =
                  [ SouthEast
                    SouthWest
                    SouthEast
                    SouthWest
                    SouthWest ]

              let finalPoint = directions |> followDirections
              let expectedDistance = 3
              let got = HexPoint.Origin.Distance finalPoint

              Expect.equal got expectedDistance "We should be three steps away"

          ]
