module DayThirteen

open FSharp.Core.Operators.Checked

let adjustForSecondDigitAndConvert (c: char) (chars: char list) =
    match chars with
    | n :: rest when n |> System.Char.IsDigit -> [| c; n |] |> System.String |> System.Int32.Parse, rest
    | _ -> c |> string |> System.Int32.Parse, chars

type Paket =
    | Int of int
    | List of Paket list

    static member FromString(s: string) =
        let rec work (acc: Paket list) (characters: char list) =
            match characters with
            | [] -> acc |> List.rev |> List, []
            | c :: rest ->
                match c with
                | ',' -> work acc rest
                | '[' ->
                    let newPaket, newRest = work [] rest
                    work (newPaket :: acc) newRest
                | ']' -> acc |> List.rev |> List, rest
                | n ->
                    let num, newRest = adjustForSecondDigitAndConvert n rest
                    work ((num |> Int) :: acc) newRest

        let paket, _ = s |> Seq.toList |> work []

        match paket with
        | List ([ pakets ]) -> pakets
        | otherwise -> otherwise

let parseInput (input: Common.Input) =
    let rec parser (remaining: string list) =
        match remaining with
        | [] -> []
        | first :: second :: [] ->
            let firstPaket = first |> Paket.FromString
            let secondPaket = second |> Paket.FromString

            [ (firstPaket, secondPaket) ]
        | first :: second :: "" :: rest ->
            let firstPaket = first |> Paket.FromString
            let secondPaket = second |> Paket.FromString

            (firstPaket, secondPaket) :: parser rest

        | bad -> failwithf "Well we shouldn't have matched this: %A" bad

    input |> Array.toList |> parser

module PartOne =
    let rec comparePakets (thePakets: Paket * Paket) =
        thePakets ||> printfn "Comparing %A and %A"
        let rec compareLists (l1: Paket list) (l2: Paket list) =
            match l1, l2 with
            | elem1 :: rest1, elem2 :: rest2 ->
                match (elem1, elem2) |> comparePakets with
                | None -> compareLists rest1 rest2
                | otherwise -> otherwise 
                
            | [], _ :: _ -> true |> Some 
            | _ :: _, [] -> false |> Some 
            | [], [] -> true |> Some             

        match thePakets with
        | Int f, Int s ->
            if f = s then
                None 
            else (f <  s) |>  Some             
        | List f, List s -> compareLists f s
        | List _ as l1, Int s ->
            let newSecond = s |> Int |> List.singleton |> List
            comparePakets (l1, newSecond)
        | Int f, (List _ as l2) ->
            let newFirst = f |> Int |> List.singleton |> List
            comparePakets (newFirst, l2)

let partOne (input: Common.Input) =
    input
    |> parseInput
    |> List.choose PartOne.comparePakets 
    |> List.mapi (fun idx ordered -> idx + 1, ordered)
    |> List.filter snd
    |> List.sumBy fst
    |> string

let partTwo (input: Common.Input) = "Not implemented!"

type Solution() =
    let name = "thirteen"

    interface Common.ISolution with
        member __.Name() = name |> Common.titleCase
        member __.LoadData(year) : Common.Input option = Common.loadData name year
        member __.PartOne(input: Common.Input) : string = input |> partOne
        member __.PartTwo(input: Common.Input) : string = input |> partTwo
