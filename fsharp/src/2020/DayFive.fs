module DayFive

open FSharp.Core.Operators.Checked

type Position =
    | Front
    | Back
    static member FromChar(input: char) : Position =
        match input with
        | 'F'
        | 'L' -> Front
        | 'B'
        | 'R' -> Back
        | invalid -> failwithf "Got invalid character %c" invalid

let binarySearch (range: int list) (pos: Position) =
    let midpoint = range |> List.length |> (fun l -> l / 2)

    match pos with
    | Front -> range.[0..midpoint - 1]
    | Back -> range.[midpoint..]

let getSeatForBoardingPass (pass: Position list) =

    let rec work range positions =
        match positions with
        | pos :: rest ->
            let newRange = binarySearch range pos
            work newRange rest
        | [] -> range |> List.exactlyOne

    let (rowSpec, colSpec) = pass |> List.splitAt 7

    let row = work [ 0 .. 127 ] rowSpec
    let col = work [ 0 .. 7 ] colSpec

    row, col

let computeRowId (row, col) = (row * 8) + col

let parseInput (input: Common.Input) =
    input
    |> Array.map
        (fun s ->
            let chars = s |> Seq.toList
            chars |> List.map Position.FromChar)

let partOne (input: Common.Input) =
    input
    |> parseInput
    |> Array.map (getSeatForBoardingPass >> computeRowId)
    |> Array.max

let partTwo (input: Common.Input) =
    let rec work firstSeat secondSeat seats =
        if secondSeat - firstSeat = 2 then
            secondSeat - 1
        else
            work secondSeat (seats |> List.head) (seats |> List.tail)

    let seatIds =
        input
        |> parseInput
        |> Array.map (getSeatForBoardingPass >> computeRowId)
        |> Array.sort
        |> List.ofArray

    work (seatIds |> List.head) (seatIds |> List.tail |> List.head) (seatIds |> List.skip 2)

type Solution() =
    let name = "five"

    interface Common.ISolution with
        member __.Name() = name |> Common.titleCase
        member __.LoadData(year) : Common.Input option = Common.loadData name year
        member __.PartOne(input: Common.Input) : string = input |> partOne |> string
        member __.PartTwo(input: Common.Input) : string = input |> partTwo |> string
