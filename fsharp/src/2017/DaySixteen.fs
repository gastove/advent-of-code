module DaySixteen

open FSharp.Core.Operators.Checked

let Dancers = [ 'a' .. 'p' ]

type DanceMove =
    | Spin of int
    | Exchange of int * int
    | Partner of char * char
    static member FromString(s: string) =
        match s |> Seq.head with
        | 's' -> s.[1..] |> int |> Spin
        | 'x' ->
            let parsed = s.[1..].Split("/") |> Array.map int
            Exchange(parsed.[0], parsed.[1])
        | 'p' ->
            let parsed = s.[1..].Split("/") |> Array.map char
            Partner(parsed.[0], parsed.[1])
        | bad -> failwithf "No dance move with indicator %A (from string %s)" bad s

    member this.Do(dancers: char list) =
        match this with
        | Spin (positions) ->
            let putAtFront = dancers.[dancers.Length - positions..]
            putAtFront @ dancers |> List.take dancers.Length

        | Exchange (firstIdx, secondIdx) ->
            let mutations =
                dancers
                |> List.mapi (fun idx elem -> idx, elem)
                |> List.choose
                    (fun (idx, elem) ->
                        if idx = firstIdx then
                            Some(secondIdx, elem)
                        else if idx = secondIdx then
                            Some(firstIdx, elem)
                        else
                            None)
                |> Map.ofList

            dancers
            |> Common.Lists.mutateElementsAtIndex mutations

        | Partner (firstDancer, secondDancer) ->
            let mutations =
                let firstIdx =
                    dancers
                    |> List.findIndex (fun c -> c = firstDancer)

                let secondIdx =
                    dancers
                    |> List.findIndex (fun c -> c = secondDancer)

                [ secondIdx, firstDancer
                  firstIdx, secondDancer ]
                |> Map.ofList

            dancers
            |> Common.Lists.mutateElementsAtIndex mutations

let parseInput (input: Common.Input) =
    input
    |> Array.exactlyOne
    |> (fun s -> s.Split(","))
    |> Array.map DanceMove.FromString
    |> Array.toList

let partOne (input: Common.Input) =
    let instructions = input |> parseInput

    instructions
    |> List.fold (fun dancers instruction -> instruction.Do dancers) Dancers
    |> Array.ofList
    |> System.String


let computeCycleLength dancers (instructions: DanceMove list) =
    let rec cycle theDancers iteration =
        if iteration > 0 && theDancers = dancers then
            iteration
        else
            let newDancers =
                instructions
                |> List.fold (fun d instruction -> instruction.Do d) theDancers

            cycle newDancers (iteration + 1)

    cycle dancers 0

let partTwo (input: Common.Input) =
    let instructions = input |> parseInput
    let neededCycles = 1_000_000_000
    let cycleLength = computeCycleLength Dancers instructions
    let actuallyDo = neededCycles % cycleLength

    let rec danceMonkeyDance dancers remaining =
        if remaining = 0 then
            dancers
            |> Array.ofList
            |> System.String
        else
            let updated =
                instructions
                |> List.fold (fun d instruction -> instruction.Do d) dancers
            danceMonkeyDance updated (remaining - 1)

    danceMonkeyDance Dancers actuallyDo

type Solution() =
    let name = "sixteen"

    interface Common.ISolution with
        member __.Name() = name |> Common.titleCase
        member __.LoadData(year) : Common.Input option = Common.loadData name year
        member __.PartOne(input: Common.Input) : string = input |> partOne
        member __.PartTwo(input: Common.Input) : string = input |> partTwo
