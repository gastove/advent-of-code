module DayFive

open FSharp.Core.Operators.Checked

let csvToPoint (csv: string) =
    let raw = csv.Split(',')
    let x = raw.[0] |> int
    let y = raw.[1] |> int

    Common.Geometry.Point.Create x y

let parseInput (input: Common.Input) =
    input
    |> Array.map (fun line ->
        let raw = line.Split(" -> ")
        let first = raw.[0] |> csvToPoint
        let second = raw.[1] |> csvToPoint

        Common.Geometry.Line.Create first second)

let partOne (input: Common.Input) =
    let lines = input |> parseInput

    let allPoints =
        lines
        |> Seq.filter (fun line -> line.IsHorizontal || line.IsVertical)
        |> Seq.collect (fun line -> line.AllPoints())

    allPoints
    |> Seq.countBy id
    |> Seq.filter (fun (_, count) -> count >= 2)
    |> Seq.length
    |> string

let partTwo (input: Common.Input) =
    let lines = input |> parseInput

    let allPoints =
        lines
        |> Seq.collect (fun line -> line.AllPoints())

    allPoints
    |> Seq.countBy id
    |> Seq.filter (fun (_, count) -> count >= 2)
    |> Seq.length
    |> string

type Solution() =
    let name = "five"

    interface Common.ISolution with
        member __.Name() = name |> Common.titleCase
        member __.LoadData(year) : Common.Input option = Common.loadData name year
        member __.PartOne(input: Common.Input) : string = input |> partOne
        member __.PartTwo(input: Common.Input) : string = input |> partTwo
