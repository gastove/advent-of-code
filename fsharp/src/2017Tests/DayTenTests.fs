module DayTenTests

open Expecto

open DayTen

[<Tests>]
let circularListTests =
    testList
        "Testing the CircularList data structure"
        [ testCase "Test rotation"
          <| fun _ ->
              let input = [ 1; 2; 3; 4; 5 ]
              let cl = { Data = input }
              let rotateAt = 2
              let expected = { Data = [ 3; 4; 5; 1; 2 ] }
              let got = cl.RotateData rotateAt

              Expect.equal got expected "We should be able to do basic rotation"

          testCase "Test unrotation"
          <| fun _ ->
              let input = [ 3; 4; 5; 1; 2 ]
              let cl = { Data = input }
              let rotateAt = 2
              let expected = { Data = [ 1; 2; 3; 4; 5 ] }
              let got = cl.UnRotateData rotateAt

              Expect.equal got expected "We should be able to invert rotation"

          testCase "Test index normalization"
          <| fun _ ->
              let input = [ 1; 2; 3; 4; 5 ]
              let cl = { Data = input }
              let withinBounds = 3
              let tooLong = 8
              let muchTooLong = 102

              let expectedWithinBoundsNormalized = 3
              let expectedTooLongNormalized = 3
              let expectedMuchTooLongNormalized = 2

              Expect.equal
                  (cl.NormalizeIndex withinBounds)
                  expectedWithinBoundsNormalized
                  "An in-bounds index should not change"

              Expect.equal
                  (cl.NormalizeIndex tooLong)
                  expectedTooLongNormalized
                  "An out-of-bounds index should be shortened"

              Expect.equal
                  (cl.NormalizeIndex muchTooLong)
                  expectedMuchTooLongNormalized
                  "An extremely out-of-bounds index should also be normalized"

          testCase "Test rotating a hunk"
          <| fun _ ->
              let input = [ 0; 1; 2; 3; 4 ]
              let cl = { Data = input }
              let idx = 0
              let length = 3
              let expected = { Data = [ 2; 1; 0; 3; 4 ] }

              let got = cl.ReverseHunk idx length

              Expect.equal got expected "We should be able to mutate a hunk"

          testCase "Test rotating a hunk that wraps"
          <| fun _ ->
              let input = [ 2; 1; 0; 3; 4 ]
              let cl = { Data = input }
              let idx = 3
              let length = 4
              let expected = { Data = [ 4; 3; 0; 1; 2 ] }

              let got = cl.ReverseHunk idx length

              Expect.equal got expected "We should be able to mutate a wrapping hunk" ]
