module DayOne

open FSharp.Core.Operators.Checked

let countIncreases (depths: int list) =
    depths
    |> List.pairwise
    |> List.sumBy (fun (firstNum, secondNum) -> if firstNum >= secondNum then 0 else 1)

let partOne (input: Common.Input) =
    input
    |> Array.map int
    |> List.ofArray
    |> countIncreases
    |> string

let partTwo (input: Common.Input) =
    let numbers = input |> Array.map int |> List.ofArray

    let numberOfNumbers = numbers |> List.length

    let triplets =
        List.zip3 numbers.[..numberOfNumbers - 3] numbers.[1..numberOfNumbers - 2] numbers.[2..]

    let summed =
        triplets |> List.map (fun (f, s, t) -> f + s + t)

    summed |> countIncreases |> string

type Solution() =
    let name = "one"

    interface Common.ISolution with
        member __.Name() = name |> Common.titleCase
        member __.LoadData(year) : Common.Input option = Common.loadData name year
        member __.PartOne(input: Common.Input) : string = input |> partOne
        member __.PartTwo(input: Common.Input) : string = input |> partTwo
