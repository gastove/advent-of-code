module DayThirteen

open FSharp.Core.Operators.Checked

type Bus(id: int) =
    member __.Arrivals() = Seq.initInfinite (fun i -> id * i)
    member __.Id = id

let earliestArrival (bus: Bus) (threshold: int) = bus.Arrivals() |> Seq.find (fun i -> i > threshold)

let genAnchorSequence id = Seq.initInfinite (fun i -> int64 id * int64 i)

let maybeParse (idx, c) =
    match c with
    | "x" -> None
    | i ->
        let parsed = i |> (string >> System.Int64.Parse)
        Some(idx, parsed)

let prepData (input: string) =
    input.Split(',')
    |> Array.mapi (fun idx c -> (int64 idx), c)
    |> Array.choose maybeParse

type Congruence = int64 * int64

let rec sieve (congruences: Congruence list) computedRemainder product =
    match congruences with
    | [] -> Some(computedRemainder)
    | (a, n) :: rest ->
        let arrProgress = Seq.unfold (fun x -> Some(x, x + product)) computedRemainder
        let firstXmodNequalA = Seq.tryFind (fun x -> a = x % n)
        // I'm going to have to downcast my Int64 to Int32, which is sketchy as fuck. Panic as needed.
        if n > (int64 System.Int32.MaxValue) then failwith (sprintf "It's too big: %i" n)
        match firstXmodNequalA (Seq.take (int n) arrProgress) with
        | None -> None
        | Some(x) -> sieve rest x (product * n)

let computeCRT (congruences: Congruence list) =
    let cs =
        congruences
        |> List.map (fun (a, n) -> (a % n, n))
        |> List.sortBy snd
        |> List.rev

    let an = List.head cs
    sieve (List.tail cs) (fst an) (snd an)

let rec gcd (x, y) =
    if x = y then x
    else if x > y then gcd (x - y, y)
    else gcd (x, y - x)

let even x = x % 2L = 0L
let odd x = not (even x)

let rec binaryGcd (x: int64) (y: int64) =
    if x = y then
        x
    else if x = 0L then
        x
    else if y = 0L then
        y
    else
        match (even x), (even y) with
        | true, true -> 2L * (binaryGcd (x / 2L) (y / 2L))
        | true, false -> binaryGcd (x / 2L) y
        | false, true -> binaryGcd x (y / 2L)
        | false, false ->
            if x > y then binaryGcd ((x - y) / 2L) y else binaryGcd ((y - x) / 2L) x

let lcm (a: int64) (b: int64) = System.Math.Abs(a * b) / (binaryGcd a b)

let copyPolinasHomework (input: Congruence array) =
    let mutable ts = 0L
    let mutable remaining = input.Length
    let mutable increment = input.[0] |> snd
    while remaining > 0 do
        let minutes, id = input.[input.Length - remaining]
        if (ts + minutes) % id = 0L then
            increment <- lcm increment id
            remaining <- remaining - 1
        else
            ts <- ts + increment

    ts


type Solution() =
    let name = "thirteen"
    interface Common.ISolution with
        member __.LoadData(year): Common.Input option = Common.loadData name year

        member __.PartOne(input: Common.Input): string =
            let earliesTimestamp = input.[0] |> System.Int32.Parse

            let busses =
                input.[1].Split(',')
                |> Array.filter (fun c -> c <> "x")
                |> Array.choose (string >> Common.tryInt)
                |> Array.map (fun i -> Bus(i))

            let earliestBus, pickupTime =
                busses
                |> Array.map (fun b -> b, earliestArrival b earliesTimestamp)
                |> Array.minBy snd

            let wait = pickupTime - earliesTimestamp

            sprintf "I arrive at %i, so I'll take bus %i at %i, giving an answer of %i" earliesTimestamp earliestBus.Id
                pickupTime (earliestBus.Id * wait)

        member __.PartTwo(input: Common.Input): string =
            let prepped = input.[1] |> prepData

            let anAnswer = copyPolinasHomework prepped

            sprintf "Got %i" anAnswer
        // match computeCRT prepped with
        // | Some(i) -> sprintf "Found a result: %i" i
        // | None -> "I got nothing, boss"

        member __.Name() = name |> Common.titleCase
