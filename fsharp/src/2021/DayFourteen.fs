module DayFourteen

open FSharp.Core.Operators.Checked

type Rules = Map<string, string>

let parseRules (input: Common.Input) =
    input
    |> Array.map (fun line ->
        let parts = line.Split(" -> ")

        (parts.[0].[0], parts.[0].[1]), parts.[1] |> char)
    |> Map.ofArray

let parseInput (input: Common.Input) =
    let startingPolymer = input |> Array.head
    let rules = input |> Array.skip 2 |> parseRules

    startingPolymer, rules

let applyRules (startingPolymer: string) (rules: Map<(char * char), char>) (times: int) =
    let rec apply (polymer: string) (remainingIterations: int) =
        match remainingIterations with
        | 0 -> polymer
        | remaining ->
            let newPolymer =
                polymer
                |> Seq.toArray
                |> Array.pairwise
                |> Array.Parallel.map (fun (firstHalf, secondHalf) ->
                    match rules |> Map.tryFind (firstHalf, secondHalf) with
                    | Some addition -> [| firstHalf; addition; secondHalf |]
                    | None -> [| firstHalf; secondHalf |])
                |> Array.reduce (fun firstArr secondArr ->
                    let combined =
                        [| firstArr; secondArr |> Array.tail |]
                        |> Array.concat

                    combined)
                |> System.String

            apply newPolymer (remaining - 1)

    apply startingPolymer times


let applyRulesDeep (startingPolymer: string) (rules: Map<(char * char), char>) (times: int) =
    let combineMaps (mapOne: Map<char, int>) (mapTwo: Map<char, int>) =
        mapTwo
        |> Map.toArray
        |> Array.fold
            (fun m (k, v) ->
                match m |> Map.tryFind k with
                | Some _ -> m |> Map.change k (Option.map (fun ov -> ov + v))
                | None -> m |> Map.add k v)
            mapOne

    let rec apply (polymer: char array) (remainingIterations: int) =
        match remainingIterations with
        | 0 -> polymer |> Array.countBy id |> Map.ofArray
        | remaining ->
            polymer
            |> Array.pairwise
            |> Array.Parallel.map (fun (firstHalf, secondHalf) ->
                let addition =
                    rules |> Map.find (firstHalf, secondHalf)

                apply [| firstHalf; addition; secondHalf |] (remaining - 1))
            |> Array.reduce combineMaps

    apply (startingPolymer |> Seq.toArray) times


let countingApply (polymer: string) (rules: Map<char * char, char>) (iterations: int) =

    let rec apply (pairs: Map<char * char, int64>) (remainingIterations: int) =
        match remainingIterations with
        | 0 -> pairs
        | _ ->
            let updatedPairs =
                pairs
                |> Map.fold
                    (fun pairMap (f, s) _pairCount ->
                        let newChar = rules |> Map.find (f, s)
                        let firstNewPair = (f, newChar)
                        let secondNewPair = (newChar, s)

                        pairMap
                        |> Map.change firstNewPair (fun v ->
                            v
                            |> Option.defaultValue 0L
                            |> (+) 1L
                            |> Some)
                        |> Map.change secondNewPair (fun v ->
                            v
                            |> Option.defaultValue 0L
                            |> (+) 1L
                            |> Some))
                    pairs

            apply updatedPairs (remainingIterations - 1)

    let startingPairs =
        polymer
        |> Seq.pairwise
        |> Seq.countBy id
        |> Seq.map (fun (ch, cnt) -> ch, int64 cnt)
        |> Map

    apply startingPairs iterations

let update<'Key when 'Key: comparison> (key: 'Key) (value: int64) (map: Map<'Key, int64>) =
    match map |> Map.tryFind key with
    | Some oldVal -> map |> Map.add key (oldVal + value)
    | None -> map |> Map.add key value

let pairMapToCharCounts (pairMap: Map<char * char, int64>) =
    pairMap
    |> Map.fold
        (fun charCounts (f, s) charCount ->
            charCounts
            |> update f charCount
            |> update s charCount)
        Map.empty

let partOne (input: Common.Input) =
    let (polymer, rules) = input |> parseInput

    let newPolymer = applyRules polymer rules 10

    let counts =
        newPolymer |> Seq.toArray |> Array.countBy id

    let mostUsed = counts |> Array.maxBy snd |> snd
    let leastUsed = counts |> Array.minBy snd |> snd

    mostUsed - leastUsed |> string

let incrementOption (num: int64 option) = num |> Option.map (fun i -> i + 1L)

let partTwo (input: Common.Input) =
    let (polymer, rules) = input |> parseInput

    let counts =
        countingApply polymer rules 10
        |> pairMapToCharCounts
        |> Map.toArray

    let mostUsed = counts |> Array.maxBy snd |> snd
    let leastUsed = counts |> Array.minBy snd |> snd

    mostUsed - leastUsed |> string

type Solution() =
    let name = "fourteen"

    interface Common.ISolution with
        member __.Name() = name |> Common.titleCase
        member __.LoadData(year) : Common.Input option = Common.loadData name year
        member __.PartOne(input: Common.Input) : string = input |> partOne
        member __.PartTwo(input: Common.Input) : string = input |> partTwo
