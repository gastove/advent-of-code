use {regex::Regex, std::collections::HashSet};

#[derive(Debug, PartialEq)]
enum DayFourError {
    RegexError(String),
    ParseError(String),
}

impl std::fmt::Display for DayFourError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        let s = match self {
            DayFourError::RegexError(s) => format!("Regex Error: {s}"),
            DayFourError::ParseError(s) => format!("Usize Parsing Error: {s}"),
        };

        write!(f, "{}", s)
    }
}

impl From<std::num::ParseIntError> for DayFourError {
    fn from(value: std::num::ParseIntError) -> Self {
        Self::ParseError(format!("{:?}", value))
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct Card {
    id: usize,
    winning_numbers: Vec<usize>,
}

impl Card {
    pub fn score(&self, numbers: Vec<usize>) -> usize {
        let number_pile: HashSet<usize> = HashSet::from_iter(numbers);

        self.winning_numbers.iter().fold(0, |score, candidate| {
            if number_pile.contains(candidate) {
                if score == 0 {
                    1
                } else {
                    score * 2
                }
            } else {
                score
            }
        })
    }

    pub fn count_wins(&self, numbers: Vec<usize>) -> i64 {
        let number_pile: HashSet<usize> = HashSet::from_iter(numbers);

        self.winning_numbers.iter().fold(0, |score, candidate| {
            if number_pile.contains(candidate) {
                score + 1
            } else {
                score
            }
        })
    }
}

fn try_parse_line(
    line: &str,
    num_winners: usize,
    pile_size: usize,
) -> Result<(Card, Vec<usize>), DayFourError> {
    let re = Regex::new(r"(\d+)").expect("Failed to compile regex");

    let numbers: Vec<usize> = re
        .captures_iter(line)
        .map(|c| {
            c.get(0)
                .map(|m| m.as_str())
                .ok_or(DayFourError::RegexError("Failed to capture numbers".into()))
                .and_then(|s| s.parse().map_err(DayFourError::from))
        })
        .collect::<Result<Vec<usize>, DayFourError>>()?;

    // First number is the card ID
    let id = numbers[0];
    // 1 through num_winners, inclusive, is the expected number of winners.
    let winning_numbers: Vec<usize> = numbers[1..=num_winners].iter().cloned().collect();
    // num_winners through num_winners+pile_size, inclusive, is the candidate number pile
    let pile: Vec<usize> = numbers[num_winners + 1..=num_winners + pile_size]
        .iter()
        .cloned()
        .collect();

    Ok((
        Card {
            id,
            winning_numbers,
        },
        pile,
    ))
}

#[cfg(test)]
mod part_one_tests {

    use super::*;

    #[test]
    fn test_try_parse_line() {
        struct TestCase {
            input: String,
            expected: (Card, Vec<usize>),
        }

        let test_cases = &[
            TestCase {
                input: "Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53".into(),
                expected: (
                    Card {
                        id: 1,
                        winning_numbers: vec![41, 48, 83, 86, 17],
                    },
                    vec![83, 86, 6, 31, 17, 9, 48, 53],
                ),
            },
            // "Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19",
            // "Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1",
            // "Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83",
            // "Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36",
            // "Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11",
        ];

        test_cases.iter().for_each(|tc| {
            let got = try_parse_line(&tc.input, 5, 8);

            assert_eq!(
                got,
                Ok(tc.expected.clone()),
                "We should be able to reproduce the example for Card 1"
            );
        })
    }
}

mod part_one {
    use super::try_parse_line;

    pub fn run(input: Vec<String>) -> String {
        let num_winners = 10;
        let pile_size = 25;

        input
            .iter()
            .map(|line| match try_parse_line(line, num_winners, pile_size) {
                Ok((card, pile)) => card.score(pile),
                Err(e) => panic!("Error during input parsing: {e}"),
            })
            .sum::<usize>()
            .to_string()
    }
}

mod part_two {
    use super::{try_parse_line, Card};
    use std::collections::HashMap;

    fn compute_card_counts(data: &Vec<(Card, Vec<usize>)>) -> HashMap<usize, usize> {
        data.iter()
            .fold(HashMap::new(), |mut card_counts, (card, pile)| {
                let mut wins = card.count_wins(pile.to_vec());                

                // Count "self" by either incrementing by or inserting 1
                card_counts
                    .entry(card.id)
                    .and_modify(|e| *e += 1)
                    .or_insert(1);

                let how_many_of_current = card_counts.get(&card.id).unwrap_or(&1).clone();
                
                while wins > 0 {
                    let update_id = card.id + (wins as usize);                 
                    card_counts
                        .entry(update_id)
                        .and_modify(|e| *e += how_many_of_current)
                        .or_insert(how_many_of_current);

                    wins -= 1;
                }                
                card_counts
            })
    }

    fn score(data: Vec<(Card, Vec<usize>)>) -> usize {
        compute_card_counts(&data)
            .iter()
            .map(|(_, v)| v.clone())
            .sum()
    }

    pub fn run(input: Vec<String>) -> String {
        let num_winners = 10;
        let pile_size = 25;
        
        let card_pile_pairs: Vec<(Card, Vec<usize>)> = input
            .iter()
            .map(|line| match try_parse_line(line, num_winners, pile_size) {
                Ok((card, pile)) => (card, pile),
                Err(e) => panic!("Error during input parsing: {e}"),
            })
            .collect();

        score(card_pile_pairs).to_string()
    }
}

pub fn run(input: Vec<String>) {
    println!("=== Day Four! ===");
    println!("Part One\t=> {}", part_one::run(input.clone()));
    println!("Part Two\t=> {}", part_two::run(input));
}
