module DayNineTests

open Expecto

open Common.Geometry

open DayNine

let PartOneInput =
    [| "R 4"
       "U 4"
       "L 3"
       "D 1"
       "R 4"
       "D 1"
       "L 5"
       "R 2" |]

[<Tests>]
let partOneTailMotionTests =
    let testCases =
        [ {| TestName = "DontMoveAtAll"
             Head = Point.Create 5 3
             Tail = Point.Create 5 3
             ExpectedTail = Point.Create 5 3
             Message = "If Head and Tail are already colocated, Tail should not move" |}
          {| TestName = "MoveDiagonallyUpRight"
             Head = Point.Create 2 2
             Tail = Point.Create 0 0
             ExpectedTail = Point.Create 1 1
             Message = "We should move 1 unit up and right" |}
          {| TestName = "MoveDiagonallyDownRight"
             Head = Point.Create 5 -5
             Tail = Point.Create 0 0
             ExpectedTail = Point.Create 1 -1
             Message = "We should move 1 unit right and 1 unit down" |}
          {| TestName = "MoveDiagonallyDownLeft"
             Head = Point.Create -3 -3
             Tail = Point.Create 0 0
             ExpectedTail = Point.Create -1 -1
             Message = "We should move 1 unit down and left" |}
          {| TestName = "MoveDiagonallyUpLeft"
             Head = Point.Create -5 5
             Tail = Point.Create 0 0
             ExpectedTail = Point.Create -1 1
             Message = "We should move 1 unit up and left" |}
          {| TestName = "MoveLeft"
             Head = Point.Create -10 0
             Tail = Point.Create 0 0
             ExpectedTail = Point.Create -1 0
             Message = "We should move 1 unit left" |}
          {| TestName = "MoveRight"
             Head = Point.Create 10 0
             Tail = Point.Create 0 0
             ExpectedTail = Point.Create 1 0
             Message = "We should move 1 unit right" |}
          {| TestName = "MoveUp"
             Head = Point.Create 0 9
             Tail = Point.Create 0 0
             ExpectedTail = Point.Create 0 1
             Message = "We should move 1 unit up" |}
          {| TestName = "MoveDown"
             Head = Point.Create 10 -5
             Tail = Point.Create 10 0
             ExpectedTail = Point.Create 10 -1
             Message = "We should move 1 unit left" |} ]

    testCases
    |> List.map (fun tc ->
        testCase tc.TestName
        <| fun _ ->
            let newTail =
                PartOne.moveSecondTowardsFirst tc.Head tc.Tail

            Expect.equal newTail tc.ExpectedTail tc.Message)
    |> testList "DayNine.PartOne"

[<Tests>]
let partOneTests =
    testList
        "DayNine.PartOne.Example"
        [ testCase "Can we reproduce the Part One example?"
          <| fun _ ->
              let got = PartOneInput |> partOne

              Expect.equal got "13" "We should be able to reproduce the example"

         ]
