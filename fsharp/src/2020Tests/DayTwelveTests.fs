module DayTwelveTests

open Expecto

let testInput = [| "F10"; "N3"; "F7"; "R90"; "F11" |]

[<Tests>]
let partTwoTests =
    testList "Testing PartTwoShip"
        [ testCase "Can I convert degrees to radians correctly?" <| fun _ ->
            let degrees = 90
            let radians = DayTwelve.degreesToRadians degrees
            let expected = 1.5708

            Expect.floatClose Accuracy.medium radians expected "We should be pretty close"
          testCase "Does a ship wind up at the correct position given test data" <| fun _ ->
            let instructions = testInput |> Array.choose DayTwelve.Instruction.From

            let ship = DayTwelve.PartTwoShip.New()

            let finalShip =
                instructions |> Array.fold (fun (state: DayTwelve.PartTwoShip) inst -> state.Apply inst) ship

            Expect.equal finalShip.X 214 "The ship should have corrext X position"
            Expect.equal finalShip.Y -72 "The ship shouldhave correct Y position"

          testCase "Does waypoint rotation work for an artificially simple case?" <| fun _ ->
            let startingWaypoint: DayTwelve.Waypoint = {X = 0; Y = 10}
            let expected: DayTwelve.Waypoint = {X = 10; Y = 0}
            let increment = 90 |> DayTwelve.degreesToRadians
            let got = startingWaypoint.Rotate (0, 0) -increment

            Expect.equal got expected "We should wind up with the correct waypoint"
          testCase "Does waypoint rotation work for a real test case" <| fun _ ->
// F7 moves the ship to the waypoint 7 times (a total of 70 units east and 28 units north), leaving the ship at east 170, north 38. The waypoint stays 10 units east and 4 units north of the ship.
// R90 rotates the waypoint around the ship clockwise 90 degrees, moving it to 4 units east and 10 units south of the ship. The ship remains at east 170, north 38.
            let startingWaypoint: DayTwelve.Waypoint = {X = 180; Y = 42}
            let expected: DayTwelve.Waypoint = {X = 174; Y = 28}
            let increment = 90 |> DayTwelve.degreesToRadians
            let got = startingWaypoint.Rotate (170, 38) -increment

            Expect.equal got expected "We should wind up with the correct waypoint" ]
