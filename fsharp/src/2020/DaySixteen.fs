module DaySixteen

open FSharp.Core.Operators.Checked

type Range =
    { Min: int
      Max: int }

    static member Parse(input: string) =
        let parts = input.Split("-") |> Array.choose Common.tryInt

        { Min = parts.[0]
          Max = parts.[1] }

    static member ParsePair(input: string) = input.Split("or") |> Array.map (fun s -> s.Trim() |> Range.Parse)

    member this.Contains i = this.Min <= i && i <= this.Max

    override this.ToString() = sprintf "%i-%i" this.Min this.Max

type Rule =
    | DepartureLocation of Range * Range
    | DepartureStation of Range * Range
    | DeparturePlatform of Range * Range
    | DepartureTrack of Range * Range
    | DepartureDate of Range * Range
    | DepartureTime of Range * Range
    | ArrivalLocation of Range * Range
    | ArrivalStation of Range * Range
    | ArrivalPlatform of Range * Range
    | ArrivalTrack of Range * Range
    | Class of Range * Range
    | Duration of Range * Range
    | Price of Range * Range
    | Route of Range * Range
    | Row of Range * Range
    | Seat of Range * Range
    | Train of Range * Range
    | Type of Range * Range
    | Wagon of Range * Range
    | Zone of Range * Range

    static member Parse(input: string) =
        let pieces = input.Split(":") |> Array.map (fun s -> s.Trim())
        let ranges = pieces.[1] |> Range.ParsePair
        let rmin = ranges.[0]
        let rmax = ranges.[1]

        match pieces.[0] with
        | "departure location" -> Some(DepartureLocation(rmin, rmax))
        | "departure station" -> Some(DepartureStation(rmin, rmax))
        | "departure platform" -> Some(DeparturePlatform(rmin, rmax))
        | "departure track" -> Some(DepartureTrack(rmin, rmax))
        | "departure date" -> Some(DepartureDate(rmin, rmax))
        | "departure time" -> Some(DepartureTime(rmin, rmax))
        | "arrival location" -> Some(ArrivalLocation(rmin, rmax))
        | "arrival station" -> Some(ArrivalStation(rmin, rmax))
        | "arrival platform" -> Some(ArrivalPlatform(rmin, rmax))
        | "arrival track" -> Some(ArrivalTrack(rmin, rmax))
        | "class" -> Some(Class(rmin, rmax))
        | "duration" -> Some(Duration(rmin, rmax))
        | "price" -> Some(Price(rmin, rmax))
        | "route" -> Some(Route(rmin, rmax))
        | "row" -> Some(Row(rmin, rmax))
        | "seat" -> Some(Seat(rmin, rmax))
        | "train" -> Some(Train(rmin, rmax))
        | "type" -> Some(Type(rmin, rmax))
        | "wagon" -> Some(Wagon(rmin, rmax))
        | "zone" -> Some(Zone(rmin, rmax))
        | _ -> None

    member this.SatisfiedBy i =
        match this with
        | DepartureDate(minimum, maximum) -> minimum.Contains(i) || maximum.Contains(i)
        | DepartureLocation(minimum, maximum) -> minimum.Contains(i) || maximum.Contains(i)
        | DepartureStation(minimum, maximum) -> minimum.Contains(i) || maximum.Contains(i)
        | DeparturePlatform(minimum, maximum) -> minimum.Contains(i) || maximum.Contains(i)
        | DepartureTrack(minimum, maximum) -> minimum.Contains(i) || maximum.Contains(i)
        | DepartureTime(minimum, maximum) -> minimum.Contains(i) || maximum.Contains(i)
        | ArrivalLocation(minimum, maximum) -> minimum.Contains(i) || maximum.Contains(i)
        | ArrivalStation(minimum, maximum) -> minimum.Contains(i) || maximum.Contains(i)
        | ArrivalPlatform(minimum, maximum) -> minimum.Contains(i) || maximum.Contains(i)
        | ArrivalTrack(minimum, maximum) -> minimum.Contains(i) || maximum.Contains(i)
        | Class(minimum, maximum) -> minimum.Contains(i) || maximum.Contains(i)
        | Duration(minimum, maximum) -> minimum.Contains(i) || maximum.Contains(i)
        | Price(minimum, maximum) -> minimum.Contains(i) || maximum.Contains(i)
        | Route(minimum, maximum) -> minimum.Contains(i) || maximum.Contains(i)
        | Row(minimum, maximum) -> minimum.Contains(i) || maximum.Contains(i)
        | Seat(minimum, maximum) -> minimum.Contains(i) || maximum.Contains(i)
        | Train(minimum, maximum) -> minimum.Contains(i) || maximum.Contains(i)
        | Type(minimum, maximum) -> minimum.Contains(i) || maximum.Contains(i)
        | Wagon(minimum, maximum) -> minimum.Contains(i) || maximum.Contains(i)
        | Zone(minimum, maximum) -> minimum.Contains(i) || maximum.Contains(i)

type Ticket =
    { Fields: int array }
    static member Parse(input: string) =
        let fields = input.Split(",") |> Array.choose (fun s -> s.Trim() |> Common.tryInt)
        { Fields = fields }

type Rules =
    { AllTheRules: Rule array }

    member this.ApplyPartOne(ticket: Ticket) =
        ticket.Fields
        |> Array.map (fun field ->
            field,
            this.AllTheRules
            |> Array.map (fun r -> r.SatisfiedBy(field))
            |> Array.reduce (||))
        |> Array.filter (snd >> not)
        |> Array.map fst

    member this.Valid(ticket: Ticket) =
        ticket.Fields
        |> Array.map (fun field ->
            this.AllTheRules
            |> Array.map (fun r -> r.SatisfiedBy(field))
            |> Array.reduce (||))
        |> Array.reduce (&&)

    member this.ValidateField(field: int): Rule array =
        this.AllTheRules |> Array.filter (fun (r: Rule) -> r.SatisfiedBy field)

let takeAndTrim input =
    let got = input |> Array.takeWhile (fun s -> s <> "")
    let remaining = Array.skip got.Length input |> Array.skipWhile (fun s -> s = "")

    got, remaining

let parseInput (input: Common.Input) =
    let ruleSpecs, remaining = input |> takeAndTrim
    let myTicketSpec, neighbors = remaining |> takeAndTrim

    let rules = { AllTheRules = ruleSpecs |> Array.choose Rule.Parse }
    let myTicket = Ticket.Parse myTicketSpec.[1]
    let tickets = neighbors.[1..] |> Array.map Ticket.Parse

    myTicket, rules, tickets

let computePositions (tickets: Ticket array) (rules: Rules) =
    let accumulator: Rule option array = Array.zeroCreate rules.AllTheRules.Length

    let rec work (positions: Rule option array) (remainingRules: Rules) =
        // Get every unfilled index
        let emptyPositions =
            positions
            |> Array.mapi (fun i p -> i, p)
            |> Array.filter (fun (_, p) -> p |> Option.isNone)
            |> Array.map fst

        match emptyPositions.Length with
        | 0 -> positions
        | _ ->
            // Figure out which index we can fill
            let rulesForEachPosition =
                emptyPositions
                |> Array.map (fun idx ->
                    let rulesForIdx =
                        tickets
                        |> Array.map (fun t ->
                            t.Fields.[idx]
                            |> remainingRules.ValidateField
                            |> Set.ofArray)
                        |> Set.intersectMany
                    idx, rulesForIdx)

            let position, ruleSet = rulesForEachPosition |> Array.minBy (snd >> Set.count)

            let ruleToSet =
                ruleSet
                |> Set.toArray
                |> Array.exactlyOne

            Array.set positions position (Some ruleToSet)
            let rules = remainingRules.AllTheRules |> Array.filter (fun r -> r <> ruleToSet)

            work positions { AllTheRules = rules }

    work accumulator rules

type Solution() =
    let name = "sixteen"
    interface Common.ISolution with
        member __.LoadData(year): Common.Input option = Common.loadData name year

        member __.PartOne(input: Common.Input): string =
            let _, rules, tickets = parseInput input
            let found = tickets |> Array.Parallel.collect rules.ApplyPartOne

            sprintf "The number is %i" (found |> Array.sum)

        member __.PartTwo(input: Common.Input): string =
            let myTicket, rules, tickets = parseInput input
            let validTickets = tickets |> Array.filter rules.Valid

            let assignedValues = computePositions validTickets rules |> Array.choose id

            let isRuleIWant (rule: Rule) =
                match rule with
                | DepartureDate(_)
                | DepartureLocation(_)
                | DeparturePlatform(_)
                | DepartureStation(_)
                | DepartureTime(_)
                | DepartureTrack(_) -> true
                | _ -> false

            let positionsIWant =
                assignedValues
                |> Array.mapi (fun i v -> i, v)
                |> Array.filter (snd >> isRuleIWant)
                |> Array.map fst

            let final =
                positionsIWant
                |> Array.map (fun idx -> myTicket.Fields.[idx] |> int64)
                |> Array.reduce (*)

            sprintf "Got back: %i" final

        member __.Name() = name |> Common.titleCase
