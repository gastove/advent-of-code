// --- Day 4: Repose Record ---
//You've sneaked into another supply closet - this time, it's across from the
// prototype suit manufacturing lab. You need to sneak inside and fix the issues
// with the suit, but there's a guard stationed outside the lab, so this is as
// close as you can safely get.

// As you search the closet for anything that might help, you discover that
// you're not the first person to want to sneak in. Covering the walls, someone
// has spent an hour starting every midnight for the past few months secretly
// observing this guard post! They've been writing down the ID of the one guard
// on duty that night - the Elves seem to have decided that one guard was enough
// for the overnight shift - as well as when they fall asleep or wake up while
// at their post (your puzzle input).

// For example, consider the following records, which have already been organized into chronological order:

// [1518-11-01 00:00] Guard #10 begins shift
// [1518-11-01 00:05] falls asleep
// [1518-11-01 00:25] wakes up
// [1518-11-01 00:30] falls asleep
// [1518-11-01 00:55] wakes up
// [1518-11-01 23:58] Guard #99 begins shift
// [1518-11-02 00:40] falls asleep
// [1518-11-02 00:50] wakes up
// [1518-11-03 00:05] Guard #10 begins shift
// [1518-11-03 00:24] falls asleep
// [1518-11-03 00:29] wakes up
// [1518-11-04 00:02] Guard #99 begins shift
// [1518-11-04 00:36] falls asleep
// [1518-11-04 00:46] wakes up
// [1518-11-05 00:03] Guard #99 begins shift
// [1518-11-05 00:45] falls asleep
// [1518-11-05 00:55] wakes up

// Timestamps are written using year-month-day hour:minute format. The guard
// falling asleep or waking up is always the one whose shift most recently
// started. Because all asleep/awake times are during the midnight hour (00:00 -
// 00:59), only the minute portion (00 - 59) is relevant for those events.

// Visually, these records show that the guards are asleep at these times:

// Date   ID   Minute
//             000000000011111111112222222222333333333344444444445555555555
//             012345678901234567890123456789012345678901234567890123456789
// 11-01  #10  .....####################.....#########################.....
// 11-02  #99  ........................................##########..........
// 11-03  #10  ........................#####...............................
// 11-04  #99  ....................................##########..............
// 11-05  #99  .............................................##########.....

// The columns are Date, which shows the month-day portion of the relevant day;
// ID, which shows the guard on duty that day; and Minute, which shows the
// minutes during which the guard was asleep within the midnight hour. (The
// Minute column's header shows the minute's ten's digit in the first row and
// the one's digit in the second row.) Awake is shown as ., and asleep is shown
// as #.

// Note that guards count as asleep on the minute they fall asleep, and they
// count as awake on the minute they wake up. For example, because Guard #10
// wakes up at 00:25 on 1518-11-01, minute 25 is marked as awake.

// If you can figure out the guard most likely to be asleep at a specific time,
// you might be able to trick that guard into working tonight so you can have
// the best chance of sneaking in. You have two strategies for choosing the best
// guard/minute combination.

// Strategy 1: Find the guard that has the most minutes asleep. What minute does
// that guard spend asleep the most?

// In the example above, Guard #10 spent the most minutes asleep, a total of 50
// minutes (20+25+5), while Guard #99 only slept for a total of 30 minutes
// (10+10+10). Guard #10 was asleep most during minute 24 (on two days, whereas
// any other minute the guard was asleep was only seen on one day).

// While this example listed the entries in chronological order, your entries
// are in the order you found them. You'll need to organize them before they can
// be analyzed.

// What is the ID of the guard you chose multiplied by the minute you chose? (In
// the above example, the answer would be 10 * 24 = 240.)

use regex::Regex;
use std::collections::HashSet;

use crate::util;

lazy_static! {
    static ref LINE_RE: Regex =
        Regex::new(r"^\[(\d{4}-\d{2}-\d{2} \d{2}:\d{2})\] (Guard #\d+)? ?(.*)$").unwrap();
    static ref MINUTE_RE: Regex = Regex::new(r"(\d{2}):(\d{2})$").unwrap();
    static ref ID_RE: Regex = Regex::new(r"Guard #(\d+)").unwrap();
}

enum GuardState {
    BeginningShift,
    FallsAsleep,
    WakesUp,
}

impl GuardState {
    pub fn from_string(s: String) -> Option<GuardState> {
        match s.as_ref() {
            "begins shift" => Some(GuardState::BeginningShift),
            "falls asleep" => Some(GuardState::FallsAsleep),
            "wakes up" => Some(GuardState::WakesUp),
            _ => None,
        }
    }
}

enum GuardStatus {
    Awake(i32),
    Asleep(i32),
}

struct GuardLineParser {
    most_recent_id: Option<i32>,
}

impl GuardLineParser {
    fn parse_minute(date: &str) -> (i32, i32) {
        match MINUTE_RE.captures(&date) {
            Some(cap) => {
                let hour: i32 = cap
                    .get(1)
                    .unwrap()
                    .as_str()
                    .parse()
                    .expect("Failed to parse hour");
                let minute: i32 = cap
                    .get(2)
                    .unwrap()
                    .as_str()
                    .parse()
                    .expect("Failed to parse minute");
                return (hour, minute);
            }
            None => (-1, -1),
        }
    }

    pub fn new() -> Self {
        return GuardLineParser {
            most_recent_id: None,
        };
    }

    pub fn from_log_line(&mut self, line: String) -> Option<GuardEvent> {
        let cap = LINE_RE.captures(&line);
        match cap {
            Some(result) => {
                let date = &result.get(1).unwrap().as_str();
                let maybe_guard = &result.get(2);
                let action_str = &result.get(3).unwrap().as_str();

                let (hour, minute) = Self::parse_minute(&date);
                let action = GuardState::from_string(action_str.to_string()).unwrap();

                match maybe_guard {
                    Some(guard) => {
                        let id = id_from_string(guard.as_str().to_string());
                        self.most_recent_id = Some(id);
                        return Some(GuardEvent {
                            guard_id: id,
                            raw_timestamp: date.to_string(),
                            hour: hour,
                            minute: minute,
                            state: action,
                        });
                    }
                    None => {
                        if self.most_recent_id.is_some() {
                            let id = self.most_recent_id.unwrap();
                            return Some(GuardEvent {
                                guard_id: id,
                                raw_timestamp: date.to_string(),
                                hour: hour,
                                minute: minute,
                                state: action,
                            });
                        } else {
                            return None;
                        }
                    }
                }
            }
            None => None,
        }
    }
}

fn id_from_string(s: String) -> i32 {
    ID_RE
        .captures(&s)
        .unwrap()
        .get(1)
        .unwrap()
        .as_str()
        .parse()
        .expect("Failed to parse ID from string")
}

struct GuardEvent {
    guard_id: i32,
    raw_timestamp: String,
    hour: i32,
    minute: i32,
    state: GuardState,
}

struct GuardTimeline {
    id: i32,
    minute: i32,
    status: GuardStatus,
}

fn part_one(lines: &Vec<String>) -> usize {
    let mut glp = GuardLineParser::new();
    let events: Vec<GuardEvent> = lines
        .iter()
        .cloned()
        .filter_map(|l| glp.from_log_line(l))
        .filter(|ge| ge.hour == 0)
        .collect();

    let timeline: Vec<GuardTimeline> = events
        .iter()
        .map(|ge| {
            let guard_status = match ge.state {
                GuardState::BeginningShift => GuardStatus::Awake(ge.minute),
                GuardState::WakesUp => GuardStatus::Awake(ge.minute),
                GuardState::FallsAsleep => GuardStatus::Asleep(ge.minute),
            };

            return GuardTimeline {
                id: ge.guard_id,
                minute: ge.minute,
                status: guard_status,
            };
        })
        .collect();

    return events.len();
}

pub fn run() {
    let lines = util::read_input("resources/day_four_input.txt");
    let part_one = part_one(&lines);

    println!("Got {} guard events", part_one);
}
