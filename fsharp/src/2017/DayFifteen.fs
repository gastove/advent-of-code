module DayFifteen

open FSharp.Core.Operators.Checked

let Modulus = 2147483647L
let AFactor = 16807L
let BFactor = 48271L
let PartOneRounds = 40_000_000
let PartTwoRounds = 5_000_000

let generateNumber factor prev = (prev * factor) % Modulus

let createPartOneGenerator init factor =
    init
    |> Seq.unfold
        (fun state ->
            let next = generateNumber factor state
            (next, next) |> Some)

let createPartTwoGenerator init factor divisor =
    init
    |> Seq.unfold
        (fun state ->
            let rec computeWithDivisibility num =
                if num % divisor = 0L then
                    num
                else
                    computeWithDivisibility <| generateNumber factor num

            let next =
                generateNumber factor state |> computeWithDivisibility
            (next, next) |> Some)


let getLast16Bytes (i: int64) =
    let byteString =
        System.Convert.ToString(i, 2).PadLeft(16, '0')

    byteString
    |> Seq.rev
    |> Seq.take 16
    |> Seq.rev
    |> Seq.map string
    |> String.concat ""

// The examples
// let GeneratorA = createGenerator 65L AFactor
// let GeneratorB = createGenerator 8921L BFactor
// Input hard-coded here
let PartOneGeneratorA = createPartOneGenerator 883L AFactor
let PartOneGeneratorB = createPartOneGenerator 879L BFactor
let PartTwoGeneratorA = createPartTwoGenerator 883L AFactor 4L
let PartTwoGeneratorB = createPartTwoGenerator 879L BFactor 8L


type Solution() =
    let name = "fifteen"

    interface Common.ISolution with
        member __.Name() = name |> Common.titleCase
        member __.LoadData(_) : Common.Input option = [||] |> Some

        member __.PartOne(_: Common.Input) : string =
            let numbers =
                [| PartOneGeneratorA; PartOneGeneratorB |]
                |> Array.Parallel.map (fun g -> g |> Seq.take PartOneRounds)

            Seq.zip numbers.[0] numbers.[1]
            |> Seq.toArray
            |> Array.Parallel.choose
                (fun (numOne, numTwo) ->
                    let firstSixteen = numOne |> getLast16Bytes
                    let secondSixteen = numTwo |> getLast16Bytes

                    if firstSixteen = secondSixteen then
                        Some(1)
                    else
                        None)
            |> Array.sum
            |> string

        member __.PartTwo(_: Common.Input) : string =
            let numbers =
                [| PartTwoGeneratorA; PartTwoGeneratorB |]
                |> Array.Parallel.map (fun g -> g |> Seq.take PartTwoRounds)

            Seq.zip numbers.[0] numbers.[1]
            |> Seq.toArray
            |> Array.Parallel.choose
                (fun (numOne, numTwo) ->
                    let firstSixteen = numOne |> getLast16Bytes
                    let secondSixteen = numTwo |> getLast16Bytes

                    if firstSixteen = secondSixteen then
                        Some(1)
                    else
                        None)
            |> Array.sum
            |> string
