module DayFourteen

open FSharp.Core.Operators.Checked

type V1BitOp =
    | One
    | Zero
    | Noop

    static member Parse =
        function
        | '1' -> Some One
        | '0' -> Some Zero
        | 'X' -> Some Noop
        | _ -> None

    member this.Apply(c: char) =
        match this with
        | One -> '1'
        | Zero -> '0'
        | Noop -> c

type V2BitOp =
    | One
    | Zero
    | Float

    static member Parse =
        function
        | '1' -> Some One
        | '0' -> Some Zero
        | 'X' -> Some Float
        | _ -> None

    member this.Apply(c: char) =
        match this with
        | One -> [ '1' ]
        | Zero -> [ c ]
        | Float -> [ '0'; '1' ]

type Mask = V1BitOp array

let parseV1Mask (input: string) =
    input
    |> Seq.choose V1BitOp.Parse
    |> Array.ofSeq

let parseV2Mask (input: string) =
    input
    |> Seq.choose V2BitOp.Parse
    |> List.ofSeq

// Represent a 36-bit int
type WeirdInt = string

type Memory = Map<string, WeirdInt>

let applyV1Mask (input: WeirdInt) (mask: Mask) =
    Seq.zip input mask
    |> Seq.map (fun (number, bitMask) -> bitMask.Apply number)
    |> Array.ofSeq
    |> System.String


let disassembleAddress (input: string) =
    let startingIdx = input.IndexOf('[') + 1
    let endingIdx = input.IndexOf(']') - 1
    if startingIdx = endingIdx then input.[startingIdx] |> string else input.[startingIdx..endingIdx]

type Instruction =
    | UpdateMask of string
    | SetRegister of address: string * value: int64
    static member Parse(input: string) =
        let parts = input.Split('=') |> Array.map (fun s -> s.Trim())
        match parts.[0] with
        | "mask" -> Some(UpdateMask parts.[1])
        | mem when mem.[0..2] = "mem" ->
            let address = disassembleAddress mem
            let value = parts.[1] |> System.Int64.Parse
            Some(SetRegister(address, value))
        | _ -> None

let rec intToBinary i =
    match i with
    | 0L
    | 1L -> string i
    | _ ->
        let bit = string (i % 2L)
        (intToBinary (i / 2L)) + bit

let intToWeirdBinary (i: int64) =
    let binary = i |> intToBinary
    let delta = 36 - binary.Length
    let padding = String.replicate delta "0"

    padding + binary

let parseAndRaiseToPower pow toParse =
    let asNumber = System.Int64.Parse(string toParse) |> float

    asNumber * System.Math.Pow(2.0, float pow) |> int64

let weirdBinaryToInt (binary: string): int64 =
    binary
    |> Seq.rev
    |> Seq.mapi parseAndRaiseToPower
    |> Seq.reduce (+)

type PartOneState =
    { BitMask: Mask
      Memory: Memory }
    static member New() =
        { BitMask = Array.empty
          Memory = Map.empty }

type PartTwoState =
    { BitMask: V2BitOp list
      Memory: Memory }
    static member New() =
        { BitMask = List.empty
          Memory = Map.empty }

let makeNewLists (newElem: char) (bodies: string list): string list =
    match bodies with
    | [] -> [ string newElem ]
    | _ -> bodies |> List.map (fun body -> (string newElem) + body)

let rec permute (input: (V2BitOp * char) list): string list =
    match input with
    | head :: tail ->
        let op = fst head
        let c = snd head

        op.Apply c |> List.collect (fun ch -> makeNewLists ch (permute tail))
    | [] -> []

let applyV2Mask (input: WeirdInt) (mask: V2BitOp list) =
    Seq.zip mask input
    |> List.ofSeq
    |> permute

let partOneFolder (state: PartOneState) (instr: Instruction) =
    match instr with
    | UpdateMask(newMask) -> { state with BitMask = parseV1Mask newMask }
    | SetRegister(address, value) ->
        let weird = value |> intToWeirdBinary
        let masked = applyV1Mask weird state.BitMask
        let newMemory = state.Memory.Add(address, masked)
        { state with Memory = newMemory }

let partTwoFolder (state: PartTwoState) (instr: Instruction) =
    match instr with
    | UpdateMask(newMask) -> { state with BitMask = parseV2Mask newMask }
    | SetRegister(address, value) ->
        let weird =
            address
            |> int64
            |> intToWeirdBinary

        let masks = applyV2Mask weird state.BitMask
        let newMemory = List.fold (fun (mem: Memory) s -> mem.Add(s, value |> intToWeirdBinary)) state.Memory masks
        { state with Memory = newMemory }

type Solution() =
    let name = "fourteen"
    interface Common.ISolution with
        member __.LoadData(year): Common.Input option = Common.loadData name year

        member __.PartOne(input: Common.Input): string =
            let instructions = input |> Array.choose Instruction.Parse
            let init = PartOneState.New()

            let finalState = Array.fold partOneFolder init instructions

            let total = finalState.Memory |> Map.fold (fun state _ v -> state + (weirdBinaryToInt v)) 0L

            sprintf "Total left in memory is %i" total

        member __.PartTwo(input: Common.Input): string =
            let instructions = input |> Array.choose Instruction.Parse
            let init = PartTwoState.New()

            let finalState = Array.fold partTwoFolder init instructions

            let total = finalState.Memory |> Map.fold (fun state _ v -> state + (weirdBinaryToInt v)) 0L

            sprintf "Total left in memory is %i" total

        member __.Name() = name |> Common.titleCase
