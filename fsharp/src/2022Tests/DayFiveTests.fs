module DayFiveTests

open Expecto

open DayFive

let PartOneInput =
    [| "    [D]    "
       "[N] [C]    "
       "[Z] [M] [P]"
       " 1   2   3 "
       ""
       "move 1 from 2 to 1"
       "move 3 from 1 to 3"
       "move 2 from 2 to 1"
       "move 1 from 1 to 2" |]

[<Tests>]
let tests =
    testList
        "Testing stuff I dunno FSAC keeps nuking it"
        [ testCase "fuck"
          <| fun _ ->
              let inputCrates =
                  [| [ 'A'; 'D'; 'G' ]
                     [ 'B'; 'E'; 'H' ]
                     [ 'C'; 'F'; 'I' ] |]

              let inputMotion = Motion.Create 1 2 2

              let expected =
                  [| [ 'A'; 'D'; 'G' ]
                     [ 'H' ]
                     [ 'E'; 'B'; 'C'; 'F'; 'I' ] |]

              let got =
                  PartOne.followInstruction inputCrates inputMotion

              Expect.equal got expected "Fuck?"

          testCase "Testing the PartOne example"
          <| fun _ ->
              let expected = "CMZ"
              let got = PartOneInput |> partOne

              Expect.equal got expected "We should be able to re-produce the example"
          
         ]
