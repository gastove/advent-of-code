module DayEight

//--------------------------------- Part One ---------------------------------//
// Immediately before any instruction is executed a second time, what value is
// in the accumulator?

type Modifier = int -> int -> int

let parseNumber (input: string): (int * Modifier) option =
    let modSymbol = input.[0]
    let digits = input.[1..]

    let modifier =
        match modSymbol with
        | '+' -> Some (+)
        | '-' -> Some (-)
        | _ -> None

    let integer = digits |> Common.tryInt

    match modifier, integer with
    | Some(m), Some(i) -> Some(i, m)
    | _ -> None

type Instruction =
    | NOOP of integer: int * modifier: Modifier
    | Accumulate of integer: int * modifier: Modifier
    | Jump of integer: int * modifier: Modifier
    static member FromString(input: string): Instruction option =
        let pieces = input.Trim().Split(" ") |> Array.map (fun s -> s.Trim())

        match parseNumber pieces.[1] with
        | Some(integer, modifier) ->
            match pieces.[0] with
            | "nop" -> Some <| NOOP(integer, modifier)
            | "acc" -> Some <| Accumulate(integer, modifier)
            | "jmp" -> Some <| Jump(integer, modifier)
            | _ -> None
        | _ -> None

// We're going to have an easier time using match syntax if we work with lists
let drive (input: Instruction list): int * bool =
    let mutable state: Set<int> = Set.empty

    let rec compute (instructions: Instruction list) (acc: int) (idx: int): int * bool =
        // If we've seen this index before, immediately return
        if Set.contains idx state then
            acc, false
        else
            // Otherwise, record that we've seen this index before handling the instruction
            state <- state.Add(idx)
            match instructions with
            | head :: tail ->
                match head with
                // On a noop, we do nothing but move on, increment the index, keep processing the current tail
                | NOOP(_) -> compute tail acc (idx + 1)
                // On an Accumulate, we modify acc using the provided function and value, increment index, process tail
                | Accumulate(integer, modifier) -> compute tail (modifier acc integer) (idx + 1)
                // Jump is tricky. We need to compute our new index, then use it to "reset" the tail from our original input
                | Jump(integer, modifier) ->
                    let newIdx = modifier idx integer
                    let newTail = input.[newIdx..] // This will explode if we jump out the beginning of the instructions. Whee!

                    compute newTail acc newIdx

            | [] -> acc, true

    compute input 0 0

let repair (input: Instruction list): int list =
    let mutable state: int list = List.empty

    let rec findDuplicate (instructions: Instruction list) (acc: int) (idx: int): int list =
        // If we've seen this index before, immediately return the duplicate index
        if List.contains idx state then
            state
        else
            // Otherwise, record that we've seen this index before handling the instruction
            state <- idx :: state
            match instructions with
            | head :: tail ->
                match head with
                // On a noop, we do nothing but move on, increment the index, keep processing the current tail
                | NOOP(_) -> findDuplicate tail acc (idx + 1)
                // On an Accumulate, we modify acc using the provided function and value, increment index, process tail
                | Accumulate(integer, modifier) -> findDuplicate tail (modifier acc integer) (idx + 1)
                // Jump is tricky. We need to compute our new index, then use it to "reset" the tail from our original input
                | Jump(integer, modifier) ->
                    let newIdx = modifier idx integer
                    let newTail = input.[newIdx..] // This will explode if we jump out the beginning of the instructions. Whee!

                    findDuplicate newTail acc newIdx

            | [] -> state

    findDuplicate input 0 0

let swapNopJmp (inst: Instruction): Instruction =
    match inst with
    | NOOP(integer, modifier) -> Jump(integer, modifier)
    | Jump(integer, modifier) -> NOOP(integer, modifier)
    | Accumulate(integer, modifier) -> Accumulate(integer, modifier)

let trySwapAndCompute (instructions: Instruction list) (swapIdx: int) =
    let acc, worked =
        instructions
            |> List.mapi (fun idx inst ->
                          if idx = swapIdx then swapNopJmp inst else inst)
            |> drive

    if worked then Some acc else None


type Solution() =
    let name = "eight"
    interface Common.ISolution with
        member __.LoadData(year): Common.Input option = Common.loadData name year

        member __.PartOne(input: Common.Input): string =
            let instructions =
                input
                |> List.ofArray
                |> List.choose Instruction.FromString

            let finalCountdown, _ = drive instructions

            sprintf "I think the final accumulator was at: %i" finalCountdown

        member __.PartTwo(input: Common.Input): string =
            let borkedInstructions =
                input
                |> List.ofArray
                |> List.choose Instruction.FromString

            let possibleBadIndices = borkedInstructions |> repair

            let finalFinalCountdown = possibleBadIndices |> List.pick (trySwapAndCompute borkedInstructions)

            sprintf "If I'm both lucky and good, here's the proper accumulator value: %i" finalFinalCountdown

        member __.Name() = name |> Common.titleCase
