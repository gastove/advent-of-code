use regex::Regex;
// Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
// Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
// Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
// Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
// Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green

const RED_LIMIT: usize = 12;
const GREEN_LIMIT: usize = 13;
const BLUE_LIMIT: usize = 14;

fn try_extract_from(s: &str, re: Regex) -> Option<usize> {
    re.captures(s).map(|cap| {
        let m = cap.get(1).unwrap();
        m.as_str()
            .parse::<usize>()
            .expect(format!("Failed to parse as usize: {:?}", m).as_str())
    })
}

#[derive(Debug, Clone)]
struct Draw {
    pub green: Option<usize>,
    pub red: Option<usize>,
    pub blue: Option<usize>,
}

impl Draw {
    pub fn power(&self) -> usize {
        let r = self.red.unwrap_or(1);
        let g = self.green.unwrap_or(1);
        let b = self.blue.unwrap_or(1);

        r * g * b
    }

    pub fn zeroed() -> Self {
        Draw {
            green: Some(0),
            red: Some(0),
            blue: Some(0),
        }
    }
}

impl From<&str> for Draw {
    fn from(value: &str) -> Self {
        let red_re = Regex::new("(\\d+) red").expect("failed to build regex");
        let blue_re = Regex::new("(\\d+) blue").expect("failed to build regex");
        let green_re = Regex::new("(\\d+) green").expect("failed to build regex");

        Draw {
            green: try_extract_from(value, green_re),
            red: try_extract_from(value, red_re),
            blue: try_extract_from(value, blue_re),
        }
    }
}

struct Game {
    pub id: usize,
    pub draws: Vec<Draw>,
}

impl From<&String> for Game {
    fn from(value: &String) -> Self {
        let game_re = Regex::new("Game (\\d+):").expect("failed to build regex");
        let id = try_extract_from(value, game_re).unwrap();
        // Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
        let draws_re = Regex::new("^Game \\d+: (.*)$").expect("Failed to build regex");

        let raw_draws = draws_re
            .find(value)
            .expect("Failed to find any Draws")
            .as_str();

        Game {
            id,
            draws: raw_draws.split(';').map(Draw::from).collect(),
        }
    }
}

impl Game {
    pub fn possible(&self) -> bool {
        self.draws.iter().all(|d| {
            let red_passes = d
                .red
                .map(|red_count| red_count <= RED_LIMIT)
                .unwrap_or(true);
            let blue_passes = d
                .blue
                .map(|blue_count| blue_count <= BLUE_LIMIT)
                .unwrap_or(true);
            let green_passes = d
                .green
                .map(|green_count| green_count <= GREEN_LIMIT)
                .unwrap_or(true);

            red_passes && blue_passes && green_passes
        })
    }

    pub fn compute_minimum(&self) -> Draw {
        self.draws
            .iter()
            .fold(Draw::zeroed(), |min_draw, draw| Draw {
                green: std::cmp::max(min_draw.green, draw.green),
                red: std::cmp::max(min_draw.red, draw.red),
                blue: std::cmp::max(min_draw.blue, draw.blue),
            })
    }
}
// #[cfg(test)]
// mod test_try_from {

//     use super::*;

//     #[test]
//     fn test_examples() {
//         let input = vec![
//             "Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green",
//             "Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue",
//             "Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red",
//             "Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red",
//             "Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green",
//         ];

//         let expected = vec![
//             Draw{ id: 1, green: Some(4), red: Some(5), blue:Some(9) },
//             Draw{ id: 2, green: Some(6), red: Some(1), blue:Some(6) },
//             Draw{ id: 3, green: Some(26), red: Some(25), blue:Some(11) },
//             Draw{ id: 4, green: Some(7), red: Some(23), blue:Some(21) },
//             Draw{ id: 5, green: Some(5), red: Some(7), blue:Some(9) },
//         ];
//     }
// }

mod part_one {
    // The Elf would first like to know which games would have been possible if
    // the bag contained only 12 red cubes, 13 green cubes, and 14 blue cubes?

    use super::*;

    pub fn run(input: Vec<String>) -> String {
        let games: Vec<Game> = input.iter().map(|s| s.into()).collect();

        games
            .iter()
            .filter_map(|game| if game.possible() { Some(game.id) } else { None })
            .sum::<usize>()
            .to_string()
    }
}

mod part_two {

    use super::*;

    pub fn run(input: Vec<String>) -> String {
        let games: Vec<Game> = input.iter().map(|s| s.into()).collect();

        games
            .iter()
            .map(|g| g.compute_minimum().power())
            .sum::<usize>()
            .to_string()
    }
}

pub fn run(input: Vec<String>) {
    println!("=== Day Two! ===");
    println!("Part One\t=> {}", part_one::run(input.clone()));
    println!("Part Two\t=> {}", part_two::run(input));
}
