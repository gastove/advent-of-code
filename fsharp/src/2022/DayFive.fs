module DayFive

open FSharp.Core.Operators.Checked

open System.Text

open Common

let parseCrates (input: string) =
    let rec parse (working: char list) (ticker: int) (acc: (char option) list list) =
        match working with
        | _ :: tail when ticker < 3 -> parse tail (ticker + 1) acc
        | head :: tail when ticker = 3 ->
            if head = ' ' then
                parse tail 0 (List.empty :: acc)
            else
                parse tail 0 ([Some head] :: acc)
        | [] -> acc
        | bad -> failwith $"Somehow, we hit this failure case: working = {bad}, ticker = {ticker}"

    parse (input |> Seq.toList) 2 List.empty
    |> List.rev

let collectCrateColumns<'T> (rawCrates: 'T list list list) =
    rawCrates
    |> List.reduce (fun list1 list2 ->
        (list1, list2)
        ||> List.map2 (fun elem1 elem2 -> elem1 @ elem2))

let MotionRegex =
    RegularExpressions.Regex("move (?<volume>\d+) from (?<from>\d+) to (?<to>\d+)")

let tryGetGroup (regexMatch: RegularExpressions.Match) (term: string) =
    if regexMatch.Success then
        regexMatch.Groups.[term].Value |> Some
    else
        None

type Motion =
    { From: int
      To: int
      Volume: int }

    static member Create f t v = { From = f; To = t; Volume = v }

    static member FromString(s: string) =
        let groupGetter = s |> MotionRegex.Match |> tryGetGroup

        let parseInt = System.Int32.Parse
        let dec = fun i -> i - 1

        let f = "from" |> groupGetter
        let t = "to" |> groupGetter
        let v = "volume" |> groupGetter

        match (f, t, v) with
        | Some rf, Some rt, Some rv ->
            (rf |> parseInt |> dec, rt |> parseInt |> dec, rv |> parseInt)
            |||> Motion.Create
        | _, _, _ -> failwith $"Failed to parse {s} into a Motion"

// let transpose (data: char option array array) =
//     let empty: char option list array =
//         Array.init data.[0].Length (fun _ -> List.empty)

//     data
//     |> Array.fold (fun state line -> line |> List.mapi (fun idx crate -> state.[idx])) empty

let parseInput (input: Common.Input) =
    let crates =
        input
        |> Array.takeWhile (fun line -> line |> Strings.notBlank)
        |> Seqs.butLast
        |> Seq.map parseCrates
        |> Seq.toList
        |> collectCrateColumns
        |> List.map (List.choose id)
        |> List.toArray

    let toSkip = crates.Length + 1 // add back in the number line we're dropping

    let motion =
        input
        |> Array.skip toSkip
        |> Array.skipWhile Strings.blank
        |> Array.map Motion.FromString

    crates, motion

module PartOne =
    let followInstruction (crates: char list array) (instruction: Motion)=
        let takeFrom = crates.[instruction.From]
        let putInto = crates.[instruction.To]

        let toPut = takeFrom |> List.take instruction.Volume |> List.rev
        let newFrom = takeFrom |> List.skip instruction.Volume
        let newInto = toPut @ putInto

        crates.[instruction.From] <- newFrom
        crates.[instruction.To] <- newInto

        crates

module PartTwo =
    let followInstruction (crates: char list array) (instruction: Motion)=
        let takeFrom = crates.[instruction.From]
        let putInto = crates.[instruction.To]

        let toPut = takeFrom |> List.take instruction.Volume
        let newFrom = takeFrom |> List.skip instruction.Volume
        let newInto = toPut @ putInto

        crates.[instruction.From] <- newFrom
        crates.[instruction.To] <- newInto

        crates


let partOne (input: Common.Input) =
    let (crates, motion) = input |> parseInput

    motion |> Array.fold PartOne.followInstruction crates |> Array.map List.head |> System.String

let partTwo (input: Common.Input) =
    let (crates, motion) = input |> parseInput

    motion |> Array.fold PartTwo.followInstruction crates |> Array.map List.head |> System.String

type Solution() =
    let name = "five"

    interface Common.ISolution with
        member __.Name() = name |> Common.titleCase
        member __.LoadData(year) : Common.Input option = Common.loadData name year
        member __.PartOne(input: Common.Input) : string = input |> partOne
        member __.PartTwo(input: Common.Input) : string = input |> partTwo
