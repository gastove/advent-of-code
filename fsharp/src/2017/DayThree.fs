module DayThree

open FSharp.Core.Operators.Checked

let genSequenceUntilContains needle =
    let rec build acc unitsToSide =
        if (acc |> List.last |> List.contains needle) then
            acc
        else
            // Get the largest value from the previous set
            let nextStart = acc |> List.last |> List.max
            let fillRange = unitsToSide * 4

            let range =
                [ nextStart .. (nextStart + fillRange) ]
                |> List.tail

            build (acc @ [ range ]) (unitsToSide + 2)

    build [ [ 1 ] ] 2

/// Finds the chunk containing needle.
let determineChunkOfNumberInSeq (numbers: int list) needle =
    numbers
    |> List.splitInto 4
    |> List.pick
        (fun nums ->
            if (List.contains needle nums) then
                nums |> Some
            else
                None)

let computeLayerNumber numbers =
    // The layer number should be:
    // - The length of the layer
    // - Divided by 4 (to get the length of a side)
    // - Divided by 2 (to get the number of increments)
    numbers |> List.length |> (fun l -> l / 8)

let computeManhattanDistance (numbers: int list) needle =
    // let side = determineSideOfNumberInSeq numbers needle
    let layer = computeLayerNumber numbers

    let chunk =
        determineChunkOfNumberInSeq numbers needle

    // The midpoint is the last number in the first half of an evenly divided list.
    // Alternatively, it's the index one less than the length of a chunk divided by two.
    let midpoint =
        chunk
        |> List.splitInto 2
        |> List.head
        |> List.last

    let chunkSteps = (needle - midpoint) |> abs

    chunkSteps + layer

let partOne (input: int) =
    let numbers =
        genSequenceUntilContains input |> List.last

    computeManhattanDistance numbers input

let computeSpiralPoints startingX startingY maxExtent =
    // First, our max X value plus Y in the range from startingY to max positive Y
    [ for y in startingY .. 1 .. maxExtent -> maxExtent, y ]
    @
    // Then, max Y plus the descending X range
    [ for x in (startingX - 1) .. -1 .. (maxExtent * -1) -> x, maxExtent ]
    @
    // Then min X plus the descending Y range
    [ for y in (maxExtent - 1) .. -1 .. (maxExtent * -1) -> (maxExtent * -1), y ]
    @
    // Then min Y plus the ascending X range
    [ for x in (maxExtent * -1) + 1 .. 1 .. maxExtent -> x, (maxExtent * -1) ]
    // Finally, max X plus Y in the range from min Y to -1
    @ [ for y in (maxExtent * -1) + 1 .. 1 .. (startingY - 1) -> maxExtent, y ]


let getNeighbors (point: int * int) (grid: Map<int * int, int>) =
    let pointX = fst point
    let pointY = snd point

    let candidatePoints =
        [ for x in pointX - 1 .. 1 .. pointX + 1 do
              for y in pointY - 1 .. 1 .. pointY + 1 -> x, y ]

    candidatePoints
    |> List.choose (fun p -> Map.tryFind p grid)

let genSequenceUntilBigger needle =
    let initialChunk: list<(int * int) * int> = [ (0, 0), 1 ]
    let initialGrid: Map<int * int, int> = initialChunk |> Map.ofList

    let rec build lookupGrid lastPoint layer =
        // We start one X greater than the last point, same Y
        let startingX = fst lastPoint |> (+) 1
        let startingY = snd lastPoint

        let points =
            computeSpiralPoints startingX startingY layer

        let newLastPoint = points |> List.last

        let newGrid =
            points
            |> List.fold
                (fun grid point ->
                    let valForPoint = getNeighbors point grid |> List.sum
                    Map.add point valForPoint grid)
                lookupGrid

        // Once we've computed the new points, we check to see if we wrote one larger than our starting value.
        if (Map.find newLastPoint newGrid) > needle then
            // Okay, we know we're writing larger values. Let's hunt back to find the smallest larger value.
            points
            |> List.pick
                (fun point ->
                    let valAtPoint = Map.find point newGrid

                    if valAtPoint > needle then
                        Some valAtPoint
                    else
                        None)
        else
            build newGrid newLastPoint (layer + 1)

    build initialGrid (0, 0) 1

let partTwo input =
    genSequenceUntilBigger input

type Solution() =
    let name = "three"
    let input = 277678

    interface Common.ISolution with
        member __.Name() = name |> Common.titleCase
        // There's no data to load for this problem, just a single number
        member __.LoadData(_) : Common.Input option = [||] |> Some
        member __.PartOne(_input: Common.Input) : string = input |> partOne |> string
        member __.PartTwo(_input: Common.Input) : string = input |> partTwo |> string
