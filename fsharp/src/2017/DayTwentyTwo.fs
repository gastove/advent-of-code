module DayTwentyTwo

open FSharp.Core.Operators.Checked

type Turn =
    | Right
    | Left
    | Straight
    | Reverse

type Heading =
    | North
    | South
    | East
    | West
    member this.Turn(turn: Turn) =
        match this, turn with
        | North, Right -> East
        | North, Left -> West
        | North, Straight -> North
        | North, Reverse -> South
        | East, Right -> South
        | East, Left -> North
        | East, Straight -> East
        | East, Reverse -> West
        | South, Right -> West
        | South, Left -> East
        | South, Straight -> South
        | South, Reverse -> North
        | West, Right -> North
        | West, Left -> South
        | West, Straight -> West
        | West, Reverse -> East

    member this.Inverse =
        match this with
        | North -> South
        | South -> North
        | East -> West
        | West -> East

type Point =
    { X: int
      Y: int }
    static member Create x y = { X = x; Y = y }

    static member Origin = { X = 0; Y = 0 }
    // override this.ToString() = sprintf $"<{this.X}, {this.Y}>"
    override this.ToString() = sprintf "<%i, %i>" this.X this.Y

type State =
    | Clean
    | Weakened
    | Infected
    | Flagged
    static member FromChar(c: char) =
        match c with
        // We use the same initial state; weakened and flagged aren't represented
        | '.' -> Clean
        | '#' -> Infected
        | bad -> failwithf "Can't parse %c to a State" bad

    override this.ToString() =
        match this with
        | Clean -> "."
        | Infected -> "#"
        | Weakened -> "w"
        | Flagged -> "f"

type Grid =
    { mutable Internal: Map<Point, State> }
    static member Create intrnl = { Internal = intrnl }

    static member Initialize(input: Common.Input) =
        let yFactor = (input.Length - 1) / 2
        let xFaxtor = (input.[0].Length - 1) / 2

        input
        |> Array.map Seq.toArray
        |> Array.rev
        |> Array.mapi
            (fun yidx row ->
                row
                |> Array.mapi
                    (fun xidx c ->
                        { X = xidx - xFaxtor
                          Y = yidx - yFactor },
                        State.FromChar c))
        |> Array.concat
        |> Map.ofArray
        |> Grid.Create

    member this.Get(point: Point) =
        this.Internal
        |> Map.tryFind point
        |> Option.defaultValue Clean

    member this.Update (point: Point) (state: State) =
        this.Internal <- Map.add point state this.Internal

type Carrier =
    { Location: Point
      Heading: Heading }
    static member Create loc head = { Location = loc; Heading = head }

    static member Initial =
        { Location = Point.Origin
          Heading = North }

    member this.Rotate direction =
        { this with
              Heading = this.Heading.Turn direction }

    member this.Move() = { this with Location = this.NextPoint }

    member this.NextPoint =
        match this.Heading with
        | North ->
            { this.Location with
                  Y = this.Location.Y + 1 }
        | South ->
            { this.Location with
                  Y = this.Location.Y - 1 }
        | East ->
            { this.Location with
                  X = this.Location.X + 1 }
        | West ->
            { this.Location with
                  X = this.Location.X - 1 }

let doPartOneTicks ticks (grid: Grid) =
    let rec tick (carrier: Carrier) (remaining: int) (causedInfection: int) =
        if remaining = 0 then
            causedInfection
        else
            let pointCurrentState = grid.Get carrier.Location

            let turnToThe, pointUpdatedState, infections =
                match pointCurrentState with
                | Infected -> Right, Clean, 0
                | Clean -> Left, Infected, 1
                | bad -> failwithf "We hit %A, which should be impossible in part one" bad

            let turnt = carrier.Rotate turnToThe

            grid.Update carrier.Location pointUpdatedState

            tick (turnt.Move()) (remaining - 1) (causedInfection + infections)

    tick Carrier.Initial ticks 0

let doPartTwoTicks ticks (grid: Grid) =
    let rec tick (carrier: Carrier) (remaining: int) (causedInfection: int) =
        if remaining = 0 then
            causedInfection
        else
            let pointCurrentState = grid.Get carrier.Location

            let turnToThe, pointUpdatedState, infections =
                match pointCurrentState with
                | Infected -> Right, Flagged, 0
                | Clean -> Left, Weakened, 0
                | Weakened -> Straight, Infected, 1
                | Flagged -> Reverse, Clean, 0

            let turnt = carrier.Rotate turnToThe

            grid.Update carrier.Location pointUpdatedState

            tick (turnt.Move()) (remaining - 1) (causedInfection + infections)

    tick Carrier.Initial ticks 0

let partOne (input: Common.Input) =
    input |> Grid.Initialize |> doPartOneTicks 10_000

let partTwo (input: Common.Input) =
    input |> Grid.Initialize |> doPartTwoTicks 10_000_000

type Solution() =
    let name = "twenty_two"

    interface Common.ISolution with
        member __.Name() = name |> Common.titleCase
        member __.LoadData(year) : Common.Input option = Common.loadData name year
        member __.PartOne(input: Common.Input) : string = input |> partOne |> string
        member __.PartTwo(input: Common.Input) : string = input |> partTwo |> string
