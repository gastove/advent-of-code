module DayEight

open FSharp.Core.Operators.Checked

let allLower checkAgainst toCheck =
    toCheck |> Array.forall ((>) checkAgainst)

let visibleLeft (row: int array) (elemIdx: int) =
    let check = row |> Array.item elemIdx |> allLower
    let slice = row.[0..max (elemIdx - 1) 0]

    slice |> check

let visibleRight (row: int array) (elemIdx: int) =
    let check = row |> Array.item elemIdx |> allLower
    let rowMax = row.Length - 1
    row.[min (elemIdx + 1) rowMax..rowMax] |> check

let isVisible (trees: int array array) (baseHeight: int) (row: int) (col: int) =
    let theRow = trees |> Array.item row
    let theCol = trees |> Array.map (Array.item col)

    let horizontal () =
        visibleLeft theRow col || visibleRight theRow col

    let vertical () =
        visibleLeft theCol row || visibleRight theCol row

    let visible =
        row = 0
        || col = 0
        || row = theRow.Length - 1
        || col = theCol.Length - 1
        || horizontal ()
        || vertical ()

    visible

let parseLine (input: Common.Input) =
    input
    |> Array.map (fun row ->
        row
        |> Seq.map (string >> System.Int32.Parse)
        |> Seq.toArray)

let scenicScore (against: int) (trees: int list) =
    let rec work remainingTrees currentScore =
        match remainingTrees with
        | tree :: remaining ->
            if against > tree then
                work remaining (currentScore + 1)
            else
                currentScore + 1
        | [] -> currentScore

    work trees 0

let distanceToLargerLeft (trees: int array) (idx: int) =
    if idx = 0 then
        0
    else
        let toCheck =
            trees.[0..max (idx - 1) 0]
            |> Seq.rev
            |> Seq.toList

        let against = trees.[idx]

        toCheck |> scenicScore against

let distanceToLargerRight (trees: int array) (idx: int) =
    let maxIdx = trees.Length - 1

    if idx = maxIdx then
        0
    else
        let toCheck =
            trees.[min (idx + 1) maxIdx..maxIdx]
            |> List.ofArray

        let against = trees.[idx]

        toCheck |> scenicScore against

let computeScenicScore (trees: int array array) (row: int) (col: int) =
    let rowOfTrees = trees |> Array.item row
    let colOfTrees = trees |> Array.map (Array.item col)

    let left = distanceToLargerLeft rowOfTrees col
    let right = distanceToLargerRight rowOfTrees col
    let up = distanceToLargerLeft colOfTrees row
    let down = distanceToLargerRight colOfTrees row

    left * right * up * down

let partOne (input: Common.Input) =
    let trees = input |> parseLine

    trees
    |> Array.mapi (fun rowIdx row ->
        row
        |> Array.mapi (fun colIdx tree -> isVisible trees tree rowIdx colIdx)
        |> Array.filter id
        |> Array.length)
    |> Array.sum
    |> string

let partTwo (input: Common.Input) =
    let trees = input |> parseLine

    trees
    |> Array.mapi (fun rowIdx row ->
        row
        |> Array.mapi (fun colIdx _ -> computeScenicScore trees rowIdx colIdx))
    |> Array.map Array.max
    |> Array.max
    |> string

type Solution() =
    let name = "eight"

    interface Common.ISolution with
        member __.Name() = name |> Common.titleCase
        member __.LoadData(year) : Common.Input option = Common.loadData name year
        member __.PartOne(input: Common.Input) : string = input |> partOne
        member __.PartTwo(input: Common.Input) : string = input |> partTwo
