module DaySeventeenTests

open Expecto

open DaySeventeen
open Shared
open PartOne

[<Tests>]
let testTheBasics =
    testList "We should be able to work with points in 3D cartesian space"
        [ testCase "Can I update state for an active node with 3 active neighbors?" <| fun _ ->
            let stuntPoint =
                { X = 1L
                  Y = 1L
                  Z = 1L }
            let threeActiveArray =
                [| stuntPoint, Active
                   { X = 0L
                     Y = 0L
                     Z = 0L }, Active
                   { X = 0L
                     Y = 1L
                     Z = 0L }, Active
                   { X = 0L
                     Y = 2L
                     Z = 0L }, Active |]

            let active = threeActiveArray |> Map.ofArray

            let expected = Active
            let got = updateState stuntPoint Active active

            Expect.equal got expected "The state should not change"

          testCase "Can I update state for an active node with 2 active neighbors?" <| fun _ ->
              let stuntPoint =
                  { X = 1L
                    Y = 1L
                    Z = 1L }
              let twoActiveArray =
                  [| stuntPoint, Active
                     { X = 0L
                       Y = 0L
                       Z = 0L }, Active
                     { X = 0L
                       Y = 1L
                       Z = 0L }, Active |]

              let active = twoActiveArray |> Map.ofArray

              let expected = Active
              let got = updateState stuntPoint Active active

              Expect.equal got expected "The state should not change"

          testCase "Can I update state for an inactive node with 3 active neighbors?" <| fun _ ->
              let stuntPoint =
                  { X = 1L
                    Y = 1L
                    Z = 1L }
              let threeActiveArray =
                  [| stuntPoint, Inactive
                     { X = 0L
                       Y = 0L
                       Z = 0L }, Active
                     { X = 0L
                       Y = 1L
                       Z = 0L }, Active
                     { X = 0L
                       Y = 2L
                       Z = 0L }, Active |]

              let active = threeActiveArray |> Map.ofArray

              let expected = Active
              let got = updateState stuntPoint Inactive active

              Expect.equal got expected "The state should change"

          testCase "An active node with too many neighbors should update" <| fun _ ->
              let stuntPoint =
                  { X = 1L
                    Y = 1L
                    Z = 1L }
              let fourActiveArray =
                  [| stuntPoint, Active
                     { X = 0L
                       Y = 0L
                       Z = 0L }, Active
                     { X = 0L
                       Y = 1L
                       Z = 0L }, Active
                     { X = 0L
                       Y = 2L
                       Z = 0L }, Active
                     { X = 1L
                       Y = 2L
                       Z = 1L }, Active |]

              let active = fourActiveArray |> Map.ofArray

              let expected = Inactive
              let got = updateState stuntPoint Active active

              Expect.equal got expected "The state should change"

          testCase "An active node with too few neighbors should update" <| fun _ ->
              let stuntPoint =
                  { X = 1L
                    Y = 1L
                    Z = 1L }
              let fourActiveArray =
                  [| stuntPoint, Active
                     { X = 1L
                       Y = 2L
                       Z = 1L }, Active |]

              let active = fourActiveArray |> Map.ofArray

              let expected = Inactive
              let got = updateState stuntPoint Active active

              Expect.equal got expected "The state should change"

          testCase "Can we correctly find the neighbors of a point?" <| fun _ ->
              let stuntPoint =
                  { X = 1L
                    Y = 1L
                    Z = 1L }
              let expected =
                  [| { X = 0L
                       Y = 0L
                       Z = 0L }
                     { X = 0L
                       Y = 1L
                       Z = 0L }
                     { X = 0L
                       Y = 2L
                       Z = 0L }
                     { X = 1L
                       Y = 0L
                       Z = 0L }
                     { X = 1L
                       Y = 1L
                       Z = 0L }
                     { X = 1L
                       Y = 2L
                       Z = 0L }
                     { X = 2L
                       Y = 0L
                       Z = 0L }
                     { X = 2L
                       Y = 1L
                       Z = 0L }
                     { X = 2L
                       Y = 2L
                       Z = 0L }
                     { X = 0L
                       Y = 0L
                       Z = 1L }
                     { X = 0L
                       Y = 1L
                       Z = 1L }
                     { X = 0L
                       Y = 2L
                       Z = 1L }
                     { X = 1L
                       Y = 0L
                       Z = 1L }
                     { X = 1L
                       Y = 2L
                       Z = 1L }
                     { X = 2L
                       Y = 0L
                       Z = 1L }
                     { X = 2L
                       Y = 1L
                       Z = 1L }
                     { X = 2L
                       Y = 2L
                       Z = 1L }
                     { X = 0L
                       Y = 0L
                       Z = 2L }
                     { X = 0L
                       Y = 1L
                       Z = 2L }
                     { X = 0L
                       Y = 2L
                       Z = 2L }
                     { X = 1L
                       Y = 0L
                       Z = 2L }
                     { X = 1L
                       Y = 1L
                       Z = 2L }
                     { X = 1L
                       Y = 2L
                       Z = 2L }
                     { X = 2L
                       Y = 0L
                       Z = 2L }
                     { X = 2L
                       Y = 1L
                       Z = 2L }
                     { X = 2L
                       Y = 2L
                       Z = 2L } |]

              let got = (stuntPoint :> IPoint<Point>).ComputeNeighbors()

              Expect.equal got expected "These should match" ]
