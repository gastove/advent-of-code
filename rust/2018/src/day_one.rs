use std::{
    collections::HashSet,
    fs::File,
    io::{BufRead, BufReader}
};

fn part_one(ints: &Vec<i32>) {
    let res = ints.iter().fold(0, |acc, i| acc + i);

    println!("Calculated part one answer as: {}", res);
}

fn find_repeated_freq(ints: Vec<i32>) -> i32 {
    let mut seen: HashSet<i32> = HashSet::new();
    let mut freq: i32 = 0;

    seen.insert(0);

    for i in ints.iter().cycle() {
        freq = freq + i;

        if seen.contains(&freq) {
            break;
        } else {
            seen.insert(freq);
        }
    }

    return freq;
}

fn part_two(ints: Vec<i32>) {
    let found = find_repeated_freq(ints);
    println!("Found repeated frequency: {}", found);
}

pub fn run() {
    let file = File::open("resources/day_one_input.txt").expect("Failed to open input file :(");
    let buf = BufReader::new(file);

    let ints: Vec<i32> = buf
        .lines()
        .map(|line| line.expect("Failed to read line"))
        .map(|string| string.parse::<i32>().expect("Failed to parse integer"))
        .collect();

    part_one(&ints);
    part_two(ints)
}

#[cfg(test)]
mod day_one_tests {

    use super::*;

    #[test]
    fn test_find_repeated_freq() {
        assert_eq!(find_repeated_freq(vec![1, -1]), 0);
        assert_eq!(find_repeated_freq(vec![3, 3, 4, -2, -4]), 10);
        assert_eq!(find_repeated_freq(vec![-6, 3, 8, 5, -6]), 5);
        assert_eq!(find_repeated_freq(vec![7, 7, -2, -7, -4]), 14);
    }
}
