module DayTwentyFour

open FSharp.Core.Operators.Checked

type Pipe =
    { OneEnd: int
      OtherEnd: int }

    override this.ToString() =
        // sprintf $"Pipe<{this.OneEnd},{this.OtherEnd}>"
        sprintf "Pipe<%i, %i>" this.OneEnd this.OtherEnd

    static member FromString(s: string) =
        let parts =
            s.Split("/")
            |> Array.map (fun s -> s.Trim() |> int)

        { OneEnd = parts.[0]
          OtherEnd = parts.[1] }

    member this.Sum = this.OneEnd + this.OtherEnd

let rec align num (pipes: Pipe list) : int list list =
    if pipes.IsEmpty then
        [ [ 0 ] ]
    else
        let candidates =
            pipes
            |> List.filter (fun pipe -> pipe.OneEnd = num || pipe.OtherEnd = num)

        if candidates.IsEmpty then
            [ [ 0 ] ]
        else
            candidates
            |> List.collect
                (fun pipe ->
                    // Exclude the current pipe
                    let remaining =
                        pipes |> List.filter (fun p -> p <> pipe)

                    if remaining.IsEmpty then
                        [ [ pipe.Sum ] ]
                    else
                        let next =
                            if pipe.OneEnd = num then
                                pipe.OtherEnd
                            else
                                pipe.OneEnd

                        align next remaining
                        |> List.map (fun l -> pipe.Sum :: l))

let partOne (input: Common.Input) =
    let pipes =
        input |> Array.map Pipe.FromString |> List.ofArray

    align 0 pipes |> List.map List.sum |> List.max

let partTwo (input: Common.Input) =
    let pipes =
        input |> Array.map Pipe.FromString |> List.ofArray

    let bridges = align 0 pipes

    let maxLen =
        bridges |> List.maxBy List.length |> List.length

    bridges
    |> List.filter (fun l -> l.Length = maxLen)
    |> List.maxBy List.sum
    |> List.sum

type Solution() =
    let name = "twenty_four"

    interface Common.ISolution with
        member __.Name() = name |> Common.titleCase
        member __.LoadData(year) : Common.Input option = Common.loadData name year
        member __.PartOne(input: Common.Input) : string = input |> partOne |> string
        member __.PartTwo(input: Common.Input) : string = input |> partTwo |> string
