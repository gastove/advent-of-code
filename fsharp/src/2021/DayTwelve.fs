module DayTwelve

open FSharp.Core.Operators.Checked

open Common.Graph

let StartingNode = "start"
let EndingNode = "end"

let lineToEdge (line: string) =
    let parts = line.Split('-')

    { FirstNode = parts.[0]
      SecondNode = parts.[1]
      Weight = 0
      Direction = Bidirectional }

let parseInput (input: Common.Input) =
    let edges = input |> Array.map lineToEdge

    let nodes =
        edges
        |> Array.fold
            (fun allNodes node ->
                allNodes
                |> Set.add node.FirstNode
                |> Set.add node.SecondNode)
            Set.empty
        |> Set.toArray

    Graph<string>.Create nodes edges

type TunnelMap = Graph<string>

let (|BigCave|SmallCave|) (caveName: string) =
    if caveName = caveName.ToUpper() then
        BigCave
    else if caveName = caveName.ToLower() then
        SmallCave
    else
        failwithf "I don't know what to do with %s" caveName

let walkTheTunnels (map: TunnelMap) =
    let rec wander (currentNode: string) (path: string list) =
        if currentNode = EndingNode then
            1
        else
            let canTravelTo =
                map.Neighbors currentNode
                |> Seq.map (Edge.getOtherNode currentNode)
                |> Seq.filter (fun node ->
                    match node with
                    | BigCave -> true
                    | SmallCave -> List.contains node path |> not)

            canTravelTo
            |> Seq.sumBy (fun node -> wander node (currentNode :: path))

    wander StartingNode List.empty

let containsTwice (haystack: string list) (needle: string) =
    haystack
    |> List.filter (fun s -> s = needle)
    |> List.length = 2

let walkTheTunnelALot (map: TunnelMap) =
    let rec wander (currentNode: string) (path: string list) (useTwice: string option) =
        if currentNode = EndingNode then
            1
        else
            let allNeighbors =
                map.Neighbors currentNode
                |> Seq.map (Edge.getOtherNode currentNode)

            let bigCaves =
                allNeighbors
                |> Seq.choose (fun node ->
                    match node with
                    | BigCave -> node |> Some
                    | SmallCave -> None)

            let smallCaves =
                allNeighbors
                |> Seq.filter (fun node ->
                    let notStartingNode = node <> StartingNode
                    let notBigCave = Seq.contains node bigCaves |> not

                    let isDouble =
                        match useTwice with
                        | None -> false
                        | Some double ->
                            let currentIsDouble = node = double
                            let notAlreadyDoubled = containsTwice path node |> not

                            currentIsDouble && notAlreadyDoubled

                    let isUnused = List.contains node path |> not

                    notStartingNode
                    && notBigCave
                    && (isDouble || isUnused))

            smallCaves
            |> Seq.sumBy (fun cave ->
                let smallCavesWithoutCurrent =
                    smallCaves |> Seq.filter (fun c -> c <> cave)

                let allCaves =
                    seq {
                        bigCaves
                        smallCavesWithoutCurrent
                    }
                    |> Seq.concat

                match useTwice with
                | Some _ ->
                    allCaves
                    |> Seq.sumBy (fun node -> wander node (currentNode :: path) useTwice)
                | None ->
                    allCaves
                    |> Seq.sumBy (fun node -> wander node (currentNode :: path) (cave |> Some)))

    wander StartingNode List.empty None


let partOne (input: Common.Input) =
    input |> parseInput |> walkTheTunnels |> string

let partTwo (input: Common.Input) =
    input |> parseInput |> walkTheTunnelALot |> string

type Solution() =
    let name = "twelve"

    interface Common.ISolution with
        member __.Name() = name |> Common.titleCase
        member __.LoadData(year) : Common.Input option = Common.loadData name year
        member __.PartOne(input: Common.Input) : string = input |> partOne
        member __.PartTwo(input: Common.Input) : string = input |> partTwo
