module DayTwentyOne

open FSharp.Core.Operators.Checked

type FlipDirection =
    | Horizontal
    | Vertical
    static member All = [| Horizontal; Vertical |]

type Pattern =
    { Data: char array array }
    static member Create data = { Data = data }

    static member Default =
        { Data = [| ".#."; "..#"; "###" |] |> Array.map Seq.toArray }

    static member FromString(s: string) =
        s.Split([| '/' |])
        |> Array.map Seq.toArray
        |> Pattern.Create

    override this.ToString() =
        this.Data
        |> Array.map System.String
        |> String.concat ("/")

    member this.Rotate() =

        if this.Size % 2 = 0 then
            let asList =
                [|this.Data.[0]; this.Data.[1] |> Array.rev |] |> Array.concat |> List.ofArray

            let last = asList |> List.last
            let init = asList |> Common.Lists.butLast

            let parts =
                last :: init
                |> List.toArray
                |> Array.chunkBySize this.Size

            [|parts.[0]; parts.[1] |> Array.rev |]|> Pattern.Create

        else
            let newTopRow =
                this.Data |> Array.map Array.head |> Array.rev

            let newMiddleRow =
                this.Data |> Array.map (Array.item 1) |> Array.rev

            let newBottomRow =
                this.Data |> Array.map Array.last |> Array.rev

            [| newTopRow
               newMiddleRow
               newBottomRow |]
            |> Pattern.Create

    member this.Flip(direction: FlipDirection) =
        match direction with
        | Horizontal -> this.Data |> Array.map Array.rev |> Pattern.Create
        | Vertical -> this.Data |> Array.rev |> Pattern.Create

    member this.Size = this.Data.Length

    member this.Permutations =
        let rotatedOnce = this.Rotate()
        let rotatedTwice = rotatedOnce.Rotate()
        let rotatedThrice = rotatedTwice.Rotate()

        let thisFlipped = this.Flip Horizontal
        let flippedRotatedOnce = thisFlipped.Rotate()
        let flippedRotatedTwice = flippedRotatedOnce.Rotate()
        let flippedRotatedThrice = flippedRotatedTwice.Rotate()

        [| this
           rotatedOnce
           rotatedTwice
           rotatedThrice
           thisFlipped
           flippedRotatedOnce
           flippedRotatedTwice
           flippedRotatedThrice |]

    member this.Subdivide() =
        let subdivisionFactor =
            match this.Size with
            | size when size % 2 = 0 -> 2
            | size when size % 3 = 0 -> 3
            | size -> failwithf "%i is neither divisible by 2 nor 3, ugh" size

        this.Data
        |> Array.chunkBySize subdivisionFactor
        |> Array.fold
            (fun (acc: Pattern array list) rows ->
                let partitionedRows =
                    rows
                    |> Array.map (Array.chunkBySize subdivisionFactor)

                let patterns =
                    if subdivisionFactor = 2 then
                        Array.zip partitionedRows.[0] partitionedRows.[1]
                        |> Array.map (fun (r1, r2) -> [| r1; r2 |] |> Pattern.Create)
                    else
                        Array.zip3 partitionedRows.[0] partitionedRows.[1] partitionedRows.[2]
                        |> Array.map (fun (r1, r2, r3) -> [| r1; r2; r3 |] |> Pattern.Create)

                patterns :: acc)
            []
        |> List.rev
        |> List.toArray

module Pattern =
    let private unify (patterns: Pattern array) =
        let head = patterns |> Array.head

        head.Data
        |> Array.mapi
            (fun idx chunk ->
                let rest =
                    patterns
                    |> Array.tail
                    |> Array.map (fun p -> p.Data |> Array.item idx)

                Array.append [| chunk |] rest |> Array.concat)


    let combine (patterns: Pattern array array) =
        patterns |> Array.collect unify |> Pattern.Create


let stringToPatternTuple (s: string) =
    let parts = s.Split(" => ")

    match parts with
    | [| partOne; partTwo |] -> partOne |> Pattern.FromString, partTwo |> Pattern.FromString
    | bad -> failwithf "We can't build a pattern tuple from %A" bad

let buildTransformationMap (input: Common.Input) =
    input
    |> Array.map stringToPatternTuple
    |> Array.collect
        (fun (key, transform) ->
            key.Permutations
            |> Array.map (fun p -> p, transform))
    |> Map.ofArray

let processImage (iterations: int) (transformations: Map<Pattern, Pattern>) =
    let rec ``process`` (img: Pattern) (remaining: int) =
        if remaining = 0 then
            img
        else
            let processed =
                img.Subdivide()
                |> Array.Parallel.map
                    (fun row ->
                        row
                        |> Array.map
                            (fun pattern -> Map.find pattern transformations))
                |> Pattern.combine

            ``process`` processed (remaining - 1)

    ``process`` Pattern.Default iterations

let partOne (input: Common.Input) =
    let transformations = input |> buildTransformationMap

    let final = processImage 5 transformations

    final.Data
    |> Array.concat
    |> Array.sumBy (fun elem -> if elem = '#' then 1 else 0)

let partTwo (input: Common.Input) =
    let transformations = input |> buildTransformationMap

    let final = processImage 18 transformations

    final.Data
    |> Array.concat
    |> Array.sumBy (fun elem -> if elem = '#' then 1 else 0)


type Solution() =
    let name = "twenty_one"

    interface Common.ISolution with
        member __.Name() = name |> Common.titleCase
        member __.LoadData(year) : Common.Input option = Common.loadData name year
        member __.PartOne(input: Common.Input) : string = input |> partOne |> string
        member __.PartTwo(input: Common.Input) : string = input |> partTwo |> string
