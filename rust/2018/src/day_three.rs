use std::cmp::{max, min};
use std::collections::HashSet;
use std::iter::FromIterator;

use im::HashMap;

use regex::Regex;

use crate::util;

lazy_static! {
    static ref RECT_RE: Regex = Regex::new(r"^#(\d+) @ (\d+,\d+): (\d+x\d+)$").unwrap();
}

#[derive(Debug, PartialEq)]
struct Offset {
    x_offset: i32,
    y_offset: i32,
}

#[derive(Debug, PartialEq)]
struct RectSpec {
    y_dimension: i32,
    x_dimension: i32,
}

#[derive(Debug, Eq, PartialEq, Hash, PartialOrd, Ord, Clone)]
struct Point {
    x: i32,
    y: i32,
}

#[derive(Debug, Eq, Hash, PartialEq, Clone)]
struct Rectangle {
    id: i32,
    spec: String,
    ulc: Point,
    urc: Point,
    llc: Point,
    lrc: Point,
}

impl Rectangle {
    // Spec is like: #123 @ 3,2: 5x4
    pub fn new_from_spec(spec: String) -> Option<Rectangle> {
        let re_result = RECT_RE.captures(&spec);
        match re_result {
            Some(res) => {
                let id_str = &res[1];
                let offset_str = &res[2];
                let width_height_str = &res[3];

                let offset = Self::parse_offset(offset_str);
                let rect_spec = Self::parse_width_height(width_height_str);

                // Remember: width is on Y, height is on X
                let lower_left = Point {
                    x: offset.x_offset,
                    y: offset.y_offset,
                };

                let lower_right = Point {
                    x: offset.x_offset + rect_spec.x_dimension,
                    y: offset.y_offset,
                };

                let upper_left = Point {
                    x: offset.x_offset,
                    y: offset.y_offset + rect_spec.y_dimension,
                };

                let upper_right = Point {
                    x: offset.x_offset + rect_spec.x_dimension,
                    y: offset.y_offset + rect_spec.y_dimension,
                };

                Some(Rectangle {
                    id: Self::parse_id(id_str),
                    spec,
                    ulc: upper_left,
                    urc: upper_right,
                    llc: lower_left,
                    lrc: lower_right,
                })
            }
            None => None,
        }
    }

    fn parse_id(id_str: &str) -> i32 {
        let id: i32 = id_str.parse().expect("Failed to parse ID");
        id
    }

    fn parse_offset(offset_str: &str) -> Offset {
        let pieces: Vec<&str> = offset_str.split(',').collect();

        let y: i32 = pieces[0].parse().expect("Failed to parse x offset");
        let x: i32 = pieces[1].parse().expect("Failed to parse y offset");

        return Offset {
            x_offset: x,
            y_offset: y,
        };
    }

    fn parse_width_height(width_height_str: &str) -> RectSpec {
        let pieces: Vec<&str> = width_height_str.split('x').collect();

        let y_dimension = pieces[0].parse().expect("Failed to parse width");
        let x_dimension = pieces[1].parse().expect("Failed to parse height");

        return RectSpec {
            y_dimension,
            x_dimension,
        };
    }
}

// A rectangle without an ID
#[derive(Debug, PartialEq)]
struct Overlap {
    ulc: Point,
    urc: Point,
    llc: Point,
    lrc: Point,
}

// A rectangle 1 unit on each side
#[derive(Debug, Eq, PartialEq, Hash)]
struct Pixel {
    x: i32,
    y: i32,
}

impl Overlap {
    fn to_pixels(&self) -> Vec<Pixel> {
        let mut pixels: Vec<Pixel> = Vec::new();

        for x in self.llc.x..self.lrc.x {
            for y in self.llc.y..self.ulc.y {
                let pixel = Pixel { x, y };
                pixels.push(pixel);
            }
        }

        return pixels;
    }
}

fn calc_overlap(rect1: &Rectangle, rect2: &Rectangle) -> Option<Overlap> {
    // The X delta will be min of right corners minus max of left corners
    // the Y delta will be min of top corners minus max of bottom corners

    if rect1.id == rect2.id {
        return None;
    }

    let right_x = min(rect1.urc.x, rect2.urc.x);
    let left_x = max(rect1.ulc.x, rect2.ulc.x);

    let top_y = min(rect1.ulc.y, rect2.ulc.y);
    let bottom_y = max(rect1.llc.y, rect2.llc.y);

    if left_x > right_x || bottom_y > top_y {
        return None;
    } else {
        Some(Overlap {
            ulc: Point {
                x: left_x,
                y: top_y,
            },
            urc: Point {
                x: right_x,
                y: top_y,
            },
            lrc: Point {
                x: right_x,
                y: bottom_y,
            },
            llc: Point {
                x: left_x,
                y: bottom_y,
            },
        })
    }
}

fn generate_pairs(rectangles: Vec<Rectangle>) -> Vec<(Rectangle, Rectangle)> {
    rectangles
        .iter()
        .flat_map(|r| {
            rectangles
                .iter()
                .map(move |other_r| (r.clone(), other_r.clone()))
        })
        .collect()
}

fn part_one(rect_pairs: &Vec<(Rectangle, Rectangle)>) -> usize {
    let overlaps: Vec<Overlap> = rect_pairs
        .iter()
        .filter_map(|(rect1, rect2)| calc_overlap(rect1, rect2))
        .collect();

    let pixels: Vec<Pixel> = overlaps
        .iter()
        .flat_map(|overlap| overlap.to_pixels())
        .collect();

    let set: HashSet<Pixel> = HashSet::from_iter(pixels);

    return set.len();
}

fn hm_update(old_val: i32, new_val: i32) -> i32 {
    old_val + new_val
}

fn part_two(rect_pairs: &Vec<(Rectangle, Rectangle)>) -> i32 {
    let freqs = rect_pairs
        .iter()
        .flat_map(|(rect1, rect2)| match calc_overlap(rect1, rect2) {
            None => vec![rect1, rect2],
            Some(_) => vec![],
        })
        .cloned()
        .fold(HashMap::new(), |f, rect| {
            f.update_with(rect.id, 1, hm_update)
            // if f.contains_key(&rect.id) {
            //     let v = f.get(&rect.id).unwrap();
            //     f.insert(rect.id, v + 1);
            //     f
            // } else {
            //     f.insert(rect.id, 1);
            //     f
            // }
        });

    let mut highest_id = 0;
    let mut highest_count = 0;
    let mut val_sum = 0;

    for (k, v) in freqs {
        val_sum += v;
        if v > highest_count {
            highest_id = k;
            highest_count = v
        }
    }

    println!("Looked at {} instances", val_sum);
    return highest_id;
}

pub fn run() {
    let lines = util::read_input("resources/day_three_input.txt");
    let rectangles: Vec<Rectangle> = lines
        .iter()
        .map(|claim| Rectangle::new_from_spec(claim.to_string()).unwrap())
        .collect();

    let rect_pairs = generate_pairs(rectangles);

    let overlaps = part_one(&rect_pairs);
    let suit_id = part_two(&rect_pairs);

    println!("Number of square inches of overlap: {}", overlaps);
    println!("The non-overlapping claim has ID: {}", suit_id);
}

#[cfg(test)]
mod day_three_tests {
    use super::*;

    #[test]
    fn test_parse_offset() {
        let offset_str = "5,6";

        let expected = Offset {
            x_offset: 6,
            y_offset: 5,
        };

        let got = Rectangle::parse_offset(offset_str);

        assert_eq!(expected, got);
    }

    #[test]
    fn test_parse_width_height() {
        let width_height_str = "9x4";

        let expected = RectSpec {
            y_dimension: 9,
            x_dimension: 4,
        };

        let got = Rectangle::parse_width_height(width_height_str);

        assert_eq!(expected, got);
    }

    #[test]
    fn test_parse_rectangle_from_spec() {
        let spec = "#123 @ 3,2: 5x4".to_string();

        let expected = Rectangle {
            id: 123,
            spec: spec.clone(),
            ulc: Point { y: 8, x: 2 },
            llc: Point { y: 3, x: 2 },
            urc: Point { y: 8, x: 6 },
            lrc: Point { y: 3, x: 6 },
        };

        let got = Rectangle::new_from_spec(spec).expect("Failed to parse rectangle from spec");

        assert_eq!(expected, got);
    }

    #[test]
    fn test_calc_overlap_internal_overlap() {
        // A rectangle with lower-left at 0,0 and upper-right at 2,2
        // And a rectangle with lower-left at 0,0 and upper-right at 1, 1
        // Will have an overlap
        let spec_one = String::from("#0 @ 0,0: 2x2");
        let spec_two = String::from("#1 @ 0,0: 1x1");

        let rect1 = Rectangle::new_from_spec(spec_one).unwrap();
        let rect2 = Rectangle::new_from_spec(spec_two).unwrap();

        let expected = Overlap {
            llc: Point { x: 0, y: 0 },
            urc: Point { x: 1, y: 1 },
            ulc: Point { x: 0, y: 1 },
            lrc: Point { x: 1, y: 0 },
        };

        let got = calc_overlap(&rect1, &rect2);

        assert_eq!(Some(expected), got);
    }

    #[test]
    fn test_calc_overlap_no_overlap() {
        let spec_one = String::from("#0 @ 0,0: 2x2");
        let spec_two = String::from("#1 @ 4,4: 1x1");

        let rect1 = Rectangle::new_from_spec(spec_one).unwrap();
        let rect2 = Rectangle::new_from_spec(spec_two).unwrap();

        let got = calc_overlap(&rect1, &rect2);

        assert_eq!(None, got);
    }

    #[test]
    fn test_calc_overlap_corner_overlap() {
        // A rectangle with lower-left at 0,0 and upper-right at 2,2
        // And a rectangle with lower-left at 0,0 and upper-right at 1, 1
        // Will have an overlap
        let spec_one = String::from("#0 @ 0,0: 2x2");
        let spec_two = String::from("#1 @ 1,1: 2x2");

        let rect1 = Rectangle::new_from_spec(spec_one).unwrap();
        let rect2 = Rectangle::new_from_spec(spec_two).unwrap();

        let expected = Overlap {
            llc: Point { x: 1, y: 1 },
            urc: Point { x: 2, y: 2 },
            ulc: Point { x: 1, y: 2 },
            lrc: Point { x: 2, y: 1 },
        };

        let got = calc_overlap(&rect1, &rect2);

        assert_eq!(Some(expected), got);
    }

    #[test]
    fn test_calc_overlap_weird_corner_overlap() {
        let spec_one = String::from("#0 @ 1,0: 2x2");
        let spec_two = String::from("#1 @ 0,1: 2x2");

        let rect1 = Rectangle::new_from_spec(spec_one).unwrap();
        let rect2 = Rectangle::new_from_spec(spec_two).unwrap();

        let expected = Overlap {
            llc: Point { x: 1, y: 1 },
            urc: Point { x: 2, y: 2 },
            ulc: Point { x: 1, y: 2 },
            lrc: Point { x: 2, y: 1 },
        };

        let got = calc_overlap(&rect1, &rect2);

        assert_eq!(Some(expected), got);
    }

    #[test]
    fn test_overlap_to_pixels() {
        let overlap = Overlap {
            ulc: Point { x: 0, y: 2 },
            llc: Point { x: 0, y: 0 },
            urc: Point { x: 2, y: 2 },
            lrc: Point { x: 2, y: 0 },
        };

        let got = overlap.to_pixels();

        assert_eq!(4, got.len());
    }
}
