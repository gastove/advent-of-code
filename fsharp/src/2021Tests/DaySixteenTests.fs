module DaySixteenTests

open Expecto

open DaySixteen

[<Tests>]
let partOneTests =
    testList
        "Testing Day Sixteen, Part One"
        [ testCase "Test literal parsing"
          <| fun _ ->
              let input = "101111111000101000"
              let expected = 2021L
              let (gotNumber, gotUsed) = parseLiteral input
              let expectedUsed = 15

              Expect.equal gotUsed expectedUsed "We should consume the correct number of bits"
              Expect.equal gotNumber expected "We should be able to parse integer literals"

          testCase "Test padding detection"
          <| fun _ ->
              let empty = ""
              let zeroes = "000"

              Expect.isTrue (isPadding empty) "Empty string should read as padding"
              Expect.isTrue (isPadding zeroes) "Zeroes are padding"

          testCase "Can we parse a simple example hex to elf hex correctly?"
          <| fun _ ->
              let input = "D2FE28"
              let expected = "110100101111111000101000"
              let got = [| input |] |> parseElfHex

              Expect.equal got expected "We should be able to parse Elf Hex"

          testCase "Can we parse a simple binary packet?"
          <| fun _ ->
              let input = "110100101111111000101000"
              let expected = Packet.Create 6 4 (2021L |> Left)

              let got = input |> decodePacket

              Expect.equal got expected "We should be able to decode a simple packet containing a literal"

          testCase "Can we parse an example operator packet?"
          <| fun _ ->
              let input = [| "38006F45291200" |]

              let expectedBody =
                  [ Packet.Create 2 4 (20L |> Left)
                    Packet.Create 6 4 (10L |> Left) ] |> List.rev

              let expected =
                  Packet.Create 1 6 (expectedBody |> Right)

              let got = input |> parseElfHex |> decodePacket

              Expect.equal got expected "We should be able to parse an operator packet with two sub-packets."

          testCase "Can we parse a different example operator packet?"
          <| fun _ ->
              let input = [| "EE00D40C823060" |]

              let expectedBody =
                  [ Packet.Create 2 4 (1L |> Left)
                    Packet.Create 4 4 (2L |> Left)
                    Packet.Create 1 4 (3L |> Left) ]
                  |> List.rev

              let expected =
                  Packet.Create 7 3 (expectedBody |> Right)

              let got = input |> parseElfHex |> decodePacket

              Expect.equal got expected "Can we parse a packet with two sub-packets?"

          testCase "Can we parse a nested operator packet?"
          <| fun _ ->
              let input = [| "8A004A801A8002F478" |]

              let wrap = List.singleton >> Right

              let expected =
                  Packet.Create 6 4 (15L |> Left)
                  |> wrap
                  |> Packet.Create 5 2
                  |> wrap
                  |> Packet.Create 1 2
                  |> wrap
                  |> Packet.Create 4 2

              let got = input |> parseElfHex |> decodePacket

              Expect.equal got expected "We should be able to parse nested packets." ]

[<Tests>]
let partTwoTests =
    testList
        "Testing Day Sixteen, PartTwo"
        [ testCase "Can we reproduce example 1?"
          <| fun _ ->
              let input = [| "C200B40A82" |]
              let expected = "3"
              let got = input |> partTwo

              Expect.equal got expected "We should be able to sum 1 and 2"

          testCase "Can we reproduce example two?"
          <| fun _ ->
              let input = [| "04005AC33890" |]
              let expected = "54"
              let got = input |> partTwo

              Expect.equal got expected "We should be able to product 6 and 9?"

          testCase "Can we reproduce example three?"
          <| fun _ ->
              let input = [| "880086C3E88112" |]
              let expected = "7"
              let got = input |> partTwo

              Expect.equal got expected "We should be able to find the minimum of 7, 8, 9"

          testCase "Can we reproduce example four"
          <| fun _ ->
              let input = [| "CE00C43D881120" |]
              let expected = "9"
              let got = input |> partTwo

              Expect.equal got expected "We should be able to find the maximum of 7, 8, 9"

          testCase "Can we reproduce example five?"
          <| fun _ ->
              let input = [| "D8005AC2A8F0" |]
              let expected = "1"
              let got = input |> partTwo

              Expect.equal got expected "We should be able to find the minimum of 5, 15"

          testCase "Can we reproduce example six?"
          <| fun _ ->
              let input = [| "F600BC2D8F" |]
              let expected = "0"
              let got = input |> partTwo

              Expect.equal got expected "We should be able to find the maximum of 5, 15"

          testCase "Can we reproduce the last example?"
          <| fun _ ->
              let input = [| "9C0141080250320F1802104A08" |]
              let expected = "1"
              let got = input |> partTwo

              Expect.equal got expected "We should be able to find equality between 1+3 and 2*2" ]
