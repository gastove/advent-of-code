module DayNineteenTests

open Expecto

open DayNineteen

[<Tests>]
let partOneTests =
    testList
        "We should be able to parse Specifications"
        [ testCase "Test parsing a Character"
          <| fun _ ->
              let input = "\"b\""
              let expected = PartOne.Character('b')

              let got = PartOne.Specification.FromString input

              Expect.equal got expected "We should be able to parse a character"

          testCase "Test parsing a Sequence"
          <| fun _ ->
              let input = "1 2 3"
              let expected = PartOne.Sequence([ 1; 2; 3 ])

              let got = PartOne.Specification.FromString input

              Expect.equal got expected "We should be able to parse a sequence"

          testCase "Test parsing a Alternation"
          <| fun _ ->
              let input = "1 2 3 | 4 5 6"
              let seqOne = PartOne.Sequence([ 1; 2; 3 ])
              let seqTwo = PartOne.Sequence([ 4; 5; 6 ])
              let expected = PartOne.Alternation(seqOne, seqTwo)

              let got = PartOne.Specification.FromString input

              Expect.equal got expected "We should be able to parse an Alternation"

          testCase "Test regex construction"
          <| fun _ ->
              let input =
                  [ 0, PartOne.Sequence([ 1; 2 ])
                    1, PartOne.Character('a')
                    2, PartOne.Alternation(PartOne.Sequence([ 1; 3 ]), PartOne.Sequence([ 3; 1 ]))
                    3, PartOne.Character('b') ]
                  |> Map.ofList

              let expected = "^a(ab|ba)$"
              let got = PartOne.buildRegex input

              Expect.equal got expected "We should be able to build an example regex"

          ]

let longInput =
    [| "42: 9 14 | 10 1"
       "9: 14 27 | 1 26"
       "10: 23 14 | 28 1"
       "1: \"a\""
       "11: 42 31"
       "5: 1 14 | 15 1"
       "19: 14 1 | 14 14"
       "12: 24 14 | 19 1"
       "16: 15 1 | 14 14"
       "31: 14 17 | 1 13"
       "6: 14 14 | 1 14"
       "2: 1 24 | 14 4"
       "0: 8 11"
       "13: 14 3 | 1 12"
       "15: 1 | 14"
       "17: 14 2 | 1 7"
       "23: 25 1 | 22 14"
       "28: 16 1"
       "4: 1 1"
       "20: 14 14 | 1 15"
       "3: 5 14 | 16 1"
       "27: 1 6 | 14 18"
       "14: \"b\""
       "21: 14 1 | 1 14"
       "25: 1 1 | 1 14"
       "22: 14 14"
       "8: 42"
       "26: 14 22 | 1 20"
       "18: 15 15"
       "7: 14 5 | 1 21"
       "24: 14 1"
       ""
       "abbbbbabbbaaaababbaabbbbabababbbabbbbbbabaaaa"
       "bbabbbbaabaabba"
       "babbbbaabbbbbabbbbbbaabaaabaaa"
       "aaabbbbbbaaaabaababaabababbabaaabbababababaaa"
       "bbbbbbbaaaabbbbaaabbabaaa"
       "bbbababbbbaaaaaaaabbababaaababaabab"
       "ababaaaaaabaaab"
       "ababaaaaabbbaba"
       "baabbaaaabbaaaababbaababb"
       "abbbbabbbbaaaababbbbbbaaaababb"
       "aaaaabbaabaaaaababaa"
       "aaaabbaaaabbaaa"
       "aaaabbaabbaaaaaaabbbabbbaaabbaabaaa"
       "babaaabbbaaabaababbaabababaaab"
       "aabbbbbaabbbaaaaaabbbbbababaaaaabbaaabba" |]

let shortInput =
    [| "0: 4 1 5"
       "1: 2 3 | 3 2"
       "2: 4 4 | 5 5"
       "3: 4 5 | 5 4"
       "4: \"a\""
       "5: \"b\""
       ""
       "ababbb"
       "bababa"
       "abbbab"
       "aaabbb"
       "aaaabbb" |]

let subRuleAllShouldMatch =
    [| "bbabbbbaabaabba"
       "babbbbaabbbbbabbbbbbaabaaabaaa"
       "aaabbbbbbaaaabaababaabababbabaaabbababababaaa"
       "bbbbbbbaaaabbbbaaabbabaaa"
       "bbbababbbbaaaaaaaabbababaaababaabab"
       "ababaaaaaabaaab"
       "ababaaaaabbbaba"
       "baabbaaaabbaaaababbaababb"
       "abbbbabbbbaaaababbbbbbaaaababb"
       "aaaaabbaabaaaaababaa"
       "aaaabbaabbaaaaaaabbbabbbaaabbaabaaa"
       "aabbbbbaabbbaaaaaabbbbbababaaaaabbaaabba" |]

[<Tests>]
let partTwoTests =
    testList
        "Testing PartTwo code"
        [ testCase "Test we can produce the PartOne answer with a different algorithm"
          <| fun _ ->
              let expected = 12
              let got = PartTwo.partTwo longInput

              Expect.equal got expected "We should be able to reproduce the example"

          testCase "Test that we find every correct match"
          <| fun _ ->
              let ruleBook, _ = PartOne.parseInput longInput

              subRuleAllShouldMatch
              |> Array.iter
                  (fun line ->
                      let got = PartTwo.matchesRules line ruleBook

                      Expect.isTrue got (sprintf "The rules should match: %s" line))


          testCase "Can we match the first known-good case?"
          <| fun _ ->
              let shouldMatch1 = "bbabbbbaabaabba"

              let ruleBook, _ = PartOne.parseInput longInput

              Expect.isTrue
                  (PartTwo.matchesRules shouldMatch1 ruleBook)
                  "We should be able to match the first known-good case"

          testCase "Can we match the second known-good case?"
          <| fun _ ->
              let shouldMatch2 = "ababaaaaaabaaab"

              let ruleBook, _ = PartOne.parseInput longInput

              Expect.isTrue
                  (PartTwo.matchesRules shouldMatch2 ruleBook)
                  "We should be able to match the second known-good case"

          testCase "Can we match the third known-good case?"
          <| fun _ ->
              let shouldMatch3 = "ababaaaaabbbaba"

              let ruleBook, _ = PartOne.parseInput longInput

              Expect.isTrue
                  (PartTwo.matchesRules shouldMatch3 ruleBook)
                  "We should be able to match the third known-good case"

          testCase "Known-good short one"
          <| fun _ ->
              let shouldMatch = "ababbb"

              let rulebook, _ = PartOne.parseInput shortInput

              Expect.isTrue
                  (PartTwo.matchesRules shouldMatch rulebook)
                  "We should be able to match a short known-good case"


          ]
