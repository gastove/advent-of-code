use std::collections::{HashSet, VecDeque};

struct Deck {
    cards: VecDeque<usize>,
}

impl Deck {
    pub fn from_lines(lines: Vec<String>) -> Deck {
        let cards = lines.iter().map(|line| line.parse::<usize>().unwrap()).collect();
        Deck { cards }
    }
}

impl Clone for Deck {
    fn clone(&self) -> Self {
        Self { cards: self.cards.clone() }
    }
}

enum Outcome {
    DeckOneWins(Deck),
    DeckTwoWins(Deck),
}

fn make_decks(input: Vec<String>) -> (Deck, Deck) {
    let p1: Vec<String> = input.iter().take_while(|line| !line.is_empty()).cloned().collect();

    let p2: Vec<String> = input.iter().skip(p1.len()).skip_while(|line| line.is_empty()).cloned().collect();

    (Deck::from_lines(p1), Deck::from_lines(p2))
}

fn recursive_game(d1: Deck, d2: Deck) -> Outcome {
    let mut history = HashSet::new();
    let mut d1 = d1;
    let mut d2 = d2;

    loop {
        let state: (Vec<usize>, Vec<usize>) = (d1.cards.iter().copied().collect(), d2.cards.iter().copied().collect());

        if !history.insert(state) {
            return Outcome::DeckOneWins(d1.into());
        }

        let d1Card = d1.cards.pop_front();
        let d2Card = d2.cards.pop_front();

        if d1.cards.len() >= d1Card && d2.cards.len() >= d2Card {

            match recursive_game(d1.clone(), d2.clone())
        }
    }
}

pub fn run() {
    ()
}
