module DayEighteen

open System

open FSharp.Core.Operators.Checked

type IntOrString =
    | Int of int64
    | String of string
    static member FromString(s: string) =
        match s |> Common.tryInt64 with
        | Some i -> i |> Int
        | None -> s |> String


let getValue (ios: IntOrString) (register: Map<string, int64>) =
    match ios with
    | Int (i) -> i
    | String (s) -> register |> Map.find s


type Instruction =
    | Snd of IntOrString
    | Set of string * IntOrString
    | Add of string * IntOrString
    | Multiply of string * IntOrString
    | Modulus of string * IntOrString
    | Rcv of IntOrString
    | JumpGreaterThanZero of IntOrString * IntOrString

    static member FromString(s: string) =
        let parts = s.Split([| ' ' |]) |> Array.toList

        let restToTuple rest =
            let register = rest |> List.head

            let value =
                rest
                |> List.tail
                |> List.exactlyOne
                |> IntOrString.FromString

            register, value

        match parts with
        | "snd" :: rest ->
            rest
            |> List.exactlyOne
            |> IntOrString.FromString
            |> Snd
        | "set" :: rest -> rest |> restToTuple |> Set
        | "add" :: rest -> rest |> restToTuple |> Add
        | "mul" :: rest -> rest |> restToTuple |> Multiply
        | "mod" :: rest -> rest |> restToTuple |> Modulus
        | "rcv" :: rest ->
            rest
            |> List.exactlyOne
            |> IntOrString.FromString
            |> Rcv
        | "jgz" :: rest ->
            rest
            |> restToTuple
            |> (fun (f, s) -> f |> IntOrString.FromString, s)
            |> JumpGreaterThanZero
        | bad -> failwithf "Got an un-parseable string: %A" bad

type Computer =
    { Registers: Map<string, int64>
      LastSoundFrequency: int64 option }
    static member Create() =
        { Registers = Map.empty
          LastSoundFrequency = None }
    // NOTE[gastove|2021-09-10] Okay. We actually need to return a thruple of
    // the updated computer, a recovered sound option, *and* a distance to jump
    // (which will usually be zero).
    member this.ApplyInstruction(instruction: Instruction) : Computer * int64 option * int64 =
        match instruction with
        | Set (register, registerOrValue) ->
            let value = getValue registerOrValue this.Registers
            let updatedRegisters = Map.add register value this.Registers

            { this with
                  Registers = updatedRegisters },
            None,
            1L
        | Snd (registerOrFrequency) ->
            let freq =
                getValue registerOrFrequency this.Registers

            { this with
                  LastSoundFrequency = freq |> Some },
            None,
            1L
        | Add (register, registerOrValue) ->
            let value = getValue registerOrValue this.Registers

            let updatedRegisters =
                this.Registers
                |> Map.change
                    register
                    (fun found ->
                        found
                        |> Option.map (fun v -> v + value)
                        |> Option.defaultValue value
                        |> Some)

            { this with
                  Registers = updatedRegisters },
            None,
            1L
        | Multiply (register, registerOrValue) ->
            let value = getValue registerOrValue this.Registers

            let updatedRegisters =
                this.Registers
                |> Map.change
                    register
                    (fun found ->
                        found
                        |> Option.defaultValue 0L
                        |> ((*) value)
                        |> Some)

            { this with
                  Registers = updatedRegisters },
            None,
            1L
        | Modulus (register, registerOrValue) ->
            let value = getValue registerOrValue this.Registers

            let updatedRegisters =
                this.Registers
                |> Map.change
                    register
                    (fun found ->
                        found
                        |> Option.defaultValue 0L
                        |> (fun v -> v % value)
                        |> Some)

            { this with
                  Registers = updatedRegisters },
            None,
            1L
        | Rcv (register) ->
            let value = getValue register this.Registers

            if value = 0L then
                this, None, 1L
            else
                this, this.LastSoundFrequency, 1L
        | JumpGreaterThanZero (registerOrValue, registerOrJump) ->
            if getValue registerOrValue this.Registers > 0L then
                let jumpVal = getValue registerOrJump this.Registers
                this, None, jumpVal
            else
                this, None, 1L

let findFirstRecover (allInstructions: Instruction array) =
    let rec search (computer: Computer) instructionIdx =
        let instruction =
            Array.item instructionIdx allInstructions

        let updatedComputer, maybeFrequency, jump = computer.ApplyInstruction instruction

        match maybeFrequency with
        | Some f -> f
        | None -> search updatedComputer (instructionIdx + (jump |> int))

    search (Computer.Create()) 0

type State =
    | Running
    | Waiting

type ConcurrentComputer =
    { Id: int
      mutable Registers: Map<string, int64>
      mutable MessagesSent: int
      SendChan: Collections.Concurrent.ConcurrentQueue<int64>
      ReceiveChan: Collections.Concurrent.ConcurrentQueue<int64>
      mutable State: State
      mutable NextCommand: int }
    static member Create id sendChan recvChan =
        { Registers = [ "p", id |> int64 ] |> Map.ofList
          MessagesSent = 0
          SendChan = sendChan
          ReceiveChan = recvChan
          Id = id
          State = Running
          NextCommand = 0 }

    member this.ApplyInstruction(instruction: Instruction) =
        match instruction with
        | Set (register, registerOrValue) ->
            let value = getValue registerOrValue this.Registers
            let updatedRegisters = Map.add register value this.Registers

            this.Registers <- updatedRegisters
            this.NextCommand <- this.NextCommand + 1

        | Snd (registerOrFrequency) ->
            let toSend =
                getValue registerOrFrequency this.Registers

            toSend |> this.SendChan.Enqueue

            this.MessagesSent <- this.MessagesSent + 1
            this.NextCommand <- this.NextCommand + 1

        | Add (register, registerOrValue) ->
            let value = getValue registerOrValue this.Registers

            let updatedRegisters =
                this.Registers
                |> Map.change
                    register
                    (fun found ->
                        found
                        |> Option.map (fun v -> v + value)
                        |> Option.defaultValue value
                        |> Some)

            this.Registers <- updatedRegisters
            this.NextCommand <- this.NextCommand + 1

        | Multiply (register, registerOrValue) ->
            let value = getValue registerOrValue this.Registers

            let updatedRegisters =
                this.Registers
                |> Map.change
                    register
                    (fun found ->
                        found
                        |> Option.defaultValue 0L
                        |> ((*) value)
                        |> Some)

            this.Registers <- updatedRegisters
            this.NextCommand <- this.NextCommand + 1

        | Modulus (register, registerOrValue) ->
            let value = getValue registerOrValue this.Registers

            let updatedRegisters =
                this.Registers
                |> Map.change
                    register
                    (fun found ->

                        found
                        |> Option.defaultValue 0L
                        |> (fun v -> v % value)
                        |> Some)

            this.Registers <- updatedRegisters
            this.NextCommand <- this.NextCommand + 1

        | Rcv (rawRegister) ->
            let storeInRegister =
                match rawRegister with
                | String s -> s
                | Int (_) -> failwith "Ints are invalid for the RCV command running a concurrent computer"

            let value = ref 0L
            this.State <- Waiting
            let got = this.ReceiveChan.TryDequeue(value)
            if got then
                this.State <- Running

                let updatedRegisters =
                    this.Registers |> Map.add storeInRegister !value

                this.Registers <- updatedRegisters
                this.NextCommand <- this.NextCommand + 1
            else ()

        | JumpGreaterThanZero (registerOrValue, registerOrJump) ->
            let jumpVal =
                if getValue registerOrValue this.Registers > 0L then
                    getValue registerOrJump this.Registers
                else
                    1L

            this.NextCommand <- this.NextCommand + (int jumpVal)

let duet (allInstructions: Instruction array) (computerZero: ConcurrentComputer) (computerOne: ConcurrentComputer) =
    let rec run (active: ConcurrentComputer) (waiting: ConcurrentComputer) (swappedOnce: bool) =
        allInstructions
        |> Array.item active.NextCommand
        |> active.ApplyInstruction

        match active.State, waiting.State with
        | Waiting, Waiting ->
            if swappedOnce then
                sprintf $"Computer {active.Id} sent {active.MessagesSent}; Computer {waiting.Id} sent {waiting.MessagesSent}"
            else
                run waiting active true
        | Running, _ -> run active waiting false
        | Waiting, _ -> run waiting active false

    run computerZero computerOne false

let parseInput (input: Common.Input) =
    input |> Array.map Instruction.FromString

let partOne (input: Common.Input) = input |> parseInput |> findFirstRecover

let partTwo (input: Common.Input) =
    let instructions = input |> parseInput

    let compyOneToCompyTwo =
        Collections.Concurrent.ConcurrentQueue<int64>()

    let compyTwoToCompyOne =
        Collections.Concurrent.ConcurrentQueue<int64>()

    let compyOne =
        ConcurrentComputer.Create 0 compyOneToCompyTwo compyTwoToCompyOne

    let compyTwo =
        ConcurrentComputer.Create 1 compyTwoToCompyOne compyOneToCompyTwo

    duet instructions compyOne compyTwo

type Solution() =
    let name = "eighteen"

    interface Common.ISolution with
        member __.Name() = name |> Common.titleCase
        member __.LoadData(year) : Common.Input option = Common.loadData name year
        member __.PartOne(input: Common.Input) : string = input |> partOne |> string
        member __.PartTwo(input: Common.Input) : string = input |> partTwo
