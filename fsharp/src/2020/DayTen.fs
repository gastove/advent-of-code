module DayTen

//--------------------------------- Part One ---------------------------------//
// ...was honestly trivial.

//--------------------------------- Part Two ---------------------------------//
// This one is messing with me.
//
// We need all the ways to order a very long set of things, given rules.


let partOne (input: int array) =
    let sorted = input |> Array.sort
    let evenPairs = sorted |> Array.chunkBySize 2
    let oddPairs = sorted.[1..] |> Array.chunkBySize 2

    [| evenPairs
       oddPairs.[0..(oddPairs.Length - 2)] |]
    |> Array.concat
    |> Array.sortBy (fun p -> p.[0])
    |> Array.map (fun p -> p.[1] - p.[0])

let deltaCorrect x y =
    let delta = y - x
    0 < delta && delta <= 3

let folder (state: Map<int, int64>) i =
    match Map.tryFind i state with
    | Some(_) -> failwith "what the hell?"
    | None ->
        let found = Map.filter (fun k _v -> deltaCorrect i k) state

        let sumInRange =
            found
            |> Map.toArray
            |> Array.sumBy snd

        Map.add i sumInRange state

let tabulate (input: int array) =
    let sorted = input |> Array.sortByDescending id

    let initial: Map<int, int64> =
        [ (sorted.[0], 1L) ]
        |> Map.ofList

    let workingSet = sorted |> Array.tail

    let result = workingSet |> Array.fold folder initial

    result |> Map.tryFind 0


type Solution() =
    let name = "ten"
    interface Common.ISolution with
        member __.Name(): string = name

        member __.LoadData(year): Common.Input option = Common.loadData name year

        member __.PartOne(input: Common.Input): string =
            let parsed = input |> Array.choose Common.tryInt

            let biggest = Array.max parsed

            let completed =
                Array.concat
                    [| parsed
                       [| 0
                          biggest + 3 |] |]

            let deltas = partOne completed

            let huh =
                deltas
                |> Array.countBy id
                |> Array.reduce (fun oneJolt threeJolts -> (snd oneJolt) * (snd threeJolts), 0)

            sprintf "Could it be this easy? %A" <| fst huh

        member __.PartTwo(input: Common.Input): string =
            let parsed = input |> Array.choose Common.tryInt

            let built =
                Array.concat
                    [| parsed
                       [| 0 |] |]

            match tabulate built with
            | Some i -> sprintf "God willing, we have length %i" i
            | None -> "Didn't find anything, ugh"
