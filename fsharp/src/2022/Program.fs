﻿module AdventOfCode2022

[<EntryPoint>]
let main argv =
    let solutions: Common.ISolution array =
        [| DayOne.Solution()
           DayTwo.Solution()
           DayThree.Solution()
           DayFour.Solution()
           DayFive.Solution()
           DaySix.Solution()
           DaySeven.Solution()
           DayEight.Solution()
           DayNine.Solution()
           DayTen.Solution()
           DayEleven.Solution()
           DayTwelve.Solution()
           DayThirteen.Solution()
         |]

    Common.doAdventOfCode "2022" solutions argv
