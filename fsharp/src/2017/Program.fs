let Year = "2017"

let show part result = printfn "Part %s\n-> %s\n" part result

let runAndShowDay (solution: Common.ISolution) =
    solution.Name() |> printfn "Day %s!\n==========\n"

    match solution.LoadData(Year) with
    | Some (input) ->
        input |> solution.PartOne |> show "One"
        input |> solution.PartTwo |> show "Two"
    | None -> printfn "The data doesn't seem to exist!"

[<EntryPoint>]
let main argv =
    let day = argv.[0]

    match day with
    | "one" -> DayOne.Solution() |> runAndShowDay
    | "two" -> DayTwo.Solution() |> runAndShowDay
    | "three" -> DayThree.Solution() |> runAndShowDay
    | "four" -> DayFour.Solution() |> runAndShowDay
    | "five" -> DayFive.Solution() |> runAndShowDay
    | "six" -> DaySix.Solution() |> runAndShowDay
    | "seven" -> DaySeven.Solution() |> runAndShowDay
    | "eight" -> DayEight.Solution() |> runAndShowDay
    | "nine" -> DayNine.Solution() |> runAndShowDay
    | "ten" -> DayTen.Solution() |> runAndShowDay
    | "eleven" -> DayEleven.Solution() |> runAndShowDay
    | "twelve" -> DayTwelve.Solution() |> runAndShowDay
    | "thirteen" -> DayThirteen.Solution() |> runAndShowDay
    | "fourteen" -> DayFourteen.Solution() |> runAndShowDay
    | "fifteen" -> DayFifteen.Solution() |> runAndShowDay
    | "sixteen" -> DaySixteen.Solution() |> runAndShowDay
    | "seventeen" -> DaySeventeen.Solution() |> runAndShowDay
    | "eighteen" -> DayEighteen.Solution() |> runAndShowDay
    | "nineteen" -> DayNineteen.Solution() |> runAndShowDay
    | "twenty" -> DayTwenty.Solution() |> runAndShowDay
    | "twentyone" -> DayTwentyOne.Solution() |> runAndShowDay
    | "twentytwo" -> DayTwentyTwo.Solution() |> runAndShowDay
    | "twentythree" -> DayTwentyThree.Solution() |> runAndShowDay
    | "twentyfour" -> DayTwentyFour.Solution() |> runAndShowDay
    | "twentyfive" -> DayTwentyFive.Solution() |> runAndShowDay
    // | "all" -> solutions |> Array.iter runAndShowDay
    | _ -> failwith "Day invalid."

    0 // return an integer exit code
