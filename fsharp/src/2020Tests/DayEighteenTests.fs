module DayEighteenTests

open Expecto

module PartOne =
    open DayEighteen.Shared
    open DayEighteen.PartOne

    [<Tests>]
    let partOneTests =
        testList "Testing DayEighteen.PartOne"
            [ testCase "Can we parse symbols with no nesting?" <| fun _ ->
                let input = "1 * 2 + 3"

                let expected =
                    [ Number 1L
                      Multiply
                      Number 2L
                      Add
                      Number 3L ]

                let got = parseSymbols input

                Expect.equal got expected "We should get sensibly parsed symbols"

              testCase "Can we parse subexpressions?" <| fun _ ->
                  let input = "(1 + 2)"

                  let expected =
                      [ Subexpression
                          ([ Number 1L
                             Add
                             Number 2L ]) ]

                  let got = parseSymbols input

                  Expect.equal got expected "We should get back a parsed Subexpression"

              testCase "Can we parse nested subexpressions?" <| fun _ ->
                  let sub = "1 + (2 * 3) + (4 * (5 + 6))"

                  let expected =
                      [ Number 1L
                        Add
                        Subexpression
                            ([ Number 2L
                               Multiply
                               Number 3L ])
                        Add
                        Subexpression
                            ([ Number 4L
                               Multiply
                               Subexpression
                                   ([ Number 5L
                                      Add
                                      Number 6L ]) ]) ]

                  let got = parseSymbols sub

                  Expect.equal got expected "We should be able to parse nested Subexpressions"

              testCase "Can I evaluate basic expressions?" <| fun _ ->
                  let addOnly = "1 + 2 + 3"
                  let multiplyOnly = "3 * 3 * 3"
                  let addExpected = Some 6L
                  let multiplyExpected = Some 27L

                  let addGot =
                      addOnly
                      |> parseSymbols
                      |> evaluate

                  let multiplyGot =
                      multiplyOnly
                      |> parseSymbols
                      |> evaluate

                  Expect.equal addGot addExpected "We should be able to sum numbers"
                  Expect.equal multiplyGot multiplyExpected "We should be able to multiply numbers"

              testCase "Can we evaluate subexpressions at all??" <| fun _ ->
                  let sub = "(1 + 2 + 3)"
                  let expected = Some 6L

                  let got =
                      sub
                      |> parseSymbols
                      |> evaluate

                  Expect.equal got expected "We should be able to evauate a bare subexpression"

              testCase "Can we evaluate subexpressions as part of longer expressions?" <| fun _ ->
                  let sub = "2 * 3 + (4 * 5)"
                  let expected = Some 26L

                  let got =
                      sub
                      |> parseSymbols
                      |> evaluate

                  Expect.equal got expected "We should be able to evaulate singly nested subexpressions"

              testCase "Can we evaluate nested subexpressions as part of longer expressions?" <| fun _ ->
                  let sub = "1 + (2 * 3) + (4 * (5 + 6))"
                  let expected = Some 51L

                  let got =
                      sub
                      |> parseSymbols
                      |> evaluate

                  Expect.equal got expected "We should be able to evaulate nested subexpressions"

              testCase "Validating everything is working" <| fun _ ->
                  let input1 = "5 + (8 * 3 + 9 + 3 * 4 * 3)" // 437.
                  let input2 = "5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))" // becomes 12240.
                  let input3 = "((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2" // becomes 13632.
                  let input4 =
                      "((8 + 5 + 5) * 6 * (2 * 2 * 5 * 4 + 5 * 8) + 9 * 5) * 8 + 2 * 8 + (7 * (2 * 5 + 2 * 7) + (7 * 4 + 7 * 6 * 9 + 6) + 7 + 9 + (3 * 4 + 5 * 4)) * 9"
                  let input5 = "9 * 2" // 18
                  let input6 = "1 + 2 * 3 + 4 * 5 + 6" // 71

                  let compute s =
                      s
                      |> parseSymbols
                      |> evaluate

                  Expect.equal (compute input1) (Some 437L) "These should be equal"
                  Expect.equal (compute input2) (Some 12240L) "These should be equal"
                  Expect.equal (compute input3) (Some 13632L) "These should be equal"
                  Expect.equal (compute input4) (Some 211556376L) "These should be equal"
                  Expect.equal (compute input5) (Some 18L) "These should be equal"
                  Expect.equal (compute input6) (Some 71L) "These should be equal"

              testCase "Parsing complex nested subexpressions" <| fun _ ->
                  let input = "4 + 2 * ((2 + 8 * 7 * 9) * 7 * (2 + 4) + (7 + 9 + 5 + 6 * 2 * 6))" // 160704

                  let expected =
                      [ Number 4L
                        Add
                        Number 2L
                        Multiply
                        Subexpression
                            [ Subexpression
                                [ Number 2L
                                  Add
                                  Number 8L
                                  Multiply
                                  Number 7L
                                  Multiply
                                  Number 9L ]
                              Multiply
                              Number 7L
                              Multiply
                              Subexpression
                                  [ Number 2L
                                    Add
                                    Number 4L ]
                              Add
                              Subexpression
                                  [ Number 7L
                                    Add
                                    Number 9L
                                    Add
                                    Number 5L
                                    Add
                                    Number 6L
                                    Multiply
                                    Number 2L
                                    Multiply
                                    Number 6L ] ] ]

                  let got = input |> parseSymbols

                  Expect.equal got expected "Even this thorny beast should parse"

              testCase "Dear god at last a failing test case" <| fun _ ->
                  let input = "4 + 2 * ((2 + 8 * 7 * 9) * 7 * (2 + 4) + (7 + 9 + 5 + 6 * 2 * 6))" // 160704

                  let compute s =
                      s
                      |> parseSymbols
                      |> evaluate

                  Expect.equal (compute input) (Some 160704L) "These should be equal"

              testCase "Gut checking sub expressions" <| fun _ ->
                  let subexp = "(1 + 2 + 3)"
                  let regular = "1 + 2 + 3"

                  let subExpGot =
                      subexp
                      |> parseSymbols
                      |> evaluate

                  let regularGot =
                      regular
                      |> parseSymbols
                      |> evaluate

                  Expect.equal subExpGot regularGot "A sub expression should evaluate the same as a regular one"

              testCase "Precedence should always work" <| fun _ ->
                  let input1 = "1 + 2 * 3" // 9
                  let input2 = "1 + 3 * 2" // 8

                  let compute s =
                      s
                      |> parseSymbols
                      |> evaluate

                  Expect.equal (compute input1) (Some 9L) "These should be equal"
                  Expect.equal (compute input2) (Some 8L) "These should be equal" ]

module PartTwo =
    open DayEighteen.Shared
    open DayEighteen.PartTwo

    [<Tests>]
    let partTwoTests =
        testList "Testing Part Two"
            [ testCase "Can we evaluate numbers with operator precedence?" <| fun _ ->
                  let input1 = "1 + (2 * 3) + (4 * (5 + 6))" //  still becomes 51.
                  let input2 = "2 * 3 + (4 * 5)" //  becomes 46.
                  let input3 = "5 + (8 * 3 + 9 + 3 * 4 * 3)" //  becomes 1445
                  let input4 = "5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))" // " becomes 669060."
                  let input5 = "((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2" // becomes 23340."

                  let compute s = s |> parseSymbols |> evaluate

                  Expect.equal (compute input1) (Some 51L) "These should be equal"
                  Expect.equal (compute input2) (Some 46L) "These should be equal"
                  Expect.equal (compute input3) (Some 1445L) "These should be equal"
                  Expect.equal (compute input4) (Some 669060L) "These should be equal"
                  Expect.equal (compute input5) (Some 23340L) "These should be equal" ]
