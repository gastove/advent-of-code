module DayTwelve

open FSharp.Core.Operators.Checked

type Graph =
    { Nodes: int list
      Edges: Map<int, int list> }

    static member Empty = { Nodes = []; Edges = Map.empty }

    member this.Update node edges =
        { Nodes = node :: this.Nodes
          Edges = this.Edges |> Map.add node edges }

let parseLine (line: string) =
    let parts = line.Split("<->")
    let node = parts.[0].Trim() |> int

    let edges =
        parts.[1].Split(", ")
        |> Array.map (fun s -> s.Trim() |> int)
        |> Array.toList

    node, edges

let findAllInGroupWith node (graph: Graph) =
    let rec traceNodes (currentNode: int) (seen: Set<int>) =
        let edgeSet =
            graph.Edges |> Map.find currentNode |> Set.ofList

        let unvisited = Set.difference edgeSet seen

        if unvisited.Count = 0 then
            seen
        else
            unvisited
            |> Set.fold (fun newSeen node -> traceNodes node (newSeen |> Set.add node)) seen

    traceNodes node Set.empty


let partOne (input: Common.Input) =
    let graph =
        input
        |> Array.map parseLine
        |> Array.fold (fun (graph: Graph) (node, edges) -> graph.Update node edges) Graph.Empty

    graph |> findAllInGroupWith 0 |> Set.count

let partTwo (input: Common.Input) =
    let graph =
        input
        |> Array.map parseLine
        |> Array.fold (fun (graph: Graph) (node, edges) -> graph.Update node edges) Graph.Empty

    graph.Nodes
    |> List.toArray
    |> Array.Parallel.map (fun n -> findAllInGroupWith n graph)
    |> Set.ofArray
    |> Set.count

type Solution() =
    let name = "twelve"

    interface Common.ISolution with
        member __.Name() = name |> Common.titleCase
        member __.LoadData(year) : Common.Input option = Common.loadData name year
        member __.PartOne(input: Common.Input) : string = input |> partOne |> string
        member __.PartTwo(input: Common.Input) : string = input |> partTwo |> string
