module DayTwo

//--------------------------------- Part One ---------------------------------//

// For example, suppose you have the following list:

// 1-3 a: abcde
// 1-3 b: cdefg
// 2-9 c: ccccccccc

// Each line gives the password policy and then the password. The password
// policy indicates the lowest and highest number of times a given letter must
// appear for the password to be valid. For example, 1-3 a means that the
// password must contain a at least 1 time and at most 3 times.

// In the above example, 2 passwords are valid. The middle password, cdefg, is
// not; it contains no instances of b, but needs at least 1. The first and third
// passwords are valid: they contain one a or nine c, both within the limits of
// their respective policies.

// How many passwords are valid according to their policies?

//--------------------------------- Part Two ---------------------------------//

// Each policy actually describes two positions in the password, where 1 means
// the first character, 2 means the second character, and so on. (Be careful;
// Toboggan Corporate Policies have no concept of "index zero"!) Exactly one of
// these positions must contain the given letter. Other occurrences of the
// letter are irrelevant for the purposes of policy enforcement.

// Given the same example list from above:

//     1-3 a: abcde is valid: position 1 contains a and position 3 does not.
//     1-3 b: cdefg is invalid: neither position 1 nor position 3 contains b.
//     2-9 c: ccccccccc is invalid: both position 2 and position 9 contain c.

// How many passwords are valid according to the new interpretation of the policies?

let breakInTwo (input: string) (sep: char): string * string =
    let pieces = input.Split(sep)
    (pieces.[0], pieces.[1])

type OldPolicy =
    { Min: int
      Max: int
      Symbol: char }

    static member FromLine(spec: string): OldPolicy =
        let range, sym = breakInTwo spec ' '
        let min, max = breakInTwo range '-'

        { Min = System.Int32.Parse(min)
          Max = System.Int32.Parse(max)
          Symbol = (sym |> char) }
    member self.Audit(password: string): bool =
        let charMap =
            password
            |> Seq.toArray
            |> Array.countBy id
            |> Map.ofArray

        let charCount =
            Map.tryFind self.Symbol charMap
            |> Option.defaultValue 0

        self.Min <= charCount && charCount <= self.Max

type NewPolicy =
    { FirstPosition: int
      SecondPosition: int
      Symbol: char }

    static member FromLine(spec: string): NewPolicy =
        let range, sym = breakInTwo spec ' '
        let min, max = breakInTwo range '-'

        // F# is zero-indexed; the password policies are *not*. Convert on load.
        { FirstPosition = System.Int32.Parse(min) - 1
          SecondPosition = System.Int32.Parse(max) - 1
          Symbol = (sym |> char) }

    member self.Audit(password: string): bool =
        let chars =
            password
            |> Seq.toArray

        let first = Array.tryItem self.FirstPosition chars
        let second = Array.tryItem self.SecondPosition chars

        match first, second with
        | None, None -> false
        | Some(c), None | None, Some(c) -> c = self.Symbol
        | Some(fc), Some(sc) ->
            let fb = (fc = self.Symbol)
            let sb = (sc = self.Symbol)
            (fb || sb) && not (fb && sb)


let parseLine (line: string): string * string =
    let pwSpec, pw = breakInTwo line ':'
    let password = pw.Trim()

    pwSpec, password

let partOne (input: string array): int =
    input
    |> Array.map parseLine
    |> Array.map (fun (pwSpec, pw) -> (OldPolicy.FromLine pwSpec), pw)
    |> Array.filter (fun ((policy: OldPolicy), (pw: string)) -> policy.Audit(pw))
    |> Array.length

let partTwo (input: string array): int =
    input
    |> Array.map parseLine
    |> Array.map (fun (pwSpec, pw) -> (NewPolicy.FromLine pwSpec), pw)
    |> Array.filter (fun ((policy: NewPolicy), (pw: string)) -> policy.Audit(pw))
    |> Array.length

type Solution() =
    let name = "two"
    interface Common.ISolution with
        member __.Name() = Common.titleCase name
        member __.LoadData(year): Common.Input option =
            Common.loadData name year
        member __.PartOne(input: Common.Input): string =
            let partOne = partOne input
            sprintf "Found this many *valid* passwords on the *old* policy: %i" partOne

        member __.PartTwo(input: Common.Input): string =
            let partTwo = partTwo input
            sprintf "Found this many valid passwords on the *new* policy: %i" partTwo
