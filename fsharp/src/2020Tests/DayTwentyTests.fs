module DayTwentyTests

open Expecto

open DayTwenty

open Common.DataStructures

let testInputShort =
    [| "Tile 2311:"
       "..##.#..#."
       "##..#....."
       "#...##..#."
       "####.#...#"
       "##.##.###."
       "##...#.###"
       ".#.#.#..##"
       "..#....#.."
       "###...#.#."
       "..###..###"
       "" |]

let testInputLong =
    [| "Tile 2311:"
       "..##.#..#."
       "##..#....."
       "#...##..#."
       "####.#...#"
       "##.##.###."
       "##...#.###"
       ".#.#.#..##"
       "..#....#.."
       "###...#.#."
       "..###..###"
       ""
       "Tile 1951:"
       "#.##...##."
       "#.####...#"
       ".....#..##"
       "#...######"
       ".##.#....#"
       ".###.#####"
       "###.##.##."
       ".###....#."
       "..#.#..#.#"
       "#...##.#.."
       ""
       "Tile 1171:"
       "####...##."
       "#..##.#..#"
       "##.#..#.#."
       ".###.####."
       "..###.####"
       ".##....##."
       ".#...####."
       "#.##.####."
       "####..#..."
       ".....##..."
       ""
       "Tile 1427:"
       "###.##.#.."
       ".#..#.##.."
       ".#.##.#..#"
       "#.#.#.##.#"
       "....#...##"
       "...##..##."
       "...#.#####"
       ".#.####.#."
       "..#..###.#"
       "..##.#..#."
       ""
       "Tile 1489:"
       "##.#.#...."
       "..##...#.."
       ".##..##..."
       "..#...#..."
       "#####...#."
       "#..#.#.#.#"
       "...#.#.#.."
       "##.#...##."
       "..##.##.##"
       "###.##.#.."
       ""
       "Tile 2473:"
       "#....####."
       "#..#.##..."
       "#.##..#..."
       "######.#.#"
       ".#...#.#.#"
       ".#########"
       ".###.#..#."
       "########.#"
       "##...##.#."
       "..###.#.#."
       ""
       "Tile 2971:"
       "..#.#....#"
       "#...###..."
       "#.#.###..."
       "##.##..#.."
       ".#####..##"
       ".#..####.#"
       "#..#.#..#."
       "..####.###"
       "..#.#.###."
       "...#.#.#.#"
       ""
       "Tile 2729:"
       "...#.#.#.#"
       "####.#...."
       "..#.#....."
       "....#..#.#"
       ".##..##.#."
       ".#.####..."
       "####.#.#.."
       "##.####..."
       "##..#.##.."
       "#.##...##."
       ""
       "Tile 3079:"
       "#.#.#####."
       ".#..######"
       "..#......."
       "######...."
       "####.#..#."
       ".#...#.##."
       "#.#####.##"
       "..#.###..."
       "..#......."
       "..#.###..." |]

[<Tests>]
let partOneTests =
    testList
        "Testing PartOne"
        [ testCase "Testing Parsing one Tile"
          <| fun _ ->
              let expected =
                  { Grid.Id = "2311"
                    Grid.Data =
                      [| [| '.'
                            '.'
                            '#'
                            '#'
                            '.'
                            '#'
                            '.'
                            '.'
                            '#'
                            '.' |]
                         [| '#'
                            '#'
                            '.'
                            '.'
                            '#'
                            '.'
                            '.'
                            '.'
                            '.'
                            '.' |]
                         [| '#'
                            '.'
                            '.'
                            '.'
                            '#'
                            '#'
                            '.'
                            '.'
                            '#'
                            '.' |]
                         [| '#'
                            '#'
                            '#'
                            '#'
                            '.'
                            '#'
                            '.'
                            '.'
                            '.'
                            '#' |]
                         [| '#'
                            '#'
                            '.'
                            '#'
                            '#'
                            '.'
                            '#'
                            '#'
                            '#'
                            '.' |]
                         [| '#'
                            '#'
                            '.'
                            '.'
                            '.'
                            '#'
                            '.'
                            '#'
                            '#'
                            '#' |]
                         [| '.'
                            '#'
                            '.'
                            '#'
                            '.'
                            '#'
                            '.'
                            '.'
                            '#'
                            '#' |]
                         [| '.'
                            '.'
                            '#'
                            '.'
                            '.'
                            '.'
                            '.'
                            '#'
                            '.'
                            '.' |]
                         [| '#'
                            '#'
                            '#'
                            '.'
                            '.'
                            '.'
                            '#'
                            '.'
                            '#'
                            '.' |]
                         [| '.'
                            '.'
                            '#'
                            '#'
                            '#'
                            '.'
                            '.'
                            '#'
                            '#'
                            '#' |] |] }

              let got = testInputShort |> PartOne.parseInput

              Expect.equal got [ expected ] "We should be able to parse a grid"

          // testCase "Can we find the example corners"
          // <| fun _ ->
          //     let expected =
          //         [| "1951"; "3079"; "2971"; "1171" |]
          //         |> Set.ofArray

          //     let got =
          //         testInputLong |> PartOne.partOne |> Set.ofArray

          //     Expect.equal got expected "We should be able to find the example corners"

          testCase "Can we correctly tell if two tiles share an edge?"
          <| fun _ ->
              let grids = testInputLong |> PartOne.parseInput

              let corner =
                  grids |> List.find (fun g -> g.Id = "1951")

              let shouldMatch =
                  grids |> List.find (fun g -> g.Id = "2311")

              let shouldAlsoMatch =
                  grids |> List.find (fun g -> g.Id = "2729")

              let shouldNotMatch =
                  grids |> List.find (fun g -> g.Id = "1427")

              Expect.isTrue (Grid.hasMatchingEdge corner shouldMatch) "The first two should share an edge"
              Expect.isTrue (Grid.hasMatchingEdge corner shouldAlsoMatch) "The second two should share an edge"
              Expect.isFalse (Grid.hasMatchingEdge corner shouldNotMatch) "The corner and center share no edge"


          testCase "Can we get edges correctly?"
          <| fun _ ->
              // let testInputShort =
              // [| "Tile 2311:"
              //    "..##.#..#."
              //    "##..#....."
              //    "#...##..#."
              //    "####.#...#"
              //    "##.##.###."
              //    "##...#.###"
              //    ".#.#.#..##"
              //    "..#....#.."
              //    "###...#.#."
              //    "..###..###"
              //    "" |]
              let grid =
                  testInputShort |> Array.toList |> Grid.parseGrid

              let expectedTop = "..##.#..#." |> Seq.toArray
              let expectedBottom = "..###..###" |> Seq.toArray
              let expectedLeft = ".#####..#." |> Seq.toArray
              let expectedRight = "...#.##..#" |> Seq.toArray

              let gotTop = grid |> Grid.getEdge Edge.Top
              let gotBottom = grid |> Grid.getEdge Edge.Bottom
              let gotRight = grid |> Grid.getEdge Edge.Right
              let gotLeft = grid |> Grid.getEdge Edge.Left

              Expect.equal gotTop expectedTop "We should be able to get the expected top edge"
              Expect.equal gotBottom expectedBottom "We should be able to get the expected bottom edge"
              Expect.equal gotLeft expectedLeft "We should be able to get the expected left edge"
              Expect.equal gotRight expectedRight "We should be able to get the expected right edge" ]

let makeGrid id (input: string list) =
    input
    |> List.map Seq.toArray
    |> List.toArray
    |> Grid.Create id

[<Tests>]
let partTwoTests =
    testList
        "Testing 2020, Day 20, Part 2"
        [ testCase "Does tryAlignAndMatch work in simple cases?"
          <| fun _ ->
              let firstGrid =
                  [ "123"; "456"; "789" ] |> makeGrid "grid one"

              let secondGrid =
                  [ "111"; "114"; "117" ] |> makeGrid "grid two"

              let expectedEdge = Left

              match Grid.tryAlignAndMatch firstGrid secondGrid with
              | None -> failtest "This function should return Some"
              | Some (gotGrid, gotEdge) ->
                  Expect.equal gotGrid secondGrid "The grid should not be rotated"
                  Expect.equal gotEdge expectedEdge "We should identify the correct matching edge"

          testCase "Does tryAlignAndMatch work in rotated cases?"
          <| fun _ ->
              let firstGrid =
                  [ "123"; "456"; "789" ] |> makeGrid "grid one"

              let secondGrid =
                  [ "911"; "811"; "711" ] |> makeGrid "grid two"

              let expectedGrid =
                  [ "789"; "111"; "111" ] |> makeGrid "grid two"

              let expectedEdge = Bottom

              match Grid.tryAlignAndMatch firstGrid secondGrid with
              | None -> failtest "This function should return Some"
              | Some (gotGrid, gotEdge) ->
                  Expect.equal gotGrid expectedGrid "The grid should not be rotated"
                  Expect.equal gotEdge expectedEdge "We should identify the correct matching edge"

          testCase "Does tryAlignAndMatch work in flipped cases?"
          <| fun _ ->
              let firstGrid =
                  [ "123"; "456"; "789" ] |> makeGrid "grid one"

              let secondGrid =
                  [ "987"; "111"; "111" ] |> makeGrid "grid two"

              let expectedGrid =
                  [ "789"; "111"; "111" ] |> makeGrid "grid two"

              let expectedEdge = Bottom

              match Grid.tryAlignAndMatch firstGrid secondGrid with
              | None -> failtest "This function should return Some"
              | Some (gotGrid, gotEdge) ->
                  Expect.equal gotGrid expectedGrid "The grid should not be rotated"
                  Expect.equal gotEdge expectedEdge "We should identify the correct matching edge"



          ftestCase "Does PartTwo.assembleImage produce three rows from test data?"
          <| fun _ ->
              let got =
                  testInputLong
                  |> PartOne.parseInput
                  |> PartTwo.assembleImage

              Expect.equal got.Length 3 "We should form the correct number of rows"

          // testCase "Does addGridToRows correctly append an unmatched grid?"
          // <| fun _ ->
          //     let rows =
          //         [ [| [| '1'; '2' |]; [| '3'; '4' |] |]
          //           |> Grid.Create "1"
          //           [| [| '5'; '6' |]; [| '7'; '8' |] |]
          //           |> Grid.Create "2" ]

          //     let adding =
          //         [| [| '9'; '9' |]; [| '1'; '1' |] |]
          //         |> Grid.Create "3"

          //     let expected =
          //         [ [| [| '9'; '9' |]; [| '1'; '1' |] |]
          //           |> Grid.Create "3"
          //           [| [| '1'; '2' |]; [| '3'; '4' |] |]
          //           |> Grid.Create "1"
          //           [| [| '5'; '6' |]; [| '7'; '8' |] |]
          //           |> Grid.Create "2" ]

          //     let got = PartTwo.addGridToRows rows adding

          //     Expect.equal got expected "The new grid should be appended"

          // testCase "Does addGridToRows correctly merge a matching grid?"
          // <| fun _ ->
          //     let rows =
          //         [ [| [| '1'; '2' |]; [| '3'; '4' |] |]
          //           |> Grid.Create "1"
          //           [| [| '5'; '6' |]; [| '7'; '8' |] |]
          //           |> Grid.Create "2" ]

          //     let adding =
          //         [| [| '2'; '4' |]; [| '9'; '9' |] |]
          //         |> Grid.Create "3"

          //     let expected =
          //         [ [| [| '5'; '6' |]; [| '7'; '8' |] |]
          //           |> Grid.Create "2"
          //           [| [| '1'; '3' |]; [| '9'; '9' |] |]
          //           |> Grid.Create "3" ]

          //     let got = PartTwo.addGridToRows rows adding

          //     Expect.equal got expected "The new grid should be appended"

          ]
