module DaySeventeen

open FSharp.Core.Operators.Checked

let Steps = 303
let Permutations = 2017

type CircularBuffer =
    { Buffer: int list
      CurrentPosition: int }
    static member Create() = { Buffer = [ 0 ]; CurrentPosition = 0 }

    member this.IndexAfterSteps(steps: int) =
        if this.Buffer.Length = 1 then
            0
        else
            let reach = this.CurrentPosition + steps
            reach % this.Buffer.Length

    member this.Insert idx value =
        let firstHalf, secondHalf = this.Buffer |> List.splitAt idx

        { Buffer = firstHalf @ [ value ] @ secondHalf
          CurrentPosition = idx }

    member this.InsertAfterSteps steps value =
        let idxAfterSteps = this.IndexAfterSteps steps + 1
        this.Insert idxAfterSteps value


let permuteBuffer (buffer: CircularBuffer) (times: int) steps =
    let rec permute (buf: CircularBuffer) insert remaining =
        if remaining = 0 then
            buf
        else
            permute (buf.InsertAfterSteps steps insert) (insert + 1) (remaining - 1)

    permute buffer 1 times

let partOne permutations steps =
    let buffer =
        permuteBuffer (CircularBuffer.Create()) permutations steps

    buffer.Buffer.[buffer.CurrentPosition + 1]

let partTwo permutations steps =

    let rec siftSlotOne remaining current value acc =
        if remaining = 0 then
            acc
        else
            let newCurrent = ((current + steps) % value) + 1

            let newAcc =
                if newCurrent = 1 then value else acc

            siftSlotOne (remaining - 1) newCurrent (value + 1) newAcc

    siftSlotOne permutations 0 1 0

type Solution() =
    let name = "seventeen"

    interface Common.ISolution with
        member __.Name() = name |> Common.titleCase
        member __.LoadData(_) : Common.Input option = [||] |> Some
        member __.PartOne(_: Common.Input) : string = partOne Permutations Steps |> string
        member __.PartTwo(_: Common.Input) : string = partTwo 50_000_000 Steps |> string
