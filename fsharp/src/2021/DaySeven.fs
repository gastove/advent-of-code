module DaySeven

open FSharp.Core.Operators.Checked

let computeSteadyFuelForPosition (pos: int) (start: int) =
    start - pos |> abs

let computeCrabEngineeringFuelForPosition (pos: int) (start: int) =
    let rec compute (remainingSteps: int) (fuelCost: int) =
        if remainingSteps = 0 then
            0
        else
            fuelCost + (compute (remainingSteps - 1) (fuelCost + 1))

    compute (start - pos |> abs) 1

let computeFuelForCrabs (crabs: int array) (pos: int) =
    crabs |> Array.sumBy (computeSteadyFuelForPosition pos)

let computeFuelForCrabsTheCrabWay (crabs: int array) (pos: int) =
    crabs |> Array.sumBy (computeCrabEngineeringFuelForPosition pos)

let cheapestPosition (crabs: int array) =
    let topCrab = crabs |> Array.max

    [|0 .. topCrab |]
    |> Array.map (fun pos ->
                  let fuel = computeFuelForCrabs crabs pos
                  pos, fuel)
    |> Array.minBy snd

let cheapestCrabEngineeredPosition (crabs: int array) =
    let topCrab = crabs |> Array.max

    [|0 .. topCrab |]
    |> Array.map (fun pos ->
                  let fuel = computeFuelForCrabsTheCrabWay crabs pos
                  pos, fuel)
    |> Array.minBy snd

let partOne (input: Common.Input) =
    input.[0].Split(',')
    |> Array.map int
    |> cheapestPosition
    |> fun (pos, fuel) -> sprintf "Position %i can be reached with %i fuel" pos fuel

let partTwo (input: Common.Input) =
    input.[0].Split(',')
    |> Array.map int
    |> cheapestCrabEngineeredPosition
    |> fun (pos, fuel) -> sprintf "Position %i can be reached with %i fuel" pos fuel

type Solution() =
    let name = "seven"
    interface Common.ISolution with
        member __.Name() = name |> Common.titleCase
        member __.LoadData(year): Common.Input option = Common.loadData name year
        member __.PartOne(input: Common.Input): string = input |> partOne
        member __.PartTwo(input: Common.Input): string = input |> partTwo
