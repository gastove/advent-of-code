module DayFifteenTests

open Expecto

[<Tests>]
let partOneTests =
    testList "Testing Day Fifteen Part One"
        [ testCase "Test the base case" <| fun _ ->
              let input = [|0; 3; 6|]
              let expected = 3
              let got = DayFifteen.nthNumberSpoken 5 input

              Expect.equal got expected "These should be equal"

          testCase "Test the base case two" <| fun _ ->
              let input = [|0; 3; 6|]
              let expected = 3
              let got = DayFifteen.nthNumberSpoken 6 input

              Expect.equal got expected "These should be equal"

          testCase "Test inputs one" <| fun _ ->
              let input = [|0; 3; 6|]
              let expected = 436
              let got = DayFifteen.nthNumberSpoken 2020 input

              Expect.equal got expected "These should be equal"
          testCase "Test Input Two" <| fun _ ->
              let input = [|1; 3; 2|]
              let expected = 1
              let got = DayFifteen.nthNumberSpoken 2020 input

              Expect.equal got expected "These should be equal" ]

[<Tests>]
let partTwoTests =
    testList "Testing Day Fifteen Part Two"
        [ testCase "Test the base case" <| fun _ ->
              let input = [|0; 3; 6|]
              let expected = 3
              let got = DayFifteen.playGame input 5

              Expect.equal got expected "These should be equal"

          testCase "Test the base case two" <| fun _ ->
              let input = [|0; 3; 6|]
              let expected = 3
              let got = DayFifteen.playGame input 6

              Expect.equal got expected "These should be equal"

          testCase "Test inputs one" <| fun _ ->
              let input = [|0; 3; 6|]
              let expected = 436
              let got = DayFifteen.playGame input 2020

              Expect.equal got expected "These should be equal"
          testCase "Test Input Two" <| fun _ ->
              let input = [|1; 3; 2|]
              let expected = 1
              let got = DayFifteen.playGame input 2020

              Expect.equal got expected "These should be equal" ]
