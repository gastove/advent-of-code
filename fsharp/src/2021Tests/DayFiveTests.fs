module DayFiveTests

open Expecto

let input =
    [| "0,9 -> 5,9"
       "8,0 -> 0,8"
       "9,4 -> 3,4"
       "2,2 -> 2,1"
       "7,0 -> 7,4"
       "6,4 -> 2,0"
       "0,9 -> 2,9"
       "3,4 -> 1,4"
       "0,0 -> 8,8"
       "5,5 -> 8,2" |]

[<Tests>]
let tests =
    testList
        "Is this thing on?"
        [ testCase "Can we reproduce the part one example?"
          <| fun _ ->
              let expected = "5"
              let got = input |> DayFive.partOne

              Expect.equal got expected "We should be able to reproduce the example" ]
