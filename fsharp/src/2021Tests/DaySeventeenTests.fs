module DaySeventeenTests

open Expecto

open DaySeventeen

[<Tests>]
let partOneTests =
    testList
        "Testing Day Seventeen, PartOne"
        [ testCase "Can we reproduce the example?"
          <| fun _ ->
              // target area: x=20..30, y=-10..-5
              let target =
                  { XMin = 20
                    XMax = 30
                    YMin = -10
                    YMax = -5 }

              let got = target |> findYRange |> Array.max
              let expected = 45

              Expect.equal got expected "We should be able to reproduce the example" ]

let target =
      { XMin = 20
        XMax = 30
        YMin = -10
        YMax = -5 }

[<Tests>]
let partTwoTests =
    testList "Testing DaySeventeen, Part Two" [
        testCase "Can we reproduce the example?" <| fun _ ->
            let target =
                  { XMin = 20
                    XMax = 30
                    YMin = -10
                    YMax = -5 }

            let expected = "112"
            let got = target |> partTwo

            Expect.equal got expected "We should be able to reproduce the example"

        ]
