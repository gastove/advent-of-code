module DayTwentyTwoTests

open Expecto

open DayTwentyTwo

[<Tests>]
let grid =
    testList
        "Testing the Grid type"
        [ testCase "Initialize"
          <| fun _ ->
              let got =
                  [| "..#"; "#.."; "..." |]
                  |> DayTwentyTwo.Grid.Initialize

              let expected =
                  [ Point.Create -1 1, Clean
                    Point.Create -1 0, Infected
                    Point.Create -1 -1, Clean
                    Point.Create 0 1, Clean
                    Point.Create 0 0, Clean
                    Point.Create 0 -1, Clean
                    Point.Create 1 1, Infected
                    Point.Create 1 0, Clean
                    Point.Create 1 -1, Clean ]
                  |> Map.ofList
                  |> Grid.Create

              Expect.equal got expected "We chould be able to parse the grid" ]

[<Tests>]
let motionTests =
    testList
        "Testing Motion"
        [ testCase "Test that four right turns returns to center"
          <| fun _ ->

              let init = { X = 25; Y = 25 }
              let carrier = { Location = init; Heading = North }

              let turnedFirst = carrier.Rotate Right
              let first = turnedFirst.Move()

              let turnedSecond = first.Rotate Right
              let second = turnedSecond.Move()

              let turnedThird = second.Rotate Right
              let third = turnedThird.Move()

              let turnedFourth = third.Rotate Right
              let fourth = turnedFourth.Move()

              Expect.equal fourth carrier "We should wind up back where we were"

          testCase "Test that four left turns returns to center"
          <| fun _ ->
              let init = { X = -25; Y = -25 }
              let carrier = { Location = init; Heading = North }

              let turnedFirst = carrier.Rotate Left
              let first = turnedFirst.Move()
              let turnedSecond = first.Rotate Left
              let second = turnedSecond.Move()
              let turnedThird = second.Rotate Left
              let third = turnedThird.Move()
              let turnedFourth = third.Rotate Left
              let fourth = turnedFourth.Move()

              Expect.equal fourth carrier "We should wind up back where we were" ]

[<Tests>]
let doPartOneTicksTests =
    testList
        "Testing doTicks"
        [ testCase "Reproduce the Example, 7 ticks"
          <| fun _ ->
              let grid =
                  [| "..#"; "#.."; "..." |]
                  |> DayTwentyTwo.Grid.Initialize

              let expected = 5
              let ticks = 7

              Expect.equal
                  (DayTwentyTwo.doPartOneTicks ticks grid)
                  expected
                  "We should be able to reproduce the beginning of the example"

          testCase "Reproduce the Example, 70 ticks"
          <| fun _ ->
              let grid =
                  [| "..#"; "#.."; "..." |]
                  |> DayTwentyTwo.Grid.Initialize

              let expected = 41
              let ticks = 70

              Expect.equal
                  (DayTwentyTwo.doPartOneTicks ticks grid)
                  expected
                  "We should be able to reproduce the middle of the example"

          testCase "Reproduce the Example, 10,000 ticks"
          <| fun _ ->
              let grid =
                  [| "..#"; "#.."; "..." |]
                  |> DayTwentyTwo.Grid.Initialize

              let expected = 5587
              let ticks = 10_000

              Expect.equal
                  (DayTwentyTwo.doPartOneTicks ticks grid)
                  expected
                  "We should be able to reproduce the end of the example" ]

[<Tests>]
let doPartTwoTicksTests =
    testList
        "Testing doTicks"
        [ testCase "Reproduce the Part Two Example, 100 ticks"
          <| fun _ ->
              let grid =
                  [| "..#"; "#.."; "..." |]
                  |> DayTwentyTwo.Grid.Initialize

              let expected = 26
              let ticks = 100

              Expect.equal
                  (DayTwentyTwo.doPartTwoTicks ticks grid)
                  expected
                  "We should be able to reproduce the first example"

          // testCase "Reproduce the Part Two Example, 10 Million Ticks"
          // <| fun _ ->
          //     let grid =
          //         [| "..#"; "#.."; "..." |]
          //         |> DayTwentyTwo.Grid.Initialize

          //     let expected = 2_511_944

          //     let ticks = 10_000_000

          //     Expect.equal
          //         (DayTwentyTwo.doPartTwoTicks ticks grid)
          //         expected
          //         "We should be able to reproduce the second example."
          ]
