module DayThirteen

open FSharp.Core.Operators.Checked

type Direction =
    | Up
    | Down

type Layer =
    { Depth: int
      ScannerPosition: int
      ScannerTravel: Direction }

    static member Create depth =
        { Depth = depth
          ScannerPosition = 0
          ScannerTravel = Down }

    member private this.NextScannerPosition() =
        match this.ScannerTravel with
        | Up ->
            if this.ScannerPosition - 1 >= 0 then
                this.ScannerPosition - 1, Up
            else
                this.ScannerPosition + 1, Down
        | Down ->
            if this.ScannerPosition + 1 < this.Depth then
                this.ScannerPosition + 1, Down
            else
                this.ScannerPosition - 1, Up

    member this.IsScannerAtZero = this.ScannerPosition = 0

    member this.Tick() =
        let newScannerPos, travelDirection = this.NextScannerPosition()

        { this with
              ScannerPosition = newScannerPos
              ScannerTravel = travelDirection }

module Layer =
    let tick (layer: Layer) = layer.Tick()
    let isScannerAtZero (layer: Layer) = layer.IsScannerAtZero

type Firewall =
    { Layers: Layer option list }

    static member Create layers = { Layers = layers }

    member this.IsScannerAtZeroInLayer layer =
        this.Layers
        |> List.item layer
        |> Option.map Layer.isScannerAtZero
        |> Option.defaultValue false

    member this.GetDepthAtLayer(layer: int) =
        this.Layers
        |> List.item layer
        |> Option.map (fun l -> l.Depth)
        |> Option.get

    member this.Tick() =
        { Layers = this.Layers |> List.map (Option.map Layer.tick) }

let parseInput (input: Common.Input) =
    input
    |> Array.map
        (fun s ->
            s.Split(":")
            |> Array.map (fun s -> s.Trim() |> int)
            |> (fun parts -> parts.[0], parts.[1]))

let constructLayers (layerData: (int * int) array) =
    let numberOfLayers =
        layerData |> Array.maxBy fst |> fst |> (+) 1

    let layerArray: Layer option array = Array.create numberOfLayers None

    layerData
    |> Array.fold
        (fun layers (layerNo, layerDepth) ->
            let layer = Layer.Create layerDepth |> Some
            Array.set layers layerNo layer

            layers)
        layerArray
    |> Array.toList

let runPaket (firewall: Firewall) =
    let rec progress (packetPosition: int) (fw: Firewall) =
        if packetPosition = fw.Layers.Length then
            []
        else
            let severity =
                if fw.IsScannerAtZeroInLayer packetPosition then
                    packetPosition
                    * (packetPosition |> fw.GetDepthAtLayer)
                else
                    0

            severity
            :: progress (packetPosition + 1) (fw.Tick())

    progress 0 firewall

let packetNeverGetsCaught (firewall: Firewall) =
    let rec progress (packetPosition: int) (fw: Firewall) =
        if packetPosition = fw.Layers.Length then
            []
        else
            let caught = fw.IsScannerAtZeroInLayer packetPosition

            caught
            :: progress (packetPosition + 1) (fw.Tick())

    progress 0 firewall |> List.reduce (||) |> not

let partOne (input: Common.Input) =
    input
    |> parseInput
    |> constructLayers
    |> Firewall.Create
    |> runPaket
    |> List.sum

let rec computeZeroSev (fw: Firewall) (delay: int) =
    let madeIt = packetNeverGetsCaught fw

    if madeIt then
        delay
    else
        computeZeroSev (fw.Tick()) (delay + 1)

let partTwo (input: Common.Input) =
    let firewall =
        input
        |> parseInput
        |> constructLayers
        |> Firewall.Create

    computeZeroSev firewall 0

type Solution() =
    let name = "thirteen"

    interface Common.ISolution with
        member __.Name() = name |> Common.titleCase
        member __.LoadData(year) : Common.Input option = Common.loadData name year
        member __.PartOne(input: Common.Input) : string = input |> partOne |> string
        member __.PartTwo(input: Common.Input) : string = input |> partTwo |> string
