module DayEight

open FSharp.Core.Operators.Checked

// Parsing input like:
// b inc 5 if a > 1
// a inc 1 if b < 5
// c dec -10 if a >= 1
// c inc -20 if c == 10

type Operation =
    | Increment of string * int
    | Decrement of string * int

    static member FromString(s: string) =
        let parts =
            s.Split() |> Array.map (fun s -> s.Trim())

        match parts with
        | [| target; "inc"; amount |] -> Increment(target, (amount |> int))
        | [| target; "dec"; amount |] -> Decrement(target, (amount |> int))
        | bad -> failwithf "Invalid input to Operation: %A" bad

type Test =
    | GT of string * int
    | LT of string * int
    | EQ of string * int
    | GTorEQ of string * int
    | LTorEQ of string * int
    | NEQ of string * int

    static member FromString(s: string) =
        let parts = s |> Common.splitAndTrim

        match parts with
        | [| target; ">"; value |] -> GT(target, value |> int)
        | [| target; "<"; value |] -> LT(target, value |> int)
        | [| target; "=="; value |] -> EQ(target, value |> int)
        | [| target; ">="; value |] -> GTorEQ(target, value |> int)
        | [| target; "<="; value |] -> LTorEQ(target, value |> int)
        | [| target; "!="; value |] -> NEQ(target, value |> int)
        | bad -> failwithf "Invalid input to Test: %A" bad

type RegisterBank =
    { Data: Map<string, int> }
    static member Create() = { Data = Map.empty }

    member this.GetValueOfRegister registerName =
        this.Data
        |> Map.tryFind registerName
        |> Option.defaultValue 0

    member private this.MutateRegister registerName (amount: int) fn =
        let currVal = this.GetValueOfRegister registerName
        let newVal = fn currVal amount
        let newData = this.Data |> Map.add registerName newVal

        { Data = newData }

    member this.IncrementRegister registerName amount =
        this.MutateRegister registerName amount (+)

    member this.DecrementRegister registerName amount =
        this.MutateRegister registerName amount (-)

    member this.LargestValue() =
        this.Data |> Map.toArray |> Array.maxBy snd |> snd

    member this.IsEmpty() = this.Data.IsEmpty

let parseLine (line: string) : Operation * Test =
    let parts =
        line.Split("if") |> Array.map (fun s -> s.Trim())

    let operation = parts.[0] |> Operation.FromString
    let test = parts.[1] |> Test.FromString

    operation, test

let followInstruction (op: Operation) (test: Test) (bank: RegisterBank) =
    let testRegister, value, comparator =
        match test with
        | GT (r, v) -> r, v, (>)
        | LT (r, v) -> r, v, (<)
        | EQ (r, v) -> r, v, (=)
        | GTorEQ (r, v) -> r, v, (>=)
        | LTorEQ (r, v) -> r, v, (<=)
        | NEQ (r, v) -> r, v, (<>)

    let testRegisterValue = bank.GetValueOfRegister testRegister

    if comparator testRegisterValue value then
        match op with
        | Increment (register, amount) -> bank.IncrementRegister register amount
        | Decrement (register, amount) -> bank.DecrementRegister register amount
    else
        bank

let partOne (input: Common.Input) =
    let instructions = input |> Array.map parseLine

    let registerBank = RegisterBank.Create()

    let finalBank =
        instructions
        |> Array.fold (fun bank (op, test) -> followInstruction op test bank) registerBank

    finalBank.LargestValue()

let partTwo (input: Common.Input) =
    let instructions = input |> Array.map parseLine

    let registerBank = RegisterBank.Create()

    let banks =
        instructions
        |> Array.scan (fun bank (op, test) -> followInstruction op test bank) registerBank

    let largestObserved =
        banks
        |> Array.filter (fun bank -> bank.IsEmpty() |> not)
        |> Array.maxBy (fun bank -> bank.LargestValue())

    largestObserved.LargestValue()

type Solution() =
    let name = "eight"

    interface Common.ISolution with
        member __.Name() = name |> Common.titleCase
        member __.LoadData(year) : Common.Input option = Common.loadData name year
        member __.PartOne(input: Common.Input) : string = input |> partOne |> string
        member __.PartTwo(input: Common.Input) : string = input |> partTwo |> string
