module DayTwentyThree

open System

open FSharp.Core.Operators.Checked

type IntOrString =
    | Int of int64
    | String of string
    static member FromString(s: string) =
        match s |> Common.tryInt64 with
        | Some i -> i |> Int
        | None -> s |> String


let getValue (ios: IntOrString) (register: Map<string, int64>) =
    match ios with
    | Int (i) -> i
    | String (s) -> register |> Map.find s


type Instruction =
    | Set of string * IntOrString
    | Sub of string * IntOrString
    | Multiply of string * IntOrString
    | JumpNonZero of IntOrString * IntOrString

    static member FromString(s: string) =
        let parts = s.Split([| ' ' |]) |> Array.toList

        let restToTuple rest =
            let register = rest |> List.head

            let value =
                rest
                |> List.tail
                |> List.exactlyOne
                |> IntOrString.FromString

            register, value

        match parts with
        | "set" :: rest -> rest |> restToTuple |> Set
        | "sub" :: rest -> rest |> restToTuple |> Sub
        | "mul" :: rest -> rest |> restToTuple |> Multiply
        | "jnz" :: rest ->
            rest
            |> restToTuple
            |> (fun (f, s) -> f |> IntOrString.FromString, s)
            |> JumpNonZero
        | bad -> failwithf "Got an un-parseable string: %A" bad

type Computer =
    { Registers: Map<string, int64>
      Multiplications: int64 }
    static member Create() =
        let registers =
            [ 'a' .. 'h' ]
            |> List.map (fun c -> c |> string, 0L)
            |> Map.ofList

        { Registers = registers
          Multiplications = 0L }
    // NOTE[gastove|2021-09-10] Okay. We actually need to return a thruple of
    // the updated computer, a recovered sound option, *and* a distance to jump
    // (which will usually be zero).
    member this.ApplyInstruction(instruction: Instruction) : Computer * int64 =
        match instruction with
        | Set (register, registerOrValue) ->
            let value = getValue registerOrValue this.Registers
            let updatedRegisters = Map.add register value this.Registers

            { this with
                  Registers = updatedRegisters },
            1L
        | Sub (register, registerOrValue) ->
            let value = getValue registerOrValue this.Registers

            let updatedRegisters =
                this.Registers
                |> Map.change
                    register
                    (fun found ->
                        found
                        |> Option.map (fun v -> v - value))

            { this with
                  Registers = updatedRegisters },
            1L
        | Multiply (register, registerOrValue) ->
            let value = getValue registerOrValue this.Registers

            let updatedRegisters =
                this.Registers
                |> Map.change
                    register
                    (fun found -> found |> Option.map ((*) value))

            { Registers = updatedRegisters
              Multiplications = this.Multiplications + 1L},
            1L

        | JumpNonZero (registerOrValue, registerOrJump) ->
            if getValue registerOrValue this.Registers <> 0L then
                let jumpVal = getValue registerOrJump this.Registers
                this, jumpVal
            else
                this, 1L

let runToCompletion (compy: Computer) (allInstructions: Instruction array) =
    let rec run (computer: Computer) instructionIdx =
        match Array.tryItem instructionIdx allInstructions with
        | Some (instruction) ->
            let updatedComputer, jump = computer.ApplyInstruction instruction

            run updatedComputer (instructionIdx + (jump |> int))
        | None -> computer

    run compy 0

let parseInput (input: Common.Input) =
    input |> Array.map Instruction.FromString

let partOne (input: Common.Input) =
    let computer = Computer.Create()
    let compy = input |> parseInput |> runToCompletion computer
    compy.Multiplications

// If you read the assembly instructions, we're effectively filtering for primes.
let partTwo (input: Common.Input) =
    let initialB = 81
    let scaleUp = initialB * 100 + 100_000
    [ scaleUp .. 17 .. scaleUp + 17_000]
    |> List.filter (Common.Numbers.isPrime>>not)
    |> List.length

type Solution() =
    let name = "twenty_three"

    interface Common.ISolution with
        member __.Name() = name |> Common.titleCase
        member __.LoadData(year) : Common.Input option = Common.loadData name year
        member __.PartOne(input: Common.Input) : string = input |> partOne |> string
        member __.PartTwo(input: Common.Input) : string = input |> partTwo |> string
