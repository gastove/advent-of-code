module DayEleven

open FSharp.Core.Operators.Checked

type ThrowTest =
    { DivideBy: float
      True: int
      False: int }

    static member Create divideBy t f =
        { DivideBy = divideBy
          True = t
          False = f }

type Monkey =
    { Items: float list
      ComputeWorry: float -> float
      Test: ThrowTest
      Inspections: int64 }

    static member Create items worryComputer test =
        { Items = items |> List.map (fun i -> i / 1_000_000.0) 
          ComputeWorry = worryComputer
          Test = test
          Inspections = 0 }

    member this.AddItem(item: float) =
        { this with Items = this.Items @ [ item ] }

    member this.ClearItems() = { this with Items = List.empty }

    member this.Increment(incrBy: int64) =
        { this with Inspections = this.Inspections + incrBy }

module Monkey =
    let addItem (item: float) (monkey: Monkey) = monkey.AddItem item

    let clearItems (monkey: Monkey) = monkey.ClearItems()

    let increment (incrBy: int64) (monkey: Monkey) = monkey.Increment incrBy

// Why no, I'm not writing a parser for this, thanks for asking.
let Monkeys =
    [|
       // Monkey 0:
       //   Starting items: 71, 56, 50, 73
       //   Operation: new = old * 11
       //   Test: divisible by 13
       //     If true: throw to monkey 1
       //     If false: throw to monkey 7
       Monkey.Create [ 71; 56; 50; 73 ] (fun o -> o * 11.0) (ThrowTest.Create 13 1 7)

       // Monkey 1:
       //   Starting items: 70, 89, 82
       //   Operation: new = old + 1
       //   Test: divisible by 7
       //     If true: throw to monkey 3
       //     If false: throw to monkey 6
       Monkey.Create [ 70; 89; 82 ] (fun o -> o + 1.0) (ThrowTest.Create 7 3 6)

       // Monkey 2:
       //   Starting items: 52, 95
       //   Operation: new = old * old
       //   Test: divisible by 3
       //     If true: throw to monkey 5
       //     If false: throw to monkey 4
       Monkey.Create [ 52; 95 ] (fun o -> o * o) (ThrowTest.Create 3 5 4)

       // Monkey 3:
       //   Starting items: 94, 64, 69, 87, 70
       //   Operation: new = old + 2
       //   Test: divisible by 19
       //     If true: throw to monkey 2
       //     If false: throw to monkey 6
       Monkey.Create [ 94; 64; 69; 87; 70 ] (fun o -> o + 2.0) (ThrowTest.Create 19 2 6)

       // Monkey 4:
       //   Starting items: 98, 72, 98, 53, 97, 51
       //   Operation: new = old + 6
       //   Test: divisible by 5
       //     If true: throw to monkey 0
       //     If false: throw to monkey 5
       Monkey.Create [ 98; 72; 98; 53; 97; 51 ] (fun o -> o + 6.0) (ThrowTest.Create 5 0 5)

       // Monkey 5:
       //   Starting items: 79
       //   Operation: new = old + 7
       //   Test: divisible by 2
       //     If true: throw to monkey 7
       //     If false: throw to monkey 0
       Monkey.Create [ 79 ] (fun o -> o + 7.0) (ThrowTest.Create 2 7 0)

       // Monkey 6:
       //   Starting items: 77, 55, 63, 93, 66, 90, 88, 71
       //   Operation: new = old * 7
       //   Test: divisible by 11
       //     If true: throw to monkey 2
       //     If false: throw to monkey 4
       Monkey.Create [ 77; 55; 63; 93; 66; 90; 88; 71 ] (fun old -> old * 7.0) (ThrowTest.Create 11 2 4)

       // Monkey 7:
       //   Starting items: 54, 97, 87, 70, 59, 82, 59
       //   Operation: new = old + 8
       //   Test: divisible by 17
       //     If true: throw to monkey 1
       //     If false: throw to monkey 3
       Monkey.Create [ 54; 97; 87; 70; 59; 82; 59 ] (fun old -> old + 8.0) (ThrowTest.Create 17 1 3) |]

let updateMonkeys (reducer: float -> float) (monkeys: Monkey array) (currentMonkey: int) =
    let monkey = monkeys |> Array.item currentMonkey

    let noItemsMonkey =
        monkey
        |> Monkey.clearItems
        |> Monkey.increment (monkey.Items |> List.length |> int64)

    monkeys.[currentMonkey] <- noItemsMonkey

    monkey.Items
    |> List.fold
        (fun distributedMonkeys item ->
            let newWorry = item |> monkey.ComputeWorry |> reducer

            let passTo =
                if newWorry % monkey.Test.DivideBy = 0 then
                    monkey.Test.True
                else
                    monkey.Test.False

            let monkeyToUpdate = distributedMonkeys |> Array.item passTo

            let updatedMonkey =
                monkeyToUpdate |> Monkey.addItem newWorry

            distributedMonkeys.[passTo] <- updatedMonkey
            distributedMonkeys)
        monkeys

let updateMonkeysScaled (reducer: float -> float) (monkeys: Monkey array) (currentMonkey: int) =
    let monkey = monkeys |> Array.item currentMonkey

    let noItemsMonkey =
        monkey
        |> Monkey.clearItems
        |> Monkey.increment (monkey.Items |> List.length |> int64)

    monkeys.[currentMonkey] <- noItemsMonkey

    monkey.Items
    |> List.fold
        (fun distributedMonkeys item ->
            let newWorry = item |> monkey.ComputeWorry |> reducer

            let passTo =
                if newWorry % (monkey.Test.DivideBy / 1_000_000.0) = 0 then
                    monkey.Test.True
                else
                    monkey.Test.False

            let monkeyToUpdate = distributedMonkeys |> Array.item passTo

            let updatedMonkey =
                monkeyToUpdate |> Monkey.addItem newWorry

            distributedMonkeys.[passTo] <- updatedMonkey
            distributedMonkeys)
        monkeys


module PartOne =

    let reduceWorry worry = (worry / 3.0) |> floor

    let rec conductMonkeyBusiness (rounds: int) (monkeys: Monkey array) =
        if rounds = 0 then
            monkeys
        else
            let updated =
                let maxIdx = (monkeys |> Array.length) - 1

                { 0 .. maxIdx }
                |> Seq.fold (updateMonkeys reduceWorry) monkeys

            conductMonkeyBusiness (rounds - 1) updated

module PartTwo =

    let reduceWorry worry = worry / 1_000_000.0

    let rec conductMonkeyBusiness (rounds: int) (monkeys: Monkey array) =
        if rounds = 0 then
            monkeys
        else
            let updated =
                let maxIdx = (monkeys |> Array.length) - 1

                { 0 .. maxIdx }
                |> Seq.fold (updateMonkeysScaled reduceWorry) monkeys

            conductMonkeyBusiness (rounds - 1) updated

let partOne (_: Common.Input) =
    let topTwoMonkeys =
        Monkeys
        |> PartOne.conductMonkeyBusiness 20
        |> Array.sortByDescending (fun monkey -> monkey.Inspections)
        |> Array.take 2

    match topTwoMonkeys with
    | [| f; s |] -> f.Inspections * s.Inspections |> string
    | wrong -> failwithf "I dunno what happened, but I got this mess:\n%A" wrong

let partTwo (_input: Common.Input) =
    let topTwoMonkeys =
        Monkeys
        |> PartTwo.conductMonkeyBusiness 10_000
        |> Array.sortByDescending (fun monkey -> monkey.Inspections)
        |> Array.take 2

    match topTwoMonkeys with
    | [| f; s |] -> f.Inspections * s.Inspections |> string
    | wrong -> failwithf "I dunno what happened, but I got this mess:\n%A" wrong


type Solution() =
    let name = "eleven"

    interface Common.ISolution with
        member __.Name() = name |> Common.titleCase
        member __.LoadData(_) : Common.Input option = [||] |> Some
        member __.PartOne(input: Common.Input) : string = input |> partOne
        member __.PartTwo(input: Common.Input) : string = input |> partTwo
