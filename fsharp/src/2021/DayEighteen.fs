module DayEighteen

open FSharp.Core.Operators.Checked

type SnailNumber =
    | Int of int
    | Pair of left: SnailNumber * right: SnailNumber

    static member Zero = 0 |> Int

    override this.ToString() =
        match this with
        | Int i -> i |> string
        | Pair (l, r) -> "[" + (string l) + "," + (string r) + "]"

    static member FromString(s: string) =
        let chars = s |> List.ofSeq

        let intify (chars: char list) =
            chars
            |> List.rev
            |> Array.ofList
            |> System.String
            |> int
            |> Int

        let rec parse (input: char list) (acc: char list) =
            match input with
            | [] -> intify acc, []
            | '[' :: rest ->
                let left, cont = parse rest acc
                let right, rightCont = parse cont []

                Pair(left, right), rightCont
            | ']' :: rest -> parse rest acc
            | ',' :: rest -> intify acc, rest
            | digit :: rest -> parse rest (digit :: acc)

        let got, _ = parse chars []

        got

module SnailNumber =

    let add sn1 sn2 = Pair(sn1, sn2)

    let rec addLeft (sn: SnailNumber) (num: int) =
        match sn with
        | Int i -> (i + num) |> Int
        | Pair (l, r) -> Pair(addLeft l num, r)

    let maybeAddLeft (sn: SnailNumber) (maybeAdd: int option) =
        match maybeAdd with
        | Some i -> addLeft sn i
        | None -> sn

    let rec addRight (sn: SnailNumber) (num: int) =
        match sn with
        | Int i -> (i + num) |> Int
        | Pair (l, r) -> Pair(l, addRight r num)

    let maybeAddRight (sn: SnailNumber) (maybeAdd: int option) =
        match maybeAdd with
        | Some i -> addRight sn i
        | None -> sn

    /// Unwraps the int inside a SnailNumber.Int. Panics if called with a Pair
    /// variant.
    let getInt =
        function
        | Int i -> i
        | Pair _ -> failwith "getInt can only be called on Int variants"

    /// Unwraps the SnailNumbers nested inside a Pair variant, returning them as
    /// a tuple. Panics if called with an Int variant.
    let getNested =
        function
        | Int _ -> failwith "getNested can only be called on Nested variants"
        | Pair (l, r) -> l, r

    let isPair =
        function
        | Pair _ -> true
        | _ -> false

    let isInt =
        function
        | Int _ -> true
        | _ -> false

    let split (sn: SnailNumber) =
        let wrapped = sn |> getInt

        if wrapped < 10 then
            sn
        else
            let left =
                (float wrapped / 2.0) |> floor |> int |> Int

            let right =
                (float wrapped / 2.0) |> ceil |> int |> Int

            Pair(left, right)

    let rec magnitude (sn: SnailNumber) =
        match sn with
        | Int i -> i
        | Pair (left, right) ->
            let leftMag = magnitude left
            let rightMag = magnitude right

            (3 * leftMag) + (2 * rightMag)

    let reduce (sn: SnailNumber) =
        let maybeAdd (i: int) (maybe: int option) =
            let addTo = maybe |> Option.defaultValue 0
            addTo + i

        let rec doExplode (snailNo: SnailNumber) (depth: int) =
            match snailNo with
            | Pair ((Int l as left), (Int r as right)) ->
                if depth = 4 then // Explode!
                    let newAddLeft = l |> Some
                    let newAddRight = r |> Some
                    SnailNumber.Zero, newAddLeft, newAddRight
                else
                    Pair(left, right), None, None
            | Pair (Int leftInt, (Pair (_) as rightPair)) ->
                let newRight, addLeft, addRight = doExplode rightPair (depth + 1)

                let addedLeft = maybeAdd leftInt addLeft |> Int

                Pair(addedLeft, newRight), None, addRight

            | Pair ((Pair (_) as leftPair), Int rightInt) ->
                let newLeft, addLeft, addRight = doExplode leftPair (depth + 1)

                let addedRight = maybeAdd rightInt addRight |> Int

                Pair(newLeft, addedRight), addLeft, None

            | Pair (leftPair, rightPair) ->
                // So this is the twistiest bit. We have to:
                // 1. Compute the reduction of the left pair
                // 2. Check for changes.
                // 3. If changes, handle addLeft, then return
                // 4. If no changes, compute the reduction of the right pair
                // 5. Handle the addRight (if any) and return

                let newLeft, leftAddLeft, leftAddRight = doExplode leftPair (depth + 1)

                if newLeft = leftPair then // no change, move on to the reduction of the right pair
                    let newRight, rightAddLeft, rightAddRight = doExplode rightPair (depth + 1)
                    let addedLeft = maybeAddRight newLeft rightAddLeft

                    Pair(addedLeft, newRight), None, rightAddRight
                else // changed, handle addLeft and return
                    let newRight = maybeAddLeft rightPair leftAddRight

                    Pair(newLeft, newRight), leftAddLeft, None

            | Int _ as reg -> reg, None, None

        let rec doSplit (snailNo: SnailNumber) =
            match snailNo with
            | Pair (left, right) ->
                let newLeft = left |> doSplit

                if newLeft = left then // no change
                    let newRight = right |> doSplit
                    Pair(newLeft, newRight)
                else
                    Pair(newLeft, right)
            | Int i as reg -> if i >= 10 then reg |> split else reg

        let (newSn, _, _) = doExplode sn 0

        // If the SnailNumber changed, return it, else check for splits.
        if newSn <> sn then
            newSn
        else
            newSn |> doSplit

    let snailAddition (sn1: SnailNumber) (sn2: SnailNumber) =
        let rec doReduceUntilStable (sn: SnailNumber) =
            let reduced = sn |> reduce

            if reduced = sn then
                reduced
            else
                doReduceUntilStable reduced

        add sn1 sn2 |> doReduceUntilStable

let partOne (input: Common.Input) =
    input
    |> Array.map SnailNumber.FromString
    |> Array.reduce SnailNumber.snailAddition
    |> SnailNumber.magnitude
    |> string

let partTwo (input: Common.Input) =
    let magnitudeIfDifferent sn1 sn2 =
        if sn1 = sn2 then
            0
        else
            SnailNumber.snailAddition sn1 sn2
            |> SnailNumber.magnitude

    let snumbers =
        input |> Array.map SnailNumber.FromString

    snumbers
    |> Array.collect (fun sn1 -> snumbers |> Array.map (fun sn2 -> magnitudeIfDifferent sn1 sn2))
    |> Array.max
    |> string

type Solution() =
    let name = "eighteen"

    interface Common.ISolution with
        member __.Name() = name |> Common.titleCase
        member __.LoadData(year) : Common.Input option = Common.loadData name year
        member __.PartOne(input: Common.Input) : string = input |> partOne
        member __.PartTwo(input: Common.Input) : string = input |> partTwo
