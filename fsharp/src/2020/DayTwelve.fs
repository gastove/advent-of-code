module DayTwelve

open System

type Instruction =
    | North of int
    | South of int
    | East of int
    | West of int
    | Left of int
    | Right of int
    | Forward of int
    static member From(input: string) =
        let inst = input.ToLower().[0]
        let digits = input.[1..] |> System.Int32.Parse

        match inst with
        | 'n' -> Some <| North digits
        | 's' -> Some <| South digits
        | 'e' -> Some <| East digits
        | 'w' -> Some <| West digits
        | 'l' -> Some <| Left digits
        | 'r' -> Some <| Right digits
        | 'f' -> Some <| Forward digits
        | _ -> None

let computeDirectionFrom (degrees: int) (travelAmount: int) : Instruction option =
    let mutable reduced = degrees

    while reduced > 360 do
        reduced <- reduced - 360

    while reduced < 0 do
        reduced <- 360 + reduced

    match reduced / 90 with
    | 0
    | 4 -> Some <| East travelAmount
    | 1 -> Some <| South travelAmount
    | 2 -> Some <| West travelAmount
    | 3 -> Some <| North travelAmount
    | _ -> None

type PartOneShip =
    { Heading: int
      X: int
      Y: int }

    static member New() = { Heading = 0; X = 0; Y = 0 }

    member this.Apply(instruction: Instruction) =
        match instruction with
        | North i -> { this with Y = this.Y + i }
        | South i -> { this with Y = this.Y - i }
        | East i -> { this with X = this.X + i }
        | West i -> { this with X = this.X - i }
        | Left i -> { this with Heading = this.Heading - i }
        | Right i -> { this with Heading = this.Heading + i }
        | Forward i ->
            match computeDirectionFrom this.Heading i with
            | Some inst -> this.Apply inst
            | None ->
                failwith
                <| sprintf "Wasn't able to compute an instruction from heading %i" this.Heading

    member this.Manhattan() =
        System.Math.Abs this.X + System.Math.Abs this.Y

let degreesToRadians (degrees: int) = float degrees * Math.PI / 180.0

let radiansToDegrees (radians: float) = (radians * 180.0) / Math.PI |> int

type Waypoint =
    { X: int
      Y: int }

    static member New() = { X = 10; Y = 1 }
    // Remember to work in *RADIANS*
    member this.Rotate (aroundX: int, aroundY: int) (increment: float) =
        let xDelta = this.X - aroundX |> float
        let yDelta = this.Y - aroundY |> float

        let rotatedX =
            Math.Cos(increment) * xDelta
            - Math.Sin(increment) * yDelta
            + (float aroundX)
            |> int

        let rotatedY =
            Math.Sin(increment) * xDelta
            + Math.Cos(increment) * yDelta
            + (float aroundY)
            |> int

        { X = rotatedX; Y = rotatedY }

type PartTwoShip =
    { X: int
      Y: int
      Waypoint: Waypoint }

    static member New() =
        let wp = Waypoint.New()
        { X = 0; Y = 0; Waypoint = wp }

    member this.Apply(instruction: Instruction) =
        match instruction with
        | North i ->
            { this with
                  Waypoint =
                      { this.Waypoint with
                            Y = this.Waypoint.Y + i } }
        | South i ->
            { this with
                  Waypoint =
                      { this.Waypoint with
                            Y = this.Waypoint.Y - i } }
        | East i ->
            { this with
                  Waypoint =
                      { this.Waypoint with
                            X = this.Waypoint.X + i } }
        | West i ->
            { this with
                  Waypoint =
                      { this.Waypoint with
                            X = this.Waypoint.X - i } }
        | Left i ->
            let rads = i |> degreesToRadians

            let newWaypoint =
                this.Waypoint.Rotate(this.X, this.Y) rads

            { this with Waypoint = newWaypoint }
        | Right i ->
            let rads = i |> degreesToRadians

            let newWaypoint =
                this.Waypoint.Rotate(this.X, this.Y) -rads

            { this with Waypoint = newWaypoint }
        | Forward i ->
            let xDelta = (this.Waypoint.X - this.X) * i
            let yDelta = (this.Waypoint.Y - this.Y) * i

            { X = this.X + xDelta
              Y = this.Y + yDelta
              Waypoint =
                  { X = this.Waypoint.X + xDelta
                    Y = this.Waypoint.Y + yDelta } }

    member this.Manhattan() =
        System.Math.Abs this.X + System.Math.Abs this.Y

type Solution() =
    let name = "twelve"

    interface Common.ISolution with
        member __.LoadData(year) : Common.Input option = Common.loadData name year

        member __.PartOne(input: Common.Input) : string =
            let instructions = input |> Array.choose Instruction.From

            let ship = PartOneShip.New()

            let finalShip =
                instructions
                |> Array.fold (fun (state: PartOneShip) inst -> state.Apply inst) ship

            sprintf "Final position in Manhattan Distance is %i"
            <| finalShip.Manhattan()

        member __.PartTwo(input: Common.Input) : string =
            let instructions = input |> Array.choose Instruction.From

            let ship = PartTwoShip.New()

            let finalShip =
                instructions
                |> Array.fold (fun (state: PartTwoShip) inst -> state.Apply inst) ship

            sprintf "Final position in Manhattan Distance is %i"
            <| finalShip.Manhattan()

        member __.Name() = name |> Common.titleCase
