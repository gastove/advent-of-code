module DayThreeTests

open Expecto

[<Tests>]
let testGenSequenceUntilContains =
    testList
        "Testing genSequenceUntilContains"
        [ testCase "We can generate the first 9 numbers"
          <| fun _ ->
              let containsEight = [ [ 1 ]; [ 2; 3; 4; 5; 6; 7; 8; 9 ] ]
              let got = DayThree.genSequenceUntilContains 8

              Expect.equal got containsEight "We should get back a short list of lists"

          testCase "We can generate the first 25 numbers"
          <| fun _ ->
              let containsFifteen = [ [ 1 ]; [ 2 .. 9 ]; [ 10 .. 25 ] ]
              let got = DayThree.genSequenceUntilContains 15

              Expect.equal got containsFifteen "We should get back a short list of lists" ]


[<Tests>]
let testComputeManhattanDistance =
    testList
        "Testing computeManhattanDistance"
        [ testCase "We can correctly compute the MD for value 12"
          <| fun _ ->
              let chunk =
                  DayThree.genSequenceUntilContains 12 |> List.last

              Expect.equal (DayThree.computeManhattanDistance chunk 12) 3 "We should get a known right answer"

          testCase "We can correctly compute the MD for value 23"
          <| fun _ ->
              let chunk =
                  DayThree.genSequenceUntilContains 23 |> List.last

              Expect.equal (DayThree.computeManhattanDistance chunk 23) 2 "We should get a known right answer"

          testCase "We can correctly compute the MD for value 1024"
          <| fun _ ->
              let chunk =
                  DayThree.genSequenceUntilContains 1024
                  |> List.last

              Expect.equal (DayThree.computeManhattanDistance chunk 1024) 31 "We should get a known right answer"

          ]

[<Tests>]
let testComputeSpiralPoints =
    testList
        "Testing computeSpiralPoints"
        [ testCase "We can correctly compute simple, known points"
          <| fun _ ->
              let expected =
                  [ 1, 0
                    1, 1
                    0, 1
                    -1, 1
                    -1, 0
                    -1, -1
                    0, -1
                    1, -1 ]

              let got = DayThree.computeSpiralPoints 1 0 1

              Expect.equal got expected "We should get correct points" ]
