module DayThirteenTests

open Expecto

open DayThirteen

[<Tests>]
let layerTests =
    testList
        "Testing the Layer type"
        [ testCase "Test Layer Tick"
          <| fun _ ->
              let layer = Layer.Create 2

              let got = layer.Tick()

              let expected =
                  { Depth = 2
                    ScannerPosition = 1
                    ScannerTravel = Down }

              let equivalent = layer.Tick().Tick().Tick()

              Expect.equal got expected "We should get the correct result after a single tick"
              Expect.equal equivalent got "Two more ticks should get us the same layer"

          testCase "Test that direction reversal works"
          <| fun _ ->
              let layer = Layer.Create 2

              let got = layer.Tick().Tick()

              let expected =
                  { Depth = 2
                    ScannerPosition = 0
                    ScannerTravel = Up }

              Expect.equal got expected "We should get the correct result after a single tick" ]

[<Tests>]
let packetTests =
    testList
        "Testing Part Two"
        [ testCase "Test that we can reproduce the example"
          <| fun _ ->
              let layers =
                  [ Layer.Create 3 |> Some
                    Layer.Create 2 |> Some
                    None
                    None
                    Layer.Create 4 |> Some
                    None
                    Layer.Create 4 |> Some ]

              let firewall = Firewall.Create layers

              let got = computeZeroSev firewall 0
              let expected = 10

              Expect.equal got expected "We should be able to reproduce the official example" ]
