module DayEleven

open FSharp.Core.Operators.Checked

open Common.Geometry

type Grid =
    { Data: Map<Point, int> }

    static member Create data = { Data = data }

type Octopi = Map<Point, int>

module PartOne =
    // Energy level increases
    let stepOne (octopi: Octopi) =
        octopi
        |> Map.map (fun _octopusPosition octopusEnergy -> octopusEnergy + 1)

    // Illuminate!
    let stepTwo (octopi: Octopi) =
        let rec flash (theOctopi: Octopi) (haveAlreadyFlashed: Set<Point>) =

            let maybeGoingToFlash =
                theOctopi
                |> Map.filter (fun loc energy ->
                    let energyOver9 = energy > 9

                    let hasntAlreadyFlashed =
                        haveAlreadyFlashed |> Set.contains loc |> not

                    // printfn "At location %s, energy %i should flash: %b" (string loc) energy (energyOver9 && hasntAlreadyFlashed)
                    energyOver9 && hasntAlreadyFlashed)
                |> Map.toArray
                |> Array.map fst

            // printfn "There are %i that might flash, %i that already have" maybeGoingToFlash.Length (haveAlreadyFlashed |> Set.count)

            match maybeGoingToFlash with
            | [||] -> theOctopi
            | goingToFlash ->
                let neighborsToIncrease =
                    goingToFlash |> Seq.collect Point.allNeighbors

                let flashedOctopi =
                    neighborsToIncrease
                    |> Seq.fold
                        (fun updatingOctopi neighbor ->
                            match updatingOctopi |> Map.tryFind neighbor with
                            | None -> updatingOctopi
                            | Some energy -> updatingOctopi |> Map.add neighbor (energy + 1))
                        theOctopi

                let updatedHaveAlreadyFlashed =
                    goingToFlash
                    |> Seq.fold (fun set point -> Set.add point set) haveAlreadyFlashed


                flash flashedOctopi updatedHaveAlreadyFlashed

        flash octopi Set.empty

    let stepThree (theOctopi: Octopi) =
        theOctopi
        |> Map.map (fun _loc energy -> if energy > 9 then 0 else energy)

    let runNumberOfSteps (steps: int) (octopi: Octopi) =
        let rec step (theOctopi: Octopi) (remainingSteps: int) =
            match remainingSteps with
            | 0 -> 0
            | rs ->
                let updatedOctopi =
                    theOctopi |> stepOne |> stepTwo |> stepThree

                let flashed =
                    updatedOctopi
                    |> Map.fold
                        (fun haveFlashed _loc energy ->
                            if energy = 0 then
                                haveFlashed + 1
                            else
                                haveFlashed)
                        0

                flashed + (step updatedOctopi (rs - 1))

        step octopi steps

module PartTwo =
    let runUntilAllFlash (octopi: Octopi) =
        let rec step (theOctopi: Octopi) (steps: int) =
            let updatedOctopi =
                theOctopi
                |> PartOne.stepOne
                |> PartOne.stepTwo
                |> PartOne.stepThree

            let flashed =
                updatedOctopi
                |> Map.fold
                    (fun haveFlashed _loc energy ->
                        if energy = 0 then
                            haveFlashed + 1
                        else
                            haveFlashed)
                    0

            if flashed = 100 then
                steps
            else
                (step updatedOctopi (steps + 1))

        step octopi 1

let parseInput (input: Common.Input) =
    input
    |> Array.map (fun line -> line |> Seq.toArray |> Array.map (string >> int))
    |> Array.mapi (fun yIdx row ->
        row
        |> Array.mapi (fun xIdx height -> Point.Create xIdx yIdx, height))
    |> Array.concat
    |> Map.ofArray

let partOne (input: Common.Input) =
    input
    |> parseInput
    |> PartOne.runNumberOfSteps 100
    |> string

let partTwo (input: Common.Input) =
    input
    |> parseInput
    |> PartTwo.runUntilAllFlash
    |> string

type Solution() =
    let name = "eleven"

    interface Common.ISolution with
        member __.Name() = name |> Common.titleCase
        member __.LoadData(year) : Common.Input option = Common.loadData name year
        member __.PartOne(input: Common.Input) : string = input |> partOne
        member __.PartTwo(input: Common.Input) : string = input |> partTwo
