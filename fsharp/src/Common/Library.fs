﻿module Common

type Input = string array

let show part result = printfn "Part %s\n-> %s\n" part result

type ISolution =
    abstract Name : unit -> string
    abstract PartOne : input: Input -> string
    abstract PartTwo : input: Input -> string
    abstract LoadData : string -> Input option

let runAndShowDay year (solution: ISolution) =
    solution.Name() |> printfn "Day %s!\n==========\n"

    match solution.LoadData(year) with
    | Some (input) ->
        input |> solution.PartOne |> show "One"
        input |> solution.PartTwo |> show "Two"
    | None -> printfn "The data doesn't seem to exist!"

let doAdventOfCode year (solutions: ISolution array) argv =
    let day =
        match argv |> Array.tryItem 0 with
        | Some day -> day
        | None -> failwith "No day provided!"

    let dayIdx =
        match day with
        | "one" -> 0
        | "two" -> 1
        | "three" -> 2
        | "four" -> 3
        | "five" -> 4
        | "six" -> 5
        | "seven" -> 6
        | "eight" -> 7
        | "nine" -> 8
        | "ten" -> 9
        | "eleven" -> 10
        | "twelve" -> 11
        | "thirteen" -> 12
        | "fourteen" -> 13
        | "fifteen" -> 14
        | "sixteen" -> 15
        | "seventeen" -> 16
        | "eighteen" -> 17
        | "nineteen" -> 18
        | "twenty" -> 19
        | "twentyone" -> 20
        | "twentytwo" -> 21
        | "twentythree" -> 22
        | wrong -> failwith $"Invalid Day: {wrong}."

    match solutions |> Array.tryItem dayIdx with
    | Some solution -> runAndShowDay year solution
    | None -> printfn $"Day {dayIdx + 1} hasn't been implemented yet!"

    0 // return an integer exit code


let loadData (day: string) (year: string) : Input option =
    let path =
        sprintf "%s/data/%s/day_%s.txt" (System.IO.Directory.GetCurrentDirectory()) year day

    match System.IO.File.Exists path with
    | true ->
        System.IO.File.ReadLines(path)
        |> Array.ofSeq
        |> Some
    | false -> None

let titleCase n =
    System.Globalization.CultureInfo.InvariantCulture.TextInfo.ToTitleCase(n)

let tryInt toTry =
    try
        Some(System.Int32.Parse(toTry))
    with
    | _ -> None

let tryInt64 toTry =
    try
        Some(System.Int64.Parse(toTry))
    with
    | _ -> None

let splitAndTrim (s: string) =
    s.Split() |> Array.map (fun s -> s.Trim())

module Types =
    // TODO[gastove|2022-06-06] Require qualified access
    type Either<'a, 'b> =
        | Left of 'a
        | Right of 'b

    module Either =
        let left =
            function
            | Left v -> v
            | Right _ -> failwith "function `left` given a Right"

        let right =
            function
            | Left _ -> failwith "function `right` given a Left"
            | Right v -> v

    type RangeInt32 =
        { Min: int32
          Max: int32 }
        static member Create mn mx = { Min = mn; Max = mx }

        member this.ContainsInclusive elem = elem >= this.Min && elem <= this.Max

        member this.FullyContains(other: RangeInt32) =
            this.ContainsInclusive other.Min
            && this.ContainsInclusive other.Max

        member this.Overlaps(other: RangeInt32) =
            this.ContainsInclusive other.Min
            || this.ContainsInclusive other.Max
            || other.ContainsInclusive this.Min
            || other.ContainsInclusive this.Max

        member this.IsSubsetOf(other: RangeInt32) =
            this.Min > other.Min && this.Max < other.Max

        member this.IsSupersetOf(other: RangeInt32) =
            this.Min > other.Min && this.Max > other.Max

        member this.TryFindOverlap(other: RangeInt32) : RangeInt32 option =
            if this = other then
                this |> Some
            else if this.Min >= other.Min && this.Max <= other.Max then
                this |> Some
            else if this.Min < other.Min && this.Max > other.Max then
                other |> Some
            else if this.ContainsInclusive other.Min
                    && other.ContainsInclusive this.Max then
                RangeInt32.Create other.Min this.Max |> Some
            else if this.ContainsInclusive other.Max
                    && other.ContainsInclusive this.Min then
                RangeInt32.Create this.Min other.Max |> Some
            else
                None

        member this.MakeNonOverlappingRanges(other: RangeInt32) =
            match this.TryFindOverlap other with
            | None ->
                if this.Max < other.Min then
                    [ this; other ]
                else
                    [ other; this ]
            | Some (overlap) ->
                if this = overlap then
                    [ RangeInt32.Create other.Min overlap.Min
                      RangeInt32.Create(overlap.Min + 1) (overlap.Max - 1)
                      RangeInt32.Create overlap.Max other.Max ]
                else
                    []



module Geometry =
    type Point =
        { X: int
          Y: int }

        static member Create x y = { X = x; Y = y }

        override this.ToString() = sprintf "<X=%i, Y=%i>" this.X this.Y

        member this.AdjacentNeighbors =
            let minX = this.X - 1
            let maxX = this.X + 1
            let minY = this.Y - 1
            let maxY = this.Y + 1

            seq {
                Point.Create minX this.Y
                Point.Create maxX this.Y
                Point.Create this.X minY
                Point.Create this.X maxY
            }

        member this.DiagonalNeighbors =
            let minX = this.X - 1
            let maxX = this.X + 1
            let minY = this.Y - 1
            let maxY = this.Y + 1

            seq {
                Point.Create minX minY
                Point.Create maxX minY
                Point.Create minX maxY
                Point.Create maxX maxY
            }

        member this.AllNeighbors =
            seq {
                this.AdjacentNeighbors
                this.DiagonalNeighbors
            }
            |> Seq.concat

    module Point =
        let allNeighbors (point: Point) = point.AllNeighbors

        let manhattanDistance (firstPoint: Point) (secondPoint: Point) =
            (firstPoint.X - secondPoint.X |> abs)
            + (firstPoint.Y - secondPoint.Y |> abs)

        let diagonalDistance (firstPoint: Point) (secondPoint: Point) =
            let x =
                firstPoint.X - secondPoint.X |> abs |> float

            let y =
                firstPoint.Y - secondPoint.Y |> abs |> float

            (x ** 2.0 + y ** 2.0) |> sqrt

        let diagonalFloor (firstPoint: Point) (secondPoint: Point) =
            diagonalDistance firstPoint secondPoint
            |> floor
            |> int

        let diagonalCiel (firstPoint: Point) (secondPoint: Point) =
            diagonalDistance firstPoint secondPoint
            |> ceil
            |> int

        let diagonalRound (firstPoint: Point) (secondPoint: Point) =
            diagonalDistance firstPoint secondPoint
            |> round
            |> int

    type Line =
        { Start: Point
          End: Point }

        static member Create start stop = { Start = start; End = stop }

        member this.IsVertical = this.Start.X = this.End.X

        member this.IsHorizontal = this.Start.Y = this.End.Y

        member this.AllPoints() =
            let xStep =
                if this.Start.X <= this.End.X then
                    1
                else
                    -1

            let yStep =
                if this.Start.Y <= this.End.Y then
                    1
                else
                    -1

            if this.IsVertical then
                seq {
                    for y in this.Start.Y .. yStep .. this.End.Y do
                        Point.Create this.Start.X y
                }
            else if this.IsHorizontal then
                seq {
                    for x in this.Start.X .. xStep .. this.End.X do
                        Point.Create x this.Start.Y
                }
            else
                let ys =
                    seq {
                        for y in this.Start.Y .. yStep .. this.End.Y do
                            y
                    }

                let xs =
                    seq {
                        for x in this.Start.X .. xStep .. this.End.X do
                            x
                    }

                Seq.zip xs ys
                |> Seq.map (fun (x, y) -> Point.Create x y)

module Maps =
    let addIfNotExists<'K, 'V when 'K: comparison> (key: 'K) (value: 'V) (table: Map<'K, 'V>) =
        match table |> Map.tryFind key with
        | None -> table |> Map.add key value
        | Some setVal -> failwithf "Failed to overwrite key %A with value %A, already set to %A" key value setVal

module Option =
    // let mapNone<'t> (f: unit -> 't) (opt: 't option) =
    //     match opt with
    //     | None -> f () |> Option.ofObj
    //     | other -> other

    let bindNone<'t> (f: unit -> 't option) (opt: 't option) =
        match opt with
        | None -> f ()
        | other -> other

module Lists =
    let mutateElementsAtIndex mutations theList =
        theList
        |> List.mapi (fun idx elem ->
            mutations
            |> Map.tryFind idx
            |> Option.defaultValue elem)

    let butLast (list: 'a list) = list.[0..list.Length - 2]

module Seqs =
    let butLast s =
        let numItems = s |> Seq.length
        s |> Seq.take (numItems - 1)

module Seq =
    let rec cycle items =
        seq {
            yield! items
            yield! cycle items
        }

module Numbers =
    let isPrime num =
        let squareRoot = System.Math.Sqrt(float num)

        [| 2 .. squareRoot |> System.Math.Ceiling |> int |]
        |> Array.tryPick (fun i -> if num % i = 0 then Some(i) else None)
        |> Option.isNone

    let int32FromBinaryString (input: string) = System.Convert.ToInt32(input, 2)

    let int64FromBinaryString (input: string) = System.Convert.ToInt64(input, 2)

module Strings =

    let blank s = System.String.IsNullOrWhiteSpace(s)

    let notBlank s =
        System.String.IsNullOrWhiteSpace(s) |> not

    let toTitleCase =
        System.Globalization.CultureInfo.InvariantCulture.TextInfo.ToTitleCase

    let trim (s: string) = s.Trim()

module DataStructures =

    type PriorityQueue<'Item when 'Item: equality> =
        { Data: (int * 'Item) list }

        static member Create data = { Data = data |> List.sortBy fst }

        member this.Push item priority =
            let data = (priority, item) :: this.Data
            { Data = data |> List.sortBy fst }

        member this.Pop() =
            let head = this.Data |> List.head |> snd

            let tail =
                this.Data
                |> List.tail
                |> PriorityQueue.Create<'Item>

            head, tail

        member this.Contains elem =
            this.Data
            |> List.tryFind (fun (_priority, item) -> item = elem)
            |> Option.isSome

        member this.IsEmpty = this.Data.IsEmpty

    type Edge =
        | Left
        | Right
        | Top
        | Bottom

        static member All = [ Left; Right; Top; Bottom ]

        member this.Inverse =
            match this with
            | Top -> Bottom
            | Left -> Right
            | Right -> Left
            | Bottom -> Top

    module Edge =
        let inverse (edge: Edge) = edge.Inverse

    type Grid<'Item> =
        { Data: 'Item array array
          Id: string }

        static member Create<'Item> id (data: 'Item array array) = { Data = data; Id = id }
        static member Empty<'Item> id : Grid<'Item> = { Id = id; Data = Array.empty }

        member this.Rotate() =
            this.Data
            |> Array.head
            |> Array.mapi (fun idx _ ->
                this.Data
                |> Array.map (Array.item idx)
                |> Array.rev)
            |> Grid.Create this.Id

        member this.Flip() : Grid<'Item> =
            this.Data
            |> Array.map Array.rev
            |> Grid.Create<'Item> this.Id

        /// Merge two Grid<_>s together along the given edge. The edge is
        /// interpreted as referring to the instance Merge is being called from.
        /// That is: grid One.Merge Right gridTwo will merge the Right edge of
        /// gridOne with the *Left* edge of gridTwo.
        member this.Merge<'Item> (id: string) (edge: Edge) (other: Grid<'Item>) =
            match edge with
            | Top ->
                [| other.Data; this.Data |]
                |> Array.concat
                |> Grid.Create<'Item> id
            | Left ->
                Array.map2 (fun a1 a2 -> [| a1; a2 |] |> Array.concat) other.Data this.Data
                |> Grid.Create<'Item> id
            | Right ->
                Array.map2 (fun a1 a2 -> [| a1; a2 |] |> Array.concat) this.Data other.Data
                |> Grid.Create<'Item> id
            | Bottom ->
                [| this.Data; other.Data |]
                |> Array.concat
                |> Grid.Create<'Item> id

        member this.GetEdge(edge: Edge) =
            match edge with
            | Top -> this.Data.[0]
            | Left -> this.Data |> Array.map (Array.item 0)
            | Right -> this.Data |> Array.map Array.last
            | Bottom -> this.Data |> Array.last

    module Grid =
        let rotate (grid: Grid<_>) = grid.Rotate()
        let flip (grid: Grid<_>) = grid.Flip()

        let getEdge (edge: Edge) (grid: Grid<_>) = grid.GetEdge edge

        let getEdgeAndRotation (edge: Edge) (grid: Grid<_>) =
            let firstEdge = getEdge edge grid
            let opposite = getEdge (edge.Inverse) grid |> Array.rev

            firstEdge, opposite

        let getAllEdges (grid: Grid<_>) =
            Edge.All
            |> List.map (fun e -> getEdge e grid)
            |> List.collect (fun e -> [ e; e |> Array.rev ])
            |> Set.ofList

        let getAllEdgePairs (grid: Grid<_>) =
            Edge.All
            |> List.map (fun e -> e, getEdge e grid)
            |> List.map (fun (edgeLabel, gridEdge) -> edgeLabel, gridEdge, gridEdge |> Array.rev)

        let hasMatchingEdge<'T when 'T: comparison> (firstGrid: Grid<'T>) (secondGrid: Grid<'T>) =
            let firstEdges = getAllEdges firstGrid
            let secondEdges = getAllEdges secondGrid

            Set.intersect firstEdges secondEdges |> Set.count > 0

        /// Try to find an edge that matches between two Grid<'T>. Returns an
        /// Option<Edge, boolean> from the perspective of *firstGrid*. The
        /// boolean parameter indicates whether the matching edge on secondGrid
        /// has to reversed in order to match. For example:
        let tryFindMatchingEdge<'T when 'T: equality> (firstGrid: Grid<'T>) (secondGrid: Grid<'T>) =
            let baseEdges =
                firstGrid
                |> getAllEdgePairs
                |> List.map (fun (edgeLabel, forward, _backward) -> edgeLabel, forward)
                |> Map.ofList

            let comparisonEdges =
                secondGrid
                |> getAllEdgePairs
                |> List.map (fun (edgeLabel, forward, backward) -> edgeLabel, (forward, backward))
                |> Map.ofList

            Edge.All
            |> List.tryPick (fun edge ->
                let firstF = baseEdges |> Map.find edge
                let compF, compR = comparisonEdges |> Map.find edge

                if firstF = compF then
                    Some(edge, false)
                else if firstF = compR then
                    Some(edge, true)
                else
                    None)

// let rec checkEdge (edges: Edge list) =
//     match edges with
//     | edge :: remainingEdges ->
//         let firstEdge = getEdge edge firstGrid
//         let (secondEdge, secondEdgeRotation) = getEdgeAndRotation edge secondGrid
//         printfn $"Comparing %A{firstEdge} with %A{secondEdge}"

//         if firstEdge = secondEdge
//            || firstEdge = secondEdgeRotation then
//             edge |> Some
//         else
//             checkEdge remainingEdges
//     | [] -> None

// checkEdge Edge.All

module Graph =

    type Direction =
        | FirstToSecond
        | SecondToFirst
        | Bidirectional

    type Edge<'Node when 'Node: equality and 'Node: comparison> =
        { FirstNode: 'Node
          SecondNode: 'Node
          Weight: int
          Direction: Direction }
        static member Create (firstNode: 'Node) (secondNode: 'Node) weight direction =
            { FirstNode = firstNode
              SecondNode = secondNode
              Weight = weight
              Direction = direction }

        member this.IsForNodes firstNode secondNode =
            (firstNode = this.FirstNode
             && secondNode = this.SecondNode)
            || (secondNode = this.FirstNode
                && firstNode = this.SecondNode)

        member this.IsForNode node =
            this.FirstNode = node || this.SecondNode = node

        member this.CanTravelFrom node =
            this.Direction = Bidirectional
            || (this.FirstNode = node
                && this.Direction = FirstToSecond)
        // NOTE[gastove|2021-12-22] I think this is wrong.
        // || (this.SecondNode = node
        //     && this.Direction = SecondToFirst)

        member this.TryGetOtherNode node =
            if this.FirstNode = node then
                this.SecondNode |> Some
            else if this.SecondNode = node then
                this.FirstNode |> Some
            else
                None

        member this.GetOtherNode node =
            match this.TryGetOtherNode node with
            | Some (n) -> n
            | None -> failwithf "Edge %A contains no member %A" this node

    module Edge =
        let isForNodes firstNode secondNode (edge: Edge<_>) = edge.IsForNodes firstNode secondNode

        let isForNode node (edge: Edge<_>) = edge.IsForNode node

        let canTravelDirectlyFrom node (edge: Edge<_>) = edge.CanTravelFrom node

        let getOtherNode node (edge: Edge<_>) = edge.GetOtherNode node

        let tryGetOtherNode node (edge: Edge<_>) = edge.TryGetOtherNode node


    type Graph<'Node when 'Node: equality and 'Node: comparison> =
        { Nodes: 'Node array
          Edges: Edge<'Node> array }

        static member Create nodes edges = { Nodes = nodes; Edges = edges }

        member this.AddNode node = seq { node } |> this.AddNodes

        member this.AddNodes nodes =
            { this with Nodes = this.Nodes |> Seq.append nodes |> Seq.toArray }

        member this.AddEdge edge = seq { edge } |> this.AddEdges

        member this.AddEdges edges =
            { this with Edges = this.Edges |> Seq.append edges |> Seq.toArray }

        member this.ShareEdge firstNode secondNode =
            this.Edges
            |> Seq.exists (Edge.isForNodes firstNode secondNode)

        member this.Neighbors node =
            this.Edges
            |> Seq.filter (fun edge ->
                Edge.isForNode node edge
                && Edge.canTravelDirectlyFrom node edge)

    module Graph =

        let construct<'Node when 'Node: equality and 'Node: comparison>
            (nodes: 'Node array)
            (edgeFn: 'Node -> Edge<'Node> array)
            =
            let edges = nodes |> Array.collect edgeFn

            Graph<'Node>.Create nodes edges

        // type AStarPath<'Node when 'Node: equality> = { Previous: 'Node }

        module PathFinding =

            let reconstructPath (current: 'Node) (cameFrom: Map<'Node, 'Node>) =
                let rec reconstruct (active: 'Node) (path: 'Node list) =
                    match cameFrom |> Map.tryFind active with
                    | Some nextNode -> reconstruct nextNode (active :: path)
                    | None -> (active :: path)

                reconstruct current List.empty


            let aStar<'Node when 'Node: equality and 'Node: comparison>
                (start: 'Node)
                (goal: 'Node)
                (heuristic: 'Node -> int)
                (graph: Graph<'Node>)
                =
                let openSet =
                    DataStructures.PriorityQueue<'Node>.Create [ (0, start) ]

                let initialGScore = Map.empty |> Map.add start 0

                let initialFScore =
                    Map.empty |> Map.add start (heuristic start)

                let rec findPath
                    (path: DataStructures.PriorityQueue<'Node>)
                    gScore
                    fScore
                    (cameFrom: Map<'Node, 'Node>)
                    =
                    if path.IsEmpty then
                        None
                    else
                        let (current, pathWithoutCurrent) = path.Pop()

                        if current = goal then
                            reconstructPath current cameFrom |> Some
                        else
                            let neighbors = current |> graph.Neighbors

                            let currentGScore =
                                Map.tryFind current gScore
                                |> Option.defaultValue System.Int32.MaxValue

                            let (newPath, newGScore, newFScore, newCameFrom) =
                                neighbors
                                |> Seq.fold
                                    (fun ((p: DataStructures.PriorityQueue<'Node>), gs, fs, cfrm) edge ->

                                        let weightToNeighbor = edge.Weight
                                        let tentative = currentGScore + weightToNeighbor

                                        let neighbor = edge.GetOtherNode current

                                        let neighborGScore =
                                            gs
                                            |> Map.tryFind neighbor
                                            |> Option.defaultValue System.Int32.MaxValue

                                        if tentative < neighborGScore then
                                            let newCameFrom = cfrm |> Map.add neighbor current
                                            let newGScores = gs |> Map.add neighbor tentative
                                            let newFScore = tentative + (heuristic neighbor)
                                            let newFScores = fs |> Map.add neighbor newFScore

                                            if p.Contains neighbor |> not then
                                                let newPath = p.Push neighbor newFScore

                                                newPath, newGScores, newFScores, newCameFrom
                                            else
                                                p, newGScores, newFScores, newCameFrom
                                        else
                                            p, gs, fs, cfrm)
                                    (pathWithoutCurrent, gScore, fScore, cameFrom)

                            findPath newPath newGScore newFScore newCameFrom

                findPath openSet initialGScore initialFScore Map.empty

module AoCHelpers =
    open Geometry
    open Graph

    /// For Advent of Code puzzles, Graphs are often provided with node weights
    /// instead of edge weights. This function computes edge weight from node
    /// weights by using two directed edges. Between nodes A(aw) and B(bw),
    /// where aw/bw are weights, the weight from A->B is bw, B->A is aw.
    let adjacentEdgeFn (weightMap: Map<Point, int>) (point: Point) =
        let pointToNeighbor = weightMap |> Map.find point

        let neighbors =
            point.AdjacentNeighbors
            |> Seq.filter (fun neighbor -> weightMap |> Map.containsKey neighbor)

        neighbors
        |> Seq.collect (fun n ->
            let neighborToPoint = weightMap |> Map.find n

            seq {
                Edge.Create point n neighborToPoint FirstToSecond
                Edge.Create n point pointToNeighbor SecondToFirst
            })
        |> Seq.toArray

    let constructAdjacentGraphFromStandardInput (input: Input) =
        let pointsAndWeights =
            input
            |> Array.mapi (fun yIdx row ->
                row
                |> Seq.toArray
                |> Array.mapi (fun xIdx weight -> { X = xIdx; Y = yIdx }, weight |> string |> int))
            |> Array.concat

        let points = pointsAndWeights |> Array.map fst

        let weightMap = pointsAndWeights |> Map

        Graph.construct points (adjacentEdgeFn weightMap), weightMap

    let constructAdjacentGraphFromParsedInput (input: int array array) =
        let pointsAndWeights =
            input
            |> Array.mapi (fun yIdx row ->
                row
                |> Array.mapi (fun xIdx weight -> { X = xIdx; Y = yIdx }, weight))
            |> Array.concat

        let points = pointsAndWeights |> Array.map fst

        let weightMap = pointsAndWeights |> Map

        Graph.construct points (adjacentEdgeFn weightMap), weightMap

