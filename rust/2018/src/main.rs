use std::env;

extern crate im;
extern crate regex;
extern crate rayon;

#[macro_use]
extern crate lazy_static;

mod util;
mod day_one;
mod day_two;
mod day_three;
mod day_four;

fn dispatch_day(day: String) {
    match day.as_ref() {
        "one" => day_one::run(),
        "two" => day_two::run(),
        "three" => day_three::run(),
        "four" => day_four::run(),
        _ => println!("Couldn't match."),
    }
}

fn main() {
    println!("Hello, world!");
    let mut passed_args = env::args();
    match passed_args.nth(1) {
        Some(dispatch) => dispatch_day(dispatch),
        None => println!("No args passed :|"),
    }
}
