module DayTwentyTwo

open FSharp.Core.Operators.Checked

let between minimum maximum num = num >= minimum && num <= maximum

type Power =
    | On
    | Off

type Point =
    { X: int
      Y: int
      Z: int }
    static member Create x y z = { X = x; Y = y; Z = z }

type Toggle =
    { XMin: int
      XMax: int
      YMin: int
      YMax: int
      ZMin: int
      ZMax: int
      SetTo: Power }

    static member Create xmin xmax ymin ymax zmin zmax pwr =
        { XMin = xmin
          XMax = xmax
          YMin = ymin
          YMax = ymax
          ZMin = zmin
          ZMax = zmax
          SetTo = pwr }

    static member From(s: string) =
        let parseNumberRange (nums: string) =
            let parts = nums.Split("..")

            if parts.Length <> 2 then
                failwithf "Failed to parse %s" nums
            else
                parts.[0] |> int, parts.[1] |> int

        let parseXYZ (xyz: string) =
            match xyz.Split("=") with
            | [| "x"; numbers |]
            | [| "y"; numbers |]
            | [| "z"; numbers |] -> numbers |> parseNumberRange
            | bad -> failwithf "Dunno how, but we got %A" bad

        let (powerState, numbers) =
            match s.Split(" ") with
            | [| "on"; rest |] -> On, rest
            | [| "off"; rest |] -> Off, rest
            | bad -> failwithf "Dunno how, but we got %A" bad

        let chunks = numbers.Split(',')
        let (xmin, xmax) = chunks.[0] |> parseXYZ
        let (ymin, ymax) = chunks.[1] |> parseXYZ
        let (zmin, zmax) = chunks.[2] |> parseXYZ

        Toggle.Create xmin xmax ymin ymax zmin zmax powerState

    member this.Contains(point: Point) =
        between this.XMin this.XMax point.X
        && between this.YMin this.YMax point.Y
        && between this.ZMin this.ZMax point.Z

    member this.Points =
        seq {
            for x in this.XMin .. this.XMax do
                for y in this.YMin .. this.YMax do
                    for z in this.ZMin .. this.ZMax -> Point.Create x y z
        }
        |> Set.ofSeq


let generateInitialMap minimum maximum =
    seq {
        for x in minimum .. maximum do
            for y in minimum .. maximum do
                for z in minimum .. maximum -> Point.Create x y z
    }
    |> Seq.map (fun p -> p, Off)
    |> Map

let parseInput (input: Common.Input) = input |> Array.map Toggle.From

let partOne (input: Common.Input) =
    input
    |> parseInput
    |> Array.fold
        (fun grid toggle ->
            grid
            |> Map.map (fun point pwr ->
                if toggle.Contains point then
                    toggle.SetTo
                else
                    pwr))
        (generateInitialMap -50 50)
    |> Map.toArray
    |> Array.map snd
    |> Array.filter (fun pwr -> pwr = On)
    |> Array.length
    |> string

module PartTwo =
    type EdgeBehavior =
        | Inclusive
        | Exclusive

    type Range3D =
        { X: Common.Types.RangeInt32
          Y: Common.Types.RangeInt32
          Z: Common.Types.RangeInt32 }

    type ActiveRanges = Range3D list

    module Range3D =

        let isSubset first second =
            let x = first.X.IsSubsetOf second.X
            let y = first.Y.IsSubsetOf second.Y
            let z = first.Z.IsSubsetOf second.Z

            x && y && z

        let isSuperset first second = true

        let overlap first second = true

    let (|Superset|Subset|Overlap|Disjoint|) ((first: Range3D), (second: Range3D)) =
        if Range3D.isSubset first second then
            Subset
        else if Range3D.isSuperset first second then
            Superset
        else if Range3D.overlap first second then
            Overlap
        else
            Disjoint

    let add (active: ActiveRanges) (range: Range3D) =
        let rec work (toCheck: ActiveRanges) (checkAgainst: Range3D) (acc: ActiveRanges) =
            match toCheck with
            | head :: remaining ->
                match head, checkAgainst with
                | Superset -> acc
                | Subset -> acc
                | Overlap -> acc
                | Disjoint -> acc
            | [] -> checkAgainst :: acc

        work active range []

    let remove (active: ActiveRanges) (range: Range3D) = ()

let partTwo (input: Common.Input) =
    input
    |> parseInput
    |> Array.Parallel.map (fun toggle -> toggle.Points, toggle.SetTo)
    |> Array.fold
        (fun (grid: Set<Point>) (points, setTo) ->
            match setTo with
            | On -> Set.union grid points
            | Off -> Set.difference grid points)
        Set.empty
    |> Set.count
    |> string

type Solution() =
    let name = "twenty_two"

    interface Common.ISolution with
        member __.Name() = name |> Common.titleCase
        member __.LoadData(year) : Common.Input option = Common.loadData name year
        member __.PartOne(input: Common.Input) : string = input |> partOne
        member __.PartTwo(input: Common.Input) : string = input |> partTwo
