module DaySeventeenTests

open Expecto

open DaySeventeen

[<Tests>]
let circularBufferTests =
    testList
        "Testing the CircularBuffer type"
        [ testCase "IndexAfterSteps"
          <| fun _ ->
              let steps = 3

              Expect.equal
                  (({ Buffer = [ 0 ]; CurrentPosition = 0 })
                      .IndexAfterSteps steps)
                  0
                  "We should wind up at the beginning"

              Expect.equal
                  (({ Buffer = [ 0; 1 ]
                      CurrentPosition = 1 })
                      .IndexAfterSteps steps)
                  0
                  "We should wind up at the beginning, again"

              Expect.equal
                  (({ Buffer = [ 0; 2; 1 ]
                      CurrentPosition = 1 })
                      .IndexAfterSteps steps)
                  1
                  "We should wind up at index 1"

          testCase "Insert"
          <| fun _ ->
              let cb =
                  { Buffer = [ 0; 2; 1 ]
                    CurrentPosition = 1 }

              let expected =
                  { Buffer = [ 0; 2; 3; 1 ]
                    CurrentPosition = 2 }

              let got = cb.Insert 2 3

              Expect.equal got expected "We should be able to insert a value correctly"
          testCase "InsertAfterSteps"
          <| fun _ ->
              let cb = CircularBuffer.Create()

              let first =
                  { Buffer = [ 0; 1 ]
                    CurrentPosition = 1 }

              let second =
                  { Buffer = [ 0; 2; 1 ]
                    CurrentPosition = 1 }

              let third =
                  { Buffer = [ 0; 2; 3; 1 ]
                    CurrentPosition = 2 }

              let fourth =
                  { Buffer = [ 0; 2; 4; 3; 1 ]
                    CurrentPosition = 2 }

              let fifth =
                  { Buffer = [ 0; 5; 2; 4; 3; 1 ]
                    CurrentPosition = 1 }

              let sixth =
                  { Buffer = [ 0; 5; 2; 4; 3; 6; 1 ]
                    CurrentPosition = 5 }

              let seventh =
                  { Buffer = [ 0; 5; 7; 2; 4; 3; 6; 1 ]
                    CurrentPosition = 2 }

              let eighth =
                  { Buffer = [ 0; 5; 7; 2; 4; 3; 8; 6; 1 ]
                    CurrentPosition = 6 }

              let ninth =
                  { Buffer = [ 0; 9; 5; 7; 2; 4; 3; 8; 6; 1 ]
                    CurrentPosition = 1 }


              let called1 = cb.InsertAfterSteps 3 1
              let called2 = called1.InsertAfterSteps 3 2
              let called3 = called2.InsertAfterSteps 3 3
              let called4 = called3.InsertAfterSteps 3 4
              let called5 = called4.InsertAfterSteps 3 5
              let called6 = called5.InsertAfterSteps 3 6
              let called7 = called6.InsertAfterSteps 3 7
              let called8 = called7.InsertAfterSteps 3 8
              let called9 = called8.InsertAfterSteps 3 9

              Expect.equal called1 first "Called once"
              Expect.equal called2 second "Called twice"
              Expect.equal called3 third "Called thrice"
              Expect.equal called4 fourth "Called four times"
              Expect.equal called5 fifth "Called five times"
              Expect.equal called6 sixth "Called six times"
              Expect.equal called7 seventh "Called seven times"
              Expect.equal called8 eighth "Called eight times"
              Expect.equal called9 ninth "Called nine times"

          testCase "Insert at end of list"
          <| fun _ ->
              let cb =
                  { Buffer = [ 0; 1 ]
                    CurrentPosition = 1 }

              let expected =
                  { Buffer = [ 0; 1; 5 ]
                    CurrentPosition = 2 }

              let got = cb.Insert 2 5
              Expect.equal got expected "We should be able to insert at the end of a list" ]
