module DayNineTests

open Expecto

open DayNine

let input =
    [| "2199943210"
       "3987894921"
       "9856789892"
       "8767896789"
       "9899965678" |]

[<Tests>]
let partOneTests =
    testList
        "Testing part one"
        [ testCase "Can we reproduce the example?"
          <| fun _ ->
              let grid =
                  input
                  |> Array.map (fun line -> line |> Seq.toArray |> Array.map (string>>int))
                  |> Grid.Create

              let expectedPoints =
                  [ Point.Create 1 0
                    Point.Create 9 0
                    Point.Create 2 2
                    Point.Create 6 4 ]
                  |> Set.ofList

              let expectedHeights = [ 1; 0; 5; 5 ] |> Set.ofList
              let (gotPoints, gotHeights) = grid.LowPoints() |> List.unzip

              Expect.equal (gotPoints |> Set.ofList) expectedPoints "We should get the correct points"
              Expect.equal (gotHeights |> Set.ofList) expectedHeights "The points should have the correct values"

         ]
