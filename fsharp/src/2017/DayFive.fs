module DayFive

open FSharp.Core.Operators.Checked

let incrAtIdx (data: int array) idx =
    let currVal = Array.get data idx
    Array.set data idx (currVal + 1)
    data

let decrAtIdx (data: int array) idx =
    let currVal = Array.get data idx
    Array.set data idx (currVal - 1)
    data

let partOne (input: Common.Input) =
    let parsedInput = input |> Array.choose Common.tryInt

    let rec followJumps (register: int array) (nextIdx: int) steps =
        let goto = Array.get register nextIdx
        let newRegister = incrAtIdx register nextIdx

        let newNext = nextIdx + goto

        if newNext > (register.Length - 1) || newNext < 0 then
            steps
        else
            followJumps newRegister newNext (steps + 1)

    followJumps parsedInput 0 1

let mutateRegister offset data idx =
    if offset >= 3 then
        decrAtIdx data idx
    else
        incrAtIdx data idx


let partTwo (input: Common.Input) =
    let parsedInput = input |> Array.choose Common.tryInt

    let rec followJumps (register: int array) (nextIdx: int) steps =
        let goto = Array.get register nextIdx
        let newRegister = mutateRegister goto register nextIdx

        let newNext = nextIdx + goto

        if newNext > (register.Length - 1) || newNext < 0 then
            steps
        else
            followJumps newRegister newNext (steps + 1)

    followJumps parsedInput 0 1


type Solution() =
    let name = "five"

    interface Common.ISolution with
        member __.Name() = name |> Common.titleCase
        member __.LoadData(year) : Common.Input option = Common.loadData name year
        member __.PartOne(input: Common.Input) : string = input |> partOne |> string
        member __.PartTwo(input: Common.Input) : string = input |> partTwo |> string
