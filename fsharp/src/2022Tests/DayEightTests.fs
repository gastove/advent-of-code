module DayEightTests

open Expecto

open DayEight

let PartOneData =
    [| "30373"
       "25512"
       "65332"
       "33549"
       "35390" |]

let PartOneParsed = PartOneData |> parseLine

[<Tests>]
let testsIHope =
    testList
        "DayEight.PartOne"
        [ testCase "Is a specific index visible from the correct sides?"
          <| fun _ ->
              let row = PartOneParsed |> Array.item 1
              let left = visibleLeft row 1
              let right = visibleRight row 1

              Expect.isTrue left "This element is visible from the left"
              Expect.isFalse right "This element is not visible from the right"

          testCase "Is a specific index correctly visible from top and bottom?"
          <| fun _ ->
              let row =
                  PartOneParsed |> Array.map (Array.item 1)

              let top = visibleLeft row 1
              let bottom = visibleRight row 1

              Expect.isTrue top "This element is visible from the top"
              Expect.isFalse bottom "This element is not visible from the bottom"

          testCase "found a bad"
          <| fun _ ->
              let got =
                  isVisible (PartOneData |> parseLine) 3 2 3

              Expect.isTrue got "This should be true"

          testCase "dont fuk up"
          <| fun _ ->
              let got = PartOneData |> partOne

              Expect.equal got "21" "hope is not a strategy" ]

[<Tests>]
let partTwoTests =
    testList
        "PartTwo.Scenery"
        [ testCase "Can we compute a correct scenic score left?"
          <| fun _ ->
              let row = PartOneParsed |> Array.item 1
              let actual = distanceToLargerLeft row 2
              let expected = 1
              Expect.equal actual expected "We should be able to compute the expected distance"

          testCase "Can we compute a correct scenic score right?"
          <| fun _ ->
              let row = PartOneParsed |> Array.item 1
              let actual = distanceToLargerRight row 2
              let expected = 2
              Expect.equal actual expected "We should be able to compute the expected distance"

          testCase "Can we compute a full score?" <| fun _ ->
            let row = 1
            let col = 2
            let expected = 4
            let got = computeScenicScore PartOneParsed row col

            Expect.equal got expected "We should be able to reproduce the example"
          ]
