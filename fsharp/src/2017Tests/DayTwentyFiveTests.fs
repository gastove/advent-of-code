module DayTwentyFiveTests

open Expecto

open DayTwentyFive

[<Tests>]
let tapeTests =
    testList
        "Testing the Tape type"
        [ testCase "Get"
          <| fun _ ->
              let tape = Tape.Empty

              Expect.equal (tape.Get -1) Zero "We should be able to resize down" ]

let runTestTape (iterations: int) =
    let blankTape = Tape.Empty

    let rec run (tape: Tape) cursor state remaining =
        if remaining = 0 then
            tape
        else
            let currentValue = tape.Get cursor
            let newRemaining = remaining - 1

            match state with
            | A ->
                match currentValue with
                | Zero ->
                    tape.Set cursor One
                    run tape (cursor + 1) B newRemaining
                | One ->
                    tape.Set cursor Zero
                    run tape (cursor - 1) B newRemaining
            | B ->
                match currentValue with
                | Zero ->
                    tape.Set cursor One
                    run tape (cursor - 1) A newRemaining
                | One ->
                    tape.Set cursor One
                    run tape (cursor + 1) A newRemaining
            | _ -> failwith "We should never hit anything but A and B"

    run blankTape 0 State.Initial iterations

[<Tests>]
let testExample =
    testList
        "Trying to reproduce the example"
        [ testCase "Example"
          <| fun _ ->
              let expected = 3
              let got = runTestTape 6

              Expect.equal got.CheckSum expected "We should be able to reproduce the example" ]
