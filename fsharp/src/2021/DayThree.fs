module DayThree

open FSharp.Core.Operators.Checked

let binaryCharArrayToInt (arr: char array) =
    let s = System.String(arr)
    System.Convert.ToInt32(s, 2)

let findMostFrequent arr = arr |> Array.countBy id |> Array.maxBy snd |> fst

let partOne (input: Common.Input) =
    let invert c = if c = '1' then '1', '0' else '0', '1'

    let gammaPieces, epsilonPieces =
        input
        |> Array.map Seq.toArray
        |> Array.transpose
        |> Array.map (findMostFrequent>>invert)
        |> Array.unzip

    let gamma = binaryCharArrayToInt gammaPieces
    let epsilon = binaryCharArrayToInt epsilonPieces

    (gamma * epsilon) |> string

let partTwo (input: Common.Input) =
    let rec sift (numbers: char array array) (criteria: (char * int) array -> char) (considerIdx: int) =
        if numbers.Length = 1 then
            numbers |> Array.exactlyOne
        else
            let consider = numbers |> Array.map (Array.item considerIdx) |> Array.countBy id
            let want = consider |> criteria
            let remaining = numbers |> Array.filter (fun arr -> arr.[considerIdx] = want)

            sift remaining criteria (considerIdx + 1)

    let oxygenCriteria (numbers: (char * int) array) =
        let ones = numbers |> Array.find (fun (c, _) -> c = '1') |> snd
        let zeroes = numbers |> Array.find (fun (c, _) -> c = '0') |> snd

        if ones >= zeroes then '1' else '0'

    let c02ScrubberCriteria (numbers: (char * int) array) =
        let ones = numbers |> Array.find (fun (c, _) -> c = '1') |> snd
        let zeroes = numbers |> Array.find (fun (c, _) -> c = '0') |> snd

        if zeroes <= ones then '0' else '1'

    let rawNumbers = input |> Array.map Seq.toArray

    let oxygenRating = sift rawNumbers oxygenCriteria 0 |> binaryCharArrayToInt
    let c02Rating = sift rawNumbers c02ScrubberCriteria 0 |> binaryCharArrayToInt

    (oxygenRating * c02Rating) |> string


type Solution() =
    let name = "three"

    interface Common.ISolution with
        member __.Name() = name |> Common.titleCase
        member __.LoadData(year) : Common.Input option = Common.loadData name year
        member __.PartOne(input: Common.Input) : string = input |> partOne
        member __.PartTwo(input: Common.Input) : string = input |> partTwo
