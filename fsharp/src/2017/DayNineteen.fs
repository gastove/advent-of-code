module DayNineteen

open FSharp.Core.Operators.Checked

let CapAlphas = [ 'A' .. 'Z' ]

type Directions =
    | Left
    | Right
    | Up
    | Down

type Point = { X: int; Y: int }

let findStartingX (input: Common.Input) =
    input.[0] |> Seq.findIndex (fun c -> c = '|')



let followPath (path: Common.Input) =
    let rec follow xpos ypos currentDirection letters steps =
        let currentChar = path.[ypos].[xpos]

        // printfn $"Processing a '{currentChar}' at position ({xpos}, {ypos}), traveling {currentDirection}"

        match currentChar with
        | '|'
        | '-' ->
            match currentDirection with
            | Up -> follow xpos (ypos - 1) currentDirection letters (steps + 1)
            | Down -> follow xpos (ypos + 1) currentDirection letters (steps + 1)
            | Left -> follow (xpos - 1) ypos currentDirection letters (steps + 1)
            | Right -> follow (xpos + 1) ypos currentDirection letters (steps + 1)
        | '+' ->
            match currentDirection with
            | Up
            | Down ->

                let newDirection, nextX =
                    match path.[ypos].[xpos - 1..xpos + 1]
                          |> Seq.findIndex (fun c -> c = '-') with
                    | 0 -> Left, xpos - 1
                    | 2 -> Right, xpos + 1
                    | _ -> failwithf "Something is weird trying to go left/right from %i, %i" xpos ypos

                follow nextX ypos newDirection letters (steps + 1)
            | Left
            | Right ->
                let upOne =
                    path
                    |> Array.tryItem (ypos - 1)
                    |> Option.bind (fun s -> s |> Seq.tryItem xpos)

                let downOne =
                    path
                    |> Array.tryItem (ypos + 1)
                    |> Option.bind (fun s -> s |> Seq.tryItem xpos)

                let newDirection, nextY =
                    match upOne, downOne with
                    | Some ('|'), Some ('|') -> failwithf "Something is weird trying to go up/down from %i,%i" xpos ypos
                    | Some ('|'), _ -> Up, ypos - 1
                    | _, Some ('|') -> Down, ypos + 1
                    | _ -> failwithf "Something is weird trying to go left/right from up"

                follow xpos nextY newDirection letters (steps + 1)

        | letter when List.contains letter CapAlphas ->
            let newX, newY =
                match currentDirection with
                | Up -> xpos, ypos - 1
                | Down -> xpos, ypos + 1
                | Left -> xpos - 1, ypos
                | Right -> xpos + 1, ypos

            follow newX newY currentDirection (letter :: letters) (steps + 1)

        | _ -> letters |> List.rev, steps

    let initX = path |> findStartingX

    follow initX 0 Down [] 0

let partOne (input: Common.Input) = input |> followPath |> fst

let partTwo (input: Common.Input) = input |> followPath |> snd

type Solution() =
    let name = "nineteen"

    interface Common.ISolution with
        member __.Name() = name |> Common.titleCase
        member __.LoadData(year) : Common.Input option = Common.loadData name year

        member __.PartOne(input: Common.Input) : string =
            input |> partOne |> Array.ofList |> System.String

        member __.PartTwo(input: Common.Input) : string = input |> partTwo |> string
