module DayTwenty

open FSharp.Core.Operators.Checked

type Pixel =
    | Light
    | Dark

    static member FromString =
        function
        | "." -> Dark
        | "#" -> Light
        | wrong -> failwithf $"Can't convert to pixel: {wrong}"

    static member FromChar =
        function
        | '.' -> Dark
        | '#' -> Light
        | wrong -> failwithf $"Can't convert to pixel: {wrong}"

    override this.ToString() =
        match this with
        | Light -> "#"
        | Dark -> "."

    member this.ToInt() =
        match this with
        | Light -> 1
        | Dark -> 0

let printPixels pixels =
    pixels
    |> Array.map (fun arr -> arr |> Array.map string |> String.concat "")
    |> String.concat "\n"

type Image =
    { Pixels: Pixel array array
      DefaultPixel: Pixel }

    static member Create defaultPixel pixels = { Pixels = pixels; DefaultPixel = defaultPixel }

    member this.GetGrid (row: int) (col: int) =
        let getPixels (starting: int) (arr: Pixel array) =
            [ starting - 1; starting; starting + 1 ]
            |> List.map (fun idx ->
                arr
                |> Array.tryItem idx
                |> Option.defaultValue this.DefaultPixel)
            |> Array.ofList

        let firstTriplet =
            this.Pixels
            |> Array.tryItem (row - 1)
            |> Option.defaultValue Array.empty
            |> getPixels col

        let secondTriplet =
            this.Pixels |> Array.item row |> getPixels col

        let thirdTriplet =
            this.Pixels
            |> Array.tryItem (row + 1)
            |> Option.defaultValue Array.empty
            |> getPixels col

        let result =
            [| firstTriplet
               secondTriplet
               thirdTriplet |]

        // let printed = result |> printPixels
        // printfn $"For row {row}, col {col}:\n{printed}"

        result

let gridToInt (grid: Pixel array array) =
    grid
    |> Array.concat
    |> Array.map (fun p -> p.ToInt() |> string)
    |> String.concat ""
    |> Common.Numbers.int32FromBinaryString

let parseImage (input: Common.Input) =
    input
    |> Array.map (fun s -> s |> Array.ofSeq |> Array.map Pixel.FromChar)
    |> Image.Create Dark

let parseAlgo (s: string) =
    s |> Seq.toArray |> Array.map Pixel.FromChar

let parseInput (input: Common.Input) =
    let algo = input |> Array.head |> parseAlgo
    let image = input |> Array.skip 2 |> parseImage

    algo, image

let convertPixel (algo: Pixel array) (img: Image) (row: int) (col: int) =
    let grid = img.GetGrid row col
    let asInt = grid |> gridToInt
    algo |> Array.item asInt

let enhanceImg converter (algo: Pixel array) (img: Image) =
    let newDefaultPixel =
        Array.create 3 img.DefaultPixel
        |> Array.create 3
        |> gridToInt
        |> fun idx -> Array.item idx algo

    img.Pixels
    |> Array.mapi (fun rowIdx row ->
        row
        |> Array.mapi (fun colIdx _ -> converter img rowIdx colIdx))
    |> Image.Create newDefaultPixel

let padWithDefault (img: Image) =
    let pixels = img.Pixels

    let width = pixels |> Array.head |> Array.length

    let topBottomPadding = Array.create width img.DefaultPixel

    let topBottomPadded =
        [| [| topBottomPadding; topBottomPadding; topBottomPadding |]
           pixels
           [| topBottomPadding; topBottomPadding; topBottomPadding |] |]
        |> Array.concat

    let twoDarks = Array.create 2 img.DefaultPixel

    let newPixels = topBottomPadded |> Array.map (fun row -> [| twoDarks; row; twoDarks |] |> Array.concat)

    { img with Pixels = newPixels }


let partOne (input: Common.Input) =
    let algo, img = input |> parseInput

    let converter = convertPixel algo
    let enhancer = enhanceImg converter algo

    let rec enhance img times =
        if times > 0 then
            let newImg = img |> padWithDefault |> enhancer
            enhance newImg (times - 1)
        else img

    let enhanced = enhance img 2

    enhanced.Pixels
    |> Array.concat
    |> Array.map (fun p -> p.ToInt())
    |> Array.sum
    |> string

let partTwo (input: Common.Input) =
    let algo, img = input |> parseInput

    let converter = convertPixel algo
    let enhancer = enhanceImg converter algo

    let rec enhance img times =
        if times > 0 then
            let newImg = img |> padWithDefault |> enhancer
            enhance newImg (times - 1)
        else img

    let enhanced = enhance img 50

    enhanced.Pixels
    |> Array.concat
    |> Array.map (fun p -> p.ToInt())
    |> Array.sum
    |> string


type Solution() =
    let name = "twenty"

    interface Common.ISolution with
        member __.Name() = name |> Common.titleCase
        member __.LoadData(year) : Common.Input option = Common.loadData name year
        member __.PartOne(input: Common.Input) : string = input |> partOne
        member __.PartTwo(input: Common.Input) : string = input |> partTwo
