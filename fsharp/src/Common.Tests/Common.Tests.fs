namespace Common.Tests

open Expecto

module Lists =

    [<Tests>]
    let listsTests =
        testList
            "Testing the Lists module"
            [ testCase "Testing mutateElementsAtIndex"
              <| fun _ ->
                  let elements = [ 1; 2; 3; 4; 5 ]
                  let mutations = [ 1, 10; 3, 9 ] |> Map.ofList
                  let expected = [ 1; 10; 3; 9; 5 ]

                  let got =
                      Common.Lists.mutateElementsAtIndex mutations elements

                  Expect.equal got expected "We should be able to mutate elements."

             ]

module Types =

    open Common.Types

    [<Tests>]
    let rangeTests =
        testList
            "Testing the RangeInt32 type"
            [ testCase "Can we detect overlap?"
              <| fun _ ->
                  let r1 = RangeInt32.Create 1 10
                  let doesOverlap = RangeInt32.Create 8 15
                  let overlapsAtMin = RangeInt32.Create -5 1
                  let overlapsAtMax = RangeInt32.Create 10 11
                  let totalSubset = RangeInt32.Create 2 8
                  let fullSuperset = RangeInt32.Create 0 15


                  Expect.isTrue (r1.Overlaps doesOverlap) "We should be able to detect a normal overlap"
                  Expect.isTrue (r1.Overlaps overlapsAtMin) "We should be able to detect only min overlaps"
                  Expect.isTrue (r1.Overlaps overlapsAtMax) "We should be able to detect only max overlaps"
                  Expect.isTrue (r1.Overlaps totalSubset) "We should correctly detect total subsets"
                  Expect.isTrue (r1.Overlaps fullSuperset) "We should be able to detect a full superset"

              testCase "Can we detect single point overlap?"
              <| fun _ ->
                  let r1 = RangeInt32.Create 1 10
                  let singlePoint = RangeInt32.Create 1 1

                  Expect.isTrue
                      (r1.Overlaps singlePoint)
                      "We should be able to detect overlaps when one range is a point"

                  Expect.isTrue
                      (singlePoint.Overlaps r1)
                      "We should be able to detect overlaps when one range is a point"


              testCase "Can we correctly fail on non-overlapping ranges?"
              <| fun _ ->
                  let r1 = RangeInt32.Create 1 10

                  let higher = RangeInt32.Create 22 30

                  Expect.isFalse (r1.Overlaps higher) "We should correctly false on a non-overlap"
                  Expect.isFalse (higher.Overlaps r1) "We should correctly false on a non-overlap"


              ]

module Geometry =

    open Common.Geometry

    [<Tests>]
    let lineTests =
        testList
            "Testing the Line type"
            [ testCase "Testing point generation for horizontal lines"
              <| fun _ ->
                  let start = { X = 0; Y = 0 }
                  let stop = { X = 5; Y = 0 }

                  let hline = Line.Create start stop

                  let expected =
                      seq {
                          { X = 0; Y = 0 }
                          { X = 1; Y = 0 }
                          { X = 2; Y = 0 }
                          { X = 3; Y = 0 }
                          { X = 4; Y = 0 }
                          { X = 5; Y = 0 }
                      }
                      |> Set.ofSeq

                  Expect.equal (hline.AllPoints() |> Set.ofSeq) expected "Can we generate points on a line"

              testCase "Testing point generation for vertical lines"
              <| fun _ ->
                  let start = { X = 0; Y = 0 }
                  let stop = { X = 0; Y = 5 }

                  let vline = Line.Create start stop

                  let expected =
                      seq {
                          { X = 0; Y = 0 }
                          { X = 0; Y = 1 }
                          { X = 0; Y = 2 }
                          { X = 0; Y = 3 }
                          { X = 0; Y = 4 }
                          { X = 0; Y = 5 }
                      }
                      |> Set.ofSeq

                  Expect.equal (vline.AllPoints() |> Set.ofSeq) expected "Can we generate points on a line"

              testCase "Testing point generation for vertical lines spelled weird"
              <| fun _ ->
                  let start = { X = 7; Y = 0 }
                  let stop = { X = 7; Y = 4 }

                  let vline = Line.Create start stop

                  let expected =
                      seq {
                          { X = 7; Y = 0 }
                          { X = 7; Y = 1 }
                          { X = 7; Y = 2 }
                          { X = 7; Y = 3 }
                          { X = 7; Y = 4 }
                      }
                      |> Set.ofSeq

                  Expect.equal (vline.AllPoints() |> Set.ofSeq) expected "Can we generate points on a line"

              testCase "Testing point generation for horizontal lines spelled weird"
              <| fun _ ->
                  let start = { X = 9; Y = 4 }
                  let stop = { X = 3; Y = 4 }

                  let vline = Line.Create start stop

                  let expected =
                      seq {
                          { X = 9; Y = 4 }
                          { X = 8; Y = 4 }
                          { X = 7; Y = 4 }
                          { X = 6; Y = 4 }
                          { X = 5; Y = 4 }
                          { X = 4; Y = 4 }
                          { X = 3; Y = 4 }
                      }
                      |> Set.ofSeq

                  Expect.equal (vline.AllPoints() |> Set.ofSeq) expected "Can we generate points on a line" ]

module Graph =

    open Common.Geometry
    open Common.Graph

    // Let's test something like:
    // 1 2 1
    // 1 5 9
    // 2 3 5

    let makeAdjacentsTestGrid () =
        let points =
            [| { X = 0; Y = 0 }
               { X = 1; Y = 0 }
               { X = 2; Y = 0 }
               { X = 0; Y = 1 }
               { X = 1; Y = 1 }
               { X = 2; Y = 1 }
               { X = 0; Y = 2 }
               { X = 1; Y = 2 }
               { X = 2; Y = 2 } |]

        let weights = [| 1; 2; 1; 1; 5; 9; 2; 3; 5 |]

        let pointWeights = Array.zip points weights |> Map

        Graph.construct points (Common.AoCHelpers.adjacentEdgeFn pointWeights)

    // 1 2 1
    // 1 5 9
    // 2 3 5

    [<Tests>]
    let aStarTests =
        testList
            "Testing the A* Implementation"
            [ testCase "Can we find a simple path?"
              <| fun _ ->
                  let expected =
                      [ { X = 0; Y = 0 }
                        { X = 0; Y = 1 }
                        { X = 0; Y = 2 }
                        { X = 1; Y = 2 }
                        { X = 2; Y = 2 } ]

                  let startingPoint = { X = 0; Y = 0 }
                  let goal = { X = 2; Y = 2 }

                  let got =
                      makeAdjacentsTestGrid ()
                      |> Graph.PathFinding.aStar startingPoint goal (Point.manhattanDistance goal)

                  Expect.isSome got "We should get a path"
                  Expect.equal (got |> Option.get) expected "We should get the expected path" ]

module DataStructures =

    open Common.DataStructures

    [<Tests>]
    let gridTests =
        testList
            "Testing Common.DataStructures.Grid"
            [ testCase "Test Grid.Rotate()"
              <| fun _ ->
                  let grid =
                      [| [| 1; 2; 3 |]
                         [| 4; 5; 6 |]
                         [| 7; 8; 9 |] |]
                      |> Grid.Create "123"

                  let expected =
                      [| [| 7; 4; 1 |]
                         [| 8; 5; 2 |]
                         [| 9; 6; 3 |] |]
                      |> Grid.Create "123"

                  let got = grid.Rotate()

                  Expect.equal expected got "We should be able to rotate a grid"

              testCase "Test Grid.Flip()"
              <| fun _ ->
                  let grid =
                      [| [| 1; 2; 3 |]
                         [| 4; 5; 6 |]
                         [| 7; 8; 9 |] |]
                      |> Grid.Create "123"

                  let expected =
                      [| [| 3; 2; 1 |]
                         [| 6; 5; 4 |]
                         [| 9; 8; 7 |] |]
                      |> Grid.Create "123"

                  let got = grid.Flip()

                  Expect.equal expected got "We should be able to rotate a grid" ]
