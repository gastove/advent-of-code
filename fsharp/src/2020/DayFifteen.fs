module DayFifteen

open FSharp.Core.Operators.Checked

let baseInput = [| 9; 6; 0; 10; 18; 2; 1 |]

let makeFolder skip =
    let partOneFolder (seen: int array) i =
        if i <= skip then
            seen
        else
            let last = Array.last seen
            let rest = seen.[0..seen.Length - 2]
            match Array.tryFindIndexBack (fun x -> x = last) rest with
            | Some idx ->
                let turn = idx + 1
                let delta = i - 1 - turn
                // printfn "Turn %i, looked for %i in %A, which was on turn %i and delta %i" i last rest turn delta
                Array.append seen [| delta |]
            | None ->
                // printfn "Turn %i, Didn't find %i in %A" i last rest
                Array.append seen [| 0 |]

    partOneFolder

let nthNumberSpoken n (input: int array) =
    let folder = makeFolder input.Length
    [| 1 .. n |]
    |> Array.fold folder input
    |> Array.last


let playGame startingInts turns =
    let startingLastSaid = Array.last startingInts
    let startingTurn = startingInts.Length + 1
    let startingState =
        startingInts.[..startingInts.Length - 2]
        |> Array.mapi (fun i x -> x, i + 1)
        |> Map.ofArray

    let rec work (state: Map<int, int>) turn lastSaid =
        let newLastSaid =
            match Map.tryFind lastSaid state with
            | Some(previouslySaidOn) ->
                let delta = turn - 1 - previouslySaidOn
                delta
            | None -> 0
        if turn = turns then
            newLastSaid
        else
            let newState = Map.add lastSaid (turn - 1) state
            work newState (turn + 1) newLastSaid

    let lastNumberSaid = work startingState startingTurn startingLastSaid

    lastNumberSaid

type Solution() =
    let name = "fifteen"
    interface Common.ISolution with
        member __.LoadData(_): Common.Input option = Some Array.empty

        member __.PartOne(_: Common.Input): string =
            let num = nthNumberSpoken 2020 baseInput

            sprintf "I say %i" num

        member __.PartTwo(_: Common.Input): string =
            let num = playGame baseInput 30_000_000

            sprintf "After a billion years, I say %i" num

        member __.Name() = name |> Common.titleCase
