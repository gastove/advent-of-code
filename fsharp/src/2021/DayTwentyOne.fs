module DayTwentyOne

open FSharp.Core.Operators.Checked

open Common

type Outcome =
    { Winner: string
      WinningScore: int
      LoosingScore: int
      Rolls: int }

    static member Create w ws ls r =
        { Winner = w
          WinningScore = ws
          LoosingScore = ls
          Rolls = r }

type DiracDice =
    { P1Position: int
      P2Position: int
      Die: int seq
      P1Score: int
      P2Score: int
      Rounds: int
      Rolls: int }

    static member Create p1 p2 die =
        { P1Position = p1
          P2Position = p2
          P1Score = 0
          P2Score = 0
          Die = die
          Rounds = 0
          Rolls = 0 }

    member this.Roll() =
        let withdraw = this.Rolls + 3
        let passOver = this.Rolls

        let got =
            this.Die |> Seq.take withdraw |> Seq.skip passOver

        got |> Seq.sum, { this with Rolls = this.Rolls + 3 }

    member _.IncrementPosition initialPos incrBy =
        let rec boilDown num =
            if num > 10 then
                boilDown (num - 10)
            else
                num

        boilDown (initialPos + incrBy)

    member this.PlayerOne() =
        let (roll, rolled) = this.Roll()

        let newP1 =
            this.IncrementPosition this.P1Position roll

        { rolled with
            P1Position = newP1
            P1Score = rolled.P1Score + newP1 }

    member this.PlayerTwo() =
        let (roll, rolled) = this.Roll()

        let newScore =
            this.IncrementPosition this.P2Position roll

        { rolled with
            P2Position = newScore
            P2Score = rolled.P2Score + newScore }

let hasWinner (game: DiracDice) =
    match game.P1Score, game.P2Score with
    | winner, looser when winner >= 1000 ->
        Outcome.Create "player_one" winner looser game.Rolls
        |> Some
    | looser, winner when winner >= 1000 ->
        Outcome.Create "player_two" winner looser game.Rolls
        |> Some
    | _, _ -> None


let rec playUntilWinner (game: DiracDice) =
    let p1played = game.PlayerOne()

    match p1played |> hasWinner with
    | Some outcome -> outcome
    | None ->
        let p2played = p1played.PlayerTwo()

        match p2played |> hasWinner with
        | Some outcome -> outcome
        | None -> p2played |> playUntilWinner

let deterministicDie =
    Seq.initInfinite (fun idx -> (idx % 100) + 1)

module PartTwo =
    let AllRolls =
        [ 1 + 1 + 1
          1 + 1 + 2
          1 + 1 + 3
          1 + 2 + 1
          1 + 2 + 2
          1 + 2 + 3
          1 + 3 + 1
          1 + 3 + 2
          1 + 3 + 3
          2 + 1 + 1
          2 + 1 + 2
          2 + 1 + 3
          2 + 2 + 1
          2 + 2 + 2
          2 + 2 + 3
          2 + 3 + 1
          2 + 3 + 2
          2 + 3 + 3
          3 + 1 + 1
          3 + 1 + 2
          3 + 1 + 3
          3 + 2 + 1
          3 + 2 + 2
          3 + 2 + 3
          3 + 3 + 1
          3 + 3 + 2
          3 + 3 + 3 ]

    let incrementPosition initialPos incrBy =
        let rec boilDown num =
            if num > 10 then
                boilDown (num - 10)
            else
                num

        boilDown (initialPos + incrBy)

    let ScoreMap =
        AllRolls
        |> List.collect (fun roll ->
            [ 1 .. 10 ]
            |> List.map (fun pos -> incrementPosition pos roll))


    let generateScores initialPos initialScore =
        AllRolls
        |> List.map (fun roll ->
            let newPos = incrementPosition initialPos roll
            newPos, initialScore + newPos)
    // |> List.partition (fun (_pos, score) -> score >= 21)

    // So here, we make something like a  Map<position * score, new position * score list>

    let generateScoreCountMap (finalScore: int) =
        let positions = [ 1 .. 10 ]

        let rec compute (startingScore: int) =
            let results =
                positions
                |> List.map (fun position -> (position, startingScore), generateScores position startingScore)

            let nextStartingScore = startingScore - 1

            if nextStartingScore < 0 then
                results
            else
                results @ compute nextStartingScore

        compute finalScore

    let computeVictoriesForStartingPointAndScore (startingPoint: int) (winAt: int) =
        let scoreMap = generateScoreCountMap winAt |> Map

        let rec play (pos: int) (currentScore: int) =
            let dimensions = scoreMap |> Map.find (pos, currentScore)

            let (winning, continuations) =
                dimensions
                |> List.partition (fun (_pos, score) -> score >= winAt)

            let wins = winning.Length |> int64

            if continuations |> List.isEmpty then
                wins
            else
                printfn $"Handling {continuations.Length} continuations"
                wins
                + (continuations
                   |> List.sumBy (fun (newPos, score) -> play newPos score))

        play startingPoint 0

// let playQuantumDice (p1Start: int) (p2Start: int) =
//     let rec play (p1Pos: int) (p1Score: int) (p2Pos: int) (p2Score: int) =
//         let (p1Victories, p1NotYets) = generateScores p1Pos p1Score
//         let (p2Victories, p2NotYets) = generateScores p2Pos p2Score

//         let (futureP1Victories, futureP2Victories) =

//         let totalP1Victories = p1Victories |> List.sum |> int64
//         let totalP2Victories = p2Victories |> List.sum |> int64

//         totalP1Victories, totalP2Victories


//     ()

let partOne (input: Common.Input) =
    let result =
        DiracDice.Create 10 3 deterministicDie
        |> playUntilWinner

    result.Rolls * result.LoosingScore |> string

let partTwo (input: Common.Input) =
    PartTwo.computeVictoriesForStartingPointAndScore 4 21
    |> string

type Solution() =
    let name = "twenty_one"

    interface Common.ISolution with
        member __.Name() = name |> Common.titleCase
        member __.LoadData(_year) : Common.Input option = [||] |> Some
        member __.PartOne(input: Common.Input) : string = input |> partOne
        member __.PartTwo(input: Common.Input) : string = input |> partTwo
