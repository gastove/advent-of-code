module DayNine

open FSharp.Core.Operators.Checked

type Point =
    { X: int
      Y: int }
    static member Create x y = { X = x; Y = y }

    member this.AdjacentPoints =
        let minX = this.X - 1
        let maxX = this.X + 1
        let minY = this.Y - 1
        let maxY = this.Y + 1

        seq {
            Point.Create minX this.Y
            Point.Create maxX this.Y
            Point.Create this.X minY
            Point.Create this.X maxY
        }

    override this.ToString() = sprintf "<X=%i, Y=%i>" this.X this.Y

type Grid =
    { Points: Map<Point, int> }

    static member Create(data: int array array) =
        let parsed =
            data
            |> Array.mapi (fun yIdx row ->
                row
                |> Array.mapi (fun xIdx height -> Point.Create xIdx yIdx, height))
            |> Array.concat
            |> Map.ofArray

        { Points = parsed }

    member this.LowPoints() =
        let rec findLowPoints (points: Point list) (acc: (Point * int) list) =
            match points with
            | [] -> acc
            | point :: remainingPoints ->
                let currentHeight = this.Points |> Map.find point

                let neighborHeights =
                    point.AdjacentPoints
                    |> Seq.choose (fun p -> this.Points |> Map.tryFind p)

                let isLowPoint =
                    neighborHeights
                    |> Seq.forall (fun neighborHeight -> neighborHeight > currentHeight)

                if isLowPoint then
                    findLowPoints remainingPoints ((point, currentHeight) :: acc)
                else
                    findLowPoints remainingPoints acc

        let pointsList =
            this.Points |> Map.toList |> List.map fst

        findLowPoints pointsList List.empty

    member this.FindBasins() =
        let rec findPointsInBasin (point: Point) (acc: Point list) =
            let accWithCurrentPoint = point :: acc
            // Remove any neighbors already in the accumulator *or* with height 9
            let maybeNeighbors =
                point.AdjacentPoints
                |> Seq.filter (fun neighborPoint ->
                    // Our neighbor generation is pretty optimistic, make sure each point is actually in set
                    match this.Points |> Map.tryFind neighborPoint with
                    | None -> false
                    | Some neighborHeight ->
                        let neighborNotInAcc =
                            accWithCurrentPoint
                            |> List.contains neighborPoint
                            |> not

                        neighborNotInAcc && neighborHeight <> 9)
                |> Seq.toList

            // printfn "Point %s has neighbors to check %s" (string point) (maybeNeighbors |> List.map string |> String.concat ", ")

            match maybeNeighbors with
            | [] -> accWithCurrentPoint
            | neighbors ->
                neighbors
                |> List.fold (fun newAcc p -> findPointsInBasin p newAcc) accWithCurrentPoint

        this.LowPoints()
        |> Seq.toList
        |> List.map (fun (point, _height) -> findPointsInBasin point [] |> Set.ofList)

let parseInput (input: Common.Input) =
    input
    |> Array.map (fun line -> line |> Seq.toArray |> Array.map (string >> int))
    |> Grid.Create


let partOne (input: Common.Input) =
    let grid = input |> parseInput

    grid.LowPoints()
    |> List.sumBy (fun (_, height) -> height + 1)
    |> string

let partTwo (input: Common.Input) =
    let grid = input |> parseInput

    grid.FindBasins()
    |> List.map Set.count
    |> List.sortDescending
    |> List.take 3
    |> List.reduce (*)
    |> string

type Solution() =
    let name = "nine"

    interface Common.ISolution with
        member __.Name() = name |> Common.titleCase
        member __.LoadData(year) : Common.Input option = Common.loadData name year
        member __.PartOne(input: Common.Input) : string = input |> partOne
        member __.PartTwo(input: Common.Input) : string = input |> partTwo
