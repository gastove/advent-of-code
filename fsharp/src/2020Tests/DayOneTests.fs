module DayOneTests

open Expecto

[<Tests>]
let testTryInt =
    testList "Testing tryInt" [
        testCase "tryInt can parse an int string nicely" <| fun _ ->
            let input = "5"
            let got = input |> DayOne.tryInt

            Expect.isSome got "This should have worked"
            Expect.equal got (Some 5) "And it should have worked correctly"

        testCase "tryInt returns none when input is silly and/or wrong" <| fun _ ->
            let wrong = "beep"
            let superWrong = "HOOBASTANK"
            let yepStillWrong = ""

            Expect.isNone (DayOne.tryInt wrong) "Input was no kind of number"
            Expect.isNone (DayOne.tryInt superWrong) "This is a terrible band, let alone a number"
            Expect.isNone (DayOne.tryInt yepStillWrong) "Empty is not a number"
    ]
