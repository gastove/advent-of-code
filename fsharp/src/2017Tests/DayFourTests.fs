module DayFourTests

open Expecto




// abcde fghij is a valid passphrase.
// abcde xyz ecdab is not valid - the letters from the third word can be rearranged to form the first word.
// a ab abc abd abf abj is a valid passphrase, because all letters need to be used when forming another word.
// iiii oiii ooii oooi oooo is valid.
// oiii ioii iioi iiio is not valid - any of these words can be rearranged to form any other word.

[<Tests>]
let testAnagramsinLine =
    testList
        "Testing anagramsInLine"
        [ testCase "Test we can compute known anagram cases"
          <| fun _ ->
              let noAnagrams = "abcde fghij"
              let alsoNoAnagrams = "iiii oiii ooii oooi oooo"
              let oneAnagram = "abcde xyz ecdab"
              let fourAnagrams = "oiii ioii iioi iiio"

              Expect.equal (DayFour.anagramsInLine noAnagrams) 0 "No anagrams"
              Expect.equal (DayFour.anagramsInLine alsoNoAnagrams) 0 "No anagrams"
              // Expect.equal (DayFour.anagramsInLine oneAnagram) 1 "Should find one"
              // Expect.equal (DayFour.anagramsInLine fourAnagrams) 4 "Should find all four"
          ]
