module DaySeven

open System

open FSharp.Core.Operators.Checked

let WeightRegex =
    Text.RegularExpressions.Regex(@"(\w+) \((\d+)\)")

let TreeRegex =
    Text.RegularExpressions.Regex(@"(\w+) \((\d+)\)(?: -> )(.+)")

type Node =
    { Name: string
      Weight: int option
      Leaves: Node list }

    static member Create name weight leaves =
        { Name = name
          Weight = weight
          Leaves = leaves }

    static member FromList(data: string list) =
        let name = data.[0]
        let weight = data.[1] |> Common.tryInt

        let leaves =
            data.[2].Split([| ',' |])
            |> Array.map (fun n -> { Name = n.Trim(); Weight = None; Leaves = [] })
            |> Array.toList

        Node.Create name weight leaves

    member this.UpdateWeight name weight =
        if this.Name = name then
            { this with Weight = weight }
        else
            let updatedLeaves =
                this.Leaves
                |> List.map (fun l -> l.UpdateWeight name weight)

            { this with Leaves = updatedLeaves }

    member this.UpdateLeaves name leaves =
        if this.Name = name then
            { this with Leaves = leaves }
        else
            let updatedLeaves =
                this.Leaves
                |> List.map (fun l -> l.UpdateLeaves name leaves)

            { this with Leaves = updatedLeaves }

    member this.HasLeafNamed name =
        let hasOwnLeaf = this.Leaves |> List.tryFind (fun n -> n.Name = name) |> Option.isSome

        if hasOwnLeaf then
            true
        else
            this.Leaves |> List.exists (fun l -> l.HasLeafNamed name)

    member this.ComputeWeights() =
        match this.Weight with
            | Some w -> w + (this.Leaves |> List.sumBy (fun n -> n.ComputeWeights()))
            | None -> failwithf "Something went wrong, node %s has no weight" this.Name


    member this.LeavesAreBalanced() =
        let counts = this.Leaves |> List.map (fun n -> n.ComputeWeights()) |> Set.ofList |> Set.count
        counts = 1

module Node =
    let computeWeights (node: Node) = node.ComputeWeights()

// We need to parse input like this:
//
// pbga (66)
// xhth (57)
// ebii (61)
// havc (66)
// ktlj (57)
// fwft (72) -> ktlj, cntj, xhth
// qoyq (66)
// padx (45) -> pbga, havc, qoyq
// tknk (41) -> ugml, padx, fwft
// jptl (61)
// ugml (68) -> gyxo, ebii, jptl
// gyxo (61)
// cntj (57)

let tryGroups (m: Text.RegularExpressions.Match) =
    if m.Success then
        Some(List.tail [ for g in m.Groups -> g.Value.Trim() ])
    else
        None

let parseWeights (input: Common.Input) =
    input
    |> Array.Parallel.choose
        (fun line ->
            WeightRegex.Match(line)
            |> tryGroups
            |> Option.map
                (fun data ->
                    Common.tryInt data.[1]
                    |> Option.map (fun i -> data.[0], i)))
    |> Array.choose id // Because of the innner Option.map, we have to unwrap a second time
    |> Map.ofArray

let parseTreeData (input: Common.Input) =
    input
    |> Array.Parallel.choose
        (fun line ->
            TreeRegex.Match(line)
            |> tryGroups
            |> Option.map Node.FromList)

let assignWeights (weightMap: Map<string, int>) (tree: Node) =
    weightMap
    |> Map.fold (fun (node: Node) nodeName weight -> node.UpdateWeight nodeName (weight |> Some)) tree

let parseInput (input: Common.Input) =
    let weightMap = input |> parseWeights
    let treeData = input |> parseTreeData

    weightMap, treeData

let assembleTree (treeData: Node array): Node =

    let rec treeBuilder (root: Node) (unassigned: Node list) =
        match unassigned with
        | [] -> root
        | head :: rest ->
            if root.HasLeafNamed head.Name then
                let newRoot = root.UpdateLeaves head.Name head.Leaves
                treeBuilder newRoot rest
            else if head.HasLeafNamed root.Name then
                let newRoot = head.UpdateLeaves root.Name root.Leaves
                treeBuilder newRoot rest
            else
                treeBuilder root (rest @ [head])

    treeBuilder (treeData |> Array.head) (treeData |> Array.toList |> List.tail)

let partOne (input: Common.Input) =
    let _, treeData = input |> parseInput

    let tree = treeData |> assembleTree

    tree.Name

type OffBalance = Low of int | High of int

let findImbalance (tree: Node) =
    let weights = tree.Leaves |> List.map Node.computeWeights

    let weightCounts = weights |> List.countBy id

    let wrongNumber = weightCounts |> List.minBy snd |> fst
    let rightNumber = weightCounts |> List.maxBy snd |> fst

    let delta = (wrongNumber - rightNumber) |> abs

    let classification = if wrongNumber > rightNumber then delta |> High else delta |> Low

    let rec rollUp (node: Node) =
        let weights = node.Leaves |> List.map Node.computeWeights
        let incorrectWeight =
            match classification with
            | High(_) -> weights |> List.max
            | Low(_) -> weights |> List.min

        let offBalanceNode = node.Leaves |> List.filter (fun n -> n.ComputeWeights() = incorrectWeight) |> List.exactlyOne

        // If the leaves of the node are balanced or missing, then it must be
        // the weight of the node itself.
        if offBalanceNode.Leaves.IsEmpty || offBalanceNode.LeavesAreBalanced() then
            let weight = offBalanceNode.Weight.Value
            printfn "Found off-balanced (%A) node %s with weight %i" classification (offBalanceNode.Name) weight
            match classification with
                | High(amount) -> weight - amount
                | Low(amount) -> weight + amount
        else
            rollUp offBalanceNode

    rollUp tree

let partTwo (input: Common.Input) =
    let weightData, treeData = input |> parseInput

    let tree = treeData |> assembleTree |> assignWeights weightData

    tree |> findImbalance

type Solution() =
    let name = "seven"

    interface Common.ISolution with
        member __.Name() = name |> Common.titleCase
        member __.LoadData(year) : Common.Input option = Common.loadData name year
        member __.PartOne(input: Common.Input) : string = input |> partOne
        member __.PartTwo(input: Common.Input) : string =
            input |> partTwo |> string
