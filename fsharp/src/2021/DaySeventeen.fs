module DaySeventeen

open FSharp.Core.Operators.Checked

// target area: x=56..76, y=-162..-134

type Target =
    { XMin: int
      XMax: int
      YMin: int
      YMax: int }

type Comparison =
    | NotFarEnough
    | TooFar
    | InRange

let yAccel i = i - 1

let xAccel i =
    if i = 0 then 0
    else if i > 0 then i - 1
    else i + 1

let xInRange xmin xmax pos =
    if pos < xmin then NotFarEnough
    else if pos > xmax then TooFar
    else InRange

let yInRange ymin ymax pos =
    if pos > ymax then NotFarEnough
    else if pos < ymin then TooFar
    else InRange

let velocityStopsInRange velocity (accel: int -> int) (compare: int -> Comparison) (stop: int -> bool) =
    let rec work pos vel maxPos =
        let newPos = pos + vel
        let newVel = vel |> accel

        match compare newPos with
        | TooFar -> None
        | InRange ->
            if newPos + newVel |> compare = InRange
               && stop vel |> not then
                work newPos newVel (max newPos maxPos)
            else
                maxPos |> Some
        | NotFarEnough ->
            if stop vel then
                None
            else
                work newPos newVel (max newPos maxPos)

    work 0 velocity 0

let partTwoVelocityInRange xStartingVel yStartingVel (xComp: int -> Comparison) (yComp: int -> Comparison) =
    let rec work xPos yPos xVel yVel =
        let newX = xPos + xVel
        let newY = yPos + yVel
        let newXVel = xAccel xVel
        let newYVel = yAccel yVel

        match xComp newX, yComp newY with
        | TooFar, _
        | _, TooFar -> false
        | NotFarEnough, _ ->
            if newXVel = 0 then
                false
            else
                work newX newY newXVel newYVel
        | _, NotFarEnough -> work newX newY newXVel newYVel
        | InRange, InRange -> true

    work 0 0 xStartingVel yStartingVel

let findXRange (target: Target) =
    let compare = xInRange target.XMin target.XMax

    [ 1 .. 1000 ]
    |> List.toArray
    |> Array.choose (fun vel -> velocityStopsInRange vel xAccel compare (fun x -> x = 0))

let findYRange (target: Target) =
    let compare = yInRange target.YMin target.YMax

    [ 1 .. 1000 ]
    |> List.toArray
    |> Array.choose (fun vel -> velocityStopsInRange vel yAccel compare (fun _y -> false))

// target area: x=56..76, y=-162..-134
let TheTarget =
    { XMin = 56
      XMax = 76
      YMin = -162
      YMax = -134 }

let partOne (_input: Common.Input) =
    TheTarget |> findYRange |> Array.max |> string

let partTwo (target: Target) =
    let xComp = xInRange target.XMin target.XMax
    let yComp = yInRange target.YMin target.YMax
    let numbers = [ -1000 .. 1000 ]

    let pairs =
        numbers
        |> List.collect (fun x -> numbers |> List.map (fun y -> x, y))

    pairs
    |> Seq.distinct
    |> Seq.filter (fun (x, y) -> partTwoVelocityInRange x y xComp yComp)
    |> Seq.length
    |> string

type Solution() =
    let name = "seventeen"

    interface Common.ISolution with
        member __.Name() = name |> Common.titleCase
        member __.LoadData(_year) : Common.Input option = Array.empty |> Some
        member __.PartOne(input: Common.Input) : string = input |> partOne
        member __.PartTwo(input: Common.Input) : string = TheTarget |> partTwo
