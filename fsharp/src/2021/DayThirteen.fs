module DayThirteen

open FSharp.Core.Operators.Checked

type Fold =
    | Horizontal of int
    | Vertical of int

    static member FromString(s: string) =
        match s.[11..].Split('=') with
        | [| "x"; xIdx |] -> xIdx |> int |> Vertical
        | [| "y"; yIdx |] -> yIdx |> int |> Horizontal
        | bad -> failwithf "Can't figure out how to parse: %A" bad

let parsePoints (input: Common.Input) =
    input
    |> Array.map (fun s ->
        let parts = s.Split(',')
        parts.[0] |> int, parts.[1] |> int)

let buildGrid (oneLocations: (int * int) array) =
    let maxX = oneLocations |> Array.maxBy fst |> fst
    let maxY = oneLocations |> Array.maxBy snd |> snd

    let pointSet = oneLocations |> Set.ofArray

    [| for y in 0 .. maxY do
           [| for x in 0 .. maxX do
                  if Set.contains (x, y) pointSet then
                      1
                  else
                      0 |] |]

let parseInput (input: Common.Input) =
    let points =
        input
        |> Array.takeWhile Common.Strings.notBlank
        |> parsePoints
        |> buildGrid

    let folds =
        input
        |> Array.skipWhile Common.Strings.notBlank
        |> Array.skipWhile Common.Strings.blank
        |> Array.map Fold.FromString

    points, folds


let foldAtY (grid: int array array) (yIdx: int) =
    let topHalf = grid.[0..yIdx - 1]
    let bottomHalf = grid.[yIdx + 1..] |> Array.rev

    Array.zip topHalf bottomHalf
    |> Array.map (fun arrays -> arrays ||> Array.map2 (|||))

let foldAtX (grid: int array array) (xIdx: int) =
    let leftHalf =
        grid |> Array.map (fun row -> row.[0..xIdx - 1])

    let rightHalf =
        grid
        |> Array.map (fun row -> row.[xIdx + 1..] |> Array.rev)

    Array.zip leftHalf rightHalf
    |> Array.map (fun arrays -> arrays ||> Array.map2 (|||))

let fold (grid: int array array) (theFold: Fold) =
    match theFold with
    | Horizontal y -> foldAtY grid y
    | Vertical x -> foldAtX grid x

let partOne (input: Common.Input) =
    let (points, folds) = input |> parseInput

    let firstFold = folds |> Array.head

    let folded = fold points firstFold

    folded |> Array.sumBy Array.sum |> string

let visualize (data: int array array) =
    data
    |> Array.map (fun row ->
        let rowAsString =
            row
            |> Array.map (fun num -> if num = 1 then "#" else ".")
            |> Array.reduce (+)
        rowAsString + "\n")
    |> Array.reduce (+)

let partTwo (input: Common.Input) =
    let (points, folds) = input |> parseInput

    folds |> Array.fold fold points |> visualize |> ((+) "\n")

type Solution() =
    let name = "thirteen"

    interface Common.ISolution with
        member __.Name() = name |> Common.titleCase
        member __.LoadData(year) : Common.Input option = Common.loadData name year
        member __.PartOne(input: Common.Input) : string = input |> partOne
        member __.PartTwo(input: Common.Input) : string = input |> partTwo
