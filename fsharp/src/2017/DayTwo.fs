module DayTwo

open FSharp.Core.Operators.Checked

let cleanData (input: string array) =
    input
    |> Array.map
        (fun s ->
            s.Split([| ' '; '\t' |])
            |> Array.choose Common.tryInt)

type Diff =
    { Min: int
      Max: int }
    static member OfArray(arr: int array) =
        let min = arr |> Array.min
        let max = arr |> Array.max

        { Min = min; Max = max }

    member this.TakeDiff() = this.Max - this.Min

module Diff =
    let takeDiff (diff: Diff) = diff.TakeDiff()

let tryDivide smaller bigger =
    if bigger % smaller = 0
    then bigger / smaller |> Some
    else None

let findDivisiblePair (nums: int array) =
    nums
    |> Array.sort
    |> Array.mapi (fun idx num ->
                   let remaining = nums.[idx + 1 .. ]
                   Array.tryPick (tryDivide num) remaining)
    |> Array.choose id
    |> Array.exactlyOne

let partTwo (input: Common.Input) =
    input
    |> cleanData
    |> Array.sumBy (Array.sort>>findDivisiblePair)

let partOne (input: string array) =
    input
    |> cleanData
    |> Array.sumBy (Diff.OfArray >> Diff.takeDiff)


type Solution() =
    let name = "two"

    interface Common.ISolution with
        member __.Name() = name |> Common.titleCase
        member __.LoadData(year) : Common.Input option = Common.loadData name year
        member __.PartOne(input: Common.Input) : string = input |> partOne |> sprintf "%A"
        member __.PartTwo(input: Common.Input) : string = input |> partTwo |> sprintf "%i"
