module DayTenTests

open Expecto

// Zeros are added here; in the DayTen module, they're added in the PartTwo function
let testSetOne = [| 16; 10; 15; 5; 1; 11; 7; 19; 6; 12; 4; 0 |]

let testSetTwo =
    [| 28; 33; 18; 42; 31; 14; 46; 20; 48; 47; 24; 23; 49; 45; 19; 38; 39; 11; 1; 32; 25; 35; 8; 17; 7; 9; 4; 2; 34; 10; 3; 0 |]

[<Tests>]
let tabulationTests =
    testList "Testing tablulate"
        [ testCase "Tabulate works for the first test case" <| fun _ ->
            let expected = Some 8L
            let got = testSetOne |> DayTen.tabulate

            Expect.equal got expected "These should be equal"
          testCase "Tabulate works for the second test case" <| fun _ ->
              let expected = Some 19208L
              let got = testSetTwo |> DayTen.tabulate

              Expect.equal got expected "These should be equal" ]

[<Tests>]
let folderTests =
    testList "Testing folder"
        [ testCase "behaves as expected for normal data" <| fun _ ->
            let testI = 5

            let testState =
                [| (6, 1L)
                   (7, 1L)
                   (8, 1L) |]
                |> Map.ofArray

            let mapBack = DayTen.folder testState testI
            let got = mapBack.[testI]
            let expected = 3L

            Expect.equal got expected "Thes should be equal" ]
