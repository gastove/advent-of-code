module DaySix

open FSharp.Core.Operators.Checked

let ageAFish (aFishsAge: int) =
    match aFishsAge with
    | 0 -> 6
    | other -> other - 1

let addOrReplace (definitelyInt: int64) (maybeInt: int64 option)=
    match maybeInt with
    | Some i -> i + definitelyInt |> Some
    | None -> definitelyInt |> Some

let lanternfish (startingAge: int) (days: int) =
    let rec mapTheDaily (ages: (int * int64) array) (remainingDays: int) =
        if remainingDays = 0 then
            ages
        else
            let newAges: Map<int, int64> =
                ages
                |> Array.sortByDescending fst
                |> Array.fold
                    (fun newAges (age, numFish) ->
                        if age = 0 then
                            newAges |> Map.change 6 (addOrReplace numFish) |> Map.add 8 numFish
                        else
                            newAges
                            |> Map.change (age - 1) (addOrReplace numFish))
                    Map.empty

            mapTheDaily (newAges |> Map.toArray) (remainingDays - 1)

    mapTheDaily [| startingAge, 1L |] days

let parseInput (input: Common.Input) = input.[0].Split(',') |> Array.map int

let partOne (input: Common.Input) =
    input
    |> parseInput
    |> Array.countBy id
    |> Array.Parallel.map (fun (num, count) ->
        let resultForStart =
            lanternfish num 80
            |> Array.map snd
            |> Array.reduce (+)

        resultForStart * (int64 count))
    |> Array.sum
    |> string

let partTwo (input: Common.Input) =
    input
    |> parseInput
    |> Array.countBy id
    |> Array.Parallel.map (fun (num, count) ->
        let resultForStart =
            lanternfish num 256
            |> Array.map snd
            |> Array.reduce (+)

        resultForStart * (int64 count))
    |> Array.sum
    |> string


type Solution() =
    let name = "six"

    interface Common.ISolution with
        member __.Name() = name |> Common.titleCase
        member __.LoadData(year) : Common.Input option = Common.loadData name year
        member __.PartOne(input: Common.Input) : string = input |> partOne
        member __.PartTwo(input: Common.Input) : string = input |> partTwo
