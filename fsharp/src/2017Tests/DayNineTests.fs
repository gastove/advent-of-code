module DayNineTests

open Expecto


[<Tests>]
let streamProcessingTests =
    testList
        "Test stream processing"
        [ testCase "Test clearGarbage"
          <| fun _ ->
              let emptyGarbage = "<>"
              let randomGarbage = "<random characters>"
              let extraOpeners = "<<<<>"
              let firstCloseCancelled = "<{!>}>"
              let bangCancelled = "<!!>"
              let manyBangsCancelled = "<!!!>>"
              let messy = """<{o"i!a,<{i<a>"""

              Expect.isEmpty (emptyGarbage |> Seq.toList |> DayNine.clearGarbage) "This should be easy"

              Expect.isEmpty
                  (randomGarbage
                   |> Seq.toList
                   |> DayNine.clearGarbage)
                  "Random characters are all garbage"

              Expect.isEmpty (extraOpeners |> Seq.toList |> DayNine.clearGarbage) "Openers should be ignored"

              Expect.isEmpty
                  (firstCloseCancelled
                   |> Seq.toList
                   |> DayNine.clearGarbage)
                  "We should be able to cancel a close"

              Expect.isEmpty
                  (bangCancelled
                   |> Seq.toList
                   |> DayNine.clearGarbage)
                  "Bangs cancel each other out"

              Expect.isEmpty
                  (manyBangsCancelled
                   |> Seq.toList
                   |> DayNine.clearGarbage)
                  "Complex bang cancellation should work"

              Expect.isEmpty (messy |> Seq.toList |> DayNine.clearGarbage) "This is a mess, but should work"

          testCase "Test scoreStream"
          <| fun _ ->

              // {}, score of 1.
              // {{{}}}, score of 1 + 2 + 3 = 6.
              // {{},{}}, score of 1 + 2 + 2 = 5.
              // {{{},{},{{}}}}, score of 1 + 2 + 3 + 3 + 3 + 4 = 16.
              // {<a>,<a>,<a>,<a>}, score of 1.
              // {{<ab>},{<ab>},{<ab>},{<ab>}}, score of 1 + 2 + 2 + 2 + 2 = 9.
              // {{<!!>},{<!!>},{<!!>},{<!!>}}, score of 1 + 2 + 2 + 2 + 2 = 9.
              // {{<a!>},{<a!>},{<a!>},{<ab>}}, score of 1 + 2 = 3.

              let easy = "{}" // 1
              let nested = "{{{}}}" // 6
              let nestedDouble = "{{}, {}}" // 5
              let complexNested = "{{{},{},{{}}}}" // 16
              let onlyGarbage = "{<a>,<a>,<a>,<a>}" // 1
              let differentGarbage = "{{<ab>},{<ab>},{<ab>},{<ab>}}" // 9
              let garbageWithCancels = "{{<!!>},{<!!>},{<!!>},{<!!>}}" // 9
              let complexWithCancels = "{{<a!>},{<a!>},{<a!>},{<ab>}}" // 3

              Expect.equal (easy |> Seq.toList |> DayNine.scoreStream) 1 "We should succeed in the trivial case"
              Expect.equal (nested |> Seq.toList |> DayNine.scoreStream) 6 "We should succeed in basic nested cases"
              Expect.equal (nestedDouble |> Seq.toList |> DayNine.scoreStream) 5 "We should succeed in multiple nested cases"

              Expect.equal
                  (complexNested |> Seq.toList |> DayNine.scoreStream)
                  16
                  "We should succeed in complex nested cases"

              Expect.equal (onlyGarbage |> Seq.toList |> DayNine.scoreStream) 1 "We should succeed when the only content is garbage"

              Expect.equal
                  (differentGarbage
                   |> Seq.toList
                   |> DayNine.scoreStream)
                  9
                  "We should succeed with repeated nesting garbage"

              Expect.equal
                  (garbageWithCancels
                   |> Seq.toList
                   |> DayNine.scoreStream)
                  9
                  "We should succeed when the garbage is full of cancels"

              Expect.equal
                  (complexWithCancels
                   |> Seq.toList
                   |> DayNine.scoreStream)
                  3
                  "We should succeed even when complex cancels are present"

          ]
