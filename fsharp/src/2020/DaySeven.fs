module DaySeven

//--------------------------------- Part One ---------------------------------//
// For example, consider the following rules:

// light red bags contain 1 bright white bag, 2 muted yellow bags.
// dark orange bags contain 3 bright white bags, 4 muted yellow bags.
// bright white bags contain 1 shiny gold bag.
// muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
// shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
// dark olive bags contain 3 faded blue bags, 4 dotted black bags.
// vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
// faded blue bags contain no other bags.
// dotted black bags contain no other bags.

// These rules specify the required contents for 9 bag types. In this example,
// every faded blue bag is empty, every vibrant plum bag contains 11 bags (5
// faded blue and 6 dotted black), and so on.

// You have a shiny gold bag. If you wanted to carry it in at least one other
// bag, how many different bag colors would be valid for the outermost bag? (In
// other words: how many colors can, eventually, contain at least one shiny gold
// bag?)

// In the above rules, the following options would be available to you:

//     A bright white bag, which can hold your shiny gold bag directly.
//     A muted yellow bag, which can hold your shiny gold bag directly, plus some other bags.
//     A dark orange bag, which can hold bright white and muted yellow bags, either of which could then hold your shiny gold bag.
//     A light red bag, which can hold bright white and muted yellow bags, either of which could then hold your shiny gold bag.

// So, in this example, the number of bag colors that can eventually contain at least one shiny gold bag is 4.

// How many bag colors can eventually contain at least one shiny gold bag?

//--------------------------------- Part Two ---------------------------------//
// So, a single shiny gold bag must contain 1 dark olive bag (and the 7 bags within it) plus 2 vibrant plum bags (and the 11 bags within each of those): 1 + 1*7 + 2 + 2*11 = 32 bags!

// Of course, the actual rules have a small chance of going several levels deeper than this example; be sure to count all of the bags, even if the nesting becomes topologically impractical!

// Here's another example:

// shiny gold bags contain 2 dark red bags.
// dark red bags contain 2 dark orange bags.
// dark orange bags contain 2 dark yellow bags.
// dark yellow bags contain 2 dark green bags.
// dark green bags contain 2 dark blue bags.
// dark blue bags contain 2 dark violet bags.
// dark violet bags contain no other bags.

// In this example, a single shiny gold bag must contain 126 other bags.

// How many individual bags are required inside your single shiny gold bag?

type CanHold =
    { BagName: string
      CanHold: int }
    static member FromString(input: string): CanHold option =
        let parts = input.Trim().Split(" ", 2)
        let bagName = parts.[1].Trim().TrimEnd('s')

        match parts.[0].Trim() with
        | "no" -> None
        | i ->
            Some
                { BagName = bagName
                  CanHold = i |> int }

type Nodes = string array

type Edges = (string * CanHold) array

let canContain (bagIWant: string) (nodes: Nodes) (edges: Edges): string array =
    let rec search (lookFor: string): bool =
        let hasEdgesWith =
            edges
            |> Array.filter (fun (bagName, _) -> bagName = lookFor) // Get all the edges for the current node
            |> Array.map (fun (_, canHold) -> canHold.BagName) // Get all the bags the current node can hold

        if Array.contains bagIWant hasEdgesWith then
            true
        else
            hasEdgesWith
            |> Array.Parallel.map search
            |> Array.exists id

    nodes |> Array.filter search

let hasCapacity (startingBag: string) (edges: Edges): int =
    let rec compute (lookFor: string): int =
        // Get all the edges for the current node
        let _, hasEdgesWith =
            edges
            |> Array.filter (fun (bagName, _) -> bagName = lookFor)
            |> Array.unzip

        hasEdgesWith |> Array.sumBy (fun edge -> edge.CanHold + (edge.CanHold * compute edge.BagName))

    compute startingBag

// I need to parse this:
// dark orange bags contain 3 bright white bags, 4 muted yellow bags.
let parseLine (line: string): string * (string * CanHold) array =
    let parts = line.Trim('.').Split("contain") // remove trailing period
    let bag = parts.[0].Trim().TrimEnd('s')
    let contents = parts.[1].Split(",") |> Array.choose CanHold.FromString

    bag, (Array.allPairs [| bag |] contents)

let parseGraph (input: string array): Nodes * Edges =
    let nodes, edgesRaw =
        input
        |> Array.map parseLine
        |> Array.unzip

    let edges = edgesRaw |> Array.concat

    nodes, edges

type Solution() =
    let name = "seven"
    let iWant = "shiny gold bag"
    interface Common.ISolution with
        member __.LoadData(year): Common.Input option = Common.loadData name year

        member __.PartOne(input: Common.Input): string =
            let numberOfBags =
                let nodes, edges = input |> parseGraph
                let possibilities = canContain iWant nodes edges

                possibilities |> Array.length

            sprintf "The number of bags is: %i" numberOfBags

        member __.PartTwo(input: Common.Input): string =
            let _, edges = input |> parseGraph
            let bagIsHolding = hasCapacity iWant edges

            sprintf "My %s can hold: %i" iWant bagIsHolding

        member __.Name() = name |> Common.titleCase
