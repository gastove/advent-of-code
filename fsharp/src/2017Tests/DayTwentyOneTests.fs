module DayTwentyOneTests

open Expecto

open DayTwentyOne

[<Tests>]
let patternTests =
    testList
        "Testing the Pattern type"
        [ testCase "Rotate Size=3"
          <| fun _ ->
              let rotateOnce =
                  [| "#.."; "#.#"; "##." |]
                  |> Array.map Seq.toArray
                  |> Pattern.Create

              let rotateTwice =
                  [| "###"; "#.."; ".#." |]
                  |> Array.map Seq.toArray
                  |> Pattern.Create

              let rotateThrice =
                  [| ".##"; "#.#"; "..#" |]
                  |> Array.map Seq.toArray
                  |> Pattern.Create

              let rotatedOnce = Pattern.Default.Rotate()
              let rotatedTwice = rotatedOnce.Rotate()
              let rotatedThrice = rotatedTwice.Rotate()
              let backAtStart = rotatedThrice.Rotate()

              Expect.equal rotatedOnce rotateOnce "We should be able to rotate once"
              Expect.equal rotatedTwice rotateTwice "We should be able to rotate twice"
              Expect.equal rotatedThrice rotateThrice "We should be able to rotate thrice"
              Expect.equal backAtStart Pattern.Default "We should be able to rotate back to original"

          testCase "Rotate Size=2"
          <| fun _ ->
              let pattern =
                  [| "#."; ".."|] |> Array.map Seq.toArray |> Pattern.Create

              let rotateOnce =
                  [| ".#"; ".." |]
                  |> Array.map Seq.toArray
                  |> Pattern.Create

              let rotateTwice =
                  [| ".."; ".#" |]
                  |> Array.map Seq.toArray
                  |> Pattern.Create

              let rotateThrice =
                  [| ".."; "#." |]
                  |> Array.map Seq.toArray
                  |> Pattern.Create

              let rotatedOnce = pattern.Rotate()
              let rotatedTwice = rotatedOnce.Rotate()
              let rotatedThrice = rotatedTwice.Rotate()
              let backAtStart = rotatedThrice.Rotate()

              Expect.equal rotatedOnce rotateOnce "We should be able to rotate once"
              Expect.equal rotatedTwice rotateTwice "We should be able to rotate twice"
              Expect.equal rotatedThrice rotateThrice "We should be able to rotate thrice"
              Expect.equal backAtStart pattern "We should be able to rotate back to original"

          testCase "Flip"
          <| fun _ ->
              let flippedVertical =
                  [| "###"; "..#"; ".#." |]
                  |> Array.map Seq.toArray
                  |> Pattern.Create

              let flippedHorizontal =
                  [| ".#."; "#.."; "###" |]
                  |> Array.map Seq.toArray
                  |> Pattern.Create

              Expect.equal (Pattern.Default.Flip Vertical) flippedVertical "We should be able to flip vertically"
              Expect.equal (Pattern.Default.Flip Horizontal) flippedHorizontal "We should be able to flip horizontally"

          testCase "Subdivide"
          <| fun _ ->
              let input =
                  [| "#..#"; "...."; "...."; "#..#" |]
                  |> Array.map Seq.toArray
                  |> Pattern.Create

              let expected =
                  [| [| [| "#."; ".." |]
                        |> Array.map Seq.toArray
                        |> Pattern.Create
                        [| ".#"; ".." |]
                        |> Array.map Seq.toArray
                        |> Pattern.Create |]
                     [| [| ".."; "#." |]
                        |> Array.map Seq.toArray
                        |> Pattern.Create
                        [| ".."; ".#" |]
                        |> Array.map Seq.toArray
                        |> Pattern.Create |] |]

              Expect.equal (input.Subdivide()) expected "We should be able to subdivide a Pattern"

          testCase "Combine"
          <| fun _ ->
              let input =
                  [| [| [| "#."; ".." |]
                        |> Array.map Seq.toArray
                        |> Pattern.Create
                        [| ".#"; ".." |]
                        |> Array.map Seq.toArray
                        |> Pattern.Create |]
                     [| [| ".."; "#." |]
                        |> Array.map Seq.toArray
                        |> Pattern.Create
                        [| ".."; ".#" |]
                        |> Array.map Seq.toArray
                        |> Pattern.Create |] |]

              let expected =
                  [| "#..#"; "...."; "...."; "#..#" |]
                  |> Array.map Seq.toArray
                  |> Pattern.Create

              Expect.equal (input |> Pattern.combine) expected "We should be able to combine."

          ]
