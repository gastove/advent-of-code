module DaySevenTests

open Expecto

open DaySeven

[<Tests>]
let nodeTests =
    testList
        "Testing the Node datatype"
        [ testCase "Test that updating a weight works for the root node"
          <| fun _ ->
              let tree =
                  { Name = "root"
                    Weight = None
                    Leaves =
                        [ { Name = "leaf1"
                            Weight = None
                            Leaves = [] }
                          { Name = "leaf2"
                            Weight = None
                            Leaves = [] } ] }

              let expected =
                  { Name = "root"
                    Weight = Some(1)
                    Leaves =
                        [ { Name = "leaf1"
                            Weight = None
                            Leaves = [] }
                          { Name = "leaf2"
                            Weight = None
                            Leaves = [] } ] }

              let got = tree.UpdateWeight "root" (1 |> Some)

              Expect.equal got expected "Only the root should update; it should have an updated weight"

          testCase "Test that updating a weight works in a simple nested case"
          <| fun _ ->
              let tree =
                  { Name = "root"
                    Weight = Some(1)
                    Leaves =
                        [ { Name = "leaf1"
                            Weight = None
                            Leaves = [] }
                          { Name = "leaf2"
                            Weight = None
                            Leaves = [] } ] }

              let expected =
                  { Name = "root"
                    Weight = Some(1)
                    Leaves =
                        [ { Name = "leaf1"
                            Weight = None
                            Leaves = [] }
                          { Name = "leaf2"
                            Weight = Some(2)
                            Leaves = [] } ] }

              let got = tree.UpdateWeight "leaf2" (2 |> Some)

              Expect.equal got expected "Only leaf2 should update; it should have an updated weight"

          testCase "Test that updating a weight works in a more nested case"
          <| fun _ ->
              let tree =
                  { Name = "root"
                    Weight = Some(1)
                    Leaves =
                        [ { Name = "leaf1"
                            Weight = None
                            Leaves = [] }
                          { Name = "leaf2"
                            Weight = None
                            Leaves =
                                [ { Name = "leaf3"
                                    Weight = None
                                    Leaves = [] } ] } ] }

              let expected =
                  { Name = "root"
                    Weight = Some(1)
                    Leaves =
                        [ { Name = "leaf1"
                            Weight = None
                            Leaves = [] }
                          { Name = "leaf2"
                            Weight = None
                            Leaves =
                                [ { Name = "leaf3"
                                    Weight = Some(123)
                                    Leaves = [] } ] } ] }

              let got = tree.UpdateWeight "leaf3" (123 |> Some)

              Expect.equal got expected "Only leaf3 should update; it should have an updated weight"

          testCase "Test that we can correctly detect if a tree contains a node with a specific name"
          <| fun _ ->
              let tree =
                  { Name = "root"
                    Weight = Some(1)
                    Leaves =
                        [ { Name = "leaf1"
                            Weight = None
                            Leaves = [] }
                          { Name = "leaf2"
                            Weight = None
                            Leaves =
                                [ { Name = "leaf3"
                                    Weight = None
                                    Leaves = [] } ] } ] }

              Expect.isTrue (tree.HasLeafNamed "leaf3") "We do have a leaf3"
              Expect.isTrue (tree.HasLeafNamed "leaf1") "We do have a leaf1"
              Expect.isFalse (tree.HasLeafNamed "hoobastank") "We should never have a leaf named hoobastank" ]

let exampleData =
    [| "pbga (66)"
       "xhth (57)"
       "ebii (61)"
       "havc (66)"
       "ktlj (57)"
       "fwft (72) -> ktlj, cntj, xhth"
       "qoyq (66)"
       "padx (45) -> pbga, havc, qoyq"
       "tknk (41) -> ugml, padx, fwft"
       "jptl (61)"
       "ugml (68) -> gyxo, ebii, jptl"
       "gyxo (61)"
       "cntj (57)" |]

[<Tests>]
let testDataParsing =
    testList
        "Test that we can parse data as expected"
        [ testCase "Test that we can parse tree data correctly"
          <| fun _ ->
              let expected =
                  [| { Name = "fwft"
                       Weight = Some(72)
                       Leaves =
                           [ { Name = "ktlj"
                               Weight = None
                               Leaves = [] }
                             { Name = "cntj"
                               Weight = None
                               Leaves = [] }
                             { Name = "xhth"
                               Weight = None
                               Leaves = [] } ] }
                     { Name = "padx"
                       Weight = Some(45)
                       Leaves =
                           [ { Name = "pbga"
                               Weight = None
                               Leaves = [] }
                             { Name = "havc"
                               Weight = None
                               Leaves = [] }
                             { Name = "qoyq"
                               Weight = None
                               Leaves = [] } ] }
                     { Name = "tknk"
                       Weight = Some(41)
                       Leaves =
                           [ { Name = "ugml"
                               Weight = None
                               Leaves = [] }
                             { Name = "padx"
                               Weight = None
                               Leaves = [] }
                             { Name = "fwft"
                               Weight = None
                               Leaves = [] } ] }
                     { Name = "ugml"
                       Weight = Some(68)
                       Leaves =
                           [ { Name = "gyxo"
                               Weight = None
                               Leaves = [] }
                             { Name = "ebii"
                               Weight = None
                               Leaves = [] }
                             { Name = "jptl"
                               Weight = None
                               Leaves = [] } ] } |]

              let got = exampleData |> parseTreeData
              Expect.equal got expected "We should get back what we expect" ]

[<Tests>]
let testAssembleTree =
    testList
        "Test assembleTree"
        [ testCase "Test that we can assemble the example tree from the problem"
          <| fun _ -> Expect.isTrue true "True is true" ]
