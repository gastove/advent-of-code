
module DayTwentyTests

open Expecto

open DayTwenty

let partOneAlgo = "..#.#..#####.#.#.#.###.##.....###.##.#..###.####..#####..#....#..#..##..###..######.###...####..#..#####..##..#.#####...##.#.#..#.##..#.#......#.###.######.###.####...#.##.##..#..#..#####.....#.#....###..#.##......#.....#..#..#..##..#...##.######.####.####.#.#...#.......#..#.#.#...####.##.#......#..#...##.#.##..#...##.#.##..###.#......#.#.......#.#.#.####.###.##...#.....####.#..#..#.##.#....##..#.####....##...##..#...#......#.#.......#.......##..####..#...#.#.#...##..#.#..###..#####........#..####......#..#"

let partOneImage = [|
                    "#..#."
                    "#...."
                    "##..#"
                    "..#.."
                    "..###"
                    |]

[<Tests>]
let partOneTests =
    testList "Can we reproduce the part one examples?" [
        testCase "Do we get the correct binary?" <| fun _ ->
            let grid = [| [|Dark; Dark; Dark|];
                          [|Light; Dark; Dark|];
                          [|Dark; Light; Dark|] |]

            let expected = 34
            let got = grid |> gridToInt

            Expect.equal got expected "We should get 34"

        testCase "Can we reproduce the Part One example?" <| fun _ ->
            let input = [|[| partOneAlgo; ""|]; partOneImage|] |> Array.concat

            let expected = "35"
            let got = input |> partOne

            Expect.equal got expected "We should get 35"
        ]
