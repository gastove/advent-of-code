module DayFour

open FSharp.Core.Operators.Checked

open Common

let rangeFromDashed (s: string) =
    let parts = s.Split("-")
    Types.RangeInt32.Create(parts.[0] |> System.Int32.Parse) (parts.[1] |> System.Int32.Parse)

let parseLine (line: string) =
    let pair = line.Split(",")

    pair.[0] |> rangeFromDashed, pair.[1] |> rangeFromDashed

let partOne (input: Common.Input) =
    input
    |> Array.map parseLine
    |> Array.choose (fun (range1, range2) ->
        if range1.FullyContains range2
           || range2.FullyContains range1 then
            Some 1
        else
            None)
    |> Array.sum
    |> string

let partTwo (input: Common.Input) =
    input
    |> Array.map parseLine
    |> Array.filter (fun (r1, r2) -> r1.Overlaps r2)
    |> Array.length
    |> string
 
type Solution() =
    let name = "four"

    interface Common.ISolution with
        member __.Name() = name |> Common.titleCase
        member __.LoadData(year) : Common.Input option = Common.loadData name year
        member __.PartOne(input: Common.Input) : string = input |> partOne
        member __.PartTwo(input: Common.Input) : string = input |> partTwo
