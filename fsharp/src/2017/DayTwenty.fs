module DayTwenty

open FSharp.Core.Operators.Checked

type Acceleration =
    { X: int64
      Y: int64
      Z: int64 }
    static member Create x y z =
        { Acceleration.X = x
          Acceleration.Y = y
          Acceleration.Z = z }

    static member FromString(s: string) =
        let parts = s.Split("=")

        let data =
            parts.[1].[1..parts.[1].Length - 2].Split(",")
            |> Array.map int64

        match data with
        | [| x; y; z |] -> Acceleration.Create x y z
        | _ -> failwithf "Can't parse an Acceleration from %A" data

type Velocity =
    { X: int64
      Y: int64
      Z: int64 }
    static member Create x y z =
        { Velocity.X = x
          Velocity.Y = y
          Velocity.Z = z }

    static member FromString(s: string) =
        let parts = s.Split("=")

        let data =
            parts.[1].[1..parts.[1].Length - 2].Split(",")
            |> Array.map int64

        match data with
        | [| x; y; z |] -> Velocity.Create x y z
        | _ -> failwithf "Can't parse a Velocity from %A" data

    member this.Accelerate(accel: Acceleration) =
        Velocity.Create(this.X + accel.X) (this.Y + accel.Y) (this.Z + accel.Z)

type Position =
    { X: int64
      Y: int64
      Z: int64 }
    static member Create x y z = { X = x; Y = y; Z = z }

    static member FromString(s: string) =
        let parts = s.Split("=")

        let data =
            parts.[1].[1..parts.[1].Length - 2].Split(",")
            |> Array.map int64

        match data with
        | [| x; y; z |] -> Position.Create x y z
        | _ -> failwithf "Can't parse a Position from %A" data


    member this.DistanceToOrigin() =
        [ this.X |> abs
          this.Y |> abs
          this.Z |> abs ]
        |> List.sum

    member this.Move(vel: Velocity) =
        Position.Create(this.X + vel.X) (this.Y + vel.Y) (this.Z + vel.Z)

type Particle =
    { Position: Position
      Acceleration: Acceleration
      Velocity: Velocity }

    static member Create p a v =
        { Position = p
          Acceleration = a
          Velocity = v }

    static member FromString(s: string) =
        let parts =
            s.Split(", ") |> Array.map (fun s -> s.Trim())

        Particle.Create
            (parts.[0] |> Position.FromString)
            (parts.[2] |> Acceleration.FromString)
            (parts.[1] |> Velocity.FromString)

    member this.Update() =
        let newV =
            this.Velocity.Accelerate this.Acceleration

        let newPos = this.Position.Move newV

        { this with
              Position = newPos
              Velocity = newV }

let parseInput (input: Common.Input) =
    input
    |> Array.map Particle.FromString

let updateParticles (particles: Particle array) =
    particles |> Array.Parallel.map (fun p -> p.Update())

let computeDistances (particles: Particle array)=
    particles |> Array.Parallel.mapi (fun idx p -> idx, p.Position.DistanceToOrigin())

let rec runUpdateAndFindClosest (particles: Particle array) times acc =
    if times = 0 then
        acc
    else
        let updated = particles |> updateParticles

        let closestToOrigin = updated |> computeDistances |> Array.minBy snd |> fst

        runUpdateAndFindClosest updated (times - 1) (closestToOrigin :: acc)

let rec runUpdateAndRemoveCollisions timesStable (particles: Particle array) =
    if timesStable = 10_000 then
        particles
    else
        let updated = particles |> updateParticles

        let collisions =
            updated
            |> Array.groupBy (fun p -> p.Position)
            |> Array.filter (fun (_, particlesAtPosition) -> particlesAtPosition.Length > 1)
            |> Array.map fst

        let withNoCollisions = updated |> Array.filter (fun p -> Array.contains p.Position collisions |> not)

        if particles.Length = withNoCollisions.Length then
            runUpdateAndRemoveCollisions (timesStable + 1) withNoCollisions
        else
            runUpdateAndRemoveCollisions 0 withNoCollisions

let partOne (input: Common.Input) =
    let particles = input |> parseInput

    let closestToOrigin =
        runUpdateAndFindClosest particles 10000 []
        |> List.countBy id
        |> List.maxBy snd

    closestToOrigin |> fst

let partTwo (input: Common.Input) =
    let particles = input |> parseInput

    particles |> runUpdateAndRemoveCollisions 0 |> Array.length

type Solution() =
    let name = "twenty"

    interface Common.ISolution with
        member __.Name() = name |> Common.titleCase
        member __.LoadData(year) : Common.Input option = Common.loadData name year
        member __.PartOne(input: Common.Input) : string =
            input |> partOne |> string
        member __.PartTwo(input: Common.Input) : string =
            input |> partTwo |> string
