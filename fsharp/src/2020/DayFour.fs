module DayFour

//--------------------------------- Part One ---------------------------------//
// The automatic passport scanners are slow because they're having trouble
// detecting which passports have all required fields. The expected fields are
// as follows:

//     byr (Birth Year)
//     iyr (Issue Year)
//     eyr (Expiration Year)
//     hgt (Height)
//     hcl (Hair Color)
//     ecl (Eye Color)
//     pid (Passport ID)
//     cid (Country ID)

// Passport data is validated in batch files (your puzzle input). Each passport is represented as a sequence of key:value pairs separated by spaces or newlines. Passports are separated by blank lines.

// Here is an example batch file containing four passports:

// ecl:gry pid:860033327 eyr:2020 hcl:#fffffd
// byr:1937 iyr:2017 cid:147 hgt:183cm
//
// iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884
// hcl:#cfa07d byr:1929
//
// hcl:#ae17e1 iyr:2013
// eyr:2024
// ecl:brn pid:760753108 byr:1931
// hgt:179cm
//
// hcl:#cfa07d eyr:2025 pid:166559648
// iyr:2011 ecl:brn hgt:59in

// The first passport is valid - all eight fields are present. The second passport is invalid - it is missing hgt (the Height field).

// The third passport is interesting; the only missing field is cid, so it looks like data from North Pole Credentials, not a passport at all! Surely, nobody would mind if you made the system temporarily ignore missing cid fields. Treat this "passport" as valid.

// The fourth passport is missing two fields, cid and byr. Missing cid is fine, but missing any other field is not, so this passport is invalid.

// According to the above rules, your improved system would report 2 valid passports.

// Count the number of valid passports - those that have all required fields. Treat cid as optional. In your batch file, how many passports are valid?

//--------------------------------- Part Two ---------------------------------//
// The line is moving more quickly now, but you overhear airport security talking about how passports with invalid data are getting through. Better add some data validation, quick!

// You can continue to ignore the cid field, but each other field has strict rules about what values are valid for automatic validation:

//     byr (Birth Year) - four digits; at least 1920 and at most 2002.
//     iyr (Issue Year) - four digits; at least 2010 and at most 2020.
//     eyr (Expiration Year) - four digits; at least 2020 and at most 2030.
//     hgt (Height) - a number followed by either cm or in:
//         If cm, the number must be at least 150 and at most 193.
//         If in, the number must be at least 59 and at most 76.
//     hcl (Hair Color) - a # followed by exactly six characters 0-9 or a-f.
//     ecl (Eye Color) - exactly one of: amb blu brn gry grn hzl oth.
//     pid (Passport ID) - a nine-digit number, including leading zeroes.
//     cid (Country ID) - ignored, missing or not.

// Your job is to count the passports where all required fields are both present and valid according to the above rules.

open System.Text.RegularExpressions

let parseKeyValue (kv: string): string * string =
    let parts = kv.Split(':')
    parts.[0].Trim(), parts.[1].Trim()

let maybeParseI32 (s: string): int32 option =
    let mutable receiver = 0
    match System.Int32.TryParse(s, &receiver) with
    | true -> Some(receiver)
    | false -> None

type Passport =
    { BirthYear: string option
      IssueYear: string option
      ExpirationYear: string option
      Height: string option
      HairColor: string option
      EyeColor: string option
      PassportID: string option
      CountryID: string option }

    static member FromLine(input: string): Passport =
        let pieces =
            input.Split(' ')
            |> Array.map parseKeyValue
            |> Map.ofArray

        { BirthYear = pieces.TryFind "byr"
          IssueYear = pieces.TryFind "iyr"
          ExpirationYear = pieces.TryFind "eyr"
          Height = pieces.TryFind "hgt"
          HairColor = pieces.TryFind "hcl"
          EyeColor = pieces.TryFind "ecl"
          PassportID = pieces.TryFind "pid"
          CountryID = pieces.TryFind "cid" }

    member this.RequiredFieldsPresent(): bool =
        this.BirthYear.IsSome && this.ExpirationYear.IsSome && this.EyeColor.IsSome && this.HairColor.IsSome
        && this.Height.IsSome && this.IssueYear.IsSome && this.PassportID.IsSome

    member private _.ParseAndValidateRange (minimum: int) (maximum: int) (target: string option): bool =
        target
        |> Option.bind maybeParseI32
        |> Option.map (fun comp -> minimum <= comp && comp <= maximum)
        |> Option.defaultValue false

    //     byr (Birth Year) - four digits; at least 1920 and at most 2002.
    member this.IsBirthYearValid() = this.ParseAndValidateRange 1920 2002 this.BirthYear

    //     iyr (Issue Year) - four digits; at least 2010 and at most 2020.
    member this.IsIssueYearValid() = this.ParseAndValidateRange 2010 2020 this.IssueYear

    //     eyr (Expiration Year) - four digits; at least 2020 and at most 2030.
    member this.IsExpirationYearValid() = this.ParseAndValidateRange 2020 2030 this.ExpirationYear

    //     hgt (Height) - a number followed by either cm or in:
    //         If cm, the number must be at least 150 and at most 193.
    //         If in, the number must be at least 59 and at most 76.
    member this.IsHeightValid() =
        let pattern = Regex("(?<Digits>\d{2,3})(?<Unit>\w{2})")
        match this.Height with
        | Some(h) ->
            let found = pattern.Match(h)
            match found.Groups.["Unit"].Value with
            | "cm" -> Some(found.Groups.["Digits"].Value) |> this.ParseAndValidateRange 150 193
            | "in" -> Some(found.Groups.["Digits"].Value) |> this.ParseAndValidateRange 59 76
            | _ -> false
        | None -> false

    //     hcl (Hair Color) - a # followed by exactly six characters 0-9 or a-f.
    member this.IsHairColorValid() =
        this.HairColor
        |> Option.map (fun hc -> Regex.IsMatch(hc, "#[0-9a-f]{6}"))
        |> Option.defaultValue false

    //     ecl (Eye Color) - exactly one of: amb blu brn gry grn hzl oth.
    member this.IsEyeColorValid() =
        let validateColor =
            function
            | "amb"
            | "blu"
            | "brn"
            | "gry"
            | "grn"
            | "hzl"
            | "oth" -> true
            | _ -> false

        this.EyeColor
        |> Option.map validateColor
        |> Option.defaultValue false

    //     pid (Passport ID) - a nine-digit number, including leading zeroes.
    member this.IsPassportIDValid() =
        this.PassportID
        |> Option.map (fun pid -> Regex.IsMatch(pid, "^\d{9}$"))
        |> Option.defaultValue false

    member this.AllFieldsValid() =
        this.IsHeightValid() && this.IsEyeColorValid() && this.IsBirthYearValid() && this.IsHairColorValid()
        && this.IsIssueYearValid() && this.IsPassportIDValid() && this.IsExpirationYearValid()

type LineBuilder =
    { mutable Current: string list
      mutable Final: string list list }

    member this.Clear() =
        this.Final <- this.Final @ [ this.Current ]
        this.Current <- []

    member this.Append(item: string) = this.Current <- this.Current @ [ item ]

    member this.Return(): string array =
        let res =
            this.Final
            |> List.map (fun l -> String.concat " " l)
            |> List.toArray
        res

    static member New() =
        { Current = []
          Final = [] }

let isBlank (s: string) = System.String.IsNullOrEmpty(s) || System.String.IsNullOrWhiteSpace(s)

// take us from kv pairs across many lines to kv pairs all on a single line,
// separated by spaces.
let cleanLines (lines: string array): string array =
    let folder (acc: LineBuilder) (item: string): LineBuilder =
        if isBlank item then acc.Clear() else acc.Append item

        acc

    let builder = lines |> Array.fold folder (LineBuilder.New())
    // Clear before we return, make sure there's nothing in the buffer
    builder.Clear()
    builder.Return()

let partOne (input: string array) =
    input
    |> cleanLines
    |> Array.map Passport.FromLine
    |> Array.filter (fun passport -> passport.RequiredFieldsPresent())
    |> Array.length

let partTwo (input: string array) =
    let valid =
        input
        |> cleanLines
        |> Array.map Passport.FromLine
        |> Array.filter (fun passport -> passport.AllFieldsValid())

    valid |> Array.length

type Solution() =
    let name = "four"
    interface Common.ISolution with
        member __.LoadData(year): Common.Input option =
            Common.loadData name year
        member __.PartOne(input: Common.Input): string =
            let partOne = partOne input
            sprintf "Number of valid passports, part one: %i" partOne

        member __.PartTwo(input: Common.Input): string =
            let partTwo = partTwo input
            sprintf "Number of passports with all fields correct: %i" partTwo

        member __.Name() = name |> Common.titleCase
