module DayTwentyOneTests

open Expecto

open DayTwentyOne

let exampleData =
    [| "mxmxvkd kfcds sqjhc nhms (contains dairy, fish)"
       "trh fvjkl sbzzf mxmxvkd (contains dairy)"
       "sqjhc fvjkl (contains soy)"
       "sqjhc mxmxvkd sbzzf (contains fish)" |]

// NOTE: I came back a *while* later and re-did this solution from scratch, and
// it works! But I wrote it cold, without writing unit tests, so these don't
// pass.

// [<Tests>]
// let sharedTests =
//     testList
//         "Testing the shared module"
//         [ testCase "Can I parse the example data?"
//           <| fun _ ->
//               let raw = exampleData |> Shared.parseInput

//               let singles, unions = raw |> PartOne.separateOutData

//               let expectedSingles =
//                   [ "dairy",
//                     [| "trh"; "fvjkl"; "sbzzf"; "mxmxvkd" |]
//                     |> Set.ofArray
//                     "soy", [| "sqjhc"; "fvjkl" |] |> Set.ofArray
//                     "fish", [| "sqjhc"; "mxmxvkd"; "sbzzf" |] |> Set.ofArray ]
//                   |> Map.ofList

//               let expectedUnions =
//                   [ [| "dairy"; "fish" |],
//                     [| "mxmxvkd"
//                        "kfcds"
//                        "sqjhc"
//                        "nhms" |]
//                     |> Set.ofArray ]
//                   |> Map.ofList

//               Expect.equal singles expectedSingles "We should get the right single data"
//               Expect.equal unions expectedUnions "We should get the right union data"

//           testCase "Can I clean up parsed data?"
//           <| fun _ ->
//               let raw = exampleData |> Shared.parseInput

//               let singles, unions = raw |> PartOne.separateOutData
//               // [| "mxmxvkd kfcds sqjhc nhms (contains dairy, fish)"
//               //    "trh fvjkl sbzzf mxmxvkd (contains dairy)"
//               //    "sqjhc fvjkl (contains soy)"
//               //    "sqjhc mxmxvkd sbzzf (contains fish)" |]

//               let finalSingles, finalUnions = PartOne.cleanDataTilDone singles unions

//               let expectedSingles = Map.empty
//               let expectedUnions = Map.empty

//               Expect.equal finalSingles expectedSingles "I don't know what to expect"
//               Expect.equal finalUnions expectedUnions "this should be weird" ]
