﻿let Year = "2020"

let show part result = printfn "Part %s\n-> %s\n" part result

let runAndShowDay (solution: Common.ISolution) =
    solution.Name() |> printfn "Day %s!\n==========\n"

    match solution.LoadData(Year) with
    | Some (input) ->
        input |> solution.PartOne |> show "One"
        input |> solution.PartTwo |> show "Two"
    | None -> printfn "The data doesn't seem to exist!"

[<EntryPoint>]
let main argv =
    let day = argv.[0]

    let solutions: Common.ISolution array =
        [| DayOne.Solution()
           DayTwo.Solution()
           DayThree.Solution()
           DayFour.Solution()
           DayFive.Solution()
           DaySix.Solution()
           DaySeven.Solution()
           DayEight.Solution()
           DayNine.Solution()
           DayTen.Solution()
           DayEleven.Solution()
           DayTwelve.Solution()
           DayThirteen.Solution()
           DayFourteen.Solution()
           DayFifteen.Solution()
           DaySixteen.Solution()
           DaySeventeen.Solution()
           DayEighteen.Solution()
           DayNineteen.Solution()
           DayTwenty.Solution()
           DayTwentyOne.Solution()
           DayTwentyTwo.Solution() |]

    match day with
    | "one" -> solutions.[0] |> runAndShowDay
    | "two" -> solutions.[1] |> runAndShowDay
    | "three" -> solutions.[2] |> runAndShowDay
    | "four" -> solutions.[3] |> runAndShowDay
    | "five" -> solutions.[4] |> runAndShowDay
    | "six" -> solutions.[5] |> runAndShowDay
    | "seven" -> solutions.[6] |> runAndShowDay
    | "eight" -> solutions.[7] |> runAndShowDay
    | "nine" -> solutions.[8] |> runAndShowDay
    | "ten" -> solutions.[9] |> runAndShowDay
    | "eleven" -> solutions.[10] |> runAndShowDay
    | "twelve" -> solutions.[11] |> runAndShowDay
    | "thirteen" -> solutions.[12] |> runAndShowDay
    | "fourteen" -> solutions.[13] |> runAndShowDay
    | "fifteen" -> solutions.[14] |> runAndShowDay
    | "sixteen" -> solutions.[15] |> runAndShowDay
    | "seventeen" -> solutions.[16] |> runAndShowDay
    | "eighteen" -> solutions.[17] |> runAndShowDay
    | "nineteen" -> solutions.[18] |> runAndShowDay
    | "twenty" -> solutions.[19] |> runAndShowDay
    | "twentyone" -> solutions.[20] |> runAndShowDay
    | "twentytwo" -> solutions.[21] |> runAndShowDay
    | "all" -> solutions |> Array.iter runAndShowDay
    | _ -> failwith "Day invalid."

    0 // return an integer exit code
