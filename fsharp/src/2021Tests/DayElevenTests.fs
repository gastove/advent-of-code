module DayElevenTests

open Expecto

open DayEleven

let shortInput =
    [| "11111"
       "19991"
       "19191"
       "19991"
       "11111" |]

let input =
    [| "5483143223"
       "2745854711"
       "5264556173"
       "6141336146"
       "6357385478"
       "4167524645"
       "2176841721"
       "6882881134"
       "4846848554"
       "5283751526" |]

[<Tests>]
let partOneTests =
    testList
        "Testing part one"
        [ testCase "Can we reproduce steps from the example?"
          <| fun _ ->
              let got =
                  input |> parseInput |> PartOne.runNumberOfSteps 2

              let expected = 35

              Expect.equal got expected "We should get the right answer"

          testCase "Can we reproduce the short example?"
          <| fun _ ->
              let got =
                  shortInput
                  |> parseInput
                  |> PartOne.runNumberOfSteps 1

              let expected = 9

              Expect.equal got expected "Can we at least reproduce the short example?"

          testCase "Seriously WTF" <| fun _ ->
              let got =
                  input |> parseInput |> PartOne.runNumberOfSteps 100

              let expected = 1656

              Expect.equal got expected "We should get the right answer"

          ]
