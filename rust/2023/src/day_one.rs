mod part_one {

    fn find_number(s: &String) -> Result<String, String> {
        let first_idx = s.find(char::is_numeric);
        let second_idx = s.rfind(char::is_numeric);

        match (first_idx, second_idx) {
            (Some(fi), Some(si)) => {
                let first_char = s.chars().nth(fi).expect("Got a bad index");
                let second_char = s.chars().nth(si).expect("Got a bad index");

                Ok(format!("{}{}", first_char, second_char))
            }
            (_, _) => Err(format!("Failed to find a digit in {}", s)),
        }
    }

    pub fn run(input: Vec<String>) -> String {
        let nums: Result<Vec<String>, String> = input.iter().map(find_number).collect();
        match nums {
            Ok(nums) => nums
                .iter()
                .map(|n| n.parse::<usize>().expect("Failed to parse to usize"))
                .sum::<usize>()
                .to_string(),
            Err(e) => e,
        }
    }
}

mod part_two {

    use regex::Regex;

    fn resolve_str_to_string_digit(s: &str) -> String {
        let new_s = match s {
            "one" | "eno" => "1",
            "two" | "owt" => "2",
            "three" | "eerht" => "3",
            "four" | "ruof" => "4",
            "five" | "evif" => "5",
            "six" | "xis" => "6",
            "seven" | "neves" => "7",
            "eight" | "thgie" => "8",
            "nine" | "enin" => "9",
            or_else => or_else,
        };

        new_s.into()
    }

    fn extact_number_with_regex(s: &str, pattern: &str) -> Option<String> {
        let re = Regex::new(pattern).expect("Failed to build regex");

        let mut captures = re.find_iter(s);

        captures
            .nth(0)
            .map(|c| resolve_str_to_string_digit(c.as_str()))
    }

    fn find_number_more_harder(s: &String) -> String {
        let the_numbers = "one|two|three|four|five|six|seven|eight|nine";
        
        let forward_pattern = format!("({}|\\d)", the_numbers);
        let backwards_pattern = format!("({}|\\d)", the_numbers.chars().rev().collect::<String>());

        let first = extact_number_with_regex(s.as_str(), &forward_pattern);

        let second = extact_number_with_regex(s.chars().rev().collect::<String>().as_str(), &backwards_pattern);

        // println!("From {:?}, captured {:?} and {:?}", s, first, second);

        match (first, second) {
            (Some(fs), Some(ss)) => fs + &ss,
            // Sometimes we only have a single digit, so use it twice
            (Some(fs), None) => fs.clone() + &fs,
            (_, _) => panic!("Something fucked up"),
        }
    }

    pub fn run(input: Vec<String>) -> String {
        input
            .iter()
            .map(find_number_more_harder)
            .map(|n| n.parse::<usize>().expect("Failed to parse to usize"))
            .sum::<usize>()
            .to_string()
    }

    #[cfg(test)]
    mod test {

        use super::*;

        #[test]
        fn test_run() {
            let input: Vec<String> = vec![
                "two1nine",
                "eightwothree",
                "abcone2threexyz",
                "xtwone3four",
                "4nineeightseven2",
                "zoneight234",
                "7pqrstsixteen",
            ]
            .iter()
            .map(|s| s.to_string())
            .collect();

            let got = run(input);

            assert_eq!(got, "281", "We should be able to reproduce the example");
        }
    }
}

pub fn run(input: Vec<String>) {
    println!("=== Day One! ===");
    println!("Part One\t=> {}", part_one::run(input.clone()));
    println!("Part Two\t=> {}", part_two::run(input));
}
