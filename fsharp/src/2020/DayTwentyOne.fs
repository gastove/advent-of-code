module DayTwentyOne

open FSharp.Core.Operators.Checked

let fakeInput =
    [| "mxmxvkd kfcds sqjhc nhms (contains dairy, fish)"
       "trh fvjkl sbzzf mxmxvkd (contains dairy)"
       "sqjhc fvjkl (contains soy)"
       "sqjhc mxmxvkd sbzzf (contains fish)" |]

module Parsing =
    open System.Text.RegularExpressions

    let AllergensRe = Regex("""(.+)\(contains (.+)\)""")

    let parseLine (line: string) =
        let groups = AllergensRe.Match(line)

        let captures = groups.Groups

        let ingredients = captures.[1].Value
        let allergens = captures.[2].Value

        ingredients.Split(" ")
        |> Array.filter Common.Strings.notBlank,
        allergens.Split(", ")
        |> Array.filter Common.Strings.notBlank

    let computeOccurences (ingredients: string array array) =
        ingredients
        |> Array.concat
        |> Array.countBy id
        |> Map.ofArray

    let computeAllIngredients (ingredients: string array array) =
        ingredients |> Array.concat |> Set.ofArray

    let reduceSets (data: (string * Set<string>) array) =
        data
        |> Array.fold
            (fun (allergenMap: Map<string, Set<string>>) (allergen: string, ingredients: Set<string>) ->
                allergenMap
                |> Map.change allergen (fun maybeV ->
                    maybeV
                    |> Option.map (Set.intersect ingredients)
                    |> Option.defaultValue ingredients
                    |> Some))
            Map.empty

    let flattenSets (ingredients: string array) (allergens: string array) =
        let ingredientSet = ingredients |> Set.ofArray

        allergens
        |> Array.map (fun allergen -> allergen, ingredientSet)

module PartOne =

    let computeAllergenMap (raw: (string array * string array) array) =
        raw
        |> Array.collect (fun (i, a) -> Parsing.flattenSets i a)
        |> Parsing.reduceSets

    let computeCannotBeAllergens (ingredients: string array array) (allergenMap: Map<string, Set<string>>) =
        let allIngredients =
            ingredients |> Parsing.computeAllIngredients

        allergenMap
        |> Map.fold (fun notAllergens _ ingredients -> Set.difference notAllergens ingredients) allIngredients

    let partOne (input: Common.Input) =
        let raw = input |> Array.map Parsing.parseLine
        let ingredients = raw |> Array.map fst
        let allergenMap = raw |> computeAllergenMap

        let occurences = ingredients |> Parsing.computeOccurences

        let cannotBeAllergens =
            computeCannotBeAllergens ingredients allergenMap

        cannotBeAllergens
        |> Set.toArray
        |> Array.map (fun i -> Map.find i occurences)
        |> Array.sum
        |> string

module PartTwo =

    let identifyAllergens (allergenCandidatesMap: Map<string, Set<string>>) (cannotBeAllergens: Set<string>) =
        let rec work (inProgress: Map<string, Set<string>>) (identified: Map<string, Set<string>>) =
            if inProgress.Count = 0 then
                identified
            else
                // Find every allergen with only a single ingredient; combine with the identified map
                let identifiedAllergens =
                    inProgress
                    |> Map.filter (fun _ ingredients -> ingredients.Count = 1)
                    |> Map.fold (fun newIded allergen ingredient -> Map.add allergen ingredient newIded) identified

                let identifiedIngredients =
                    identifiedAllergens
                    |> Map.toArray
                    |> Array.map snd
                    |> Set.unionMany

                let cleaned =
                    inProgress
                    |> Map.filter (fun allergen _ ->
                        Map.containsKey allergen identifiedAllergens
                        |> not)
                    |> Map.map (fun _ ingredients -> Set.difference ingredients identifiedIngredients)

                work cleaned identifiedAllergens

        let baseAllergenMap =
            allergenCandidatesMap
            |> Map.map (fun _ ingredients -> Set.difference ingredients cannotBeAllergens)

        work baseAllergenMap Map.empty
        |> Map.map (fun _ ingredients -> ingredients |> Set.toArray |> Array.exactlyOne)

    let partTwo (input: Common.Input) =
        let raw = input |> Array.map Parsing.parseLine

        let ingredients = raw |> Array.map fst

        let allergenMap = raw |> PartOne.computeAllergenMap

        let cannotBeAllergens =
            PartOne.computeCannotBeAllergens ingredients allergenMap

        let allergens =
            identifyAllergens allergenMap cannotBeAllergens

        allergens
        |> Map.toArray
        |> Array.map (fun (a, i) -> i, a)
        |> Array.sortBy snd
        |> Array.map fst
        |> String.concat ","

type Solution() =
    let name = "twenty_one"

    interface Common.ISolution with
        member __.Name() = name |> Common.titleCase
        member __.LoadData(year) : Common.Input option = Common.loadData name year
        member __.PartOne(input: Common.Input) : string = input |> PartOne.partOne
        member __.PartTwo(input: Common.Input) : string = input |> PartTwo.partTwo
