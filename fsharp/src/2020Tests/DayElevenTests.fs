module DayElevenTests

open Expecto

[<Tests>]
let inputParsingTests =
    testList
        "Testing DayEleven.Position"
        [ testCase "Parse parses correctly"
          <| fun _ ->
              let input = [ '#'; 'L'; '.' ]

              let expected =
                  [ Some DayEleven.Occupied
                    Some DayEleven.Empty
                    Some DayEleven.Floor ]

              let got = input |> List.map DayEleven.State.Parse

              Expect.equal got expected "We should be able to parse positions" ]

[<Tests>]
let neighborTests =
    let testNodes = Array.allPairs [| 0 .. 2 |] [| 0 .. 2 |]
    let xMax = 2
    let yMax = 2
    let neighborFn = DayEleven.makeIsNeighbor xMax yMax

    testList
        "Testing DayEleven Neighbor functions"
        [ testCase "isNeighbor returns correct neighbors in simple cases"
          <| fun _ ->
              let testNode = 1, 1

              let expected =
                  testNodes |> Array.filter (fun n -> n <> testNode)

              let got =
                  testNodes |> Array.filter (neighborFn testNode)

              Expect.equal got expected "All nodes are neighbors of 1,1 except itself"

          testCase "isNeighbor correctly handles nodes at edges"
          <| fun _ ->
              let testNode = 0, 1

              let expected =
                  [| 0, 0; 1, 0; 1, 1; 1, 2; 0, 2 |] |> Set.ofArray

              let got =
                  testNodes
                  |> Array.filter (neighborFn testNode)
                  |> Set.ofArray

              Expect.equal got expected "We should get the correct neighbors" ]

[<Tests>]
let partTwoSeatingChartTests =
    testList
        "PartTwoSeatingChart"
        [ testCase "New should correctly parse a seating chart"
          <| fun _ ->
              let input = [| ".#L" |]
              let testChart = DayEleven.PartTwoSeatingChart.New input

              let expectedIndex =
                  [| (0, 0), DayEleven.Floor
                     (1, 0), DayEleven.Occupied
                     (2, 0), DayEleven.Empty |]

              Expect.equal testChart.Indexed expectedIndex "We should get a correct parse" ]

// occupied seat is at 3,4
// 9 rows, 9 columns
let testBlockOne =
    """
.......#.
...#.....
.#.......
.........
..#L....#
....#....
.........
#........
...#.....
"""

let testBlockTwo =
    """
.............
.L.L.#.#.#.#.
.............
"""

let testBlockThree =
    """
.##.##.
#.#.#.#
##...##
...L...
##...##
#.#.#.#
.##.##.
"""

let toInput (from: string) : Common.Input =
    from.Split '\n'
    |> Array.filter (fun s -> s.Length > 0)
    |> Array.map (fun s -> s.Trim())

[<Tests>]
let findAndCheckTests =
    testList
        "Testing PartTwo.occupado"
        [ testCase "findSeatAtPosition works, literally at all"
          <| fun _ ->
              let input = toInput testBlockOne
              let testChart = DayEleven.PartTwoSeatingChart.New input
              let seat = 2, 4
              let expected = Some(seat, DayEleven.Occupied)

              let got =
                  DayEleven.findSeatAtPosition testChart.Indexed seat

              Expect.equal got expected "We should be able to find a known-good seat"

          testCase "finds all expected occupied seats"
          <| fun _ ->
              let input = toInput testBlockOne
              let testChart = DayEleven.PartTwoSeatingChart.New input
              let seat = 3, 4
              let expected = 8
              let got = testChart.OccupiedNeighbors seat

              Expect.equal got expected "We should get the correct number of occupied seats"

          testCase "finds no occupied seats immediately to the right"
          <| fun _ ->
              let input = toInput testBlockTwo
              let testChart = DayEleven.PartTwoSeatingChart.New input
              let seat = 1, 1
              let expected = 0
              let got = testChart.OccupiedNeighbors seat

              Expect.equal got expected "We should find no occupied seats"

          testCase "finds no occupied seats in cardinal directions"
          <| fun _ ->
              let input = toInput testBlockThree
              let testChart = DayEleven.PartTwoSeatingChart.New input
              let seat = 3, 3
              let expected = 0
              let got = testChart.OccupiedNeighbors seat

              Expect.equal got expected "We should find no occupied seats" ]
