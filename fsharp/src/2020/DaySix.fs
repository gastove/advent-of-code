module DaySix

//--------------------------------- Part One ---------------------------------//
// This list represents answers from five groups:

//     The first group contains one person who answered "yes" to 3 questions: a, b, and c.
//     The second group contains three people; combined, they answered "yes" to 3 questions: a, b, and c.
//     The third group contains two people; combined, they answered "yes" to 3 questions: a, b, and c.
//     The fourth group contains four people; combined, they answered "yes" to only 1 question, a.
//     The last group contains one person who answered "yes" to only 1 question, b.

// In this example, the sum of these counts is 3 + 3 + 3 + 1 + 1 = 11.

// For each group, count the number of questions to which anyone answered "yes". What is the sum of those counts?

//--------------------------------- Part Two ---------------------------------//


type LineBuilder =
    { mutable Current: string list
      mutable Final: string list list }

    member this.Clear() =
        this.Final <- this.Final @ [ this.Current ]
        this.Current <- []

    member this.Append(item: string) = this.Current <- this.Current @ [ item ]

    member this.Return(): string array =
        let res =
            this.Final
            |> List.map (fun l -> String.concat "" l)
            |> List.toArray
        res

    static member New() =
        { Current = []
          Final = [] }

let isBlank (s: string) = System.String.IsNullOrEmpty(s) || System.String.IsNullOrWhiteSpace(s)

let cleanLines (lines: string array): string array =
    let folder (acc: LineBuilder) (item: string): LineBuilder =
        if isBlank item then acc.Clear() else acc.Append item

        acc

    let builder = lines |> Array.fold folder (LineBuilder.New())
    // Clear before we return, make sure there's nothing in the buffer
    builder.Clear()
    builder.Return()

let buildSet (input: string): Set<char> =
    input
    |> Seq.toArray
    |> set

let partOne (input: string array) =
    input
    |> cleanLines
    |> Array.sumBy (buildSet >> Set.count)

// Compute the set intersection of each group
type IntersectionBuilder =
    { mutable Current: Set<char> list
      mutable Final: Set<char> list }

    member this.Clear() =
        this.Final <- this.Final @ [ this.Current |> Set.intersectMany]
        this.Current <- []

    member this.Add(item: string) =
        this.Current <- this.Current @ [item |> set]

    member this.Return(): Set<char> array =
        let res =
            this.Final
            |> List.toArray
        res

    static member New() =
        { Current = []
          Final = [] }


let computeSets (lines: string array): Set<char> array =
    let folder (acc: IntersectionBuilder) (item: string): IntersectionBuilder =
        if isBlank item then acc.Clear() else acc.Add item

        acc

    let builder = lines |> Array.fold folder (IntersectionBuilder.New())
    // Clear before we return, make sure there's nothing in the buffer
    builder.Clear()
    builder.Return()

let partTwo (input: string array) =
    input
    |> computeSets
    |> Array.sumBy Set.count

type Solution() =
    let name = "six"
    interface Common.ISolution with
        member __.LoadData(year): Common.Input option =
            Common.loadData name year
        member __.PartOne(input: Common.Input): string =
            let partOne = partOne input
            sprintf "Sum of yes questions: %i" partOne

        member __.PartTwo(input: Common.Input): string =
            let partTwo = partTwo input
            sprintf "Sum of unanimous yes questions: %i" partTwo

        member __.Name() = name |> Common.titleCase
