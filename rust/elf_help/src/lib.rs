use std::{
    collections::HashSet,
    fs::File,
    io::{BufRead, BufReader, Error},
};

pub fn read_input(path: String) -> Result<Vec<String>, Error> {
    let file = File::open(path)?;
    let buf = BufReader::new(file);
    Ok(buf.lines().map(|l| l.unwrap()).collect())
}

#[derive(Debug, Clone, Default, Hash, Eq, PartialEq)]
pub struct Point {
    pub x: usize,
    pub y: usize,
}

impl Point {
    pub fn new(x: usize, y: usize) -> Self {
        Point { x, y }
    }

    pub fn neighbors(&self) -> HashSet<Self> {
        let mut points = HashSet::new();
        (self.x - 1..=self.x + 1).for_each(|x| {
            (self.y - 1..=self.y + 1).for_each(|y| {
                let _ = points.insert(Point::new(x, y));
            })
        });

        points
    }
}

#[derive(Debug, Clone)]
pub struct Range {
    pub minimum: usize,
    pub maxiumum: usize,
}

impl Range {
    pub fn new(min: usize, max: usize) -> Self {
        Range {
            minimum: min,
            maxiumum: max,
        }
    }

    pub fn contains(&self, needle: &usize) -> bool {
        &self.minimum <= needle && needle <= &self.maxiumum
    }

    pub fn numbers(&self) -> Vec<usize> {
        (self.minimum..self.maxiumum).collect()
    }

    pub fn overlaps(&self, other: &Self) -> bool {
        self.contains(&other.minimum) || self.contains(&other.maxiumum)
    }
}
