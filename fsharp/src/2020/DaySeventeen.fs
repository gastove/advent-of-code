module DaySeventeen

open FSharp.Core.Operators.Checked

module Shared =

    type IPoint<'T> =
        abstract ComputeNeighbors: unit -> 'T array

    type State =
        | Active
        | Inactive // Note: default state
        static member Parse c =
            match c with
            | '.' -> Some Inactive
            | '#' -> Some Active
            | _ -> None

    let currentState point (space: Map<_, State>): State =
        match space |> Map.tryFind point with
        | Some state -> state
        | None -> Inactive

    let updateState (point: IPoint<_>) (initialState: State) (space: Map<_, State>): State =
        let neighbors = point.ComputeNeighbors()

        let activeNeighbors =
            neighbors
            |> Array.map (fun p -> currentState p space)
            |> Array.filter (fun s -> s = Active)
            |> Array.length

        match initialState, activeNeighbors with
        | Active, 2
        | Active, 3 -> Active
        | Inactive, 3 -> Active
        | _, _ -> Inactive

    let encorporateNeighbors<'P when 'P :> IPoint<'P> and 'P: comparison> (space: Map<'P, State>) =
        let neighbors =
            space
            |> Map.toArray
            |> Array.Parallel.collect (fun (point, _) -> point.ComputeNeighbors())

        neighbors
        |> Array.fold
            (fun (state: Map<'P, State>) point ->
                if Map.containsKey point state then state else Map.add point Inactive state) space

    let update<'P when 'P :> IPoint<'P> and 'P: comparison> (space: Map<'P, State>) =
        space
        |> encorporateNeighbors
        |> Map.toArray
        |> Array.Parallel.map (fun (point, state) -> point, updateState point state space)
        |> Map.ofArray


    let parseLine (input: string) =
        input
        |> Seq.toArray
        |> Array.choose State.Parse

module PartOne =

    type Point =
        { X: int64
          Y: int64
          Z: int64 }
        interface Shared.IPoint<Point> with
            member this.ComputeNeighbors() =
                let spread i =
                    [| i - 1L
                       i
                       i + 1L |]

                let xPositions = spread this.X
                let yPositions = spread this.Y
                let zPositions = spread this.Z

                Array.allPairs xPositions yPositions
                |> Array.allPairs zPositions
                |> Array.map (fun (z, (x, y)) ->
                    { X = x
                      Y = y
                      Z = z })
                |> Array.filter (fun point -> point <> this)


module PartTwo =
    type Point =
        { X: int64
          Y: int64
          Z: int64
          W: int64 }
        interface Shared.IPoint<Point> with
            member this.ComputeNeighbors() =
                let spread i =
                    [| i - 1L
                       i
                       i + 1L |]

                let xPositions = spread this.X
                let yPositions = spread this.Y
                let zPositions = spread this.Z
                let wPositions = spread this.W

                Array.allPairs xPositions yPositions
                |> Array.allPairs zPositions
                |> Array.allPairs wPositions
                |> Array.map (fun (w, (z, (x, y))) ->
                    { X = x
                      Y = y
                      Z = z
                      W = w })
                |> Array.filter (fun point -> point <> this)


open Shared

type Solution() =
    let name = "seventeen"
    interface Common.ISolution with
        member __.Name() = name |> Common.titleCase
        member __.LoadData(year): Common.Input option = Common.loadData name year

        member __.PartOne(input: Common.Input): string =
            let mutable space =
                input
                |> Array.map parseLine
                |> Array.mapi (fun yIdx row ->
                    row
                    |> Array.mapi (fun xIdx state ->
                        { PartOne.Point.X = int64 xIdx
                          PartOne.Point.Y = int64 yIdx
                          PartOne.Point.Z = 0L }, state))
                |> Array.concat
                |> Map.ofArray

            {1..6} |> Seq.iter (fun _ -> space <- update space)

            sprintf "Found this many active cubes: %i"
                (space
                 |> Map.toArray
                 |> Array.map snd
                 |> Array.filter (fun s -> s = Active)
                 |> Array.length)

        member __.PartTwo(input: Common.Input): string =
            let mutable space =
                input
                |> Array.map parseLine
                |> Array.mapi (fun yIdx row ->
                    row
                    |> Array.mapi (fun xIdx state ->
                        { PartTwo.Point.X = int64 xIdx
                          PartTwo.Point.Y = int64 yIdx
                          PartTwo.Point.Z = 0L
                          PartTwo.Point.W = 0L}, state))
                |> Array.concat
                |> Map.ofArray

            {1..6} |> Seq.iter (fun _ -> space <- update space)

            sprintf "Found this many active cubes: %i"
                (space
                 |> Map.toArray
                 |> Array.map snd
                 |> Array.filter (fun s -> s = Active)
                 |> Array.length)
