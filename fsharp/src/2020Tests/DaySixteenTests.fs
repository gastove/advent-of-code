module DaySixteenTests

open Expecto

[<Tests>]
let tests =
    testList "Can we do some basic stuff"
        [ testCase "Test an individual Rule" <| fun _ ->
            let testRule = "class: 1-3 or 5-7" |> DaySixteen.Rule.Parse |> Option.get

            Expect.isTrue (testRule.SatisfiedBy 2) "This is in the specified ranges"
            Expect.isFalse (testRule.SatisfiedBy 4) "This is not in the specified ranges"

          testCase "Can we run a basic unit test against provided data?" <| fun _ ->
            let testData =
                [| [| 7; 3; 47 |]
                   [| 40; 4; 50 |]
                   [| 55; 2; 20 |]
                   [| 38; 6; 12 |] |]
                |> Array.map (fun row -> { DaySixteen.Ticket.Fields = row })

            let expected = [| 4; 55; 12 |]
            let rulesInput = [| "class: 1-3 or 5-7"; "row: 6-11 or 33-44"; "seat: 13-40 or 45-50" |]
            let rules =
                { DaySixteen.Rules.AllTheRules = rulesInput |> Array.choose (fun s -> DaySixteen.Rule.Parse s) }
            let got = testData |> Array.collect rules.ApplyPartOne

            Expect.equal got expected "These should match"

          testCase "Can we correctly filter valid tickets?" <| fun _ ->
            let testData =
                [| [| 7; 3; 47 |]
                   [| 40; 4; 50 |]
                   [| 55; 2; 20 |]
                   [| 38; 6; 12 |] |]
                |> Array.map (fun row -> { DaySixteen.Ticket.Fields = row })

            let expected = {DaySixteen.Ticket.Fields = [| 7; 3; 47 |]} |> Array.singleton
            let rulesInput = [| "class: 1-3 or 5-7"; "row: 6-11 or 33-44"; "seat: 13-40 or 45-50" |]
            let rules =
                { DaySixteen.Rules.AllTheRules = rulesInput |> Array.choose (fun s -> DaySixteen.Rule.Parse s) }
            let got = testData |> Array.filter rules.Valid

            Expect.equal got expected "These should match"
          testCase "Can we validate a field?" <| fun _ ->

            let rulesInput = [| "class: 1-3 or 5-7"; "row: 6-11 or 33-44"; "seat: 13-40 or 45-50" |]
            let rules =
                { DaySixteen.Rules.AllTheRules = rulesInput |> Array.choose (fun s -> DaySixteen.Rule.Parse s) }

            let howManyMatch num =
                rules.ValidateField num |> Array.length

            Expect.equal 2 (howManyMatch 6) "These should match"
            Expect.equal 0 (howManyMatch 4) "These should match" ]
