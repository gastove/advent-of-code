module DayOne

// Specifically, they need you to find the two entries that sum to 2020 and then multiply those two numbers together.

// For example, suppose your expense report contained the following:

// 1721
// 979
// 366
// 299
// 675
// 1456

// In this list, the two entries that sum to 2020 are 1721 and 299. Multiplying them together produces 1721 * 299 = 514579, so the correct answer is 514579.

type Pair = int * int

let tryInt toTry =
    try
        Some(System.Int32.Parse(toTry))
    with _ -> None

let genTuples (input: int array): Pair array = Array.allPairs input input

let compute (input: Pair array): (Pair * int) array =
    input
    |> Array.map (fun (f, s) -> (f, s), f + s)

let partOne (input: string array): int option =
    input
    |> Array.choose tryInt
    |> genTuples
    |> compute
    |> Array.tryFind (fun (_pair, sum) -> sum = 2020)
    |> Option.map (fun (pair, _) -> pair)
    |> Option.map (fun (f, s) -> f * s)

let partTwo (input: string array): int option =
    let parsed = input |> Array.choose tryInt

    parsed
    |> genTuples
    |> compute
    |> Array.tryFind (fun (_pair, sum) -> Array.contains (2020 - sum) parsed)
    |> Option.map (fun ((f, s), sum) -> f * s * (2020 - sum))

type Solution() =
    let name = "one"
    interface Common.ISolution with
        member __.Name() = Common.titleCase name
        member __.LoadData year: Common.Input option =
            Common.loadData name year
        member __.PartOne (input: Common.Input): string =
            match partOne input with
            | Some v -> sprintf "For part one, got: %i" v
            | None -> sprintf "Failed to find a result"
        member __.PartTwo(input: Common.Input): string =
            match partTwo input with
            | Some v -> sprintf "For part two, got: %i" v
            | None -> sprintf "Failed to find a result"
