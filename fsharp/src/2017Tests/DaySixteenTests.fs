module DaySixteenTests

open Expecto

[<Tests>]
let danceMoveTests =
    testList
        "Testing our Dance Moves"
        [ testCase "Test Spin"
          <| fun _ ->
              let dancers = [ 'a'; 'b'; 'c'; 'd'; 'e' ]
              let instruction = DaySixteen.Spin 3

              let got = instruction.Do dancers

              let expected = [ 'c'; 'd'; 'e'; 'a'; 'b' ]

              Expect.equal got expected "We should be able to spin"

          testCase "Test Exchange" <| fun _ ->
              let dancers = [ 'a'; 'b'; 'c'; 'd'; 'e' ]
              let instruction = DaySixteen.Exchange(2, 4)

              let got = instruction.Do dancers

              let expected = [ 'a'; 'b'; 'e'; 'd'; 'c' ]

              Expect.equal got expected "We should be able to exchange"

          testCase "Test Partner" <| fun _ ->
              let dancers = [ 'a'; 'b'; 'c'; 'd'; 'e' ]
              let instruction = DaySixteen.Partner('a', 'c')

              let got = instruction.Do dancers

              let expected = [ 'c'; 'b'; 'a'; 'd'; 'e' ]

              Expect.equal got expected "We should be able to partner"
          ]
