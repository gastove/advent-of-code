module DayEleven

type Seat = int * int

type State =
    | Occupied
    | Empty
    | Floor
    static member Parse(input: char) =
        match input with
        | '#' -> Some Occupied
        | 'L' -> Some Empty
        | '.' -> Some Floor
        | _ -> None

let makeIsNeighbor (xMax: int) (yMax: int) =
    let isNeighbor (seat: int * int) (maybeNeighbor: int * int): bool =
        // Exit early if we're asking if a node is its own neighbor
        if seat = maybeNeighbor then
            false
        else
            let seatXMin, seatXMax =
                let seatX = fst seat
                max (seatX - 1) 0, min (seatX + 1) xMax

            let seatYMin, seatYMax =
                let seatY = snd seat
                max (seatY - 1) 0, min (seatY + 1) yMax

            let maybeX, maybeY = maybeNeighbor

            seatXMin <= maybeX && maybeX <= seatXMax && seatYMin <= maybeY && maybeY <= seatYMax

    isNeighbor

type ISeatingChart =
    abstract Update: unit -> ISeatingChart * int

type PartOneSeatingChart =
    { Layout: State array array
      Indexed: ((int * int) * State) array
      XMax: int
      YMax: int }

    static member Index(layout: State array array) =
        layout
        |> Array.mapi (fun xidx row -> row |> Array.mapi (fun yidx pos -> (xidx, yidx), pos))
        |> Array.concat

    static member New(input: Common.Input) =
        let layout =
            input
            |> Array.map (fun s ->
                s
                |> Seq.toArray
                |> Array.choose State.Parse)

        let indexed = PartOneSeatingChart.Index layout

        let yMax = layout |> Array.length
        let xMax = layout.[0] |> Array.length

        { Layout = layout
          Indexed = indexed
          XMax = xMax
          YMax = yMax }

    member this.NeighboringStates(seat: Seat) =
        let neighborFn = makeIsNeighbor this.XMax this.YMax
        this.Indexed
        |> Array.filter (fun (maybeNeighbor, _) -> neighborFn seat maybeNeighbor)
        |> Array.map (fun (_, pos) -> pos)

    member this.UpdateSeat (seat: Seat) (startingState: State): State =
        let neighbors = this.NeighboringStates seat

        let occupiedAdjacent =
            neighbors
            |> Array.filter (fun s -> s = Occupied)
            |> Array.length

        match startingState with
        | Empty ->
            if occupiedAdjacent = 0 then Occupied else Empty
        | Occupied ->
            if occupiedAdjacent >= 4 then Empty else Occupied
        | Floor -> Floor

    interface ISeatingChart with
        member this.Update(): ISeatingChart * int =
            let newIndexed =
                this.Indexed |> Array.Parallel.map (fun (seat, state) -> seat, (this.UpdateSeat seat state))

            let changes =
                Array.zip this.Indexed newIndexed
                |> Array.sumBy (fun (f, s) ->
                    if f = s then 0 else 1)

            { this with Indexed = newIndexed } :> ISeatingChart, changes

let findSeatAtPosition (layout: ((int * int) * State) array) (position: int * int) =
    let posX, posY = position

    layout |> Array.tryFind (fun (seat, _) -> fst seat = posX && snd seat = posY)

let findAndCheck (layout: ((int * int) * State) array) (x: int) (y: int): int option =
    findSeatAtPosition layout (x, y)
        |> Option.bind (fun (_, state) ->
            match state with
            | Occupied -> Some 1
            | Empty -> Some 0
            | _ -> None)

let truncateAndZip<'T> (xArray: 'T array) (yArray: 'T array): ('T * 'T) array =
    let correctedX, correctedY =
        match xArray.Length, yArray.Length with
        | xLen, yLen when xLen > yLen ->
            let trimmedX = Array.truncate yArray.Length xArray
            trimmedX, yArray
        | xLen, yLen when xLen < yLen ->
            let trimmedY = Array.truncate xArray.Length yArray
            xArray, trimmedY
        | _, _ -> xArray, yArray

    Array.zip correctedX correctedY

let occupado (layout: ((int * int) * State) array) (xLimit: int) (yLimit: int) (seat: Seat): int =
    let seatX, seatY = seat
    let xMin, xMax = 0, xLimit
    let yMin, yMax = 0, yLimit

    let leftIndices = [| seatX - 1 .. -1 .. xMin |]
    let rightIndices = [| seatX + 1 .. xMax |]
    let upIndicies = [| seatY - 1 .. -1 .. yMin |]
    let downIndices = [| seatY + 1 .. yMax |]

    let left = leftIndices |> Array.tryPick (fun x -> findAndCheck layout x seatY)
    let right = rightIndices |> Array.tryPick (fun x -> findAndCheck layout x seatY)
    let up = upIndicies |> Array.tryPick (fun y -> findAndCheck layout seatX y)
    let down = downIndices |> Array.tryPick (fun y -> findAndCheck layout seatX y)

    let finder = fun (x, y) -> findAndCheck layout x y
    let upLeft = truncateAndZip leftIndices upIndicies |> Array.tryPick finder
    let upRight = truncateAndZip rightIndices upIndicies |> Array.tryPick finder
    let downLeft = truncateAndZip leftIndices downIndices |> Array.tryPick finder
    let downRight = truncateAndZip rightIndices downIndices |> Array.tryPick finder

    [| left; right; up; down; upLeft; upRight; downLeft; downRight |] |> Array.choose id |> Array.sum


type PartTwoSeatingChart =
    { Indexed: ((int * int) * State) array
      XMax: int
      YMax: int }

    static member Index(layout: State array array) =
        layout
        |> Array.mapi (fun yidx row -> row |> Array.mapi (fun xidx pos -> (xidx, yidx), pos))
        |> Array.concat

    static member New(input: Common.Input) =
        let layout =
            input
            |> Array.map (fun s ->
                s.Trim()
                |> Seq.toArray
                |> Array.choose State.Parse)

        let indexed = PartTwoSeatingChart.Index layout

        let yMax = (layout |> Array.length) - 1
        let xMax = (layout.[0] |> Array.length) - 1

        { Indexed = indexed
          XMax = xMax
          YMax = yMax }

    member this.OccupiedNeighbors(seat: Seat) = occupado this.Indexed this.XMax this.YMax seat

    member this.UpdateSeat (seat: Seat) (startingState: State): State =
        let occupiedAdjacent = this.OccupiedNeighbors seat

        match startingState with
        | Empty ->
            if occupiedAdjacent = 0 then Occupied else Empty
        | Occupied ->
            if occupiedAdjacent >= 5 then Empty else Occupied
        | Floor -> Floor

    interface ISeatingChart with
        member this.Update(): ISeatingChart * int =
            let newIndexed =
                this.Indexed |> Array.Parallel.map (fun (seat, state) -> seat, (this.UpdateSeat seat state))

            let changes =
                Array.zip this.Indexed newIndexed
                |> Array.sumBy (fun (f, s) ->
                    if f = s then 0 else 1)

            { this with Indexed = newIndexed } :> ISeatingChart, changes


let rec solve (chart: ISeatingChart) =
    match chart.Update() with
    | newChart, 0 -> newChart
    | newChart, _ -> solve newChart

type Solution() =
    let name = "eleven"
    interface Common.ISolution with
        member __.LoadData(year): Common.Input option = Common.loadData name year

        member __.PartOne(input: Common.Input): string =
            let chart = PartOneSeatingChart.New input
            let solved = chart |> solve :?> PartOneSeatingChart // Unpack our interface back to a SeatingChart

            let occupiedSeats =
                solved.Indexed
                |> Array.countBy (fun (_, pos) -> pos)
                |> Map.ofArray

            sprintf "I think this is the number of occupied seats: %i" occupiedSeats.[Occupied]

        member __.PartTwo(input: Common.Input): string =
            let chart = PartTwoSeatingChart.New input
            // sprintf "Data has XMax %i and YMax %i" chart.XMax chart.YMax
            let solved = chart |> solve :?> PartTwoSeatingChart // Unpack our interface back to a SeatingChart

            let occupiedSeats =
                solved.Indexed
                |> Array.countBy (fun (_, pos) -> pos)
                |> Map.ofArray

            sprintf "I think this is the number of occupied seats: %i" occupiedSeats.[Occupied]

        member __.Name() = name |> Common.titleCase
