module DayTwentyFive

open FSharp.Core.Operators.Checked

type State =
    | A
    | B
    | C
    | D
    | E
    | F
    static member Initial = A

// Begin in state A.
// Perform a diagnostic checksum after 12261543 steps.

// In state A:
//   If the current value is 0:
//     - Write the value 1.
//     - Move one slot to the right.
//     - Continue with state B.
//   If the current value is 1:
//     - Write the value 0.
//     - Move one slot to the left.
//     - Continue with state C.

// In state B:
//   If the current value is 0:
//     - Write the value 1.
//     - Move one slot to the left.
//     - Continue with state A.
//   If the current value is 1:
//     - Write the value 1.
//     - Move one slot to the right.
//     - Continue with state C.

// In state C:
//   If the current value is 0:
//     - Write the value 1.
//     - Move one slot to the right.
//     - Continue with state A.
//   If the current value is 1:
//     - Write the value 0.
//     - Move one slot to the left.
//     - Continue with state D.

// In state D:
//   If the current value is 0:
//     - Write the value 1.
//     - Move one slot to the left.
//     - Continue with state E.
//   If the current value is 1:
//     - Write the value 1.
//     - Move one slot to the left.
//     - Continue with state C.

// In state E:
//   If the current value is 0:
//     - Write the value 1.
//     - Move one slot to the right.
//     - Continue with state F.
//   If the current value is 1:
//     - Write the value 1.
//     - Move one slot to the right.
//     - Continue with state A.

// In state F:
//   If the current value is 0:
//     - Write the value 1.
//     - Move one slot to the right.
//     - Continue with state A.
//   If the current value is 1:
//     - Write the value 1.
//     - Move one slot to the right.
//     - Continue with state E.

type CursorMove =
    | Left
    | Right

type TapeValue =
    | Zero
    | One
    member this.ToInt() =
        match this with
        | Zero -> 0
        | One -> 1

module TapeValue =
    let toInt (v: TapeValue) = v.ToInt()

type Tape =
    { mutable ZeroAndPositive: TapeValue ResizeArray
      mutable Negative: TapeValue ResizeArray }
    static member Create pos neg =
        { ZeroAndPositive = pos
          Negative = neg }

    static member Empty =
        let zap = ResizeArray()
        zap.Insert(0, Zero)
        let n = ResizeArray()
        n.Insert(0, Zero)
        Tape.Create zap n

    member this.Get idx =
        if idx >= 0 then
            if idx >= this.ZeroAndPositive.Count then
                this.ZeroAndPositive.Add Zero

                while idx >= this.ZeroAndPositive.Count do
                    this.ZeroAndPositive.Add Zero

            this.ZeroAndPositive.[idx]
        else
            let absIdx = (idx |> abs) - 1

            if absIdx >= this.Negative.Count then
                this.Negative.Add Zero

                while absIdx >= this.Negative.Count do
                    this.Negative.Add Zero

            this.Negative.[absIdx]

    member this.CheckSum =
        [| this.ZeroAndPositive.ToArray()
           this.Negative.ToArray() |]
        |> Array.concat
        |> Array.Parallel.map TapeValue.toInt
        |> Array.sum

    member this.Set idx value =

        if idx >= 0 then
            this.ZeroAndPositive.RemoveAt(idx)
            this.ZeroAndPositive.Insert(idx, value)
        else
            let absIdx = (idx |> abs) - 1
            this.Negative.RemoveAt(absIdx)
            this.Negative.Insert(absIdx, value)

let runTape (iterations: int) =
    let blankTape = Tape.Empty

    let rec run (tape: Tape) cursor state remaining =
        if remaining = 0 then
            tape
        else
            let currentValue = tape.Get cursor
            let newRemaining = remaining - 1

            match state with
            | A ->
                match currentValue with
                | Zero ->
                    tape.Set cursor One
                    run tape (cursor + 1) B newRemaining
                | One ->
                    tape.Set cursor Zero
                    run tape (cursor - 1) C newRemaining
            | B ->
                match currentValue with
                | Zero ->
                    tape.Set cursor One
                    run tape (cursor - 1) A newRemaining
                | One ->
                    tape.Set cursor One
                    run tape (cursor + 1) C newRemaining
            | C ->
                match currentValue with
                | Zero ->
                    tape.Set cursor One
                    run tape (cursor + 1) A newRemaining
                | One ->
                    tape.Set cursor Zero
                    run tape (cursor - 1) D newRemaining
            | D ->
                match currentValue with
                | Zero ->
                    tape.Set cursor One
                    run tape (cursor - 1) E newRemaining
                | One ->
                    tape.Set cursor One
                    run tape (cursor - 1) C newRemaining
            | E ->
                match currentValue with
                | Zero ->
                    tape.Set cursor One
                    run tape (cursor + 1) F newRemaining
                | One ->
                    tape.Set cursor One
                    run tape (cursor + 1) A newRemaining
            | F ->
                match currentValue with
                | Zero ->
                    tape.Set cursor One
                    run tape (cursor + 1) A newRemaining
                | One ->
                    tape.Set cursor One
                    run tape (cursor + 1) E newRemaining

    run blankTape 0 State.Initial iterations

let partOne () =
    let finalTape = runTape 12_261_543

    finalTape.CheckSum

type Solution() =
    let name = "twenty_five"

    interface Common.ISolution with
        member __.Name() = name |> Common.titleCase
        member __.LoadData(_) : Common.Input option = [||] |> Some
        member __.PartOne(_input: Common.Input) : string = partOne () |> string
        member __.PartTwo(_: Common.Input) : string = "Merry Xmas"

