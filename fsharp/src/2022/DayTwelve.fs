module DayTwelve

open FSharp.Core.Operators.Checked

let partOne (input: Common.Input) = "Not implemented!"

let partTwo (input: Common.Input) = "Not implemented!"

type Solution() =
    let name = "twelve"
    interface Common.ISolution with
        member __.Name() = name |> Common.titleCase
        member __.LoadData(year): Common.Input option = Common.loadData name year
        member __.PartOne(input: Common.Input): string = input |> partOne
        member __.PartTwo(input: Common.Input): string = input |> partTwo
