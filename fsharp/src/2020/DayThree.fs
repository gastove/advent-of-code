module DayThree

//--------------------------------- Part One ---------------------------------//
// You start on the open square (.) in the top-left corner and need to reach the
// bottom (below the bottom-most row on your map).

// The toboggan can only follow a few specific slopes (you opted for a cheaper
// model that prefers rational numbers); start by counting all the trees you
// would encounter for the slope right 3, down 1:

// From your starting position at the top-left, check the position that is right
// 3 and down 1. Then, check the position that is right 3 and down 1 from there,
// and so on until you go past the bottom of the map.

// The locations you'd check in the above example are marked here with O where
// there was an open square and X where there was a tree:

// ..##.........##.........##.........##.........##.........##.......  --->
// #..O#...#..#...#...#..#...#...#..#...#...#..#...#...#..#...#...#..
// .#....X..#..#....#..#..#....#..#..#....#..#..#....#..#..#....#..#.
// ..#.#...#O#..#.#...#.#..#.#...#.#..#.#...#.#..#.#...#.#..#.#...#.#
// .#...##..#..X...##..#..#...##..#..#...##..#..#...##..#..#...##..#.
// ..#.##.......#.X#.......#.##.......#.##.......#.##.......#.##.....  --->
// .#.#.#....#.#.#.#.O..#.#.#.#....#.#.#.#....#.#.#.#....#.#.#.#....#
// .#........#.#........X.#........#.#........#.#........#.#........#
// #.##...#...#.##...#...#.X#...#...#.##...#...#.##...#...#.##...#...
// #...##....##...##....##...#X....##...##....##...##....##...##....#
// .#..#...#.#.#..#...#.#.#..#...X.#.#..#...#.#.#..#...#.#.#..#...#.#  --->

// In this example, traversing the map using this slope would cause you to
// encounter 7 trees.

// Starting at the top-left corner of your map and following a slope of right 3
// and down 1, how many trees would you encounter?

//--------------------------------- Part Two ---------------------------------//
// Determine the number of trees you would encounter if, for each of the
// following slopes, you start at the top-left corner and traverse the map all
// the way to the bottom:

//     Right 1, down 1.
//     Right 3, down 1. (This is the slope you already checked.)
//     Right 5, down 1.
//     Right 7, down 1.
//     Right 1, down 2.

// In the above example, these slopes would find 2, 7, 3, 4, and 2 tree(s)
// respectively; multiplied together, these produce the answer 336.

// What do you get if you multiply together the number of trees encountered on
// each of the listed slopes?

let getWithWrap (row: char array) (idx: int): char =
    let mutable fetch = idx
    while fetch >= row.Length do
        fetch <- fetch - row.Length

    row.[fetch]

type TreeMap =
    { Map: char array array }

    static member FromLines(lines: string array): TreeMap =
        let newLines = lines |> Array.map Seq.toArray
        { Map = newLines }

    member this.Get (right: int) (down: int): char option =
        this.Map
        |> Array.tryItem down
        |> Option.map (fun row -> getWithWrap row right)

type TravelState =
    { mutable CurrentRow: int
      mutable CurrentColumn: int
      RowIncr: int
      ColIncr: int }

    static member Init (row: int) (col: int) =
        { CurrentRow = 0
          CurrentColumn = 0
          RowIncr = row
          ColIncr = col }

    member this.Progress(): TravelState =
        { this with
              CurrentRow = this.CurrentRow + this.RowIncr
              CurrentColumn = this.CurrentColumn + this.ColIncr }

let howManyTrees (treeMap: TreeMap) (down: int) (right: int): int64 =
    let unfolder (state: TravelState): (char * TravelState) option =
        let newState = state.Progress()

        treeMap.Get newState.CurrentColumn newState.CurrentRow |> Option.map (fun c -> c, newState)

    let trees = Seq.unfold unfolder (TravelState.Init down right)

    let howMany =
        trees
        |> Seq.filter (fun c -> c = '#')
        |> Seq.length

    howMany |> int64

let partOne (lines: string array) =
    let treeMap = lines |> TreeMap.FromLines
    let down = 1
    let right = 3

    howManyTrees treeMap down right

let partTwo (lines: string array) =
    let treeMap = lines |> TreeMap.FromLines

    //     Right 1, down 1.
    //     Right 3, down 1. (This is the slope you already checked.)
    //     Right 5, down 1.
    //     Right 7, down 1.
    //     Right 1, down 2.
    let paths =
        [| (1, 1)
           (1, 3)
           (1, 5)
           (1, 7)
           (2, 1) |]

    paths
    |> Array.Parallel.map (fun (down, right) -> howManyTrees treeMap down right)
    |> Array.reduce (*)

type Solution() =
    let name = "three"
    interface Common.ISolution with
        member __.LoadData(year): Common.Input option =
            Common.loadData name year

        member __.PartOne(input: Common.Input): string =
            let partOne = partOne input
            sprintf "Hit this many trees on the way down: %i" partOne

        member __.PartTwo(input: Common.Input): string =
            let partTwo = partTwo input
            sprintf "Using many different path, found this many total trees: %i" partTwo

        member __.Name() = name |> Common.titleCase
