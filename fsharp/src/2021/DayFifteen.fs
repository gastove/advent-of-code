module DayFifteen

open FSharp.Core.Operators.Checked

open Common.Geometry
open Common.Graph

// TODO[gastove|2021-12-19] Gotta write this fn.
let computePathWeight (weightMap: Map<Point, int>) (path: Point list) =
    path
    |> List.tail
    |> List.sumBy (fun point -> Map.find point weightMap)

let computeGoal (input: Common.Input) =
    let y = input.Length - 1

    let x =
        input
        |> Array.last
        |> String.length
        |> fun len -> len - 1

    Point.Create x y

let partOne (input: Common.Input) =
    let start = Point.Create 0 0
    let goal = input |> computeGoal

    printfn "Heading to %O" goal

    let (graph, weightMap) =
        input
        |> Common.AoCHelpers.constructAdjacentGraphFromStandardInput

    graph
    |> Graph.PathFinding.aStar start goal (Common.Geometry.Point.manhattanDistance goal)
    |> Option.map (
        (fun path ->
            // printfn "Found path:\n%s" (path |> List.map string |> String.concat "\n -> ")

            computePathWeight weightMap path)
        >> string
    )
    |> Option.defaultValue "It didn't work, boss"

module PartTwo =
    let parseToNumberGrid (input: Common.Input) =
        input
        |> Array.map (fun row -> row |> Seq.toArray |> Array.map (string >> int))

    let fiveByFive (baseGrid: int array array) =
        let row = Array.init 5 (fun _ -> baseGrid)
        Array.init 5 (fun _ -> row)

    let weirdIncrement amount num =
        let newNum = num + amount

        if newNum > 9 then
            newNum - 9
        else
            newNum

    let incrementGrid (grid: int array array) amount =
        grid
        |> Array.map (fun row -> row |> Array.map (weirdIncrement amount))

    let joinGrids (firstGrid: int array array) (secondGrid: int array array) =
        Array.zip firstGrid secondGrid
        |> Array.map (fun (f, s) -> Array.append f s)

    let mapGrids (incoming: int array array array array) =
        incoming
        |> Array.mapi (fun yIdx row ->
            row
            |> Array.mapi (fun xIdx grid -> incrementGrid grid (xIdx + yIdx)))
        |> Array.collect (fun row -> row |> Array.reduce joinGrids)

    let parse (input: Common.Input) =
        input
        |> parseToNumberGrid
        |> fiveByFive
        |> mapGrids

    let computeGoal (input: int array array) =
        let y = input.Length - 1

        let x =
            input
            |> Array.last
            |> Array.length
            |> fun len -> len - 1

        Point.Create x y

let partTwo (input: Common.Input) =
    let parsedInput = input |> PartTwo.parse
    let start = Point.Create 0 0
    let goal = parsedInput |> PartTwo.computeGoal

    printfn "Heading to %O" goal

    let (graph, weightMap) =
        parsedInput
        |> Common.AoCHelpers.constructAdjacentGraphFromParsedInput

    printfn "Graph parsed, performing A*"

    graph
    |> Graph.PathFinding.aStar start goal (Common.Geometry.Point.manhattanDistance goal)
    |> Option.map (
        (fun path ->
            // printfn "Found path:\n%s" (path |> List.map string |> String.concat "\n -> ")

            computePathWeight weightMap path)
        >> string
    )
    |> Option.defaultValue "It didn't work, boss"


type Solution() =
    let name = "fifteen"

    interface Common.ISolution with
        member __.Name() = name |> Common.titleCase
        member __.LoadData(year) : Common.Input option = Common.loadData name year
        member __.PartOne(input: Common.Input) : string = input |> partOne
        member __.PartTwo(input: Common.Input) : string = input |> partTwo
