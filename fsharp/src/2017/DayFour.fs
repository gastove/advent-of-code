module DayFour

open FSharp.Core.Operators.Checked

let removeSimpleBadPassphrases (line: string) =
    let parts = line.Split()
    let numWords = parts |> Array.length

    let uniqueWords = parts |> Set.ofArray

    if numWords = (uniqueWords |> Set.count) then
        Some(line)
    else
        None

let partOne (input: Common.Input) =
    input
    |> Array.filter (fun line -> line.Trim().Length > 0)
    |> Array.choose removeSimpleBadPassphrases
    |> Array.length

let anagramsInLine (line: string) =
    let parts = line.Split()

    parts
    |> Array.fold
        (fun state word ->
            let partsWithoutCurrentWord =
                parts |> Array.filter (fun w -> w <> word)

            let wordComposition = word |> Seq.countBy id |> Set.ofSeq

            let anagrams =
                partsWithoutCurrentWord
                |> Array.filter
                    (fun innerWord ->
                        let wc = innerWord |> Seq.countBy id |> Set.ofSeq
                        wc  = wordComposition)

            state + anagrams.Length)
        0


let partTwo (input: Common.Input) =
    input
    |> Array.filter (fun line -> line.Trim().Length > 0)
    |> Array.Parallel.choose removeSimpleBadPassphrases
    |> Array.Parallel.map anagramsInLine
    |> Array.filter (fun numAnagrams -> numAnagrams = 0)
    |> Array.length

type Solution() =
    let name = "four"

    interface Common.ISolution with
        member __.Name() = name |> Common.titleCase
        member __.LoadData(year) : Common.Input option = Common.loadData name year
        member __.PartOne(input: Common.Input) : string = input |> partOne |> string
        member __.PartTwo(input: Common.Input) : string = input |> partTwo |> string
