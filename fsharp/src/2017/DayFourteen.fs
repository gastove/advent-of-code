module DayFourteen

open System

open FSharp.Core.Operators.Checked

let hexDigestToBinaryString (digest: string) =
    digest
    |> Seq.map (
        string
        >> (fun s ->
            Convert
                .ToString(Convert.ToInt32(s, 16), 2)
                .PadLeft(4, '0'))
    )
    |> String.concat ""

let genGrid key =
    [| 0 .. 127 |] |> Array.map (sprintf "%s-%i" key)

let explode (s: string) = s |> Seq.toArray |> Array.map string

let partOne input =
    input
    |> genGrid
    |> Array.Parallel.map DayTen.KnotHash.computeHash
    |> Array.collect (hexDigestToBinaryString >> explode)
    |> Array.sumBy int

module Points =
    let areNeighbors ((pointOneX, pointOneY): int * int) ((pointTwoX, pointTwoY): int * int) =
        ((pointOneX - pointTwoX |> abs = 1)
         && pointOneY = pointTwoY)
        || ((pointOneY - pointTwoY |> abs = 1)
            && pointOneX = pointTwoX)

    let anyNeighbors (points: (int * int) Set) (point: int * int) =
        points
        |> Set.toList
        |> List.tryFind (areNeighbors point)
        |> Option.isSome

module Groups =
    let areNeighbors (groupOne: Set<int * int>) (groupTwo: Set<int * int>) =
        groupOne
        |> Set.filter (Points.anyNeighbors groupTwo)
        |> Set.isEmpty
        |> not

type Groups =
    { State: Map<int, (int * int) Set> }

    static member Create state = { State = state }

    member private this.AddPointToGroup point group =
        let existingGroup =
            this.State
            |> Map.tryFind group
            |> Option.defaultValue Set.empty

        let newState =
            Map.add group (Set.add point existingGroup) this.State

        { State = newState }

    member this.MaxGroup =
        this.State |> Map.toList |> List.maxBy fst |> fst

    member this.CombineGroups groupsToCombine =
        // Combine all matched groups
        let combinedGroup =
            groupsToCombine
            |> List.fold (fun combinedGroups group -> Set.union combinedGroups (this.State |> Map.find group)) Set.empty

        // Drop combined groups from the state
        let newState =
            groupsToCombine
            |> List.fold (fun state group -> Map.remove group state) this.State

        let putStateAtGroup = groupsToCombine |> List.min

        newState
        |> Map.add putStateAtGroup combinedGroup
        |> Groups.Create

    member this.Update(point: int * int) =
        if this.State.IsEmpty then
            this.AddPointToGroup point 0
        else
            let groupsToUpdate =
                this.State
                // Produce a list of tuples of groupNumber, isMemberOfGroup
                |> Map.filter (fun _ points -> Points.anyNeighbors points point)
                // Go from a Map<groupNo, isMember> to a List<isMember * groupNo list>
                |> Map.toList
                |> List.map fst

            if groupsToUpdate.IsEmpty then
                this.AddPointToGroup point (this.MaxGroup + 1)
            else
                let newGroup =
                    this.AddPointToGroup point (groupsToUpdate |> List.min)

                groupsToUpdate |> newGroup.CombineGroups

    member this.Reduce() =
        let rec reduce (groupsToCombine: Set<int * int> list) (combinedGroups: Set<int * int> list) =
            match groupsToCombine with
            | group :: remaining ->
                let combineWith =
                    remaining
                    |> List.filter (fun remainingGroup -> Groups.areNeighbors remainingGroup group)

                if combineWith.IsEmpty then
                    reduce remaining (group :: combinedGroups)
                else
                    let newGroup =
                        Set.unionMany combineWith |> Set.union group

                    let whatsLeft =
                        remaining
                        |> List.filter (fun group -> combineWith |> List.contains group |> not)

                    reduce (newGroup :: whatsLeft @ combinedGroups) []
            | [] -> combinedGroups

        let groups = this.State |> Map.toList |> List.map snd
        reduce groups []

let partTwo (input: string) =
    let disk =
        input
        |> genGrid
        |> Array.Parallel.map DayTen.KnotHash.computeHash
        |> Array.map (hexDigestToBinaryString >> explode)

    let digits =
        disk
        |> Array.mapi
            (fun yIdx row ->
                row
                |> Array.mapi (fun xIdx digit -> (xIdx, yIdx), digit))
        |> Array.concat

    let groups =
        digits
        |> Array.fold
            (fun (groups: Groups) (point, digit) ->
                if digit = "1" then
                    groups.Update point
                else
                    groups)
            (Groups.Create Map.empty)

    groups.Reduce() |> List.length

type Solution() =
    let name = "fourteen"
    let input = "hxtvlmkl"

    interface Common.ISolution with
        member __.Name() = name |> Common.titleCase
        member __.LoadData(_) : Common.Input option = [||] |> Some
        member __.PartOne(_input: Common.Input) : string = input |> partOne |> string
        member __.PartTwo(_input: Common.Input) : string = input |> partTwo |> string
