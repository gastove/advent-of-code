mod part_one {
    pub fn run(input: Vec<String>) -> String {
        "not implemented".into()
    }
}

mod part_two {
    pub fn run(input: Vec<String>) -> String {
        "not implemented".into()
    }    
}

pub fn run(input: Vec<String>) {
    println!("=== Day Three! ===");
    println!("Part One\t=> {}", part_one::run(input.clone()));
    println!("Part Two\t=> {}", part_two::run(input));
}
