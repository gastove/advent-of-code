module DayTwo

open FSharp.Core.Operators.Checked

module Scores =
    let win = 6
    let draw = 3
    let lose = 0

type Throw =
    | Rock
    | Paper
    | Scissors

    static member FromString =
        function
        | "A"
        | "X" -> Rock
        | "B"
        | "Y" -> Paper
        | "C"
        | "Z" -> Scissors
        | wrong -> failwith $"Cannot parse {wrong} as Throw"

    member this.Score =
        match this with
        | Rock -> 1
        | Paper -> 2
        | Scissors -> 3

type SpecifiedOutcome =
    | Win
    | Lose
    | Draw

    static member FromString =
        function
        | "X" -> Lose
        | "Y" -> Draw
        | "Z" -> Win
        | wrong -> failwith $"Cannot parse {wrong} as SpecifiedOutcome"

    member this.ToAchieveThis(theyThrow: Throw) =
        match (this, theyThrow) with
        | Win, Rock -> Paper
        | Win, Paper -> Scissors
        | Win, Scissors -> Rock
        | Draw, Rock -> Rock
        | Draw, Paper -> Paper
        | Draw, Scissors -> Scissors
        | Lose, Rock -> Scissors
        | Lose, Paper -> Rock
        | Lose, Scissors -> Paper

    member this.Score =
        match this with
        | Win -> Scores.win
        | Lose -> Scores.lose
        | Draw -> Scores.draw


let scoreP2 (p1: Throw) (p2: Throw) =
    p2.Score
    + match (p1, p2) with
      | Rock, Rock
      | Paper, Paper
      | Scissors, Scissors -> Scores.draw
      | Rock, Paper
      | Paper, Scissors
      | Scissors, Rock -> Scores.win
      | Rock, Scissors
      | Paper, Rock
      | Scissors, Paper -> Scores.lose


let partOne (input: Common.Input) =
    input
    |> Array.map (fun line ->
        let parts = line.Split(" ")
        let p1 = parts.[0] |> Throw.FromString
        let p2 = parts.[1] |> Throw.FromString
        scoreP2 p1 p2)
    |> Array.sum
    |> string

let partTwo (input: Common.Input) =
    input
    |> Array.map (fun line ->
        let parts = line.Split(" ")
        let p1Throw = parts.[0] |> Throw.FromString
        let outcome = parts.[1] |> SpecifiedOutcome.FromString
        let p2Throw = p1Throw |> outcome.ToAchieveThis
        p2Throw.Score + outcome.Score)
    |> Array.sum
    |> string


type Solution() =
    let name = "two"

    interface Common.ISolution with
        member __.Name() = name |> Common.titleCase
        member __.LoadData(year) : Common.Input option = Common.loadData name year
        member __.PartOne(input: Common.Input) : string = input |> partOne
        member __.PartTwo(input: Common.Input) : string = input |> partTwo
