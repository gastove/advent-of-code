module DayTen

open FSharp.Core.Operators.Checked

let (|Opener|_|) c =
    match c with
    | '{'
    | '('
    | '['
    | '<' -> c |> Some
    | _ -> None

let (|Closer|_|) c =
    match c with
    | '}'
    | ')'
    | '>'
    | ']' -> c |> Some
    | _ -> None

type ProcessingError =
    | Incomplete of char list
    | NoOpeners
    | WrongCloser of char

let processLine (line: char list) =
    let rec work (characters: char list) (stack: char list) : Result<unit, ProcessingError> =
        match characters, stack with
        | [], [] -> () |> Ok
        | [], leftoverStack -> leftoverStack |> Incomplete |> Error
        | workingChar :: remaining, _ ->
            match workingChar with
            | Opener opener -> work remaining (opener :: stack)
            | Closer closer ->
                match stack, closer with
                | [], _ -> NoOpeners |> Error
                | '{' :: newStack, '}' -> work remaining newStack
                | '(' :: newStack, ')' -> work remaining newStack
                | '[' :: newStack, ']' -> work remaining newStack
                | '<' :: newStack, '>' -> work remaining newStack
                | _, _ -> closer |> WrongCloser |> Error

            | _ -> failwithf "I dunno what's going on, but we got a %c" workingChar

    work line []


// ): 3 points.
// ]: 57 points.
// }: 1197 points.
// >: 25137 points.
let scoreIllegalChar =
    function
    | ')' -> 3
    | ']' -> 57
    | '}' -> 1197
    | '>' -> 25137
    | bad -> failwithf "Somehow, we got a %c" bad

let getCloser =
    function
    | '(' -> ')'
    | '{' -> '}'
    | '[' -> ']'
    | '<' -> '>'
    | bad -> failwithf "Somehow, we got a %c" bad

let generateCompletionString (toComplete: char list) =
    toComplete |> List.map getCloser

// Start with a total score of 0. Then, for each character, multiply the total score by 5 and then increase the total score by the point value given for the character in the following table:

//    ): 1 point.
//    ]: 2 points.
//    }: 3 points.
//    >: 4 points.
let scoreCompletionString (characters: char list) =
    characters
    |> List.fold
        (fun score char ->
            score * 5L
            + match char with
              | ')' -> 1L
              | ']' -> 2L
              | '}' -> 3L
              | '>' -> 4L
              | _ -> 0L)
        0L


let partOne (input: Common.Input) =
    input
    |> Array.map Seq.toList
    |> Array.Parallel.choose (fun line ->
        match processLine line with
        | Error (WrongCloser c) -> Some c
        | _ -> None)
    |> Array.sumBy scoreIllegalChar
    |> string

let scoreInput (input: Common.Input) =
        input
        |> Array.map Seq.toList
        |> Array.Parallel.choose (fun line ->
            match processLine line with
            | Error (Incomplete unclosed) -> unclosed |> Some
            | _ -> None)
        |> Array.map (generateCompletionString>>scoreCompletionString)

let partTwo (input: Common.Input) =
    let scores =
        input
        |> scoreInput

    let middleIdx = ((scores |> Array.length) / 2)

    scores |> Array.sort |> Array.item middleIdx |> string


type Solution() =
    let name = "ten"

    interface Common.ISolution with
        member __.Name() = name |> Common.titleCase
        member __.LoadData(year) : Common.Input option = Common.loadData name year
        member __.PartOne(input: Common.Input) : string = input |> partOne
        member __.PartTwo(input: Common.Input) : string = input |> partTwo
