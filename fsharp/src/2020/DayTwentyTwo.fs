module DayTwentyTwo

open FSharp.Core.Operators.Checked

module Shared =

    open System.Collections.Generic

    type Deck =
        { Cards: Queue<int> }

        static member Create(cards: int seq) =
            let q = Queue<int>()

            cards |> Seq.iter (fun card -> q.Enqueue card)

            { Cards = q }

        override this.ToString() =
            let cards =
                this.Cards |> Seq.map string |> String.concat ","

            let numCards = this.Cards.Count
            $"Cards:{numCards}={cards}"

        member this.DrawCardReturn() =
            let card = this.Cards.Dequeue()
            // let deck = this.Cards |> List.tail

            card, this

        member this.Peek() = this.Cards.Peek()

        member this.DrawCard() = this.Cards.Dequeue()

        member this.AddCardsReturn firstCard secondCard =
            this.Cards.Enqueue firstCard
            this.Cards.Enqueue secondCard
            this

        member this.AddCards firstCard secondCard =
            this.Cards.Enqueue firstCard
            this.Cards.Enqueue secondCard

        member this.IsEmpty = this.Cards.Count = 0

        member this.HashKey = this.Cards |> Seq.map string |> String.concat ""

    module Deck =
        let fromLines (lines: Common.Input) =
            lines
            |> Array.filter Common.Strings.notBlank
            |> Array.skip 1
            |> Array.map int
            |> List.ofArray
            |> Deck.Create

        let copy (deck: Deck) =
            let newCards = deck.Cards.ToArray()

            Deck.Create newCards

    let parseInput (input: Common.Input) =
        let middle = (input.Length / 2) + 1

        let deckone = input.[..middle - 1] |> Deck.fromLines

        let deckTwo = input.[middle..] |> Deck.fromLines

        deckone, deckTwo

    let combat (deckOne: Deck) (deckTwo: Deck) =
        let oneCard, newOne = deckOne.DrawCardReturn()
        let twoCard, newTwo = deckTwo.DrawCardReturn()

        if oneCard > twoCard then
            newOne.AddCardsReturn oneCard twoCard, newTwo
        else
            newOne, newTwo.AddCardsReturn twoCard oneCard

    let fightFightFight (deckOne: Deck) (deckTwo: Deck) =
        let rec fight (one: Deck) (two: Deck) =
            let (newOne, newTwo) = combat one two

            if newOne.IsEmpty then newTwo
            else if newTwo.IsEmpty then newOne
            else fight newOne newTwo

        fight deckOne deckTwo

    let canRecurse (card: int) (deck: Deck) = card <= deck.Cards.Count

    [<RequireQualifiedAccess>]
    type Outcome =
        | D1Wins of Deck
        | D2Wins of Deck

    let evaluateResult (d1: Deck) (d2: Deck) =
        if d1.IsEmpty then
            Outcome.D2Wins d2 |> Some
        else if d2.IsEmpty then
            Outcome.D1Wins d1 |> Some
        else
            None

    let recursiveFightFightFight (deckOne: Deck) (deckTwo: Deck) =
        let rec fight (d1: Deck) (d2: Deck) (history: Set<string * string>) game round =
            // printfn $"Game {game}, Round {round} Cards:\n\tDeck 1:{d1}\n\tDeck 2:{d2}"

            if history.Contains(d1.HashKey, d2.HashKey) then
                d1 |> Outcome.D1Wins
            else
                let newHistory =
                    history
                    |> Set.add (d1.HashKey, d2.HashKey)

                let d1Card = d1.DrawCard()
                let d2Card = d2.DrawCard()

                let thunk() = fight d1 d2 newHistory game (round + 1)

                // printfn $"Deck 1 drew {d1Card} and has {d1.Cards.Count} left\nDeck 2 drew {d2Card} and has {d2.Cards.Count} left"

                // We can recurse
                if (d1.Cards.Count >= d1Card)
                   && (d2.Cards.Count >= d2Card) then
                    // printfn "We're recursing!"
                    // When we have our recursive fight, it's a parallel universe, so we put in copies
                    let d1Clone = d1.Cards |> Seq.take d1Card |> Deck.Create
                    let d2Clone = d2.Cards |> Seq.take d2Card |> Deck.Create
                    match fight d1Clone d2Clone Set.empty (game + 1) 1 with
                    | Outcome.D1Wins _ ->
                        // printfn $"D1 won game {game + 1} in the recurse"
                        d1.AddCards d1Card d2Card

                    | Outcome.D2Wins _ ->
                        // printfn $"D2 won game {game + 1} in the recurse"
                        d2.AddCards d2Card d1Card

                    evaluateResult d1 d2
                    |> Option.defaultWith thunk

                else
                    if d1Card > d2Card then
                        // printfn $"D1 wins round {round} on comparison"
                        d1.AddCards d1Card d2Card
                    else
                        // printfn $"D2 wins round {round} on comparison"
                        d2.AddCards d2Card d1Card

                    evaluateResult d1 d2
                    |> Option.defaultWith thunk

        match fight deckOne deckTwo Set.empty 1 1 with
        | Outcome.D1Wins deck -> deck
        | Outcome.D2Wins deck -> deck

    let computeScore (deck: Deck) =
        deck.Cards.ToArray()
        |> Array.rev
        |> Array.mapi (fun idx num -> (idx + 1) * num)
        |> Array.sum

module PartOne =

    let partOne (input: Common.Input) =
        let deckOne, deckTwo = input |> Shared.parseInput

        let winner = Shared.fightFightFight deckOne deckTwo

        winner |> Shared.computeScore |> string

module PartTwo =

    let partTwo (input: Common.Input) =
        let deckOne, deckTwo = input |> Shared.parseInput

        let winner =
            Shared.recursiveFightFightFight deckOne deckTwo

        winner |> Shared.computeScore |> string

type Solution() =
    let name = "twenty_two"

    interface Common.ISolution with
        member __.Name() = name |> Common.titleCase
        member __.LoadData(year) : Common.Input option = Common.loadData name year
        member __.PartOne(input: Common.Input) : string = PartOne.partOne input
        member __.PartTwo(input: Common.Input) : string = PartTwo.partTwo input
