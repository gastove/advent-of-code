module DayThree

open FSharp.Core.Operators.Checked

let LowerChars = [ 'a' .. 'z' ] |> Set.ofList
let UpperCharss = [ 'A' .. 'Z' ] |> Set.ofList

let scoreItem (item: char) =
    if Set.contains item LowerChars then
        (int item) - 96
    else if Set.contains item UpperCharss then
        (int item) - 38
    else
        failwith $"Couldn't score: {item}"

let parseLine (line: string) : (char seq * char seq) =
    let middle = line.Length / 2
    let firstHalf = line |> Seq.take middle
    let secondHalf = line |> Seq.skip middle

    firstHalf, secondHalf

let findMatchingItem ((firstCompartment: char seq), (secondCompartment: char seq)) =
    [ firstCompartment; secondCompartment ]
    |> List.map Set.ofSeq
    |> Set.intersectMany
    |> Set.toArray
    |> Array.exactlyOne

let partOne (input: Common.Input) =
    input
    |> Array.map parseLine
    |> Array.Parallel.map (findMatchingItem >> scoreItem)
    |> Array.sum
    |> string

let partTwo (input: Common.Input) =
    input
    |> Array.chunkBySize 3
    |> Array.Parallel.map (fun group ->
        group
        |> Array.map Set.ofSeq
        |> Set.intersectMany
        |> Set.toArray
        |> Array.exactlyOne
        |> scoreItem)
    |> Array.sum
    |> string

type Solution() =
    let name = "three"

    interface Common.ISolution with
        member __.Name() = name |> Common.titleCase
        member __.LoadData(year) : Common.Input option = Common.loadData name year
        member __.PartOne(input: Common.Input) : string = input |> partOne
        member __.PartTwo(input: Common.Input) : string = input |> partTwo
