module DaySix

open FSharp.Core.Operators.Checked

// The device will send your subroutine a datastream buffer (your puzzle input);
// your subroutine needs to identify the first position where the four most
// recently received characters were all different.
//
// Specifically, it needs to report the number of characters from the beginning
// of the buffer to the end of the first such four-character marker.

let scanForMarker (uniqueChars: int) (stream: string) =
    let rec scan index =
        if (index + uniqueChars) > stream.Length then
            failwith "Failed to find the marker, ugh"
        else if stream.[index..index + uniqueChars - 1]
                |> Set.ofSeq
                |> Set.count = uniqueChars then
            index + uniqueChars
        else
            scan (index + 1)

    scan 0

let partOne (input: Common.Input) =
    input
    |> Array.exactlyOne
    |> scanForMarker 4
    |> string

let partTwo (input: Common.Input) =
    input
    |> Array.exactlyOne
    |> scanForMarker 14
    |> string
    
type Solution() =
    let name = "six"

    interface Common.ISolution with
        member __.Name() = name |> Common.titleCase
        member __.LoadData(year) : Common.Input option = Common.loadData name year
        member __.PartOne(input: Common.Input) : string = input |> partOne
        member __.PartTwo(input: Common.Input) : string = input |> partTwo
