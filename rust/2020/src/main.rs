use std::env;

pub mod util;
mod day_twenty_two;

fn dispatch_day(day: String) {
    match day.as_ref() {
        "twentytwo" => day_twenty_two::run(),
        // "two" => day_two::run(),
        // "three" => day_three::run(),
        // "four" => day_four::run(),
        _ => println!("Couldn't match."),
    }
}

fn main() {
    println!("Hello, world!");
    let mut passed_args = env::args();
    match passed_args.nth(1) {
        Some(dispatch) => dispatch_day(dispatch),
        None => println!("No args passed :|"),
    }
}
