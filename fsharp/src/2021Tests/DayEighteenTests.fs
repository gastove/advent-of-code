module DayEighteenTests

open Expecto

open DayEighteen

[<Tests>]
let parsingTests =
    testList
        "Testing SnailNumber Parsing"
        [ testCase "Test Parsing just an int"
          <| fun _ ->
              let input = "5"
              let expected = Int 5
              let got = input |> SnailNumber.FromString

              Expect.equal got expected "We should be able to parse an Int"

          testCase "Test Parsing a simple Pair"
          <| fun _ ->
              let input = "[1,2]"
              let expected = Pair(Int 1, Int 2)
              let got = input |> SnailNumber.FromString

              Expect.equal got expected "We should be able to parse a simple pair"

          testCase "Test Parsing a Nested Pair"
          <| fun _ ->
              let input = "[[1,2],3]"
              let expected = Pair(Pair(Int 1, Int 2), Int 3)
              let got = input |> SnailNumber.FromString

              Expect.equal got expected "We should be able to parse a simple nested pair"

          testCase "Test Parsing a Complex Nested Pair"
          <| fun _ ->
              let input = "[[[[0,7],4],[[7,8],[6,0]]],[8,1]]"

              let expected =
                  Pair(
                      Pair(Pair(Pair(Int 0, Int 7), Int 4), Pair(Pair(Int 7, Int 8), Pair(Int 6, Int 0))),
                      Pair(Int 8, Int 1)
                  )

              let got = input |> SnailNumber.FromString
              Expect.equal got expected "We should be able to parse a complex nested pair" ]

[<Tests>]
let snailNumberTests =
    testList
        "Testing basic functions on Snail Numbers"
        [ testCase "Test we can Split numbers correctly"
          <| fun _ ->
              // 10 becomes [5,5], 11 becomes [5,6], 12 becomes [6,6]
              let noop = Int 9
              let firstInput = Int 10
              let secondInput = Int 11
              let thirdInput = Int 12

              Expect.equal (noop |> SnailNumber.reduce) noop "The No Op case should be a No Op"

              Expect.equal
                  (firstInput |> SnailNumber.reduce)
                  (Pair(Int 5, Int 5))
                  "We should be able to split 10 to [5, 5]"

              Expect.equal
                  (secondInput |> SnailNumber.reduce)
                  (Pair(Int 5, Int 6))
                  "We should be able to split 10 to [5, 5]"

              Expect.equal
                  (thirdInput |> SnailNumber.reduce)
                  (Pair(Int 6, Int 6))
                  "We should be able to split 10 to [5, 5]"

          testCase "Can we print a snail number as a string?"
          <| fun _ ->
              // [1,1]
              let basic = Pair(Int 1, Int 1)
              let basicExpected = "[1,1]"

              let complexOne =
                  Pair(Pair(Pair(Pair(Int 4, Int 3), Int 4), Int 4), Pair(Int 7, Pair(Pair(Int 8, Int 4), Int 9)))

              let complexOneExpected = "[[[[4,3],4],4],[7,[[8,4],9]]]"

              let complexTwo =
                  Pair(
                      Pair(Pair(Pair(Int 0, Int 7), Int 4), Pair(Pair(Int 7, Int 8), Pair(Int 6, Int 0))),
                      Pair(Int 8, Int 1)
                  )

              let complexTwoExpected = "[[[[0,7],4],[[7,8],[6,0]]],[8,1]]"

              Expect.equal (basic.ToString()) basicExpected "We should be able to render a simple string"
              Expect.equal (complexOne.ToString()) complexOneExpected "We should be able to render a complex string"

              Expect.equal
                  (complexTwo.ToString())
                  complexTwoExpected
                  "We should be able to render a different complex string"

          testCase "Compute SnailNumber magnitude"
          <| fun _ ->
              // Here are a few more magnitude examples:

              // [[1,2],[[3,4],5]] becomes 143.
              // [[[[0,7],4],[[7,8],[6,0]]],[8,1]] becomes 1384.
              // [[[[1,1],[2,2]],[3,3]],[4,4]] becomes 445.
              // [[[[3,0],[5,3]],[4,4]],[5,5]] becomes 791.
              // [[[[5,0],[7,4]],[5,5]],[6,6]] becomes 1137.
              // [[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]] becomes 3488.

              let first =
                  Pair(Pair(Int 1, Int 2), Pair(Pair(Int 3, Int 4), Int 5))

              Expect.equal
                  (first |> SnailNumber.magnitude)
                  143
                  "We should be able to compute the magnitude of a short snail number"

          testCase "Test first Explode example, Explode Right"
          <| fun _ ->
              // [[[[[9,8],1],2],3],4] becomes [[[[0,9],2],3],4] (the 9 has no regular number to its left, so it is not added to any regular number).
              let input =
                  Pair(Pair(Pair(Pair(Pair(Int 9, Int 8), Int 1), Int 2), Int 3), Int 4)

              let expected =
                  Pair(Pair(Pair(Pair(Int 0, Int 9), Int 2), Int 3), Int 4)

              Expect.equal (input |> SnailNumber.reduce) expected "We should be able to reproduce the first example"

          testCase "Test second Explode example, Explode Left"
          <| fun _ ->
              // [7,[6,[5,[4,[3,2]]]]] becomes [7,[6,[5,[7,0]]]]
              let input =
                  Pair(Int 7, Pair(Int 6, Pair(Int 5, Pair(Int 4, Pair(Int 3, Int 2)))))

              let expected =
                  Pair(Int 7, Pair(Int 6, Pair(Int 5, Pair(Int 7, Int 0))))

              Expect.equal (input |> SnailNumber.reduce) expected "We should be able to reproduce the second example"

          testCase "Test third Explode example, Explode Left and Right"
          <| fun _ ->
              // [[6,[5,[4,[3,2]]]],1] becomes [[6,[5,[7,0]]],3].
              let input =
                  Pair(Pair(Int 6, Pair(Int 5, Pair(Int 4, Pair(Int 3, Int 2)))), Int 1)

              let expected =
                  Pair(Pair(Int 6, Pair(Int 5, Pair(Int 7, Int 0))), Int 3)

              Expect.equal (input |> SnailNumber.reduce) expected "We should be able to reproduce the second example"

          testCase "Test fourth Explode example"
          <| fun _ ->
              let input =
                  "[[3,[2,[1,[7,3]]]],[6,[5,[4,[3,2]]]]]"
                  |> SnailNumber.FromString

              let expected =
                  "[[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]"
                  |> SnailNumber.FromString

              let got = input |> SnailNumber.reduce

              Expect.equal got expected "We should be able to do a single explosion"

          testCase "Test fifth Explode example"
          <| fun _ ->
              let input =
                  "[[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]"
                  |> SnailNumber.FromString

              let expected =
                  "[[3,[2,[8,0]]],[9,[5,[7,0]]]]"
                  |> SnailNumber.FromString

              let got = input |> SnailNumber.reduce

              Expect.equal got expected "We should be able to do a single explosion"

          testCase "Test Full Addition/Reduction One"
          <| fun _ ->
              // [[[[4,3],4],4],[7,[[8,4],9]]] + [1,1]:
              let sn1 =
                  Pair(Pair(Pair(Pair(Int 4, Int 3), Int 4), Int 4), Pair(Int 7, Pair(Pair(Int 8, Int 4), Int 9)))

              let sn2 = Pair(Int 1, Int 1)

              // [[[[0,7],4],[[7,8],[6,0]]],[8,1]]
              let expected =
                  Pair(
                      Pair(Pair(Pair(Int 0, Int 7), Int 4), Pair(Pair(Int 7, Int 8), Pair(Int 6, Int 0))),
                      Pair(Int 8, Int 1)
                  )

              let got = SnailNumber.snailAddition sn1 sn2

              Expect.equal got expected "We should be able to add and reduce a snail number."

          testCase "Test Full Addition/Reduction One, With String Parsing"
          <| fun _ ->
              let sn1 =
                  "[[[[4,3],4],4],[7,[[8,4],9]]]"
                  |> SnailNumber.FromString

              let sn2 = "[1,1]" |> SnailNumber.FromString

              let expected =
                  "[[[[0,7],4],[[7,8],[6,0]]],[8,1]]"
                  |> SnailNumber.FromString

              let got = SnailNumber.snailAddition sn1 sn2

              Expect.equal got expected "We should be able to add and reduce a snail number."

          testCase "Test Full Addition/Reduction Two"
          <| fun _ ->
              let sn1 =
                  "[[[0,[4,5]],[0,0]],[[[4,5],[2,6]],[9,5]]]"
                  |> SnailNumber.FromString

              let sn2 =
                  "[7,[[[3,7],[4,3]],[[6,3],[8,8]]]]"
                  |> SnailNumber.FromString

              let expected =
                  "[[[[4,0],[5,4]],[[7,7],[6,0]]],[[8,[7,7]],[[7,9],[5,0]]]]"
                  |> SnailNumber.FromString

              let got = SnailNumber.snailAddition sn1 sn2

              Expect.equal got expected "We should be able to do complex snail addition"

          ]

let doListReduce input =
    input
    |> Array.map SnailNumber.FromString
    |> Array.reduce SnailNumber.snailAddition

[<Tests>]
let partOneTests =
    testList
        "Testing PartOne examples"
        [ testCase "Test Very Basic List Reduction"
          <| fun _ ->
              let input = [| "[1,1]"; "[2,2]"; "[3,3]"; "[4,4]" |]

              let expected =
                  "[[[[1,1],[2,2]],[3,3]],[4,4]]"
                  |> SnailNumber.FromString

              let got = input |> doListReduce

              Expect.equal got expected "We should be able to do a very simple list reduction"

          testCase "Test slightly more complex List Reduction"
          <| fun _ ->
              let input =
                  [| "[1,1]"
                     "[2,2]"
                     "[3,3]"
                     "[4,4]"
                     "[5,5]" |]

              let expected =
                  "[[[[3,0],[5,3]],[4,4]],[5,5]]"
                  |> SnailNumber.FromString

              let got = input |> doListReduce

              Expect.equal got expected "We should be able to do a slightly more complex list reduction"

          testCase "Test only a slightly more complex List Reduction"
          <| fun _ ->
              let input =
                  [| "[1,1]"
                     "[2,2]"
                     "[3,3]"
                     "[4,4]"
                     "[5,5]"
                     "[6,6]" |]

              let expected =
                  "[[[[5,0],[7,4]],[5,5]],[6,6]]"
                  |> SnailNumber.FromString

              let got = input |> doListReduce

              Expect.equal got expected "We should be able to do a very simple list reduction"

          testCase "Can we reproduce the correct final number from the example?"
          <| fun _ ->
              let input =
                  [| "[[[0,[5,8]],[[1,7],[9,6]]],[[4,[1,2]],[[1,4],2]]]"
                     "[[[5,[2,8]],4],[5,[[9,9],0]]]"
                     "[6,[[[6,2],[5,6]],[[7,6],[4,7]]]]"
                     "[[[6,[0,7]],[0,9]],[4,[9,[9,0]]]]"
                     "[[[7,[6,4]],[3,[1,3]]],[[[5,5],1],9]]"
                     "[[6,[[7,3],[3,2]]],[[[3,8],[5,7]],4]]"
                     "[[[[5,4],[7,7]],8],[[8,3],8]]"
                     "[[9,3],[[9,9],[6,[4,9]]]]"
                     "[[2,[[7,7],7]],[[5,8],[[9,3],[0,2]]]]"
                     "[[[[5,2],5],[8,[3,7]]],[[5,[7,5]],[4,4]]]" |]

              let expected =
                  "[[[[6,6],[7,6]],[[7,7],[7,0]]],[[[7,7],[7,7]],[[7,8],[9,9]]]]"
                  |> SnailNumber.FromString

              let got = input |> doListReduce

              Expect.equal got expected "We should be able to reproduce the example"

          testCase "Can we reproduce the final magnitude from the example?"
          <| fun _ ->
              let input =
                  [| "[[[0,[5,8]],[[1,7],[9,6]]],[[4,[1,2]],[[1,4],2]]]"
                     "[[[5,[2,8]],4],[5,[[9,9],0]]]"
                     "[6,[[[6,2],[5,6]],[[7,6],[4,7]]]]"
                     "[[[6,[0,7]],[0,9]],[4,[9,[9,0]]]]"
                     "[[[7,[6,4]],[3,[1,3]]],[[[5,5],1],9]]"
                     "[[6,[[7,3],[3,2]]],[[[3,8],[5,7]],4]]"
                     "[[[[5,4],[7,7]],8],[[8,3],8]]"
                     "[[9,3],[[9,9],[6,[4,9]]]]"
                     "[[2,[[7,7],7]],[[5,8],[[9,3],[0,2]]]]"
                     "[[[[5,2],5],[8,[3,7]]],[[5,[7,5]],[4,4]]]" |]

              let expected = "4140"
              let got = input |> partOne

              Expect.equal got expected "We should be able to reproduce the example" ]
