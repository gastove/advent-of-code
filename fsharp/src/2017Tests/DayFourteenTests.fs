module DayFourteenTests

open Expecto

[<Tests>]
let pointsTests =
    testList
        "Testing the Points module"
        [ testCase "areNeighbors"
          <| fun _ ->
              let basePoint = 2, 2
              let neighbors = [ 2, 1; 2, 3; 1, 2; 3, 2 ]
              let notNeighbor = 1, 1

              neighbors
              |> List.iter
                  (fun neighbor ->
                      Expect.isTrue
                          (DayFourteen.Points.areNeighbors basePoint neighbor)
                          "These are definitely neighbors")

              Expect.isFalse (DayFourteen.Points.areNeighbors basePoint notNeighbor) "Diagonals are not neighbors" ]

[<Tests>]
let groupsTests =
    testList
        "Testing the Groups type"
        [ testCase "Test Groups.Update: Add a point to an empty Groups"
          <| fun _ ->
              let groups = DayFourteen.Groups.Create Map.empty

              let point = 1, 1

              let got = groups.Update point

              let expected =
                  Set.add point Set.empty
                  |> (fun s -> Map.add 0 s Map.empty)
                  |> DayFourteen.Groups.Create

              Expect.equal got expected "We should be able to add a point"

          testCase "Test Groups.Update: Add a neighbor"
          <| fun _ ->
              let point = 1, 1
              let neighbor = 2, 1

              let groups =
                  Set.add point Set.empty
                  |> (fun s -> Map.add 0 s Map.empty)
                  |> DayFourteen.Groups.Create

              let got = groups.Update neighbor

              let expected =
                  Set.add point Set.empty
                  |> Set.add neighbor
                  |> (fun s -> Map.add 0 s Map.empty)
                  |> DayFourteen.Groups.Create

              Expect.equal got expected "We should be able to add a neighboring point"

          testCase "Test Groups.Update: Add a non-neighbor"
          <| fun _ ->
              let point = 1, 1
              let notNeighbor = 5, 5

              let groups =
                  Set.add point Set.empty
                  |> (fun s -> Map.add 0 s Map.empty)
                  |> DayFourteen.Groups.Create

              let point = 1, 1

              let got = groups.Update notNeighbor

              let expected =
                  Set.add point Set.empty
                  |> (fun s -> Map.add 0 s Map.empty)
                  |> Map.add 1 (Set.add notNeighbor Set.empty)
                  |> DayFourteen.Groups.Create

              Expect.equal got expected "We should be able to add a non-neighboring point"

          testCase "Test Groups.Combine"
          <| fun _ ->
              let baseGroups =
                  [ 0, [ 0, 0; 1, 0; 2, 0 ] |> Set.ofList
                    2, [ 0, 2; 1, 2; 2, 2 ] |> Set.ofList
                    4, [ 5, 5; 5, 6 ] |> Set.ofList ]
                  |> Map.ofList
                  |> DayFourteen.Groups.Create

              let expected =
                  [ 0,
                    [ 0, 0
                      1, 0
                      2, 0
                      1, 1
                      0, 2
                      1, 2
                      2, 2 ]
                    |> Set.ofList
                    4, [ 5, 5; 5, 6 ] |> Set.ofList ]
                  |> Map.ofList
                  |> DayFourteen.Groups.Create

              let point = 1, 1

              let got = baseGroups.Update point

              Expect.equal got expected "We should be able to update and combine" ]
