use std::fs::File;
use std::io::{BufRead, BufReader};
use std::iter::FromIterator;

use im::HashMap;

const ID_LEN: usize = 26;

fn count_for_id(id: &String) -> HashMap<char, i32> {
    let init: HashMap<char, i32> = HashMap::new();
    id.chars()
        .fold(init, |hm, c| hm.update_with(c, 1, |p, n| p + n))
}

fn filter_for_counts(counts: HashMap<char, i32>) -> (i32, i32) {
    let maybe_two = counts.iter().filter(|(_, v)| v == &2).next();
    let maybe_three = counts.iter().filter(|(_, v)| v == &3).next();
    match (maybe_two, maybe_three) {
        (Some(_two), Some(_three)) => (1, 1),
        (Some(_two), None) => (1, 0),
        (None, Some(_three)) => (0, 1),
        (None, None) => (0, 0),
    }
}

fn load_ids(filepath: &str) -> Vec<String> {
    let file = File::open(filepath).expect("Failed to read input file");
    let buf = BufReader::new(file);
    buf.lines().map(|l| l.unwrap()).collect()
}

fn one_off(id1: &Vec<char>, id2: &Vec<char>) -> Option<String> {
    let mut differences = 0;
    let mut same_same: Vec<char> = Vec::new();

    for idx in 0..ID_LEN {
        if id1[idx] != id2[idx] {
            differences = differences + 1;
            if differences > 1 {
                return None;
            }
        } else {
            same_same.push(id1[idx]);
        }
    }

    match differences {
        1 => Some(String::from_iter(same_same)),
        _ => None,
    }
}

fn generate_combinations(ids: &Vec<String>) -> Vec<(Vec<char>, Vec<char>)> {
    ids.iter()
        .flat_map(|id| {
            ids.iter()
                .map(move |second_id| (id.chars().collect(), second_id.chars().collect()))
        })
        .collect()
}

fn collapse_output(output: Vec<Option<String>>) -> Option<String> {
    let mut found: Vec<&String> = output.iter().flat_map(|opt| opt.into_iter()).collect();
    found.sort();
    found.dedup();

    match found.len() {
        0 => None,
        1 => Some(found[0].to_string()),
        _ => {
            println!("Found many results");
            None
        }
    }
}

fn part_one(ids: &Vec<String>) {
    let (two_count, three_count) = ids
        .iter()
        .map(|id| {
            let count_map = count_for_id(id);
            filter_for_counts(count_map)
        })
        .fold((0, 0), |acc, tup| (acc.0 + tup.0, acc.1 + tup.1));
    let checksum = two_count * three_count;

    println!("Produced a checksum of {}", checksum);
}

fn part_two(ids: &Vec<String>) -> Option<String> {
    let combos = generate_combinations(ids);

    let output: Vec<Option<String>> = combos
        .iter()
        .map(|(id1, id2)| one_off(&id1, &id2))
        .collect();

    collapse_output(output)
}

pub fn run() {
    let ids = load_ids("resources/day_two_input.txt");

    part_one(&ids);
    let part_two_result = part_two(&ids);

    match part_two_result {
        Some(s) => println!("Found result to be: {}", s),
        None => println!("Failed to generate a result"),
    }
}

#[cfg(test)]
mod day_two_test {

    use super::*;

    // For example, if you see the following box IDs:

    // abcdef contains no letters that appear exactly two or three times.
    // bababc contains two a and three b, so it counts for both.
    // abbcde contains two b, but no letter appears exactly three times.
    // abcccd contains three c, but no letter appears exactly two times.
    // aabcdd contains two a and two d, but it only counts once.
    // abcdee contains two e.
    // ababab contains three a and three b, but it only counts once.

    #[test]
    fn test_count_for_id() {
        let test_id_one = String::from("abcdef");
        let test_id_two = String::from("ababab");

        let expected_one = HashMap::new()
            .update('a', 1)
            .update('b', 1)
            .update('c', 1)
            .update('d', 1)
            .update('e', 1)
            .update('f', 1);

        let expected_two = HashMap::new().update('a', 3).update('b', 3);

        let got_one = count_for_id(&test_id_one);
        let got_two = count_for_id(&test_id_two);

        assert_eq!(expected_one, got_one);
        assert_eq!(expected_two, got_two);
    }

    #[test]
    fn test_filter_for_counts() {
        let test_hash_one = HashMap::new()
            .update('a', 1)
            .update('b', 1)
            .update('c', 1)
            .update('d', 1)
            .update('e', 1)
            .update('f', 1);

        let test_hash_two = HashMap::new().update('a', 1).update('b', 2).update('c', 3);
        let test_hash_three = HashMap::new().update('a', 3).update('b', 3);

        let got_one = filter_for_counts(test_hash_one);
        let got_two = filter_for_counts(test_hash_two);
        let got_three = filter_for_counts(test_hash_three);

        let expected_one = (0, 0);
        let expected_two = (1, 1);
        let expected_three = (0, 1);

        assert_eq!(expected_one, got_one);
        assert_eq!(expected_two, got_two);
        assert_eq!(expected_three, got_three);
    }

    #[test]
    fn test_one_off() {
        let id1 = String::from("abcdefghijklmnopqrstuvwxyz").chars().collect();
        let not_one_off = String::from("abcdefghijklmnopqrstuvwxyz").chars().collect();
        let also_not_one_off = String::from("eeeeeeeeeeeeeeeeeeeeeeeeee").chars().collect();
        let is_one_off = String::from("abcdefghijklmnKpqrstuvwxyz").chars().collect(); // There's a K in the middle

        assert_eq!(one_off(&id1, &not_one_off), None);
        assert_eq!(one_off(&id1, &also_not_one_off), None);
        assert_eq!(
            one_off(&id1, &is_one_off),
            Some(String::from("abcdefghijklmnpqrstuvwxyz"))
        );
    }

    #[test]
    fn test_generate_combinations() {
        let id1 = String::from("abc");
        let id2 = String::from("def");
        let id3 = String::from("ghi");

        let ids = vec![id1.clone(), id2.clone(), id3.clone()];

        let id1_chars: Vec<char> = id1.chars().collect();
        let id2_chars: Vec<char> = id2.chars().collect();
        let id3_chars: Vec<char> = id3.chars().collect();

        let expected = vec![
            (id1_chars.clone(), id1_chars.clone()),
            (id1_chars.clone(), id2_chars.clone()),
            (id1_chars.clone(), id3_chars.clone()),
            (id2_chars.clone(), id1_chars.clone()),
            (id2_chars.clone(), id2_chars.clone()),
            (id2_chars.clone(), id3_chars.clone()),
            (id3_chars.clone(), id1_chars.clone()),
            (id3_chars.clone(), id2_chars.clone()),
            (id3_chars.clone(), id3_chars.clone()),
        ];

        let combos = generate_combinations(&ids);

        assert_eq!(expected, combos);
    }

    #[test]
    fn test_collapse_output() {
        let returns_one = vec![None, Some(String::from("boop")), None];
        let returns_zero = vec![None, None, None];

        assert_eq!(collapse_output(returns_one), Some(String::from("boop")));
        assert_eq!(collapse_output(returns_zero), None);
    }

    #[test]
    fn test_part_two() {
        let ids = vec![
            String::from("abcdefghijklmnopqrstuvwxyz"),
            String::from("abcdefghijklmnopqrstuvwxyz"),
            String::from("eeeeeeeeeeeeeeeeeeeeeeeeee"),
            String::from("abcdefghijklmnKpqrstuvwxyz"), // There's a K in the middle
        ];

        let result = part_two(&ids);
        let expected = Some(String::from("abcdefghijklmnpqrstuvwxyz"));

        assert_eq!(expected, result);
    }
}
