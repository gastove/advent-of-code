module DayTenTests

open Expecto

let input =
    [| "[({(<(())[]>[[{[]{<()<>>"
       "[(()[<>])]({[<{<<[]>>("
       "{([(<{}[<>[]}>{[]{[(<()>"
       "(((({<>}<{<{<>}{[]{[]{}"
       "[[<[([]))<([[{}[[()]]]"
       "[{[{({}]{}}([{[{{{}}([]"
       "{<[[]]>}<{[{[{[]{()[[[]"
       "[<(<(<(<{}))><([]([]()"
       "<{([([[(<>()){}]>(<<{{"
       "<{([{{}}[<[[[<>{}]]]>[]]" |]

[<Tests>]
let partTwoTests =
    testList
        "Testing Part Two"
        [ testCase "Do we get the correct scores?" <| fun _ ->
              let scores = input |> DayTen.scoreInput

              let expectedScores = [288957L; 5566L; 1480781L; 995444L; 294L] |> Set.ofList

              Expect.equal scores.Length 5 "We should get back 5 scores"
              Expect.equal (scores |> Set.ofArray) expectedScores "We should get the correct scores"

          testCase "Can we get the right score for the part 2 example?"
          <| fun _ ->
              let expected = "288957"
              let got = input |> DayTen.partTwo

              Expect.equal got expected "Can we reproduce the example?" ]
