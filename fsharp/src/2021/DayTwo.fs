module DayTwo

open FSharp.Core.Operators.Checked

type Movement =
    | Up of int
    | Down of int
    | Forward of int

    static member FromString(s: string) =
        match s.Split() with
        | [| "forward"; amount |] -> amount |> int |> Forward
        | [| "up"; amount |] -> amount |> int |> Up
        | [| "down"; amount |] -> amount |> int |> Down
        | _ -> failwithf "Failed to parse as movement %s" s

type Position =
    { Depth: int
      Horizontal: int }

    static member Init = { Depth = 0; Horizontal = 0 }

type PositionWithAim =
    { Depth: int
      Horizontal: int
      Aim: int }

    static member Init = { Depth = 0; Horizontal = 0; Aim = 0 }

    member this.HandleUp x = { this with Aim = this.Aim - x }

    member this.HandleDown x = { this with Aim = this.Aim + x }

    member this.HandleForward x =
        { this with
            Horizontal = this.Horizontal + x
            Depth = this.Depth + (this.Aim * x) }

let pilotSub (movement: Movement array) =
    let rec move (remainingMovement: Movement list) pos =
        match remainingMovement with
        | thisMove :: remaining ->
            match thisMove with
            | Up v -> move remaining { pos with Position.Depth = pos.Depth - v }
            | Down v -> move remaining { pos with Depth = pos.Depth + v }
            | Forward v -> move remaining { pos with Horizontal = pos.Horizontal + v }
        | [] -> pos

    move (movement |> Array.toList) Position.Init

let pilotSubMoreDifferent (movement: Movement array) =
    let rec move (remainingMovement: Movement list) (pos: PositionWithAim) =
        match remainingMovement with
        | thisMove :: remaining ->
            match thisMove with
            | Up v -> move remaining (pos.HandleUp v)
            | Down v -> move remaining (pos.HandleDown v)
            | Forward v -> move remaining (pos.HandleForward v)
        | [] -> pos

    move (movement |> Array.toList) PositionWithAim.Init

let partOne (input: Common.Input) =
    let finalPosition =
        input |> Array.map Movement.FromString |> pilotSub

    finalPosition.Horizontal * finalPosition.Depth

let partTwo (input: Common.Input) =
    let finalPosition =
        input |> Array.map Movement.FromString |> pilotSubMoreDifferent

    finalPosition.Horizontal * finalPosition.Depth


type Solution() =
    let name = "two"

    interface Common.ISolution with
        member __.Name() = name |> Common.titleCase
        member __.LoadData(year) : Common.Input option = Common.loadData name year
        member __.PartOne(input: Common.Input) : string = input |> partOne |> string
        member __.PartTwo(input: Common.Input) : string = input |> partTwo |> string
