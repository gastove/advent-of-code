module DayEighteen

open FSharp.Core.Operators.Checked

module Shared =
    type Symbol =
        | Number of int64
        | Multiply
        | Add
        | Subexpression of Symbol list
        static member ParseLimited c =
            match c with
            | '+' -> Add
            | '*' -> Multiply
            | n -> Number(System.Int64.Parse <| string n)

    let parseSymbols (symbols: string) =
        let rec work acc remaining =
            match remaining with
            | [ elem ] ->
                match elem with
                | ')' -> acc, []
                | c -> acc @ [ (Symbol.ParseLimited c) ], []
            | head :: rest ->
                match head with
                | ')' -> acc, rest
                | '(' ->
                    let elements, newRemaining = work [] rest
                    let subexp = Subexpression elements
                    let newAcc = acc @ [ subexp ]

                    work newAcc newRemaining

                | c -> work (acc @ [ Symbol.ParseLimited c ]) rest
            | [] -> acc, []

        let chars =
            symbols
            |> Seq.toList
            |> List.filter (fun c -> c <> ' ')

        work [] chars |> fst


module PartOne =
    open Shared

    let rec evaluate (symbols: Symbol list) =
        let evalSubexp =
            function
            | Subexpression(sl) -> evaluate sl |> Option.map Number
            | s -> Some(s)

        let rec work (syms: Symbol list) =
            let cleaned = syms |> List.choose evalSubexp

            match cleaned with
            | Number(n) :: Add :: rest -> work rest |> Option.map ((+) n)
            | Number(n) :: Multiply :: rest -> work rest |> Option.map ((*) n)
            | [ Number(n) ] -> Some n
            | _ -> None

        // We're using recursion, which effectively means we're evaluating backwards.
        List.rev symbols |> work

module PartTwo =
    open Shared

    let rec evaluate (symbols: Symbol list) =
        let evalSubexp =
            function
            | Subexpression(sl) -> evaluate sl |> Option.map Number
            | s -> Some(s)

        let rec evalAddition (acc: Symbol list) (syms: Symbol list) =
            match syms with
            | Number(n) :: Add :: Number(n2) :: rest -> evalAddition acc ((n + n2 |> Number) :: rest)
            | head :: rest -> evalAddition (acc @ [ head ]) rest
            | [] -> acc

        let rec evalMultiplication (acc: Symbol list) (syms: Symbol list) =
            match syms with
            | Number(n) :: Multiply :: Number(n2) :: rest -> evalMultiplication acc ((n * n2 |> Number) :: rest)
            | head :: rest -> evalMultiplication (acc @ [ head ]) rest
            | [] -> acc

        let work (syms: Symbol list) =
            let mutable workingSymbols = syms |> List.choose evalSubexp
            while workingSymbols.Length > 1 do
                workingSymbols <- evalAddition [] workingSymbols
                workingSymbols <- evalMultiplication [] workingSymbols

            match workingSymbols with
            | [ Number(n) ] -> Some n
            | _ -> None

        let reversed = List.rev symbols

        work reversed

type Solution() =
    let name = "eighteen"
    interface Common.ISolution with
        member __.Name() = name |> Common.titleCase
        member __.LoadData(year): Common.Input option = Common.loadData name year

        member __.PartOne(input: Common.Input): string =
            let equations = input |> Array.map Shared.parseSymbols

            let solved =
                equations
                |> Array.Parallel.choose PartOne.evaluate
                |> Array.reduce (+)

            sprintf "The sum of all homeworks is %i" solved

        member __.PartTwo(input: Common.Input): string =
            let equations = input |> Array.map Shared.parseSymbols

            let solved =
                equations
                |> Array.Parallel.choose PartTwo.evaluate
                |> Array.reduce (+)

            sprintf "The sum of all homeworks is %i" solved
