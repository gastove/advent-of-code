module DayTwentyOneTests

open Expecto

open DayTwentyOne

[<Tests>]
let partOneTests =
    testList
        "Testing Day Twenty One, Part One"
        [ testCase "Testing Deterministic Die Rolling"
          <| fun _ ->
              let game = DiracDice.Create 0 0 deterministicDie
              let (firstRoll, rolledOnce) = game.Roll()
              let (secondRoll, _) = rolledOnce.Roll()
              let expectedFirst = 6
              let expectedSecond = 15

              Expect.equal firstRoll expectedFirst "We should get the correct first number in the sequence"
              Expect.equal secondRoll expectedSecond "We should get the correct second number in the sequence"

          testCase "Testing Deterministic Die Rolling on multiple rolls"
          <| fun _ ->
              let baseGame = DiracDice.Create 0 0 deterministicDie

              let game = { baseGame with Rolls = 12 }
              let (firstRoll, rolledOnce) = game.Roll()
              let (secondRoll, _) = rolledOnce.Roll()

              let expectedFirst = 42
              let expectedSecond = 51

              Expect.equal firstRoll expectedFirst "We should get the correct first number in the sequence"
              Expect.equal secondRoll expectedSecond "We should get the correct first number in the sequence"

          testCase "Testing a basic game of Dirac Dice"
          <| fun _ ->
              let p1Start = 4
              let p2Start = 8

              let game =
                  DiracDice.Create p1Start p2Start deterministicDie

              let outcome = game |> playUntilWinner

              Expect.equal outcome.Rolls 993 "We should roll the expected number of times"
              Expect.equal outcome.LoosingScore 745 "We should get the correct loosing score"
              Expect.equal outcome.WinningScore 1000 "We should win at 1000" ]
