module DaySix

open FSharp.Core.Operators.Checked

let incrAtIdx (data: int array) idx =
    let currVal = Array.get data idx
    Array.set data idx (currVal + 1)
    data


let parseInput (input: Common.Input) =
    let s = input |> Array.filter (fun l -> l.Trim() <> "") |> Array.exactlyOne

    s.Split([| '\t' |]) |> Array.choose Common.tryInt

/// Returns the integer index of the current largest memory bank
let findBiggestBank (banks: int array) =
    let biggest = banks |> Array.max

    banks
    |> Array.findIndex (fun elem -> elem = biggest)

let incrOrWrap idx banks =
    if (idx + 1) >= banks
    then 0
    else idx + 1

let redistribute (fromBank: int) (banks: int array) =
    let mutable toRedistribute = Array.get banks fromBank
    let mutable newBanks = banks |> Array.copy

    newBanks.[fromBank] <- 0

    let mutable idx = incrOrWrap fromBank newBanks.Length

    while toRedistribute > 0 do
        newBanks <- incrAtIdx newBanks idx
        toRedistribute <- toRedistribute - 1

        idx <- incrOrWrap idx newBanks.Length

    newBanks

let partOne (input: Common.Input) =
    let parsedInput = input |> parseInput

    let mutable seen = Set.empty |> Set.add parsedInput
    let mutable biggestBank = findBiggestBank parsedInput
    let mutable next = redistribute biggestBank parsedInput
    let mutable passes = 1

    while not (Set.contains next seen) do
        seen <- Set.add next seen
        biggestBank <- findBiggestBank next
        next <- redistribute biggestBank next
        passes <- passes + 1

    passes

let partTwo (input: Common.Input) =
    let parsedInput = input |> parseInput

    let mutable seen = Set.empty |> Set.add parsedInput
    let mutable biggestBank = findBiggestBank parsedInput
    let mutable next = redistribute biggestBank parsedInput

    while not (Set.contains next seen) do
        seen <- Set.add next seen
        biggestBank <- findBiggestBank next
        next <- redistribute biggestBank next

    let cycle = next |> Array.copy
    seen <- Set.add next seen
    biggestBank <- findBiggestBank next
    next <- redistribute biggestBank next
    let mutable passes = 1

    while not (next = cycle) do
        seen <- Set.add next seen
        biggestBank <- findBiggestBank next
        next <- redistribute biggestBank next
        passes <- passes + 1

    passes


type Solution() =
    let name = "six"

    interface Common.ISolution with
        member __.Name() = name |> Common.titleCase
        member __.LoadData(year) : Common.Input option = Common.loadData name year
        member __.PartOne(input: Common.Input) : string = input |> partOne |> string
        member __.PartTwo(input: Common.Input) : string = input |> partTwo |> string
