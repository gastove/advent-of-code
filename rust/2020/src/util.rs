use std::fs::File;
use std::io::{BufRead, BufReader};

pub fn read_input(path: &str) -> Vec<String> {
    let file = File::open(path).expect("Failed to open file");
    let buf = BufReader::new(file);
    buf.lines().map(|l| l.unwrap()).collect()
}
