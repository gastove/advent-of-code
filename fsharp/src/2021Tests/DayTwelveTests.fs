module DayTwelveTests

open Expecto

open DayTwelve

let shortInput =
    [| "start-A"
       "start-b"
       "A-c"
       "A-b"
       "b-d"
       "A-end"
       "b-end" |]

let slightlyLongerInput = [|
    "dc-end"
    "HN-start"
    "start-kj"
    "dc-start"
    "dc-HN"
    "LN-dc"
    "HN-end"
    "kj-sa"
    "kj-HN"
    "kj-dc"
|]

let longestInput = [|
    "fs-end"
    "he-DX"
    "fs-he"
    "start-DX"
    "pj-DX"
    "end-zg"
    "zg-sl"
    "zg-pj"
    "pj-he"
    "RW-he"
    "fs-DX"
    "pj-RW"
    "zg-RW"
    "start-pj"
    "he-WI"
    "zg-he"
    "pj-fs"
    "start-RW"
|]

[<Tests>]
let partTwoTests =
    testList
        "Testing PartTwo Code"
        [ testCase "Can we reproduce the short example?"
          <| fun _ ->
              let got = shortInput |> partTwo
              let expected = "36"

              Expect.equal got expected "We should get the answer from the example"

          testCase "Can we reproduce the slightly longer example?"
          <| fun _ ->
              let got = slightlyLongerInput |> partTwo
              let expected = "103"

              Expect.equal got expected "We should get the answer from the example"

          testCase "Can we reproduce the longest example?"
          <| fun _ ->
              let got = longestInput |> partTwo
              let expected = "3509"

              Expect.equal got expected "We should get the answer from the example"
          ]
