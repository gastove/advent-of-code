module DayFour

open FSharp.Core.Operators.Checked

type BingoNumber =
    { Number: int
      Marked: bool }

    static member Create i = { Number = i; Marked = false }

    member this.Mark() = { this with Marked = true }

module BingoNumber =
    let mark (bn: BingoNumber) = bn.Mark()

    let isMarked (bn: BingoNumber) = bn.Marked

type BingoBoard =
    { Grid: BingoNumber array array }

    static member Create numbers = { Grid = numbers }

    member this.IsValid =
        this.Grid.Length = 5
        && this.Grid
           |> Array.forall (fun arr -> arr.Length = 5)

    member this.MarkNumber num =
        this.Grid
        |> Array.indexed
        |> Array.tryPick (fun (idx, arr) ->
            arr
            |> Array.tryFindIndex (fun bn -> bn.Number = num)
            |> Option.map (fun innerIdx -> (idx, innerIdx)))
        |> Option.map (fun (outerIdx, innerIdx) ->
            let mutable newData = this.Grid |> Array.copy

            let bn =
                newData.[outerIdx].[innerIdx] |> BingoNumber.mark

            newData.[outerIdx].[innerIdx] <- bn

            newData |> BingoBoard.Create)
        |> Option.defaultValue this

    member this.Wins =
        let rowWins =
            this.Grid
            |> Array.exists (Array.forall BingoNumber.isMarked)

        let colWins =
            this.Grid
            |> Array.transpose
            |> Array.exists (Array.forall BingoNumber.isMarked)

        rowWins || colWins

    member this.UnmarkedSum =
        this.Grid
        |> Array.sumBy (fun arr -> arr |> Array.sumBy (fun bn -> if not bn.Marked then bn.Number else 0))

module BingoBoard =
    let markNumber num (bb: BingoBoard) = bb.MarkNumber num

    let wins (bb: BingoBoard) = bb.Wins

let parseInput (input: Common.Input) =
    let calledNumbers =
        input
        |> Array.head
        |> (fun s ->
            s.Split(',')
            |> Array.map (fun c -> c.Trim() |> int))
        |> Array.toList

    let boards = input |> Array.tail

    let rec makeBoards (raw: string list) (acc: BingoBoard list) =
        match raw with
        | line :: rest ->
            if Common.Strings.blank line && rest.Length >= 5 then
                let boardLines = rest |> List.take 5
                let remaining = rest |> List.skip 5

                let newBoard =
                    boardLines
                    |> List.map (fun s ->
                        s.Split(' ')
                        |> Array.filter Common.Strings.notBlank
                        |> Array.map (int >> BingoNumber.Create))
                    |> Array.ofList
                    |> BingoBoard.Create

                makeBoards remaining (newBoard :: acc)
            else
                acc
        | [] -> acc

    calledNumbers, (makeBoards (boards |> Array.toList) [])

module PartOne =
    let rec playBingo (numbers: int list) boards =
        match numbers with
        | num :: remaining ->
            let marked =
                boards |> List.map (BingoBoard.markNumber num)

            match marked |> List.tryFind BingoBoard.wins with
            | Some bb -> (bb, num) |> Some
            | None -> playBingo remaining marked
        | [] -> None

module PartTwo =
    let rec playBingo (numbers: int list) (boards: BingoBoard list) (mostRecentWinner: (BingoBoard * int) option) =
            match numbers with
            | num :: remainingNumbers ->
                let marked =
                    boards |> List.map (BingoBoard.markNumber num)

                match marked |> List.tryFind BingoBoard.wins with
                    | None -> playBingo remainingNumbers marked mostRecentWinner
                    | Some newWinner ->
                        let remainingBoards = marked |> List.filter (BingoBoard.wins>>not)

                        playBingo remainingNumbers remainingBoards (Some (newWinner, num))

            | [] -> mostRecentWinner


let partOne (input: Common.Input) =
    let (numbers, boards) = input |> parseInput

    PartOne.playBingo numbers boards
    |> Option.map (fun (w, num) -> w.UnmarkedSum * num)
    |> Option.get
    |> string

let partTwo (input: Common.Input) =
    let (numbers, boards) = input |> parseInput

    PartTwo.playBingo numbers boards None
    |> Option.map (fun (w, num) -> w.UnmarkedSum * num)
    |> Option.get
    |> string

type Solution() =
    let name = "four"

    interface Common.ISolution with
        member __.Name() = name |> Common.titleCase
        member __.LoadData(year) : Common.Input option = Common.loadData name year
        member __.PartOne(input: Common.Input) : string = input |> partOne
        member __.PartTwo(input: Common.Input) : string = input |> partTwo
