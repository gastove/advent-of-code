module DayNine

open FSharp.Core.Operators.Checked

let specialCharacters = [ '{'; '}'; '!'; '<'; '>' ]

let clearGarbage (stream: char list) =
    let rec clear (data: char list) =
        match data with
        | character :: remaining ->
            match character with
            | '!' -> clear (remaining |> List.tail)
            | '>' -> remaining
            | _ -> clear remaining
        | [] -> []

    clear stream

let scoreStream (data: char list) =
    let rec computeScore characters score =
        match characters with
        | character :: remaining ->
            match character with
            | '{' -> score + computeScore remaining (score + 1)
            | '}' -> computeScore remaining (score - 1)
            | '<' -> computeScore (remaining |> clearGarbage) score
            | _ -> computeScore remaining score
        | [] -> 0

    computeScore data 1

let countGarbage (stream: char list) =
    let mutable removedChars = 0

    let rec clear (data: char list) =
        match data with
        | character :: remaining ->
            match character with
            | '!' -> clear (remaining |> List.tail)
            | '>' -> remaining
            | _ ->
                removedChars <- removedChars + 1
                clear remaining
        | [] -> []

    let cleared = clear stream

    cleared, removedChars

let scoreGarbageInStream (data: char list) =
    let mutable totalGarbage = 0

    let rec traverseGarbage characters =
        match characters with
        | character :: remaining ->
            match character with
            | '{' -> traverseGarbage remaining
            | '}' -> traverseGarbage remaining
            | '<' ->
                let newRemaining, garbageChars = remaining |> countGarbage
                totalGarbage <- totalGarbage + garbageChars
                traverseGarbage newRemaining
            | _ -> traverseGarbage remaining
        | [] -> 0

    traverseGarbage data |> ignore

    totalGarbage

let partOne (input: Common.Input) =
    input
    |> Array.exactlyOne
    |> List.ofSeq
    |> scoreStream

let partTwo (input: Common.Input) =
    input
    |> Array.exactlyOne
    |> List.ofSeq
    |> scoreGarbageInStream

type Solution() =
    let name = "nine"

    interface Common.ISolution with
        member __.Name() = name |> Common.titleCase
        member __.LoadData(year) : Common.Input option = Common.loadData name year
        member __.PartOne(input: Common.Input) : string = input |> partOne |> string
        member __.PartTwo(input: Common.Input) : string = input |> partTwo |> string
