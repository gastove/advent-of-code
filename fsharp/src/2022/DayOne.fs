module DayOne

open FSharp.Core.Operators.Checked

open Common

let computeCaloriesPerElf ((acc: int list), (calories: int)) numberOrBlank =
    match numberOrBlank |> tryInt with
    | Some i -> acc, calories + i
    | None -> calories :: acc, 0

let partOne (input: Common.Input) =
    input
    |> Array.fold computeCaloriesPerElf (List.empty, 0)
    |> fst
    |> List.max
    |> string

let partTwo (input: Common.Input) =
    input
    |> Array.fold computeCaloriesPerElf (List.empty, 0)
    |> fst
    |> List.sortDescending
    |> List.take 3
    |> List.sum
    |> string

type Solution() =
    let name = "one"

    interface Common.ISolution with
        member __.Name() = name |> Common.titleCase
        member __.LoadData(year) : Common.Input option = Common.loadData name year
        member __.PartOne(input: Common.Input) : string = input |> partOne
        member __.PartTwo(input: Common.Input) : string = input |> partTwo
