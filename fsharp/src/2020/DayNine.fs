module DayNine

//--------------------------------- Part One ---------------------------------//
// The first step of attacking the weakness in the XMAS data is to find the
// first number in the list (after the preamble) which is not the sum of two of
// the 25 numbers before it. What is the first number that does not have this
// property?

let parseInitial (input: string array) (preambleLen: int): int64 list * int64 list =
    let numbers =
        input
        |> List.ofArray
        |> List.choose Common.tryInt64

    let preamble = numbers |> List.take preambleLen

    preamble, numbers.[preambleLen..]

let computeUniquePairs (input: int64 list): (int64 * int64) list =
    List.allPairs input input
    |> List.filter (fun (x, y) -> x <> y)
    |> List.distinct

type PartOneState =
    { Buffer: int64 list
      BufSize: int
      Answer: int64 option }

    static member New (input: int64 list) (bufSize: int) =
        { Buffer = input |> List.rev
          BufSize = bufSize
          Answer = None }

    member this.CanProduce(i: int64): bool =
        this.Buffer
        |> computeUniquePairs
        |> List.map (fun (x, y) -> x + y)
        |> List.contains i

    member this.NewBuffer(i: int64) =
        let baseBuffer = i :: this.Buffer
        baseBuffer.[0..this.BufSize - 1] // F# slices are *inclusive*, so slice to one element less than I want

    member this.Consume(i: int64) =
        let newBuffer = this.NewBuffer i

        if this.CanProduce i then
            { this with Buffer = newBuffer }
        else
            { this with
                  Buffer = newBuffer
                  Answer = Some i }

let rectifyAccumulator (acc: int64 list) (target: int64): int64 list =
    let mutable newAcc = acc

    // If the sum has gone over the target, drop numbers from the end until we're back below
    while List.sum newAcc > target do
        newAcc <- newAcc.[..(newAcc.Length - 2)]

    newAcc

let findContiguousRange (input: int64 list) (target: int64) =

    let rec work (queue: int64 list) (acc: int64 list): int64 list =
        match queue with
        | i :: rest ->
            let newAcc = i :: acc
            if (List.sum acc = target)
            then acc
            else
                work rest <| rectifyAccumulator newAcc target

        | [] -> acc


    work input List.empty

type Solution() =
    let name = "nine"
    interface Common.ISolution with

        member __.PartOne(input: Common.Input): string =
            let preambleLength = 25
            let preamble, workingSet = parseInitial input preambleLength

            let state = PartOneState.New preamble preambleLength

            let wrongNumber = workingSet |> List.fold (fun (s: PartOneState) i -> s.Consume i) state

            match wrongNumber.Answer with
            | Some(number) -> sprintf "Found a wrong number! Probably %i" number
            | None -> "It didn't work, chief"

        member __.PartTwo(input: Common.Input): string =
            let target = 22477624L

            let parsed =
                input
                |> Array.choose Common.tryInt64
                |> List.ofArray
                |> List.rev
                |> List.skipWhile (fun i -> i > target)
                |> List.rev

            let range = findContiguousRange parsed target

            printfn "Found contiguous range %A, which sums to %i, which is the target %i" range (range |> List.sum)
                target

            let minimum = List.min range
            let maximum = List.max range

            sprintf "Min %i and Max %i give an encryption weakness of %i" minimum maximum (minimum + maximum)

        member __.Name() = name
        member __.LoadData(year): Common.Input option = Common.loadData name year
