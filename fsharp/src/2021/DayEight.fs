module DayEight

open FSharp.Core.Operators.Checked

let parseInput (input: Common.Input) =
    input
    |> Array.map (fun line ->
        let parts = line.Split(" | ")
        let input = parts.[0]
        let output = parts.[1]
        input, output)

let knownDigit (digit: string) =
    match digit.Length with
    | 2 -> Some 1
    | 4 -> Some 4
    | 3 -> Some 7
    | 7 -> Some 8
    | _ -> None

let buildInitialSegmentMap (digits: string array) =
    digits
    |> Array.choose (fun d ->
        d
        |> knownDigit
        |> Option.map (fun kd -> kd, d |> Set.ofSeq))
    |> Map.ofArray

/// Checks that secondSet contains all the same elements as firstSet *and* one more.
let oneDifferent<'T when 'T: comparison> (firstSet: Set<'T>) (secondSet: Set<'T>) : bool =
    let secondContainsFirst =
        (Set.intersect secondSet firstSet) = firstSet

    let secondBiggerThanFirst =
        (Set.difference secondSet firstSet).Count = 1

    secondContainsFirst && secondBiggerThanFirst


let solveNine (digits: Set<char> array) (digitMap: Map<int, Set<char>>) =
    let requiredPositions =
        [ digitMap |> Map.find 4
          digitMap |> Map.find 7 ]
        |> Set.unionMany

    let nine =
        digits
        |> Array.find (fun maybeNine -> oneDifferent requiredPositions maybeNine)

    digitMap |> Map.add 9 nine

let solveZeroAndSix (digits: Set<char> array) (digitMap: Map<int, Set<char>>) =
    let eight = digitMap |> Map.find 8
    let one = digitMap |> Map.find 1
    let nine = digitMap |> Map.find 9

    let zeroAndSix =
        digits
        |> Array.filter (fun maybeZeroOrSix ->
            maybeZeroOrSix <> nine
            && oneDifferent maybeZeroOrSix eight)

    let zero =
        zeroAndSix
        |> Array.find (fun maybeZeroOrSix -> (Set.difference one maybeZeroOrSix).Count = 0)

    let six =
        zeroAndSix
        |> Array.filter (fun elem -> elem <> zero)
        |> Array.exactlyOne

    digitMap
    |> Common.Maps.addIfNotExists 0 zero
    |> Common.Maps.addIfNotExists 6 six

let solveFive (digits: Set<char> array) (digitMap: Map<int, Set<char>>) =
    let six = digitMap |> Map.find 6
    let nine = digitMap |> Map.find 9

    let five =
        digits
        |> Array.find (fun maybeFive -> oneDifferent maybeFive six && maybeFive <> nine)

    digitMap |> Common.Maps.addIfNotExists 5 five

let solveThree (digits: Set<char> array) (digitMap: Map<int, Set<char>>) =
    let five = digitMap |> Map.find 5
    let nine = digitMap |> Map.find 9

    let three =
        digits
        |> Array.find (fun maybeThree -> oneDifferent maybeThree nine && maybeThree <> five)

    digitMap |> Map.add 3 three

let solveTwo (digits: Set<char> array) (digitMap: Map<int, Set<char>>) =
    let two =
        digits
        |> Array.find (fun maybeTwo ->
            digitMap
            |> Map.exists (fun _ v -> maybeTwo = v)
            |> not)

    digitMap |> Common.Maps.addIfNotExists 2 two

let partOne (input: Common.Input) =
    let _, outputDigits = input |> parseInput |> Array.unzip

    outputDigits
    |> Array.collect (fun line ->
        let cleaned =
            line.Split(' ') |> Array.map Common.Strings.trim

        cleaned |> Array.choose knownDigit)
    |> Array.length
    |> string

let decodeNumber (digits: string array) (lookupMap: Map<string, string>) =
    digits
    |> Array.map (fun digit ->
        let munged =
            digit
            |> Seq.toArray
            |> Array.sort
            |> System.String

        lookupMap |> Map.find munged)
    |> String.concat ""
    |> int

let partTwo (input: Common.Input) =
    let fullInput =
        input
        |> parseInput
        |> Array.map (fun (inp, outp) ->
            let cleanedIn =
                inp.Split(' ') |> Array.map Common.Strings.trim

            let cleanedOut =
                outp.Split(' ') |> Array.map Common.Strings.trim

            cleanedIn, cleanedOut)

    let inputDigits, outputDigits = fullInput |> Array.unzip

    let numberMaps =
        inputDigits
        |> Array.map (fun digits ->
            let numberSet = digits |> Array.map Set.ofSeq

            buildInitialSegmentMap digits
            |> solveNine numberSet
            |> solveZeroAndSix numberSet
            |> solveFive numberSet
            |> solveThree numberSet
            |> solveTwo numberSet)

    let numberLookupMaps =
        numberMaps
        |> Array.map (
            Map.fold
                (fun newMap k v ->
                    let asString = System.String(v |> Set.toArray)

                    newMap
                    |> Common.Maps.addIfNotExists asString (string k))
                Map.empty
        )

    Array.zip outputDigits numberLookupMaps
    |> Array.map (fun (toDecode, lookup) -> decodeNumber toDecode lookup)
    |> Array.sum
    |> string

type Solution() =
    let name = "eight"

    interface Common.ISolution with
        member __.Name() = name |> Common.titleCase
        member __.LoadData(year) : Common.Input option = Common.loadData name year
        member __.PartOne(input: Common.Input) : string = input |> partOne
        member __.PartTwo(input: Common.Input) : string = input |> partTwo
