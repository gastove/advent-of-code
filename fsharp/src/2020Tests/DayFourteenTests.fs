module DayFourteenTests

open Expecto

let testInput = """mask = 00000X110010111111X000100XX01010000X
mem[20690] = 435
mem[54036] = 231
mem[27099] = 118644255
mem[55683] = 22299263
mem[26119] = 2279399"""

let testProgram = [| "mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X"; "mem[8] = 11"; "mem[7] = 101"; "mem[8] = 0" |]

[<Tests>]
let tests =
    testList "Parsing tests"
        [ testCase "Can I parse input correctly kinda at all?" <| fun _ -> ()
          testCase "Can I parse an integer to Weird Binary?" <| fun _ ->
              let test = 11L
              let expected = "000000000000000000000000000000001011"
              let got = DayFourteen.intToWeirdBinary test

              Expect.equal got expected "These should match"

          testCase "Can I return a Weird Binary to integer?" <| fun _ ->
              let expected = 11L
              let test = "000000000000000000000000000000001011"
              let got = DayFourteen.weirdBinaryToInt test

              Expect.equal got expected "These should match"

          testCase "Can I extract the memory address from an input?" <| fun _ ->
              let testLonger = "mem[20690]"
              let testShorter = "mem[206]"

              let longerExpected = "20690"
              let shorterExpected = "206"

              Expect.equal (DayFourteen.disassembleAddress testLonger) longerExpected "These should match"
              Expect.equal (DayFourteen.disassembleAddress testShorter) shorterExpected "These should match"

          testCase "Can I apply a mask correctly?" <| fun _ ->
              let testMask = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X" |> DayFourteen.parseV1Mask
              let testInt = 11L
              let expected = 73L

              let weird = testInt |> DayFourteen.intToWeirdBinary
              let masked = DayFourteen.applyV1Mask weird testMask
              let got = masked |> DayFourteen.weirdBinaryToInt

              Expect.equal got expected "They should match"

          testCase "Running a test program" <| fun _ ->
              let instructions = testProgram |> Array.choose DayFourteen.Instruction.Parse
              let init = DayFourteen.PartOneState.New()

              let finalState = Array.fold DayFourteen.partOneFolder init instructions

              let got = finalState.Memory |> Map.fold (fun state _ v -> state + (DayFourteen.weirdBinaryToInt v)) 0L
              let expected = 165L

              Expect.equal got expected "I should get the correct output"

          testCase "I can get all the correct permutations for a WeirdInt" <| fun _ ->
              let mask = "000000000000000000000000000000X1001X" |> DayFourteen.parseV2Mask
              let weird = 42L |> DayFourteen.intToWeirdBinary

              let expected =
                  [ "000000000000000000000000000000011010"
                    "000000000000000000000000000000011011"
                    "000000000000000000000000000000111010"
                    "000000000000000000000000000000111011" ] |> Set.ofList

              let got = DayFourteen.applyV2Mask weird mask |> Set.ofList

              Expect.equal got expected "These should match" ]
