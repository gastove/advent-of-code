module DayTen

open FSharp.Core.Operators.Checked

let LengthAddendum = [ 17; 31; 73; 47; 23 ]

type CircularList =
    { Data: int list }

    member this.RotateData(atPoint: int) =
        let firstSegment = this.Data.[..atPoint - 1]
        let secondSegment = this.Data.[atPoint..]

        { Data = secondSegment @ firstSegment }

    member this.UnRotateData(atPoint: int) =
        let rotationIdx = this.Data.Length - atPoint

        this.RotateData rotationIdx

    member this.NormalizeIndex(index: int) =
        let rec normalize idx =
            if idx > this.Data.Length then
                normalize (idx - this.Data.Length)
            else
                idx

        normalize index

    member this.GetSlice (startingPoint: int) (length: int) =
        let rotated =
            startingPoint
            |> this.NormalizeIndex
            |> this.RotateData

        rotated.Data |> List.take length

    member this.WriteSlice (startingPoint: int) (slice: int list) =
        let idx = startingPoint |> this.NormalizeIndex
        let rotated = idx |> this.RotateData

        let tail = rotated.Data |> List.skip (slice.Length)

        let newData = { Data = slice @ tail }

        newData.UnRotateData idx

    member this.ReverseHunk (startingPoint: int) (length: int) =
        let slice =
            this.GetSlice startingPoint length |> List.rev

        this.WriteSlice startingPoint slice

let rec twist (data: CircularList) (currentPosition: int) (skipSize: int) (lengths: int list) =
    match lengths with
    | length :: remainingLengths ->
        let newData = data.ReverseHunk currentPosition length

        twist newData (currentPosition + length + skipSize) (skipSize + 1) remainingLengths
    | [] -> data, currentPosition, skipSize

let partOne (input: int list) =
    let cl = { Data = [ 0 .. 255 ] }

    let twistedData, _, _ = twist cl 0 0 input

    twistedData

let getAsciiCode (s: string) =
    s
    |> System.Text.Encoding.ASCII.GetBytes
    |> Array.map int
    |> Array.toList

let computeDenseHash (cl: CircularList) =
    cl.Data
    |> List.splitInto 16
    |> List.map (fun subList -> subList |> List.reduce (^^^))

let intToPaddedHex (i: int) = sprintf "%02x" i

let rec multiTwist data times currentPosition skipSize lengths =
    if times = 0 then
        data
    else
        let twistedData, newCurrPos, newSkipSize =
            twist data currentPosition skipSize lengths

        multiTwist twistedData (times - 1) newCurrPos newSkipSize lengths

module KnotHash =
    let computeHash dataToHash =
        let asciiCodeInput =
            dataToHash
            |> getAsciiCode

        let withSuffix = asciiCodeInput @ LengthAddendum

        let cl = { Data = [ 0 .. 255 ] }

        let twisted = multiTwist cl 64 0 0 withSuffix

        let denseHash = twisted |> computeDenseHash

        denseHash
        |> List.map intToPaddedHex
        |> String.concat ""

let partTwo (input: int list) =
    let asciiCodeInput =
        input
        |> List.map string
        |> String.concat ","
        |> getAsciiCode

    let withSuffix = asciiCodeInput @ LengthAddendum

    let cl = { Data = [ 0 .. 255 ] }

    let twisted = multiTwist cl 64 0 0 withSuffix

    let denseHash = twisted |> computeDenseHash

    denseHash
    |> List.map intToPaddedHex
    |> String.concat ""

type Solution() =
    let name = "ten"

    let input =
        [ 102
          255
          99
          252
          200
          24
          219
          57
          103
          2
          226
          254
          1
          0
          69
          216 ]

    interface Common.ISolution with
        member __.Name() = name |> Common.titleCase
        member __.LoadData(_) : Common.Input option = [||] |> Some

        member __.PartOne(_input: Common.Input) : string =
            let computed = input |> partOne

            (computed.Data.[0] * computed.Data.[1]) |> string

        member __.PartTwo(_input: Common.Input) : string =
            let hash = input |> partTwo
            printfn "Got a %i character hash" (hash.Length)
            hash
