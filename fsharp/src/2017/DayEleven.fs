module DayEleven

open FSharp.Core.Operators.Checked

// Three axes, X, Y, and Z. They are _totally_ arbitrary; up and down are
// irrelevant here, just positive and negative.
//
// Here's the grid from the puzzel:
//
//   \ n  /
// nw +--+ ne
//   /    \
// -+      +-
//   \    /
// sw +--+ se
//   / s  \
//
// The ray between nw and n is Positive X.
// This means any motion N or NW increases X, SW or NE is neutral, and S, and SE decreases X
// The ray between SW and NW is Y
// The ray between N and NE is Z
type Direction =
    | North
    | NorthEast
    | SouthEast
    | South
    | SouthWest
    | NorthWest
    static member FromString(s: string) =
        match s with
        | "n" -> North
        | "ne" -> NorthEast
        | "se" -> SouthEast
        | "s" -> South
        | "sw" -> SouthWest
        | "nw" -> NorthWest
        | bad -> failwithf "Got invalid direction %s" bad

type HexPoint =
    { X: int
      Y: int
      Z: int }
    static member Origin = { X = 0; Y = 0; Z = 0 }
    static member Create x y z = { X = x; Y = y; Z = z }
    /// Returns the point in the given direction.
    member this.Travel(direction: Direction) =
        match direction with
        | North ->
            { X = this.X + 1
              Y = this.Y
              Z = this.Z + 1 }
        | NorthEast ->
            { X = this.X
              Y = this.Y - 1
              Z = this.Z + 1 }
        | SouthEast ->
            { X = this.X - 1
              Y = this.Y - 1
              Z = this.Z }
        | South ->
            { X = this.X - 1
              Y = this.Y
              Z = this.Z - 1 }
        | SouthWest ->
            { X = this.X
              Y = this.Y + 1
              Z = this.Z - 1 }
        | NorthWest ->
            { X = this.X + 1
              Y = this.Y + 1
              Z = this.Z }

    member this.IsOrigin = this.X = 0 && this.Y = 0 && this.Z = 0

    member this.Distance(other: HexPoint) =
        let x = this.X - other.X |> abs
        let y = this.Y - other.Y |> abs
        let z = this.Z - other.Z |> abs

        (x + y + z) / 2

let followDirections (directions: Direction list) =
    directions
    |> List.fold (fun (currPoint: HexPoint) (direction: Direction) -> currPoint.Travel direction) HexPoint.Origin

let parseInput (input: Common.Input) =
    let raw = input |> Array.exactlyOne

    raw.Split(",")
    |> Array.map (fun s -> s.Trim() |> Direction.FromString)
    |> Array.toList

let partOne (input: Common.Input) =
    input
    |> parseInput
    |> followDirections
    |> HexPoint.Origin.Distance

let meander (directions: Direction list) =
    directions
    |> List.scan (fun (currPoint: HexPoint) (direction: Direction) -> currPoint.Travel direction) HexPoint.Origin

let partTwo (input: Common.Input) =
    input
    |> parseInput
    |> meander
    |> List.maxBy HexPoint.Origin.Distance
    |> HexPoint.Origin.Distance

type Solution() =
    let name = "eleven"

    interface Common.ISolution with
        member __.Name() = name |> Common.titleCase
        member __.LoadData(year) : Common.Input option = Common.loadData name year
        member __.PartOne(input: Common.Input) : string = input |> partOne |> string
        member __.PartTwo(input: Common.Input) : string = input |> partTwo |> string
