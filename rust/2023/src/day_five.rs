use {
    elf_help::Range,
    std::{collections::HashMap, num::ParseIntError},
};

#[derive(Debug, Hash, PartialEq, Eq, Clone)]
enum ConverterType {
    SeedToSoil,
    SoilToFertilizer,
    FertilizerToWater,
    WaterToLight,
    LightToTemperature,
    TemperatureToHumidity,
    HumidityToLocation,
}

impl TryFrom<&str> for ConverterType {
    type Error = String;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        match value {
            "seed-to-soil map:" => Ok(Self::SeedToSoil),
            "soil-to-fertilizer map:" => Ok(Self::SoilToFertilizer),
            "fertilizer-to-water map:" => Ok(Self::FertilizerToWater),
            "water-to-light map:" => Ok(Self::WaterToLight),
            "light-to-temperature map:" => Ok(Self::LightToTemperature),
            "temperature-to-humidity map:" => Ok(Self::TemperatureToHumidity),
            "humidity-to-location map:" => Ok(Self::HumidityToLocation),
            bad => Err(format!("Can't parse as ConverterType: {:?}", bad)),
        }
    }
}

#[derive(Debug, Clone)]
struct Converter {
    pub converter_type: ConverterType,
    pub conversions: Vec<MappedRange>,
}

impl Converter {
    pub fn new(converter_type: ConverterType, conversions: Vec<MappedRange>) -> Self {
        Converter {
            converter_type,
            conversions,
        }
    }

    pub fn convert(self, elem: &usize) -> usize {
        self.conversions
            .iter()
            .find(|r| r.contains(elem))
            .map(|r| {
                let delta = elem - r.source.minimum;
                r.destination.minimum + delta
            })
            .unwrap_or(*elem)
    }
}

#[derive(Debug, Clone)]
struct MappedRange {
    source: Range,
    destination: Range,
}

impl MappedRange {
    pub fn new(src_start: usize, dest_start: usize, width: usize) -> Self {
        MappedRange {
            source: Range::new(src_start, src_start + width),
            destination: Range::new(dest_start, dest_start + width),
        }
    }

    pub fn contains(&self, needle: &usize) -> bool {
        self.source.contains(needle)
    }
}

fn parse_seeds(line: &String) -> Result<Vec<usize>, String> {
    let maybe_seeds = line
        .split(':')
        .last()
        .ok_or(format!("Failed to extract seeds from: {line}"))?;

    maybe_seeds
        .split_ascii_whitespace()
        .map(|s| {
            s.trim()
                .parse::<usize>()
                .map_err(|e| format!("Failed to parse usize: {:?}", e))
        })
        .collect()
}

fn handle_number_line(line: &String) -> Result<MappedRange, ParseIntError> {
    let parts = line
        .split_ascii_whitespace()
        .map(|word| word.parse::<usize>())
        .collect::<Result<Vec<usize>, ParseIntError>>()?;

    Ok(MappedRange::new(parts[1], parts[0], parts[2]))
}

fn try_handle_chunk(chunk: &Vec<String>) -> Result<Converter, String> {
    let type_line = &chunk[0];
    let converter_type = ConverterType::try_from(type_line.as_ref())?;

    let conversions = chunk[1..]
        .iter()
        .map(|l| handle_number_line(l).map_err(|e| format!("Failed to process line: {:?}", e)))
        .collect::<Result<Vec<MappedRange>, String>>()?;

    Ok(Converter::new(converter_type, conversions))
}

fn parse(input: Vec<String>) -> Result<(Vec<usize>, Vec<Converter>), String> {
    let seed_line = &input[0];
    let seeds = parse_seeds(seed_line)?;

    let mut chunks: Vec<Vec<String>> = vec![];
    let mut buffer: Vec<String> = vec![];

    for line in input[2..].iter() {
        if line.is_empty() {
            chunks.push(buffer.clone());
            buffer.clear();
        } else {
            buffer.push(line.clone())
        }
    }

    chunks.push(buffer);

    let converters = chunks
        .iter()
        .map(try_handle_chunk)
        .collect::<Result<Vec<Converter>, String>>()?;

    Ok((seeds, converters))
}

mod part_one {
    use super::parse;

    pub fn run(input: Vec<String>) -> String {
        match parse(input) {
            Ok((seeds, converters)) => seeds
                .iter()
                .cloned()
                .map(|seed| {
                    converters
                        .iter()
                        .cloned()
                        .fold(seed, |number, converter| converter.convert(&number))
                })
                .min()
                .map(|n| n.to_string())
                .unwrap_or("Failed to find a minimum, boo".into()),
            Err(e) => e,
        }
    }
}

mod part_two {
    use {super::parse, rayon::prelude::*};

    fn expand_seeds(seeds: Vec<usize>) -> Vec<(usize, usize)> {
        let mut total_seeds: Vec<(usize, usize)> = vec![];
        let mut idx = 0;

        while idx < seeds.len() {
            let f = seeds[idx];
            let s = seeds[idx + 1];
            total_seeds.push((f, f + s));
            idx += 2;
        }

        total_seeds
    }

    pub fn run(input: Vec<String>) -> String {
        match parse(input) {
            Ok((seeds, converters)) => {
                let seed_ranges = expand_seeds(seeds);
                seed_ranges
                    .par_iter()
                    .cloned()
                    .filter_map(|(range_start, range_end)| {
                        (range_start..range_end)
                            .map(|seed| {
                                converters
                                    .iter()
                                    .cloned()
                                    .fold(seed, |number, converter| converter.convert(&number))
                            })
                            .min()
                    })
                    .min()
                    .map(|n| n.to_string())
                    .unwrap_or("Failed to find a minimum, boo".into())
            }
            Err(e) => e,
        }
    }
}

pub fn run(input: Vec<String>) {
    let test_input: Vec<String> = [
        "seeds: 79 14 55 13",
        "",
        "seed-to-soil map:",
        "50 98 2",
        "52 50 48",
        "",
        "soil-to-fertilizer map:",
        "0 15 37",
        "37 52 2",
        "39 0 15",
        "",
        "fertilizer-to-water map:",
        "49 53 8",
        "0 11 42",
        "42 0 7",
        "57 7 4",
        "",
        "water-to-light map:",
        "88 18 7",
        "18 25 70",
        "",
        "light-to-temperature map:",
        "45 77 23",
        "81 45 19",
        "68 64 13",
        "",
        "temperature-to-humidity map:",
        "0 69 1",
        "1 0 69",
        "",
        "humidity-to-location map:",
        "60 56 37",
        "56 93 4",
    ]
    .iter()
    .cloned()
    .map(String::from)
    .collect();

    println!("=== Day Five! ===");
    println!("Part One\t=> {}", part_one::run(input.clone()));
    println!("Part Two\t=> {}", part_two::run(input));
}
