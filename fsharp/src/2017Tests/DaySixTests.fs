module DaySixTests

open Expecto

[<Tests>]
let testPartOne =
    testList
        "Test partOne"
        [ testCase "Test that the part one implementation get the correct answer for simple cases"
          <| fun _ ->
              let initial = "0	2	7	0"
              let expected = 5
              let got = DaySix.partOne [| initial |]

              Expect.equal got expected "We should get the answer from the example" ]
