module DayTwentyFourTests

open Expecto

open DayTwentyFour

let examplePipes =
    """
0/2
2/2
2/3
3/4
3/5
0/1
10/1
9/10
"""

[<Tests>]
let alignTests =
    testList
        "Testing the align function"
        [ testCase "Test the example"
          <| fun _ ->
              let input = examplePipes.Trim().Split([| '\n' |])

              let pipes =
                  input |> Array.map Pipe.FromString |> List.ofArray

              let got = align 0 pipes

              let strongest = got |> List.map List.sum |> List.max

              Expect.equal strongest 31 "We should get the correct strength" ]
