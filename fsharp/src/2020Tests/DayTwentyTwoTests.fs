module DayTwentyTwoTests

open Expecto

let testData =
    """Player 1:
9
2
6
3
1

Player 2:
5
8
4
7
10"""

let convertTestInputToInput (testInput: string) = testInput.Split("\n")


[<Tests>]
let partTwoTests =
    testList
        "Testing Part Two examples"
        [ testCase "Testing the Part Two Example Game"
          <| fun _ ->
              let expected = "291"

              let got =
                  testData
                  |> convertTestInputToInput
                  |> DayTwentyTwo.PartTwo.partTwo

              Expect.equal got expected "We should get back the expected result" ]
