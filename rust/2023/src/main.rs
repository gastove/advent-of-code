use {elf_help as eh, std::env};

mod day_one;
mod day_two;
mod day_three;
mod day_four;
mod day_five;
mod day_six;

fn main() {
    let args: Vec<String> = env::args().collect();

    let path = |day| format!("2023/data/day_{day}.txt");
    let day = args.get(1);

    day.map(|d| {
        let data = eh::read_input(path(d));
        match data {
            Ok(data) => match d.as_str() {
                "one" => day_one::run(data),
                "two" => day_two::run(data),
                "four" => day_four::run(data),
                "five" => day_five::run(data),
                "six" => day_six::run(data),
                otherwise => println!(
                    "No day implemented for {otherwise}, or else it's not a sensical input, sorry."
                ),
            },
            Err(_) => todo!(),
        }
    });
}
