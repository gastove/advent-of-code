pub struct Race {
    duration: usize,
    record: usize,
}

impl Race {
    pub fn new(duration: usize, distance: usize) -> Self {
        Race {
            duration,
            record: distance,
        }
    }

    pub fn ways_to_win(&self) -> usize {
        let mut wins = 0;
        let mut speed = 0;

        while speed < self.duration {
            let time_to_race = self.duration - speed;
            let distance = time_to_race * speed;

            if distance > self.record {
                wins += 1;
            }

            speed += 1;
        }

        wins
    }
}

mod part_one {
    use super::Race;

    pub fn run(input: Vec<Race>) -> String {
        input
            .iter()
            .map(|race| race.ways_to_win())
            .product::<usize>()
            .to_string()
    }
}

mod part_two {
    use super::Race;

    pub fn run(input: Race) -> String {
        input.ways_to_win().to_string()
    }
}

pub fn run(_input: Vec<String>) {
    // // Time:        44     70     70     80
    // // Distance:   283   1134   1134   1491

    let part_one_races = vec![
        Race::new(44, 283),
        Race::new(70, 1134),
        Race::new(70, 1134),
        Race::new(80, 1491),
    ];

    let part_two_race = Race::new(44707080, 283113411341491);

    println!("=== Day Six! ===");
    println!("Part One\t=> {}", part_one::run(part_one_races));
    println!("Part Two\t=> {}", part_two::run(part_two_race));
}
