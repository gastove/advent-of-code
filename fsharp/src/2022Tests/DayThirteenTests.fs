module DayThirteenTests

open Expecto

open DayThirteen

let ExampleInput =
    [| "[1,1,3,1,1]"
       "[1,1,5,1,1]"
       ""
       "[[1],[2,3,4]]"
       "[[1],4]"
       ""
       "[9]"
       "[[8,7,6]]"
       ""
       "[[4,4],4,4]"
       "[[4,4],4,4,4]"
       ""
       "[7,7,7,7]"
       "[7,7,7]"
       ""
       "[]"
       "[3]"
       ""
       "[[[]]]"
       "[[]]"
       ""
       "[1,[2,[3,[4,[5,6,7]]]],8,9]"
       "[1,[2,[3,[4,[5,6,0]]]],8,9]"
       "" |]

[<Tests>]
let parserTests =
    let testSpecs =
        [ {| Name = "Parse an Int"
             Input = "1"
             Expected = Int(1)
             Message = "We should be able to parse an int" |}
          {| Name = "Parse an Empty List"
             Input = "[]"
             Expected = List([])
             Message = "We should be able to parse an empty list" |}
          {| Name = "Parse a List of one Int"
             Input = "[1]"
             Expected = List([ Int(1) ])
             Message = "We should be able to parse a list of one int element" |}
          {| Name = "Parse a List of Ints"
             Input = "[1, 2, 3]"
             Expected = List([ Int(1); Int(2); Int(3) ])
             Message = "We should be able to parse a list of plain ints" |}
          {| Name = "Parse a Complex List"
             Input = "[[1],[2,3,4]]"
             Expected =
              List(
                  [ List([ Int(1) ])
                    List([ Int(2); Int(3); Int(4) ]) ]
              )
             Message = "We should be able to parse a nested list" |} ]

    testSpecs
    |> List.map (fun spec ->
        testCase spec.Name
        <| fun _ ->
            let actual = spec.Input |> Paket.FromString
            Expect.equal actual spec.Expected spec.Message)
    |> testList "DayThirteen.Testing Paket Parsing"

module PartOneTests =
    module ComparisonTests =
        type ComparisonSpec =
            { Name: string
              Pakets: Paket * Paket
              Expected: bool
              Message: string }

        let makeList ints = ints |> List.map Int |> List

        [<Tests>]
        let partOneComparisonTests =
            let testSpecs: ComparisonSpec list =
                [ { Name = "Compare Ints in Correct Order"
                    Pakets = Int 3, Int 5
                    Expected = true
                    Message = "The left int is lower, this should be true" }
                  { Name = "Compare Ints in Incorrect Order"
                    Pakets = Int 10, Int 3
                    Expected = false
                    Message = "The left int is higher, this should be false" }
                  { Name = "Compare Lists of Equal Length, ordered correctly"
                    Pakets = [ 1; 2; 3 ] |> makeList, [ 4; 5; 6 ] |> makeList
                    Expected = true
                    Message = "Lists of equal length are ordered based on element comparison" }
                  { Name = "Compare Lists of Equal Length, incorrect order"
                    Pakets = [ 1; 10; 3 ] |> makeList, [ 4; 2; 6 ] |> makeList
                    Expected = true
                    Message = "Lists of equal length are ordered based on element comparison" }
                  { Name = "Compare lists, left list is shorter"
                    Pakets = [ 1; 2 ] |> makeList, [ 4; 5; 6 ] |> makeList
                    Expected = true
                    Message = "The left list is shorter, this should return true" }
                  { Name = "Compare lists, left list is shorter but out of order"
                    Pakets = [ 1; 10 ] |> makeList, [ 4; 5; 6 ] |> makeList
                    Expected = true
                    Message = "The left list is shorter but the order is wrong, this should return false" }
                  { Name = "Compare lists, right list is shorter and order is wrong"
                    Pakets = [ 1; 2; 3 ] |> makeList, [ 4; 5 ] |> makeList
                    Expected = true
                    Message = "The right list is shorter, this should return false" }
                  { Name = "Compare lists, right list is shorter but in order"
                    Pakets = [ 4; 5; 6 ] |> makeList, [ 1; 2 ] |> makeList
                    Expected = false
                    Message = "The right list is shorter but correctly ordered, this should still return false" }
                  { Name = "Compare Left is a list, right is an int"
                    Pakets = [ 2 ] |> makeList, Int 5
                    Expected = true
                    Message = "Converting right to a list, this should be ordered" }
                  { Name = "Compare Left is in Int, Right is an List"
                    Pakets = 2 |> Int, [ 5 ] |> makeList
                    Expected = true
                    Message = "Converting left to a list, this should be ordered" } ]

            testSpecs
            |> List.map (fun spec ->
                testCase spec.Name
                <| fun _ ->
                    let actual = PartOne.comparePakets spec.Pakets

                    Expect.equal actual (Some spec.Expected) spec.Message)
            |> testList "Testing Paket comparison"

    [<Tests>]
    let parsingTest =
        testCase "We should be able to parse the expected number of pairs"
        <| fun _ ->
            let actual = ExampleInput |> parseInput

            Expect.hasLength actual 8 "We should get the correct number of pairs"

    [<Tests>]
    let endToEndIndividual =
        let testExpectations =
            [ true
              true
              false
              true
              false
              true
              false
              false ]

        ExampleInput
        |> parseInput
        |> List.zip testExpectations
        |> List.indexed        
        |> List.map (fun (idx, (sorted, paket)) ->
            testCase $"Processing index: {idx + 1}"
            <| fun _ ->
                let actual = paket |> PartOne.comparePakets

                Expect.equal actual (Some sorted) $"At index {idx + 1}, comparePakets should return {sorted}")
        |> testList "We should get the correct sorting for each example paket pair"


    [<Tests>]
    let endToEnd =
        testCase "Can we reproduce the PartOne example?"
        <| fun _ ->

            let actual = ExampleInput |> partOne

            let expected = "13"

            Expect.equal actual expected "We should be able to reproduce the example"
