module DaySixteen

open FSharp.Core.Operators.Checked

type Either<'a, 'b> =
    | Left of 'a
    | Right of 'b

module Either =
    let left =
        function
        | Left v -> v
        | Right _ -> failwith "function `left` given a Right"

    let right =
        function
        | Left _ -> failwith "function `right` given a Left"
        | Right v -> v

type Packet =
    { VersionNumber: int
      TypeId: int
      Body: Either<int64, Packet list> }
    static member Create version typeId body =
        { VersionNumber = version
          TypeId = typeId
          Body = body }

let decodeHex =
    function
    | '0' -> "0000"
    | '1' -> "0001"
    | '2' -> "0010"
    | '3' -> "0011"
    | '4' -> "0100"
    | '5' -> "0101"
    | '6' -> "0110"
    | '7' -> "0111"
    | '8' -> "1000"
    | '9' -> "1001"
    | 'A' -> "1010"
    | 'B' -> "1011"
    | 'C' -> "1100"
    | 'D' -> "1101"
    | 'E' -> "1110"
    | 'F' -> "1111"
    | bad -> failwithf "Failed to parse as Elf Hex: %c" bad

let parseElfHex (input: Common.Input) =
    input
    |> Array.exactlyOne
    |> Seq.toArray
    |> Array.map decodeHex
    |> String.concat ""

let parseLiteral (theString: string) =
    let rec parse (s: string) (bits: int) =
        match s.[0] with
        | '0' -> s.[1..4], bits + 5
        | '1' ->
            let (nextS, nextBits) = parse s.[5..] 5

            s.[1..4] + nextS, bits + nextBits
        | bad -> failwithf "Expected 0 or 1, got %c" bad

    let result, numBits = parse theString 0

    result |> Common.Numbers.int64FromBinaryString, numBits

let isPadding (s: string) =
    let blank = s |> Common.Strings.blank
    let zeroes = s |> Seq.forall (fun c -> c = '0')

    blank || zeroes

let isNotPadding = isPadding >> not

let rec sumVersions (packet: Packet) =
    packet.VersionNumber
    + match packet.Body with
      | Left _literal -> 0
      | Right subpackets -> subpackets |> List.sumBy sumVersions

let decodePacket (incoming: string) =
    let rec decode (packet: string) =
        // printfn $"Decoding packet: {packet}"
        let version =
            packet.[0..2]
            |> Common.Numbers.int32FromBinaryString

        let typeId =
            packet.[3..5]
            |> Common.Numbers.int32FromBinaryString

        let remaining = packet.[6..]

        // printfn $"Original was {packet.Length} bits, -6 bits for version + type is {remaining.Length}"

        let packet = Packet.Create version typeId

        match typeId with
        | 4 ->
            let literal, bitsUsed = parseLiteral remaining

            let remaining = remaining.[bitsUsed..]

            packet (literal |> Left), remaining
        | _ ->
            let lengthTypeId = remaining.[0]
            let remaining = remaining.[1..]

            match lengthTypeId with
            | '0' ->

                let totalPacketLength =
                    remaining.[0..14]
                    |> Common.Numbers.int32FromBinaryString

                let mutable parsePacketsFrom = remaining.[15..14 + totalPacketLength]

                let remaining = remaining.[15 + totalPacketLength..]

                let mutable packets = []

                while parsePacketsFrom.Length > 0
                      && isNotPadding parsePacketsFrom do
                    let (nextPacket, nextReadup) = decode parsePacketsFrom

                    parsePacketsFrom <- nextReadup

                    packets <- nextPacket :: packets

                packet (packets |> List.rev |> Right), remaining

            | '1' ->
                let packetBits = remaining.[0..10]

                let numberOfPackets =
                    packetBits |> Common.Numbers.int32FromBinaryString

                let mutable remaining = remaining.[11..]

                let mutable packetsRead = 0

                let mutable packets = []

                while packetsRead < numberOfPackets
                      && isNotPadding remaining do
                    let (nextPacket, nextRemaining) = decode remaining

                    remaining <- nextRemaining

                    packets <- nextPacket :: packets
                    packetsRead <- packetsRead + 1

                packet (packets |> List.rev |> Right), remaining
            | bad -> failwithf "Tried to parse length code %c, which makes no sense" bad

    let (packet, _remaining) = decode incoming

    packet

let unpack theList =
    match theList |> List.length with
    | 2 ->
        let f = theList |> List.head
        let s = theList |> List.tail |> List.exactlyOne
        f, s
    | _ -> failwith "can only unpack lists of exactly 2 items"

let processPacket (incoming: Packet) =
    let rec work (packet: Packet) =

        match packet.TypeId with
        | 4 -> packet.Body |> Either.left
        | 0 -> packet.Body |> Either.right |> List.sumBy work
        | 1 ->
            packet.Body
            |> Either.right
            |> List.map work
            |> List.reduce (*)
        | 2 -> packet.Body |> Either.right |> List.map work |> List.min
        | 3 -> packet.Body |> Either.right |> List.map work |> List.max
        | 5 ->
            let (f, s) =
                packet.Body
                |> Either.right
                |> List.map work
                |> unpack

            if f > s then 1L else 0L
        | 6 ->
            let (f, s) =
                packet.Body
                |> Either.right
                |> List.map work
                |> unpack

            if f < s then 1L else 0L
        | 7 ->
            let (f, s) =
                packet.Body
                |> Either.right
                |> List.map work
                |> unpack

            if f = s then 1L else 0L
        | bad -> failwithf "Expected an int 0-7, got %A" bad

    work incoming

let partOne (input: Common.Input) =
    let packet = input |> parseElfHex |> decodePacket

    packet |> sumVersions |> string

let partTwo (input: Common.Input) =
    input
    |> parseElfHex
    |> decodePacket
    |> processPacket
    |> string

type Solution() =
    let name = "sixteen"

    interface Common.ISolution with
        member __.Name() = name |> Common.titleCase
        member __.LoadData(year) : Common.Input option = Common.loadData name year
        member __.PartOne(input: Common.Input) : string = input |> partOne
        member __.PartTwo(input: Common.Input) : string = input |> partTwo
